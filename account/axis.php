<?php
set_time_limit(0);
// set HTTP header
$headers = array(
    'Content-Type: application/json',
);

// query string
$fields = array(
    'key' => '<your_api_key>',
    'format' => 'json',
    'ip' => $_SERVER['REMOTE_ADDR'],
);
$url = 'http://180.151.246.138/GetData/GetEmpDetails?' . http_build_query($fields);

// Open connection
$ch = curl_init();

// Set the url, number of GET vars, GET data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Execute request
$result = curl_exec($ch);

// Close connection
curl_close($ch);

// get the result and parse to JSON
$result_arr = json_decode($result, true);
$total_row=count($result_arr);
?>

<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Decon Construction Company")
							 ->setLastModifiedBy("Decon Construction Company")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP .")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$objPHPExcel->getActiveSheet()->getStyle("A1:A"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("B1:B"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("C1:C"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("D1:D"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("E1:E"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("F1:F"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("G1:G"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("H1:H"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("I1:I"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("J1:J"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("K1:K"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("L1:L"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("M1:M"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("N1:N"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("O1:O"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("P1:P"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("Q1:Q"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("R1:R"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("S1:S"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("T1:T"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("U1:U"."$total_row")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("V1:V"."$total_row")->getFont()->setSize(8);

// Create a first sheet, representing sales data
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Axis Bank Transactions');
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setName('Times');

echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('A1', '1');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'AXIS19111Q');

$objPHPExcel->getActiveSheet()->setCellValue('C1', 'TB');
$objPHPExcel->getActiveSheet()->setCellValue('D1', '3');
$objPHPExcel->getActiveSheet()->setCellValue('E1', '');
$objPHPExcel->getActiveSheet()->setCellValue('F1', '36');
$objPHPExcel->getActiveSheet()->getStyle('G1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX15);
$objPHPExcel->getActiveSheet()->setCellValue('H1', '');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'UTIB0001911');
$objPHPExcel->getActiveSheet()->setCellValue('J1', '915020021143273');
$objPHPExcel->getActiveSheet()->setCellValue('K1', '');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'DECON CONSTRUCTION COMPANY');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'GURGAON');
$objPHPExcel->getActiveSheet()->setCellValue('N1', '');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'info@deconindia.com');
$objPHPExcel->getActiveSheet()->setCellValue('P1', '');
//$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Account Holder');
//$objPHPExcel->getActiveSheet()->setCellValue('R1', 'ACCOUNT HOLDER IFSC');
$objPHPExcel->getActiveSheet()->setCellValue('Q1', '');
$objPHPExcel->getActiveSheet()->setCellValue('R1', '');
$objPHPExcel->getActiveSheet()->setCellValue('S1', '');
//$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Account Number');
$objPHPExcel->getActiveSheet()->setCellValue('T1', '');
$objPHPExcel->getActiveSheet()->setCellValue('U1', '');
//$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Transaction Ammount');

$objPHPExcel->getActiveSheet()->setCellValue('V1', "=SUM(V2:V"."$total_row)");
$i=2;
foreach($result_arr as $fetch){


	//echo $fetch['id'];
	//echo $fetch['Emp_Code'];
	//echo $fetch['Fisrt_Name'];
	//echo $fetch['Email'];
	//echo $fetch['AccNo'];
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '3');
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $fetch['BankHolderName']);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $fetch['BankHolderName']);
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $fetch['AccNo']);
	$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $fetch['Salary']);
	//echo $fetch['IFSCCode'];
	//echo $fetch['BankName'];
	//echo $fetch['BankBranchName'];
	//echo $fetch['BankHolderName'];
	//echo $fetch['Salary'];
	//echo "<br><br><br>";
	$i++;
}



$objPHPExcel->setActiveSheetIndex(0);







?>