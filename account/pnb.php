<?php
set_time_limit(0);
// set HTTP header
$headers = array(
    'Content-Type: application/json',
);

// query string
$fields = array(
    'key' => '<your_api_key>',
    'format' => 'json',
    'ip' => $_SERVER['REMOTE_ADDR'],
);
$url = 'http://180.151.246.138/GetData/GetEmpDetails?' . http_build_query($fields);

// Open connection
$ch = curl_init();

// Set the url, number of GET vars, GET data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Execute request
$result = curl_exec($ch);

// Close connection
curl_close($ch);

// get the result and parse to JSON
$result_arr = json_decode($result, true);
$total_r=count($result_arr);
$total_row=$total_r+8;
?>

<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Decon Construction Company")
							 ->setLastModifiedBy("Decon Construction Company")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP .")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

// Create a first sheet, representing sales data
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('PNB Bank Transactions');
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setName('Times');

echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'To,');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'The Branch Manager');
$objPHPExcel->getActiveSheet()->setCellValue('A5', 'PNB');
$objPHPExcel->getActiveSheet()->setCellValue('A6', 'G.T ROAD');
//$objPHPExcel->getActiveSheet()->getColumnDimension('A7')->setWidth(12);
//$objPHPExcel->getActiveSheet()->setCellValue('A7', 'Please make Neft from our A/C No..0180009300070749  under the name of Decon Construction Company to below mention accounts:-');



$styleNoneBrownBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_NONE,
			'color' => array('argb' => 'FFFFFF'),
		),
	),
);

$objPHPExcel->getActiveSheet()->getStyle("A1:A8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("B1:B8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("C1:C8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("D1:D8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("E1:E8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("F1:F8")->applyFromArray($styleNoneBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("G1:G8")->applyFromArray($styleNoneBrownBorderOutline);



$styleThickBrownBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THICK,
			'color' => array('argb' => '000000'),
		),
	),
);

$objPHPExcel->getActiveSheet()->getStyle("A8:A"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("B8:B"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("C8:C"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("D8:D"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("E8:E"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("F8:F"."$total_row")->applyFromArray($styleThickBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("G8:G"."$total_row")->applyFromArray($styleThickBrownBorderOutline);

$objPHPExcel->getActiveSheet()->setCellValue('A8', 'S.N');
$objPHPExcel->getActiveSheet()->setCellValue('B8', 'Account no.');
$objPHPExcel->getActiveSheet()->setCellValue('C8', 'Amount');
$objPHPExcel->getActiveSheet()->setCellValue('D8', 'IFSC Code');
$objPHPExcel->getActiveSheet()->setCellValue('E8', 'Account No.');
$objPHPExcel->getActiveSheet()->setCellValue('F8', 'Name');
$objPHPExcel->getActiveSheet()->setCellValue('G8','Branch Name');

$objPHPExcel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('C8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G8')->getFont()->setBold(true);

							 
$styleThinBrownBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		),
	),
);

$objPHPExcel->getActiveSheet()->getStyle("A9:A"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("B9:B"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("C9:C"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("D9:D"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("E9:E"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("F9:F"."$total_row")->applyFromArray($styleThinBrownBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("G9:G"."$total_row")->applyFromArray($styleThinBrownBorderOutline);



$objPHPExcel->getActiveSheet()->getStyle("A9:A"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("B9:B"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("C9:C"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("D9:D"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("E9:E"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("F9:F"."$total_row")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("G9:G"."$total_row")->getFont()->setSize(11);

$i=9;
$serial=1;
foreach($result_arr as $fetch){


	//echo $fetch['id'];
	//echo $fetch['Emp_Code'];
	//echo $fetch['Fisrt_Name'];
	//echo $fetch['Email'];
	//echo $fetch['AccNo'];
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $serial);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '0180009300070749');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $fetch['Salary']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $fetch['IFSCCode']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $fetch['AccNo']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $fetch['BankHolderName']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $fetch['BankBranchName']);
	//echo $fetch['IFSCCode'];
	//echo $fetch['BankName'];
	//echo $fetch['BankBranchName'];
	//echo $fetch['BankHolderName'];
	//echo $fetch['Salary'];
	//echo "<br><br><br>";
	$i++;
	$serial++;
}

$total=$total_row+1;
$objPHPExcel->getActiveSheet()->getStyle("A".$total)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("C".$total)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue("A".$total, "Total");
$objPHPExcel->getActiveSheet()->setCellValue("C".$total, "=SUM(C9:C"."$total_row)");

$objPHPExcel->setActiveSheetIndex(0);







?>