<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <script src="css/jquery.min.js"></script>
  <script src="css/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css">
    <?php /*?><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"><?php */?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/skins/skin-blue.min.css">
	<script src="<?php echo get_template_directory_uri(); ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/bootstrap/js/bootstrap.min.js"></script>
</head>
<body style="background:#D3D3D3">
<div class="row">
<div class="jumbotron"style="background: #4e5944 url('<?php echo get_template_directory_uri(); ?>/images/bg-header.jpg');">
 
    <div class="container">
    
        <div class="col-sm-9 col-sm-push-6">
            <ul class="nav nav-pills">
            <!--<li class="active"><a data-toggle="pill" href="#home">Home</a></li>-->
            <li><a data-toggle="pill" href="<?php echo esc_url( home_url( '/' ) ); ?>"><b>Home</b></a></li>
            <li><a data-toggle="pill" href="http://localhost/wordpress/about/">About</a></li>
            <li><a data-toggle="pill" href="http://localhost/wordpress/practices/">Practices</a></li>
            <li><a data-toggle="pill" href="http://localhost/wordpress/our-lawyers/">Our Lawyers</a></li>
            <li><a data-toggle="pill" href="http://localhost/wordpress/news/">News</a></li>
            <li><a data-toggle="pill" href="http://localhost/wordpress/contacts/">Contacts</a></li>
            </ul>
        </div>
    </div>     
        
</div>
