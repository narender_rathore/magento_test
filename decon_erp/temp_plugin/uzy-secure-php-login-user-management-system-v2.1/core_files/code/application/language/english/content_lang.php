<?php

// Dates
$lang['c_month1'] = "January";
$lang['c_month2'] = "February";
$lang['c_month3'] = "March";
$lang['c_month4'] = "April";
$lang['c_month5'] = "May";
$lang['c_month6'] = "June";
$lang['c_month7'] = "July";
$lang['c_month8'] = "August";
$lang['c_month9'] = "September";
$lang['c_month10'] = "October";
$lang['c_month11'] = "November";
$lang['c_month12'] = "December";

// User Access Levels
$lang['ul_1'] = "Member";
$lang['ul_2'] = "Moderator";
$lang['ul_3'] = "User Admin";
$lang['ul_4'] = "Admin";
$lang['ul_5'] = "Banned";
$lang['ul_6'] = "Unknown";


// Email Messages
$lang['email_part1'] = "Dear ";
$lang['email_part2'] = "Someone (hopefully you) requested a password reset at";
$lang['email_part3'] = "To reset your password, please follow the following link: ";
$lang['email_part4'] = "If you did not reset your password, please kindly ignore 
this email. ";
$lang['email_part5'] = "Yours, ";
$lang['email_part8'] = "Please visit ";
$lang['email_part11'] = "Please visit ";

// Nav
$lang['nav_1'] = "Home";
$lang['nav_2'] = "Register";
$lang['nav_3'] = "Login";
$lang['nav_4'] = "Admin";
$lang['nav_5'] = "Updates";
$lang['nav_6'] = "Mailbox";
$lang['nav_7'] = "Content";
$lang['nav_8'] = "Contact";

// Layout
$lang['lay_1'] = "User Panel";
$lang['lay_2'] = "Mail";
$lang['lay_3'] = "You have a";
$lang['lay_4'] = "new message";
$lang['lay_5'] = "Go to ";
$lang['lay_6'] = "Mailbox";
$lang['lay_7'] = "User Panel";
$lang['lay_8'] = "Logout";
$lang['lay_9'] = "Remember Me?";
$lang['lay_10'] = "Login";
$lang['lay_11'] = "Twitter";
$lang['lay_12'] = "Updates";
$lang['lay_13'] = "Adverts";
$lang['lay_14'] = "Notice";
$lang['lay_15'] = "UZY - Secure User Management System. Copyrighted (c) 2014.";
$lang['lay_16'] = "Your email...";
$lang['lay_17'] = "Your Password...";
$lang['lay_18'] = "Hi,";
$lang['lay_19'] = "Admin Panel";
$lang['lay_20'] = "Welcome to UZY";
$lang['lay_21'] = "Read More";
$lang['lay_22'] = "Load More";


// Tabs
$lang['tab_1'] = "Home";
$lang['tab_2'] = "Error";
$lang['tab_3'] = "Feedback";
$lang['tab_4'] = "Welcome";
$lang['tab_5'] = "Login";
$lang['tab_6'] = "Forgot Password";
$lang['tab_7'] = "Reset Password";
$lang['tab_8'] = "Mailbox";
$lang['tab_9'] = "Create";
$lang['tab_10'] = "Block List";
$lang['tab_11'] = "Viewing";
$lang['tab_12'] = "News";
$lang['tab_13'] = "Categories";
$lang['tab_14'] = "Viewing News";
$lang['tab_15'] = "Pages";
$lang['tab_16'] = "Register";
$lang['tab_17'] = "Register";

// Error Page
$lang['ctn_1'] = "Error";

// Feedback Page
$lang['ctn_2'] = "Welcome to the feedback page. This page is for member's only. You can use this to provide us feedback on our website and to let us know what you did and didn't like.";
$lang['ctn_3'] = "Provide Us Feedback";
$lang['ctn_4'] = "Email Address";
$lang['ctn_5'] = "Your Name";
$lang['ctn_6'] = "Message Title";
$lang['ctn_7'] = "What would you like to say";
$lang['ctn_8'] = "Captcha";
$lang['ctn_9'] = "Enter Code Above";
$lang['ctn_10'] = "Send";
$lang['ctn_11'] = "Welcome to UZY- the Secure User Management System! This system is built on the CodeIgniter Framework, using OOP practises and using the MVC pattern. This secure system encrypts passwords using the latest technology so you can rest assured your data is safe!";
$lang['ctn_12'] = "Your Name";
$lang['ctn_13'] = "Your Name";
$lang['ctn_14'] = "Your Name";
$lang['ctn_15'] = "Your Name";

// Login Page
$lang['ctn_16'] = "Enter your Email Address and Password below to login.";
$lang['ctn_17'] = "Email";
$lang['ctn_18'] = "Password";
$lang['ctn_19'] = "Remember Me?";
$lang['ctn_20'] = "Login";
$lang['ctn_21'] = "Forgot Your Password?";
$lang['ctn_22'] = "If you have forgotten your password, enter your email address below that you used to create an account with and we will send you instructions on what to do next.";
$lang['ctn_23'] = "Forgot Password";
$lang['ctn_24'] = "Your Email";
$lang['ctn_25'] = "Reset Password";
$lang['ctn_26'] = "Enter your new password below.";
$lang['ctn_27'] = "Reset Password";
$lang['ctn_28'] = "New Password";
$lang['ctn_29'] = "Confirm New Password";
$lang['ctn_30'] = "Reset Password";

// Mail Page
$lang['ctn_31'] = "Inbox";
$lang['ctn_32'] = "Create";
$lang['ctn_33'] = "Block List";
$lang['ctn_34'] = "Title";
$lang['ctn_35'] = "From";
$lang['ctn_36'] = "Date";
$lang['ctn_37'] = "Delete";
$lang['ctn_38'] = "You have no messages in your Inbox.";
$lang['ctn_39'] = "The mailbox allows you to send messages to user's who have registered on the site. Just enter their username that they used to sign up with and you can begin a conversation with them.";
$lang['ctn_40'] = "Compose the message";
$lang['ctn_41'] = "Send To";
$lang['ctn_42'] = "Enter username ...";
$lang['ctn_43'] = "Title";
$lang['ctn_44'] = "Message";
$lang['ctn_45'] = "Create Message";
$lang['ctn_46'] = "Users on your block list cannot send you a message via the Mailbox. You can remove a user from your Block List too.";
$lang['ctn_47'] = "Block A User Below";
$lang['ctn_48'] = "Block User";
$lang['ctn_49'] = "Enter username ...";
$lang['ctn_50'] = "Block User";
$lang['ctn_51'] = "Blocked User";
$lang['ctn_52'] = "Name";
$lang['ctn_53'] = "Delete";
$lang['ctn_54'] = "Sent on";
$lang['ctn_55'] = "by";
$lang['ctn_56'] = "You cannot reply to this message as the other user has left the conversation.";
$lang['ctn_57'] = "Reply To This Message";
$lang['ctn_58'] = "Your Reply";
$lang['ctn_59'] = "Captcha";
$lang['ctn_60'] = "Enter Code Above";
$lang['ctn_61'] = "Reply";

// News Page
$lang['ctn_62'] = "by";
$lang['ctn_63'] = "Comments";
$lang['ctn_64'] = "Read More";
$lang['ctn_65'] = "Comments Section";
$lang['ctn_66'] = "Delete Comment";
$lang['ctn_67'] = "Post a comment";
$lang['ctn_68'] = "Your Comment";
$lang['ctn_69'] = "Captcha";
$lang['ctn_70'] = "Enter Code Above";
$lang['ctn_71'] = "Post Comment";

// Pages Page
$lang['ctn_72'] = "Newest Article";
$lang['ctn_73'] = "Read More";
$lang['ctn_74'] = "This page can only be viewed by logged in users";
$lang['ctn_75'] = "This page can only be accessed by the User Group:";
$lang['ctn_76'] = "This page was useful";
$lang['ctn_77'] = "This page was not useful";
$lang['ctn_78'] = "users found this article to be useful to them";

// Register Page
$lang['ctn_79'] = "Welcome to Registration.";
$lang['ctn_80'] = "Enter your information below";
$lang['ctn_81'] = "Email Address";
$lang['ctn_82'] = "Your Password";
$lang['ctn_83'] = "Confirm Password";
$lang['ctn_84'] = "Your Name";
$lang['ctn_85'] = "Date Of Birth";
$lang['ctn_86'] = "Captcha";
$lang['ctn_87'] = "Enter Code Above";
$lang['ctn_88'] = "Terms Of Use";
$lang['ctn_89'] = "All messages posted at this site express theviews of the author, and do not necessarily reflect the views of the owners and administrators of this site. By registering at this site you agree not to post any messages that are obscene, vulgar, slanderous, hateful, threatening, or that violate any laws. We will permanently ban all users who do so.We reserve the right to remove, edit, or move any messages for any reason.";
$lang['ctn_90'] = "I agree to the Terms Of Use.";
$lang['ctn_91'] = "Register";

// User Panel
$lang['ctn_92'] = "Welcome to the User Panel. Here you can update your information.";
$lang['ctn_93'] = "Change Your Details";
$lang['ctn_94'] = "Avatar (80x80)";
$lang['ctn_95'] = "Email Address";
$lang['ctn_96'] = "Your Name";
$lang['ctn_97'] = "Update Details";
$lang['ctn_98'] = "Change Your Password";
$lang['ctn_99'] = "Your Current Password";
$lang['ctn_100'] = "New Password";
$lang['ctn_101'] = "Confirm New Password";
$lang['ctn_102'] = "Update Password";


$lang['ctn_103'] = "Weclome to UZY!";
$lang['ctn_104'] = "UZY User Management System is a secure application written in PHP based on the CodeIgniter Framework. Check out all the features this software has to offer by creating an account for free.";
$lang['ctn_105'] = "Purchase";

// V2.0
$lang['ctn_106'] = "Add Username";
$lang['ctn_107'] = "Before you continue, you must first create a username. Enter a unique username in the form below and click go.";
$lang['ctn_108'] = "Enter a Username below";
$lang['ctn_109'] = "Username";
$lang['ctn_110'] = "Add Username";
$lang['ctn_111'] = "Check Username";
$lang['ctn_112'] = "Username available";
$lang['ctn_113'] = "Username must be at least 3 characters long";
$lang['ctn_114'] = "The username must only contain numbers, letters and underscores!";
$lang['ctn_115'] = "This username is already in use!";
$lang['ctn_116'] = "Search";

//V2.1
$lang['ctn_117'] = "Profile Text";
$lang['ctn_118'] = "Disable Profile Comments";
$lang['ctn_119'] = "User Profile";
$lang['ctn_120'] = "'s Profile";
$lang['ctn_121'] = "Send Message";
$lang['ctn_122'] = "Welcome to";
$lang['ctn_123'] = "User Level:";
$lang['ctn_124'] = "User Group:";
$lang['ctn_125'] = "Profile Comments";
$lang['ctn_126'] = "Delete";
$lang['ctn_127'] = "Post Comment";
$lang['ctn_128'] = "new comment";

?>