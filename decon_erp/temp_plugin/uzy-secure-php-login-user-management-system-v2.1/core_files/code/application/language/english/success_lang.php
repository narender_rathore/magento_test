<?php 

$lang['success_1'] = "Thank you for your feedback!";
$lang['success_2'] = "Event Removed.";
$lang['success_3'] = "You logged in with your Google Account!";
$lang['success_4'] = "You logged in with your Facebook Account!";
$lang['success_5'] = "You logged in with your Twitter Account!";
$lang['success_6'] = "Your password has been reset! Feel free to login now.";
$lang['success_7'] = "An email has been sent to your email address containing instructions to reset your password.";
$lang['success_8'] = "You sent the user a message!";
$lang['success_9'] = "The message was deleted!";
$lang['success_10'] = "You have deleted the blocked user from your list!";
$lang['success_11'] = "You have blocked the user from contacting you!";
$lang['success_12'] = "The news comment was posted!";
$lang['success_13'] = "The comment was deleted.";
$lang['success_14'] = "Congratulations! You have registered an account with us!";
$lang['success_15'] = "Your information was updated!";
$lang['success_16'] = "Your password has been updated!";

// Admin
$lang['success_17'] = "The theme was deleted!";
$lang['success_18'] = "The theme was updated!";
$lang['success_19'] = "The theme was added!";
$lang['success_20'] = "The user group was added.";
$lang['success_21'] = "The user group was updated!";
$lang['success_22'] = "The user group was deleted";
$lang['success_23'] = "User was added!";
$lang['success_24'] = "The user was updated";
$lang['success_25'] = "The user was deleted!";
$lang['success_26'] = "The email was sent to the user!";
$lang['success_27'] = "Feedback was deleted!";
$lang['success_28'] = "Page Category was added.";
$lang['success_29'] = "Page Category was deleted!";
$lang['success_30'] = "Category updated!";
$lang['success_31'] = "You added a new page!";
$lang['success_32'] = "The page was deleted!";
$lang['success_33'] = "The page was updated!";
$lang['success_34'] = "News Category was added.";
$lang['success_35'] = "News Category was deleted!";
$lang['success_36'] = "Category updated!";
$lang['success_37'] = "News Post was posted";
$lang['success_38'] = "News Post was updated!";
$lang['success_39'] = "The news post was deleted";
$lang['success_40'] = "Settings updated!";
$lang['success_41'] = "The IP was deleted from the block list";
$lang['success_42'] = "The IP was blocked!";

//V2.0
$lang['success_43'] = "The comment was successfully deleted!";
$lang['success_44'] = "The comment was posted!";

?>