<?php


$lang["error_1"] = "You must be logged in to perform that action";
$lang["error_2"] = "The Captcha Code was invalid!";
$lang["error_3"] = "You cannot leave any field empty.";
$lang["error_4"] = "The Email you entered in invalid!";
$lang["error_5"] = "Invalid Event!";
$lang["error_6"] = "You are already logged in!";
$lang["error_7"] = "Social Login is disabled!";
$lang["error_8"] = "Invalid Google Account!";
$lang["error_9"] = "Failed to get Twitter Authentication. Check Twitter API Status.";
$lang["error_10"] = "There was an error with your credentials!";
$lang["error_11"] = "Your IP has been blocked so you cannot use this website.";
$lang["error_12"] = "You cannot leave email/password empty!";
$lang["error_13"] = "Invalid Details!";
$lang["error_14"] = "Invalid Token!";
$lang["error_15"] = "Invalid Request";
$lang["error_16"] = "Invalid User";
$lang["error_17"] = "Your new passwords do not match!";
$lang["error_18"] = "Your password cannot be empty!";
$lang["error_19"] = "You can only request your password every 15 minutes.";
$lang["error_20"] = "You are already logged in!";

$lang["error_21"] = "The mailbox has been disabled by an admin!";
$lang["error_22"] = "You must have an email address assigned to your account in order to use the Mailbox System. Update it in your User Panel.";
$lang["error_23"] = "Message and Title cannot be empty!";
$lang["error_24"] = "That username does not belong to a registered user";
$lang["error_25"] = "You have been blocked by this user.";
$lang["error_26"] = "You cannot send a mail to yourself!";
$lang["error_27"] = "This mail does not exist!";
$lang["error_28"] = "You cannot reply to this mail!";
$lang["error_29"] = "The user has left the conversation and can no longer be replied to.";
$lang["error_30"] = "This block does not exist!";

$lang["error_31"] = "There are no news posts available for this category.";
$lang["error_32"] = "This news article could not be found!";
$lang["error_33"] = "You cannot post a comment on this post!";
$lang["error_34"] = "Your comment cannot be empty!";
$lang["error_35"] = "Invalid Comment!";

$lang["error_36"] = "This category does not exist!";
$lang["error_37"] = "There are no pages in this category";
$lang["error_38"] = "This page does not exist!";
$lang["error_39"] = "This page cannot be accessed by your user group.";
$lang["error_40"] = "Only logged in users can vote.";
$lang["error_41"] = "Page voting is disabled.";
$lang["error_42"] = "You have already voted on this page.";


$lang["error_43"] = "Registration has been disabled by the admins";
$lang["error_44"] = "Your passwords do not match!";
$lang["error_45"] = "Year is invalid!";
$lang["error_46"] = "Month is invalid!";
$lang["error_47"] = "Day is invalid!";
$lang["error_48"] = "You did not accept the Terms Of Use!";
$lang["error_49"] = "Your password must be at least 5 characters long!";
$lang["error_50"] = "Your name is too long!";
$lang["error_51"] = "Your email cannot be empty!";
$lang["error_52"] = "That email is already in use!";
$lang["error_53"] = "Your name cannot be empty!";
$lang["error_54"] = "You have been logged out since you changed your email. Please relogin in ";
$lang["error_55"] = "You entered the wrong password for your account.";

// Admin
$lang['error_56'] = "You do not have access to this page!";
$lang['error_57'] = "This theme does not exist!";
$lang['error_58'] = "You cannot delete a theme which is set as the active theme!";
$lang['error_59'] = "The theme name cannot be empty!";
$lang['error_60'] = "Search must be at least 3 characters long.";
$lang['error_61'] = "Could not find any users with that search criteria";
$lang['error_62'] = "The name cannot be left empty!";
$lang['error_63'] = "This user group does not exist!";
$lang['error_64'] = "This group cannot be modified as it is a default group.";

$lang['error_65'] = "Your passwords do not match!";
$lang['error_66'] = "Year is invalid!";
$lang['error_67'] = "Month is invalid!";
$lang['error_68'] = "Day is invalid!";
$lang['error_69'] = "Your password must be at least 5 characters long!";
$lang['error_70'] = "Your name is too long!";
$lang['error_71'] = "Your email cannot be empty!";
$lang['error_72'] = "Your email is invalid!";
$lang['error_73'] = "That email is already in use!";
$lang['error_74'] = "User does not exist!";


$lang['error_75'] = "Feedback does not exist";

$lang['error_76'] = "Name cannot be empty!";
$lang['error_77'] = "Category does not exist!";

$lang['error_78'] = "You left the title or body empty!";
$lang['error_79'] = "Page does not exist!";

$lang['error_80'] = "Your news subject cannot be empty!";
$lang['error_81'] = "This news post does not exist";
$lang['error_82'] = "Site name and email cannot be empty!";
$lang['error_83'] = "IP Address cannot be empty.";

// V2.0
$lang['error_84'] = "Username cannot be empty!";
$lang['error_85'] = "Could not find any content with that search criteria";
$lang['error_86'] = "Search cannot be empty!";

//V2.1
$lang['error_87'] = "Could not find that user!";
$lang['error_88'] = "This comment does not exist!";
$lang['error_89'] = "Only the profile owner can delete comments from their profile!";
$lang['error_90'] = "Profile commenting has been disabled!";
$lang['error_91'] = "Invalid User!";
$lang['error_92'] = "This user has disabled comment postings!";
$lang['error_93'] = "You cannot post an empty comment!";


?>