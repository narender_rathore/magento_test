<?php

$lang["atab_1"] = "Admin Panel";
$lang['atab_2'] = "Add News";
$lang['atab_3'] = "Add News Category";
$lang['atab_4'] = "Add Page";
$lang['atab_5'] = "Add Page Category";
$lang['atab_6'] = "Add Theme";
$lang['atab_7'] = "Add User";
$lang['atab_8'] = "Add User Group";
$lang['atab_9'] = "Edit IP";
$lang['atab_10'] = "Edit News";
$lang['atab_11'] = "Edit News Category";
$lang['atab_12'] = "Edit Page";
$lang['atab_13'] = "Edit Page Category";
$lang['atab_14'] = "Edit Theme";
$lang['atab_15'] = "Edit User";
$lang['atab_16'] = "Edit User Group";
$lang['atab_17'] = "Feedback";
$lang['atab_18'] = "Search User";
$lang['atab_19'] = "Settings";
$lang['atab_20'] = "Social Media Settings";
$lang['atab_21'] = "Widget Settings";


// Add News
$lang['actn_1'] = "Here you can add news to your website.";
$lang['actn_2'] = "Add News";
$lang['actn_3'] = "News Subject";
$lang['actn_4'] = "Category";
$lang['actn_5'] = "Disable Comments";
$lang['actn_6'] = "Thumbnail";
$lang['actn_7'] = "Main Image";
$lang['actn_8'] = "News Content";
$lang['actn_9'] = "Add News";

// Add News Category
$lang['actn_10'] = "Add News Category";
$lang['actn_11'] = "Category Name";
$lang['actn_12'] = "Add News Category";

// Add Page
$lang['actn_13'] = "Add Page";
$lang['actn_14'] = "Page Title";
$lang['actn_15'] = "Category";
$lang['actn_16'] = "Registered Users Only";
$lang['actn_17'] = "Allows only registered users to view this page.";
$lang['actn_18'] = "Restrict To User Group";
$lang['actn_19'] = "Makes this page viewable only by users who are in this user group. User Admins and Admins bypass this rule.";
$lang['actn_20'] = "Page Body";
$lang['actn_21'] = "Add Page";

// Add Page Cat
$lang['actn_22'] = "Add Page Category";
$lang['actn_23'] = "Category Name";
$lang['actn_24'] = "Add Page Category";

// Add Theme
$lang['actn_25'] = "Add Theme";
$lang['actn_26'] = "Theme Name";
$lang['actn_27'] = "CSS File Name";
$lang['actn_28'] = "Enter the name of the css file for this theme e.g. theme02.css";
$lang['actn_29'] = "Extra CSS/JS";
$lang['actn_30'] = "If you would like this theme to load extra files, enter the correct HTML to load them here.";
$lang['actn_31'] = "Add Theme";

// Add User
$lang['actn_32'] = "Create A New User Below";
$lang['actn_33'] = "Email Address";
$lang['actn_34'] = "Password";
$lang['actn_35'] = "Confirm Password";
$lang['actn_36'] = "Name";
$lang['actn_37'] = "User Group";
$lang['actn_38'] = "Date Of Birth";
$lang['actn_39'] = "Add User";

// Add User Group
$lang['actn_40'] = "Add User Group";
$lang['actn_41'] = "Group Name";
$lang['actn_42'] = "Group Access Level";
$lang['actn_43'] = "Add User Group";

// Edit IP
$lang['actn_44'] = "IP Address";
$lang['actn_45'] = "Enter IP Address...";
$lang['actn_46'] = "Reason";
$lang['actn_47'] = "Block IP";
$lang['actn_48'] = "Time";
$lang['actn_49'] = "Options";
$lang['actn_50'] = "Delete";

// Edit News
$lang['actn_51'] = "Here you can modify news posts that you have added to your site.";
$lang['actn_52'] = "Subject";
$lang['actn_53'] = "Preview";
$lang['actn_54'] = "Options";
$lang['actn_55'] = "Are you sure you want to delete this?";
$lang['actn_56'] = "Delete";
$lang['actn_57'] = "Edit";

// Edit News Cat
$lang['actn_58'] = "Select a news category below to edit.";
$lang['actn_59'] = "Name";
$lang['actn_60'] = "Options";
$lang['actn_61'] = "Delete";
$lang['actn_62'] = "Edit";

// Edit News Cat Pro
$lang['actn_63'] = "Edit News Category";
$lang['actn_64'] = "Category Name";
$lang['actn_65'] = "Update News Category";

// Edit News Pro
$lang['actn_66'] = "Edit News";
$lang['actn_67'] = "News Subject";
$lang['actn_68'] = "Category";
$lang['actn_69'] = "Disable Comments";
$lang['actn_70'] = "Thumbnail";
$lang['actn_71'] = "Main Image";
$lang['actn_72'] = "News Content";
$lang['actn_73'] = "Update News";

// Edit Page
$lang['actn_74'] = "Select a page to edit below.";
$lang['actn_75'] = "Page Title";
$lang['actn_76'] = "Snippet";
$lang['actn_77'] = "Category";
$lang['actn_78'] = "Options";
$lang['actn_79'] = "Are you sure you want to delete this page?";
$lang['actn_80'] = "Delete";
$lang['actn_81'] = "Edit";

// Edit Page Cat
$lang['actn_82'] = "Select a page category below to edit.";
$lang['actn_83'] = "Name";
$lang['actn_84'] = "Options";
$lang['actn_85'] = "Delete";
$lang['actn_86'] = "Edit";

// Edit Page Cat Pro
$lang['actn_87'] = "Edit Page Category";
$lang['actn_88'] = "Category Name";
$lang['actn_89'] = "Update Page Category";

// Edit Page Pro
$lang['actn_90'] = "Edit Page";
$lang['actn_91'] = "Page Title";
$lang['actn_92'] = "Category";
$lang['actn_93'] = "Registered Users Only";
$lang['actn_94'] = "Allows only registered users to view this page.";
$lang['actn_95'] = "Restrict To User Group";
$lang['actn_96'] = "Makes this page viewable only by users who are in this user group. User Admins and Admins bypass this rule.";
$lang['actn_97'] = "Page Body";
$lang['actn_98'] = "Update Page";

// Edit Theme
$lang['actn_99'] = "Select a theme to edit below.";
$lang['actn_100'] = "Name";
$lang['actn_101'] = "Options";
$lang['actn_102'] = "Are you sure you want to delete this theme?";
$lang['actn_103'] = "Delete";
$lang['actn_104'] = "Edit";

// Edit Theme Pro
$lang['actn_105'] = "Edit Theme";
$lang['actn_106'] = "Theme Name";
$lang['actn_107'] = "CSS File Name";
$lang['actn_108'] = "Enter the name of the css file for this theme e.g. theme02.css";
$lang['actn_109'] = "Extra CSS/JS";
$lang['actn_110'] = "If you would like this theme to load extra files, enter the correct HTML to load them here.";
$lang['actn_111'] = "Edit Theme";

// Edit User
$lang['actn_112'] = "Searching users in the system ... ";
$lang['actn_113'] = "Below shows the list of users registered on your system. They are ordered by newest first.";
$lang['actn_114'] = "Email";
$lang['actn_115'] = "Name";
$lang['actn_116'] = "Group";
$lang['actn_117'] = "Options";
$lang['actn_118'] = "Are you sure you want to delete this user?";
$lang['actn_119'] = "Delete";
$lang['actn_120'] = "Edit";

// Edit User Group
$lang['actn_121'] = "Select a user group to modify below.";
$lang['actn_122'] = "Name";
$lang['actn_123'] = "Options";
$lang['actn_124'] = "Are you sure you want to delete this?";
$lang['actn_125'] = "Delete";
$lang['actn_126'] = "Edit";

// Edit User Group Pro
$lang['actn_127'] = "Edit User Group";
$lang['actn_128'] = "Group Name";
$lang['actn_129'] = "Group Access Level";
$lang['actn_130'] = "Update User Group";

// Edit User Pro
$lang['actn_131'] = "Update The User Below";
$lang['actn_132'] = "Email Address";
$lang['actn_133'] = "Password";
$lang['actn_134'] = "If you wish to change the user's password, enter it here. Otherwise, leave this field blank.";
$lang['actn_135'] = "Name";
$lang['actn_136'] = "User Group";
$lang['actn_137'] = "Date Of Birth";
$lang['actn_138'] = "Update User";

// Feedback
$lang['actn_139'] = "Here you can view feedback left by the visitors of your site.";
$lang['actn_140'] = "Sent By";
$lang['actn_141'] = "at";
$lang['actn_142'] = "Sent from Registered User";
$lang['actn_143'] = "Are you sure you want to delete this?";
$lang['actn_144'] = "Delete";
$lang['actn_145'] = "Reply By Email";

// Feedback Reply
$lang['actn_146'] = "Email Title";
$lang['actn_147'] = "Message";
$lang['actn_148'] = "Send";

// Index
$lang['actn_149'] = "Welcome to the Admin Panel. Here you can update your information.";

// Search User
$lang['actn_150'] = "Search for a User below";
$lang['actn_151'] = "Search Criteria";
$lang['actn_152'] = "Search Type";
$lang['actn_153'] = "Email";
$lang['actn_154'] = "Name";
$lang['actn_155'] = "IP Address";
$lang['actn_156'] = "Search User";

// Settings
$lang['actn_157'] = "Here you can update the global settings for the site.";
$lang['actn_158'] = "Global Settings";
$lang['actn_159'] = "Site Logo (80x60)";
$lang['actn_160'] = "Site Name";
$lang['actn_161'] = "Site Email";
$lang['actn_162'] = "Upload Path";
$lang['actn_163'] = "The path to where files are uploaded to. Use the full path such as: /var/www/yourwebsite/public_html/myfiles/uploads";
$lang['actn_164'] = "Upload Path (relative to URL)";
$lang['actn_165'] = "This should be the path that can be appended to the url; http://localhost/myfiles/uploads the path would be \"myfiles/uploads\"";
$lang['actn_166'] = "Disable Page Voting";
$lang['actn_167'] = "Disable users from voting on pages that you create.";
$lang['actn_168'] = "Disable Avatar Uploading";
$lang['actn_169'] = "Disable users uploading their own custom avatar.";
$lang['actn_170'] = "Disable Registration";
$lang['actn_171'] = "Disable users from registering on the site.";
$lang['actn_172'] = "Disable News Comments";
$lang['actn_173'] = "Stops users from posting comments on news posts, regardless of if the news post specifically allows it.";
$lang['actn_174'] = "Enable Guest Feedback";
$lang['actn_175'] = "A user does not have to be logged in to use the feedback form.";
$lang['actn_176'] = "Disable Mailbox";
$lang['actn_177'] = "Disables the Mailbox feature on the site.";
$lang['actn_178'] = "Site Theme";
$lang['actn_179'] = "Select which site theme to use.";
$lang['actn_180'] = "Update Settings";

// Sidebar
$lang['sb_1'] = "Admin Options";
$lang['sb_2'] = "Settings";
$lang['sb_3'] = "Global Settings";
$lang['sb_4'] = "Social Media Settings";
$lang['sb_5'] = "Widget Settings";
$lang['sb_6'] = "Theme Management";
$lang['sb_7'] = "Add Theme";
$lang['sb_8'] = "Edit Theme";
$lang['sb_9'] = "News Options";
$lang['sb_10'] = "Add News";
$lang['sb_11'] = "Edit News";
$lang['sb_12'] = "Add News Category";
$lang['sb_13'] = "Edit News Category";
$lang['sb_14'] = "Page Options";
$lang['sb_15'] = "Add Page";
$lang['sb_16'] = "Edit Page";
$lang['sb_17'] = "Add Page Category";
$lang['sb_18'] = "Edit Page Category";
$lang['sb_19'] = "Feedback Admin";
$lang['sb_20'] = "View Feedback";
$lang['sb_21'] = "User Management";
$lang['sb_22'] = "Add User";
$lang['sb_23'] = "Edit User";
$lang['sb_24'] = "Search User";
$lang['sb_25'] = "Add User Group";
$lang['sb_26'] = "Edit User Group";
$lang['sb_27'] = "IP Blocking";

// Social Media Settings
$lang['actn_181'] = "Here you can update the Social Media settings for the site.";
$lang['actn_182'] = "Social Media Settings";
$lang['actn_183'] = "Twitter Account";
$lang['actn_184'] = "The username of your Twitter Account";
$lang['actn_185'] = "Tweets To Display";
$lang['actn_186'] = "How many tweets you would like to display in the sidebar widget.";
$lang['actn_187'] = "Twitter Consumer Key";
$lang['actn_188'] = "In order to grab your tweets, you will need to input your consumer key. You can get one from here: https://dev.twitter.com/";
$lang['actn_189'] = "Twitter Consumer Secret";
$lang['actn_190'] = "In order to grab your tweets, you will need to input your consumer secret. You can get one from here: https://dev.twitter.com/";
$lang['actn_191'] = "Twitter Access Token";
$lang['actn_192'] = "In order to grab your tweets, you will need to input your access token. You can get one from here: https://dev.twitter.com/";
$lang['actn_193'] = "Twitter Access Secret";
$lang['actn_194'] = "In order to grab your tweets, you will need to input your access secret. You can get one from here: https://dev.twitter.com/";
$lang['actn_195'] = "Skype Name";
$lang['actn_196'] = "Your Skype Username (used for social media buttons at the top)";
$lang['actn_197'] = "Facebook Page URL";
$lang['actn_198'] = "Your Facebook Page URL (used for social media buttons at the top)";
$lang['actn_199'] = "Google+ Page URL";
$lang['actn_200'] = "Your Google+ Page URL (used for social media buttons at the top)";
$lang['actn_201'] = "WordPress Page URL";
$lang['actn_202'] = "Your WordPress Page URL (used for social media buttons at the top)";
$lang['actn_203'] = "Update Settings";

// Widget Settings
$lang['actn_204'] = "Twitter Widget";
$lang['actn_205'] = "Display Global";
$lang['actn_206'] = "Enabling this option will cause the widget to be loaded on every page. Disabling this option will make the widget load on the frontpage only.";
$lang['actn_207'] = "Disable";
$lang['actn_208'] = "Check this box if you want the Twitter Widget to not be loaded on any pages.";
$lang['actn_209'] = "News Widget";
$lang['actn_210'] = "News Items To Display";
$lang['actn_211'] = "The number of recent news articles displayed in the widget.";
$lang['actn_212'] = "Display Global";
$lang['actn_213'] = "Enabling this option will cause the widget to be loaded on every page. Disabling this option will make the widget load on the frontpage only.";
$lang['actn_214'] = "Check this box if you want the News Widget to not be loaded on any pages.";
$lang['actn_215'] = "Advert Widget";
$lang['actn_216'] = "Advert Code";
$lang['actn_217'] = "Here you can enter your Advert Code, whether it be Google Adsense or your own custom code to display.";
$lang['actn_218'] = "Display Global";
$lang['actn_219'] = "Enabling this option will cause the widget to be loaded on every page. Disabling this option will make the widget load on the frontpage only.";
$lang['actn_220'] = "Disable";
$lang['actn_221'] = "Check this box if you want the News Widget to not be loaded on any pages.";
$lang['actn_222'] = "Update Settings";


// Admin Social Options (extra)
$lang['actn_223'] = "Disable Social Login";
$lang['actn_224'] = "Disables users logging into the UZY system via their social network accounts (Twitter, Facebook, Google).";
$lang['actn_225'] = "Enable Frontpage Social Login";
$lang['actn_226'] = "Shows the Social Login Buttons on the frontpage widget. (Twitter, Facebook, Google).";
$lang['actn_227'] = "Facebook APP ID";
$lang['actn_228'] = "Enter your Facebook APP ID in order to allow for users to login using their Facebook Accounts. For more information go to: https://developers.facebook.com/ or read our documentation.";
$lang['actn_229'] = "Facebook APP Secret";
$lang['actn_230'] = "Enter your Facebook APP Secret in order to allow for users to login using their Facebook Accounts. For more information go to: https://developers.facebook.com/ or read our documentation.";
$lang['actn_231'] = "Google Client ID";
$lang['actn_232'] = "Enter your Google Client ID in order to allow for users to login using their Google Accounts. For more information go to: https://console.developers.google.com or read our documentation.";
$lang['actn_233'] = "Google Client Secret";
$lang['actn_234'] = "Enter your Google Client Secret in order to allow for users to login using their Google Accounts. For more information go to: https://console.developers.google.com or read our documentation.";


// V2.0
$lang['actn_235'] = "Username";
$lang['actn_236'] = "Twitter Cache Time";
$lang['actn_237'] = "The time in seconds which is used to cache the Twitter Tweets for.";

// V2.0.1
$lang['actn_238'] = "Description";

//v2.1
$lang['actn_239'] = "Allow Guests To View Profiles";
$lang['actn_240'] = "Disable Profile Comments";
?>