<div class="widget col-md-12 lower-margin">
<div class="widget-padding">
                    <div class="widget-title">Admin</div>
<b><?php echo lang("sb_2") ?></b><br />
<a href="<?php echo base_url("admin/settings") ?>"><?php echo lang("sb_3") ?></a><br />
<a href="<?php echo base_url("admin/socialmedia") ?>"><?php echo lang("sb_4") ?></a><br />
<a href="<?php echo base_url("admin/widgets") ?>"><?php echo lang("sb_5") ?></a><br /><br />

<b><?php echo lang("sb_6") ?></b><br />
<a href="<?php echo base_url("admin/add_theme") ?>"><?php echo lang("sb_7") ?></a><br />
<a href="<?php echo base_url("admin/edit_theme") ?>"><?php echo lang("sb_8") ?></a><br /><br />

<b><?php echo lang("sb_9") ?></b><br />
<a href="<?php echo base_url("admin/add_news") ?>"><?php echo lang("sb_10") ?></a><br />
<a href="<?php echo base_url("admin/edit_news") ?>"><?php echo lang("sb_11") ?></a><br />
<a href="<?php echo base_url("admin/add_news_cat") ?>"><?php echo lang("sb_12") ?></a><br />
<a href="<?php echo base_url("admin/edit_news_cat") ?>"><?php echo lang("sb_13") ?></a><br /><br />

<b><?php echo lang("sb_14") ?></b><br />
<a href="<?php echo base_url("admin/add_page") ?>"><?php echo lang("sb_15") ?></a><br />
<a href="<?php echo base_url("admin/edit_page") ?>"><?php echo lang("sb_16") ?></a><br />
<a href="<?php echo base_url("admin/add_page_cat") ?>"><?php echo lang("sb_17") ?></a><br />
<a href="<?php echo base_url("admin/edit_page_cat") ?>"><?php echo lang("sb_18") ?></a><br /><br />

<b><?php echo lang("sb_19") ?></b><br />
<a href="<?php echo base_url("admin/feedback") ?>"><?php echo lang("sb_20") ?></a><br /><br />

<b><?php echo lang("sb_21") ?></b><br />
<a href="<?php echo base_url("admin/add_user") ?>"><?php echo lang("sb_22") ?></a><br />
<a href="<?php echo base_url("admin/edit_user") ?>"><?php echo lang("sb_23") ?></a><br />
<a href="<?php echo base_url("admin/search_user") ?>"><?php echo lang("sb_24") ?></a><br />
<a href="<?php echo base_url("admin/add_user_group") ?>"><?php echo lang("sb_25") ?></a><br />
<a href="<?php echo base_url("admin/edit_user_group") ?>"><?php echo lang("sb_26") ?></a><br />
<a href="<?php echo base_url("admin/ip_blocking") ?>"><?php echo lang("sb_27") ?></a><br /><br />
</div>
</div>