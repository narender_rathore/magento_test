$(document).ready(function() {

	$('#wordpress_icon').hover(function() {
		$(this).attr("src", "/images/icons/social-066_wordpress_b.png");
	}, function() {
		$(this).attr("src", "/images/icons/social-066_wordpress.png");
	});

	$('#google_icon').hover(function() {
		$(this).attr("src", "/images/icons/social-080_google_plus_b.png");
	}, function() {
		$(this).attr("src", "/images/icons/social-080_google_plus.png");
	});

	$('#skype_icon').hover(function() {
		$(this).attr("src", "/images/icons/social-047_skype_b.png");
	}, function() {
		$(this).attr("src", "/images/icons/social-047_skype.png");
	});

	$('#facebook_icon').hover(function() {
		$(this).attr("src", "/images/icons/social-046_facebook_b.png");
	}, function() {
		$(this).attr("src", "/images/icons/social-046_facebook.png");
	});

	$('#twitter_icon').hover(function() {
		$(this).attr("src", "/images/icons/social-043_twitter_b.png");
	}, function() {
		$(this).attr("src", "/images/icons/social-043_twitter.png");
	});


});