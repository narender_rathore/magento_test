<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'login';
$route['customer/create']		= 'master/customer/create';
$route['customer/edit/(:any)'] 	= "master/customer/edit/$1";
$route['customer/view/(:any)'] 	= 'master/customer/view/$1';
$route['customer/delete/(:any)']= 'master/customer/delete/$1';
$route['customer'] 				= 'master/customer';
$route['customer/(:any)'] 		= 'master/customer/$1';

$route['item/create']			= 'master/item/create';
$route['item/edit/(:any)'] 		= "master/item/edit/$1";
$route['item/view/(:any)'] 		= 'master/item/view/$1';
$route['item/delete/(:any)'] 	= 'master/item/delete/$1';
$route['item'] 					= 'master/item';
$route['item/(:any)'] 			= 'master/item/$1';

$route['company/create']		= 'master/company/create';
$route['company/edit/(:any)'] 	= "master/company/edit/$1";
$route['company/view/(:any)'] 	= 'master/company/view/$1';
$route['company/delete/(:any)'] = 'master/company/delete/$1';
$route['company'] 				= 'master/company';
$route['company/(:any)'] 		= 'master/company/$1';

$route['vendor/create']			= 'master/vendor/create';
$route['vendor/edit/(:any)'] 	= "master/vendor/edit/$1";
$route['vendor/view/(:any)'] 	= 'master/vendor/view/$1';
$route['vendor/delete/(:any)'] 	= 'master/vendor/delete/$1';
$route['vendor'] 				= 'master/vendor';
$route['vendor/(:any)'] 		= 'master/vendor/$1';

$route['store/create']			= 'master/store/create';
$route['store/edit/(:any)'] 	= "master/store/edit/$1";
$route['store/view/(:any)'] 	= 'master/store/view/$1';
$route['store/delete/(:any)'] 	= 'master/store/delete/$1';
$route['store'] 				= 'master/store';
$route['store/(:any)'] 			= 'master/store/$1';

$route['country/create']		= 'master/country/create';
$route['country/edit/(:any)'] 	= "master/country/edit/$1";
$route['country/view/(:any)'] 	= 'master/country/view/$1';
$route['country/delete/(:any)'] = 'master/country/delete/$1';
$route['country'] 				= 'master/country';
$route['country/(:num)'] 		= 'master/country/$1';

$route['state/create']			= 'master/state/create';
$route['state/edit/(:any)'] 	= "master/state/edit/$1";
$route['state/view/(:any)'] 	= 'master/state/view/$1';
$route['state/delete/(:any)'] 	= 'master/state/delete/$1';
$route['state'] 				= 'master/state';
$route['state/(:any)'] 			= 'master/state/$1';

$route['city/create']			= 'master/city/create';
$route['city/edit/(:any)'] 		= "master/city/edit/$1";
$route['city/view/(:any)'] 		= 'master/city/view/$1';
$route['city/delete/(:any)'] 	= 'master/city/delete/$1';
$route['city'] 					= 'master/city';
$route['city/(:any)'] 			= 'master/city/$1';

$route['region/create']			= 'master/region/create';
$route['region/edit/(:any)'] 	= "master/region/edit/$1";
$route['region/view/(:any)'] 	= 'master/region/view/$1';
$route['region/delete/(:any)'] 	= 'master/region/delete/$1';
$route['region'] 				= 'master/region';
$route['region/(:any)'] 		= 'master/region/$1';

$route['location/create']		= 'master/location/create';
$route['location/edit/(:any)'] 	= "master/location/edit/$1";
$route['location/view/(:any)'] 	= 'master/location/view/$1';
$route['location/delete/(:any)']= 'master/location/delete/$1';
$route['location'] 				= 'master/location';
$route['location/(:any)'] 		= 'master/location/$1';

$route['branch/create']			= 'master/branch/create';
$route['branch/edit/(:any)'] 	= "master/branch/edit/$1";
$route['branch/view/(:any)'] 	= 'master/branch/view/$1';
$route['branch/delete/(:any)'] 	= 'master/branch/delete/$1';
$route['branch'] 				= 'master/branch';
$route['branch/(:any)'] 		= 'master/branch/$1';

$route['entity/create']			= 'master/entity/create';
$route['entity/edit/(:any)'] 	= "master/entity/edit/$1";
$route['entity/view/(:any)'] 	= 'master/entity/view/$1';
$route['entity/delete/(:any)'] 	= 'master/entity/delete/$1';
$route['entity'] 				= 'master/entity';
$route['entity/(:any)'] 		= 'master/entity/$1';

$route['work_package/create']		= 'master/work_package/create';
$route['work_package/edit/(:any)'] 	= "master/work_package/edit/$1";
$route['work_package/view/(:any)'] 	= 'master/work_package/view/$1';
$route['work_package/delete/(:any)']= 'master/work_package/delete/$1';
$route['work_package'] 				= 'master/work_package';
$route['work_package/(:any)'] 		= 'master/work_package/$1';

$route['designation/create']		= 'master/designation/create';
$route['designation/edit/(:any)'] 	= "master/designation/edit/$1";
$route['designation/view/(:any)'] 	= 'master/designation/view/$1';
$route['designation/delete/(:any)'] = 'master/designation/delete/$1';
$route['designation'] 				= 'master/designation';
$route['designation/(:any)'] 		= 'master/designation/$1';

$route['department/create']			= 'master/department/create';
$route['department/edit/(:any)'] 	= "master/department/edit/$1";
$route['department/view/(:any)'] 	= 'master/department/view/$1';
$route['department/delete/(:any)']  = 'master/department/delete/$1';
$route['department'] 				= 'master/department';
$route['department/(:any)'] 		= 'master/department/$1';

$route['new_user/create']			= 'master/new_user/create';
$route['new_user/edit/(:any)'] 		= "master/new_user/edit/$1";
$route['new_user/view/(:any)'] 		= 'master/new_user/view/$1';
$route['new_user/delete/(:any)'] 	= 'master/new_user/delete/$1';
$route['new_user'] 					= 'master/new_user';
$route['new_user/(:any)'] 			= 'master/new_user/$1';

$route['user_auth/create']			= 'master/user_auth/create';
$route['user_auth/edit/(:any)'] 	= "master/user_auth/edit/$1";
$route['user_auth/view/(:any)'] 	= 'master/user_auth/view/$1';
$route['user_auth/delete/(:any)'] 	= 'master/user_auth/delete/$1';
$route['user_auth'] 				= 'master/user_auth';
$route['user_auth/(:any)'] 			= 'master/user_auth/$1';

$route['employee/create']			= 'hr/employee/create';
$route['employee/edit/(:any)'] 		= "hr/employee/edit/$1";
$route['employee/view/(:any)'] 		= 'hr/employee/view/$1';
$route['employee/delete/(:any)'] 	= 'hr/employee/delete/$1';
$route['employee'] 					= 'hr/employee';
$route['employee/(:any)'] 			= 'hr/employee/$1';

$route['outsource_employee/create']				= 'hr/outsource_employee/create';
$route['outsource_employee/edit/(:any)'] 		= "hr/outsource_employee/edit/$1";
$route['outsource_employee/view/(:any)'] 		= 'hr/outsource_employee/view/$1';
$route['outsource_employee/delete/(:any)'] 		= 'hr/outsource_employee/delete/$1';
$route['outsource_employee'] 					= 'hr/outsource_employee';
$route['outsource_employee/(:any)'] 			= 'hr/outsource_employee/$1';

$route['expenses/create']						= 'hr/expenses/create';
$route['expenses/edit/(:any)'] 					= "hr/expenses/edit/$1";
$route['expenses/view/(:any)'] 					= 'hr/expenses/view/$1';
$route['expenses/delete/(:any)'] 				= 'hr/expenses/delete/$1';
$route['expenses'] 								= 'hr/expenses';
$route['expenses/(:any)'] 						= 'hr/expenses/$1';

$route['expensesapproval/create']				= 'hr/expensesapproval/create';
$route['expensesapproval/edit/(:any)'] 			= "hr/expensesapproval/edit/$1";
$route['expensesapproval/view/(:any)'] 			= 'hr/expensesapproval/view/$1';
$route['expensesapproval/delete/(:any)'] 		= 'hr/expensesapproval/delete/$1';
$route['expensesapproval'] 						= 'hr/expensesapproval';
$route['expensesapproval/(:any)'] 				= 'hr/expensesapproval/$1';

$route['accountstatus/create']					= 'hr/accountstatus/create';
$route['accountstatus/edit/(:any)'] 			= "hr/accountstatus/edit/$1";
$route['accountstatus/view/(:any)'] 			= 'hr/accountstatus/view/$1';
$route['accountstatus/delete/(:any)'] 			= 'hr/accountstatus/delete/$1';
$route['accountstatus'] 						= 'hr/accountstatus';
$route['accountstatus/(:any)'] 					= 'hr/accountstatus/$1';

$route['dailyinfo/create']						= 'hr/dailyinfo/create';
$route['dailyinfo/edit/(:any)'] 				= "hr/dailyinfo/edit/$1";
$route['dailyinfo/view/(:any)'] 				= 'hr/dailyinfo/view/$1';
$route['dailyinfo/delete/(:any)'] 				= 'hr/dailyinfo/delete/$1';
$route['dailyinfo'] 							= 'hr/dailyinfo';
$route['dailyinfo/(:any)'] 						= 'hr/dailyinfo/$1';

$route['project/create']						= 'project/project/create';
$route['project/edit/(:any)'] 					= "project/project/edit/$1";
$route['project/view/(:any)'] 					= 'project/project/view/$1';
$route['project/delete/(:any)'] 				= 'project/project/delete/$1';
$route['project'] 								= 'project/project';
$route['project/(:any)'] 						= 'project/project/$1';

$route['customerpo/create']						= 'project/customerpo/create';
$route['customerpo/edit/(:any)'] 				= "project/customerpo/edit/$1";
$route['customerpo/view/(:any)'] 				= 'project/customerpo/view/$1';
$route['customerpo/delete/(:any)'] 				= 'project/customerpo/delete/$1';
$route['customerpo'] 							= 'project/customerpo';
$route['customerpo/(:any)'] 					= 'project/customerpo/$1';

$route['workstatus/create']						= 'project/workstatus/create';
$route['workstatus/edit/(:any)'] 				= "project/workstatus/edit/$1";
$route['workstatus/view/(:any)'] 				= 'project/workstatus/view/$1';
$route['workstatus/delete/(:any)'] 				= 'project/workstatus/delete/$1';
$route['workstatus'] 							= 'project/workstatus';
$route['workstatus/(:any)'] 					= 'project/workstatus/$1';

$route['paymenttransfer/create']				= 'invoice_accounts/paymenttransfer/create';
$route['paymenttransfer/edit/(:any)'] 			= "invoice_accounts/paymenttransfer/edit/$1";
$route['paymenttransfer/view/(:any)'] 			= 'invoice_accounts/paymenttransfer/view/$1';
$route['paymenttransfer/delete/(:any)'] 		= 'invoice_accounts/paymenttransfer/delete/$1';
$route['paymenttransfer'] 						= 'invoice_accounts/paymenttransfer';
$route['paymenttransfer/(:any)'] 				= 'invoice_accounts/paymenttransfer/$1';

$route['vpo/create']							= 'invoice_accounts/vpo/create';
$route['vpo/edit/(:any)'] 						= "invoice_accounts/vpo/edit/$1";
$route['vpo/view/(:any)'] 						= 'invoice_accounts/vpo/view/$1';
$route['vpo/delete/(:any)'] 					= 'invoice_accounts/vpo/delete/$1';
$route['vpo'] 									= 'invoice_accounts/vpo';
$route['vpo/(:any)'] 							= 'invoice_accounts/vpo/$1';

$route['epo/create']							= 'invoice_accounts/epo/create';
$route['epo/edit/(:any)'] 						= "invoice_accounts/epo/edit/$1";
$route['epo/view/(:any)'] 						= 'invoice_accounts/epo/view/$1';
$route['epo/delete/(:any)'] 					= 'invoice_accounts/epo/delete/$1';
$route['epo'] 									= 'invoice_accounts/epo';
$route['epo/(:any)'] 							= 'invoice_accounts/epo/$1';

$route['purchaseinventory/create']				= 'inventory/purchaseinventory/create';
$route['purchaseinventory/edit/(:any)'] 		= "inventory/purchaseinventory/edit/$1";
$route['purchaseinventory/view/(:any)'] 		= 'inventory/purchaseinventory/view/$1';
$route['purchaseinventory/delete/(:any)'] 		= 'inventory/purchaseinventory/delete/$1';
$route['purchaseinventory'] 					= 'inventory/purchaseinventory';
$route['purchaseinventory/(:any)'] 				= 'inventory/purchaseinventory/$1';

$route['addinventory/create']					= 'inventory/addinventory/create';
$route['addinventory/edit/(:any)'] 				= "inventory/addinventory/edit/$1";
$route['addinventory/view/(:any)'] 				= 'inventory/addinventory/view/$1';
$route['addinventory/delete/(:any)'] 			= 'inventory/addinventory/delete/$1';
$route['addinventory'] 							= 'inventory/addinventory';
$route['addinventory/(:any)'] 					= 'inventory/addinventory/$1';

$route['interstore/create']						= 'inventory/interstore/create';
$route['interstore/edit/(:any)'] 				= "inventory/interstore/edit/$1";
$route['interstore/view/(:any)'] 				= 'inventory/interstore/view/$1';
$route['interstore/delete/(:any)'] 				= 'inventory/interstore/delete/$1';
$route['interstore'] 							= 'inventory/interstore';
$route['interstore/(:any)'] 					= 'inventory/interstore/$1';

$route['recievestore/create']					= 'inventory/recievestore/create';
$route['recievestore/edit/(:any)'] 				= "inventory/recievestore/edit/$1";
$route['recievestore/view/(:any)'] 				= 'inventory/recievestore/view/$1';
$route['recievestore/delete/(:any)'] 			= 'inventory/recievestore/delete/$1';
$route['recievestore'] 							= 'inventory/recievestore';
$route['recievestore/(:any)'] 					= 'inventory/recievestore/$1';

$route['itemtoemployee/create']					= 'inventory/itemtoemployee/create';
$route['itemtoemployee/edit/(:any)'] 			= "inventory/itemtoemployee/edit/$1";
$route['itemtoemployee/view/(:any)'] 			= 'inventory/itemtoemployee/view/$1';
$route['itemtoemployee/delete/(:any)'] 			= 'inventory/itemtoemployee/delete/$1';
$route['itemtoemployee'] 						= 'inventory/itemtoemployee';
$route['itemtoemployee/(:any)'] 				= 'inventory/itemtoemployee/$1';

$route['recievebyemployee/create']				= 'inventory/recievebyemployee/create';
$route['recievebyemployee/edit/(:any)'] 		= "inventory/recievebyemployee/edit/$1";
$route['recievebyemployee/view/(:any)'] 		= 'inventory/recievebyemployee/view/$1';
$route['recievebyemployee/delete/(:any)'] 		= 'inventory/recievebyemployee/delete/$1';
$route['recievebyemployee'] 					= 'inventory/recievebyemployee';
$route['recievebyemployee/(:any)'] 				= 'inventory/recievebyemployee/$1';



//$route['product/edit/(:any)'] 	= 'manage_products/edit_product/$1';
//$route['product/delete/(:any)'] = 'manage_products/delete_product/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
