<?php defined('BASEPATH')or exit('NO Direct Access is Allowed'); ?>

<?php
class Workstatus extends MY_Controller{

	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('project/workstatusmodel','workstatus_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->load->view('project/workstatus/workstatus');
		$this->load->view('panel/footer');
	}
	
	public function create(){}
	public function edit($list_id){}
	public function view($list_id){}
	
	public function delete($list_id) {}

}
?>