<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Project extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('project/projectmodel','project_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('project/project/index/'),
			'total_rows'		=>	$this->project_model->num_rows(),
			'per_page'			=>	4,
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	3,
			'num_links'			=>	20,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		//echo $config['total_rows'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$list = $this->project_model->project_list($config['per_page'],$page);
		$links=	$this->pagination->create_links();
		$this->load->view('project/project/project', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('project'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('project/project/add_project');
		}
		else{
			$data_project=array('project'=> strtolower($this->input->post('project'))	);
			$insert_id=$this->project_model->insert_project($data_project);
			$this->session->set_flashdata('message', "<p>project added successfully.</p>");
			return redirect("project/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->project_model->view_project($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('project'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('project/project/edit_project',['list'=>$list]);
		}
		else{
			$data_project=array('project'=> strtolower($this->input->post('project')) );
			echo 
			$this->project_model->update_project($data_project,$list_id);
			$this->session->set_flashdata('message', "<p>project added successfully.</p>");
			return redirect("project");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->project_model->view_project($list_id);
		$this->load->view('project/project/view_project',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
		function delete($list_id) {
		$this->project_model->delete_project($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('project');
	}

	
}
?>