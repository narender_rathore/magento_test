<?php defined('BASEPATH') or exit('NO direct Access is Allowed'); ?>
<?php
class Login extends MY_Controller{
	public function index(){
		//$this->load->helper('form');
		if($this->session->userdata('user_id')) return redirect('user/dashboard');
		$this->load->view('user_login');
	}
	
	public function user_authentication(){
	
	}
	
	public function user_login(){
		$this->load->library(['form_validation','bcrypt']);
		$this->form_validation->set_rules('email','Email','required|trim|valid_email');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()){
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			//echo $username.$password;die;
			$this->load->model('loginmodel');
			$login=$this->loginmodel->login_valid($email);
			//print_r($login);die;
			if($this->bcrypt->check_password($password, $login->password)){
				//echo $login_id;exit;
				//$this->load->library('session');
				$this->session->set_userdata('user_id',$login->id);
				$user_auth_detail=$this->loginmodel->user_authentication($login->id);
				//echo '<pre>';print_r($user_auth_detail);die;
				//echo $user_auth_detail->customer_master_view;die;
				return redirect('user/dashboard');
				//echo "Found";
			}
			else{
				$this->session->set_flashdata('login_failed','Invalid Username/Password');
				return redirect('login');
			}		
		}
		else{
			$this->load->view('user_login');
			//echo validation_errors();
		}
	}
	public function logout(){
		
		$this->session->unset_userdata('user_id');
		return redirect('login');
	}
	
	public function forget_pass(){
		
		$this->load->view('forget_pass');
	}
	
	
}
?>