<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<?php
class Accountstatus extends MY_Controller{

	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('hr/accountstatusmodel','accountstatus_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	base_url().'accountstatus',
			'per_page'			=>	10,
			'total_rows'		=>	$this->accountstatus_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	3,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->accountstatus_model->accountstatus_list($config['per_page'], $this->uri->segment(3) );
		$this->load->view('hr/accountstatus/accountstatus', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('accountstatus'	, 'accountstatus', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('hr/accountstatus/add_accountstatus');
		}
		else{
			$data_accountstatus=array('accountstatus'=> strtolower($this->input->post('accountstatus'))	);
			$insert_id=$this->accountstatus_model->insert_accountstatus($data_accountstatus);
			$this->session->set_flashdata('message', "<p>accountstatus added successfully.</p>");
			return redirect("accountstatus/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->accountstatus_model->view_accountstatus($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('accountstatus'	, 'accountstatus', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('hr/accountstatus/edit_accountstatus',['list'=>$list]);
		}
		else{
			$data_accountstatus=array('accountstatus'=> strtolower($this->input->post('accountstatus')) );
			echo 
			$this->accountstatus_model->update_accountstatus($data_accountstatus,$list_id);
			$this->session->set_flashdata('message', "<p>accountstatus added successfully.</p>");
			return redirect("accountstatus");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->accountstatus_model->view_accountstatus($list_id);
		$this->load->view('hr/accountstatus/view_accountstatus',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
		function delete($list_id) {
		$this->accountstatus_model->delete_accountstatus($list_id);
		$this->session->set_flashdata('message', '<p>accountstatus successfully deleted!</p>');
		redirect('accountstatus');
	}

}

?>