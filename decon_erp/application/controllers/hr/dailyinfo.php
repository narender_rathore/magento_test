<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<?php
class Dailyinfo extends MY_Controller{

	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('hr/dailyinfomodel','dailyinfo_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	base_url().'dailyinfo',
			'per_page'			=>	10,
			'total_rows'		=>	$this->dailyinfo_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	3,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->dailyinfo_model->dailyinfo_list($config['per_page'], $this->uri->segment(3) );
		$this->load->view('hr/dailyinfo/dailyinfo', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('dailyinfo'	, 'dailyinfo', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('hr/dailyinfo/add_dailyinfo');
		}
		else{
			$data_dailyinfo=array('dailyinfo'=> strtolower($this->input->post('dailyinfo'))	);
			$insert_id=$this->dailyinfo_model->insert_dailyinfo($data_dailyinfo);
			$this->session->set_flashdata('message', "<p>dailyinfo added successfully.</p>");
			return redirect("dailyinfo/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->dailyinfo_model->view_dailyinfo($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('dailyinfo'	, 'dailyinfo', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('hr/dailyinfo/edit_dailyinfo',['list'=>$list]);
		}
		else{
			$data_dailyinfo=array('dailyinfo'=> strtolower($this->input->post('dailyinfo')) );
			echo 
			$this->dailyinfo_model->update_dailyinfo($data_dailyinfo,$list_id);
			$this->session->set_flashdata('message', "<p>dailyinfo added successfully.</p>");
			return redirect("dailyinfo");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->dailyinfo_model->view_dailyinfo($list_id);
		$this->load->view('hr/dailyinfo/view_dailyinfo',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
		function delete($list_id) {
		$this->dailyinfo_model->delete_dailyinfo($list_id);
		$this->session->set_flashdata('message', '<p>dailyinfo successfully deleted!</p>');
		redirect('dailyinfo');
	}

}

?>