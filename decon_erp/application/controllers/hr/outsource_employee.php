<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<?php
class Outsource_employee extends MY_Controller{

	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('hr/outsource_employeemodel','outsource_employee_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	base_url().'outsource_employee',
			'per_page'			=>	10,
			'total_rows'		=>	$this->outsource_employee_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	3,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->outsource_employee_model->outsource_employee_list($config['per_page'], $this->uri->segment(3) );
		$this->load->view('hr/outsource_employee/outsource_employee', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
	
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		//print_r($_POST);die;
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('emp_code'				, 'Employee Code'			, 'required|xss_clean|is_unique[hr_outsource_employee.emp_code]');
		$this->form_validation->set_rules('emp_business_letter'		, 'Gender code'				, 'required|xss_clean');
		$this->form_validation->set_rules('emp_name'				, 'Employee Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('date_of_birth'			, 'Date of birth'			, 'required|xss_clean');
		$this->form_validation->set_rules('office_email'			, 'Official Email'			, 'required|xss_clean');
		$this->form_validation->set_rules('gender'					, 'Gender '					, 'required|xss_clean');
		$this->form_validation->set_rules('marital_status'			, 'Marriage Status'			, 'required|xss_clean');
		$this->form_validation->set_rules('referred_by'				, 'Reffered By'				, 'required|xss_clean');
		$this->form_validation->set_rules('referrer_contact'		, 'Refferer Contact'		, 'required|xss_clean');
		$this->form_validation->set_rules('refferer_email'			, 'Refferer Email'			, 'required|xss_clean');
		$this->form_validation->set_rules('experience_yr'			, 'Experience In Year'		, 'required|xss_clean');
		$this->form_validation->set_rules('experience_month'		, 'Experienec in Field'		, 'required|xss_clean');
		$this->form_validation->set_rules('joining_date'			, 'Joining Date'			, 'required|xss_clean');
		$this->form_validation->set_rules('salary'					, 'Salary Amount'			, 'required|xss_clean');
		$this->form_validation->set_rules('lda'						, 'LDA'						, 'required|xss_clean');
		$this->form_validation->set_rules('oda'						, 'ODA '					, 'required|xss_clean');
		$this->form_validation->set_rules('father_name'				, 'Father Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('mother_name'				, 'Mother Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('current_location'		, 'Current Location'		, 'required|xss_clean');
		$this->form_validation->set_rules('designation'				, 'Designation'				, 'required|xss_clean');
		$this->form_validation->set_rules('original_doc_status'		, 'Orginal Document Status'	, 'required|xss_clean');
		$this->form_validation->set_rules('original_doc_details'	, 'Orginal doc Details'		, 'required|xss_clean');
		$this->form_validation->set_rules('recieved_by'				, 'Document recieved by'	, 'required|xss_clean');
		$this->form_validation->set_rules('recieved_store_branch'	, 'Store Branch'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_holder_name'		, 'Bank holder Name'		, 'required|xss_clean');
		$this->form_validation->set_rules('bank_account_no'			, 'Bank Account No'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_name'				, 'Bank Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('bank_ifsc'				, 'Bank IFSC Code'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_branch'				, 'Bank Branch'				, 'required|xss_clean');

		$this->form_validation->set_rules('local_country'			, 'Local Country'			, 'required|xss_clean');
		$this->form_validation->set_rules('local_state'				, 'Local State'				, 'required|xss_clean');
		$this->form_validation->set_rules('local_city'				, 'Local City'				, 'required|xss_clean');
		$this->form_validation->set_rules('local_address'			, 'Local Address'			, 'required|xss_clean');
		$this->form_validation->set_rules('local_pin'				, 'Local Pin No'			, 'required|xss_clean');
		$this->form_validation->set_rules('local_contact'			, 'Local Contact Number'	, 'required|xss_clean');
		$this->form_validation->set_rules('local_mobile'			, 'Local Mobile'			, 'required|xss_clean');
		$this->form_validation->set_rules('local_email'				, 'Local Email'				, 'required|xss_clean');
		
		$this->form_validation->set_rules('permanent_country'		, 'Permanent Country'		, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_state'			, 'Permanent State'			, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_city'			, 'Permanent City'			, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_address'		, 'Permanent Address'		, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_pin'			, 'Permanent Pin No'		, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_contact'		, 'Permanent Contact Number', 'required|xss_clean');
		$this->form_validation->set_rules('permanent_mobile'		, 'Permanent Mobile'		, 'required|xss_clean');
		$this->form_validation->set_rules('permanent_email'			, 'Permanent Email'			, 'required|xss_clean');
		
		$this->form_validation->set_rules('qualification'			, 'Mother Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('university'				, 'University Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('marks'					, 'Marks Obtained'			, 'required|xss_clean|numeric|less_than[100]|greater_than[33]');
		$this->form_validation->set_rules('passing_year'			, 'Passing Year'			, 'required|xss_clean|numeric|less_than['.date("Y").']|greater_than[1960]');
		
		$cont			=	$this->outsource_employee_model->country_list();
		$stat			=	$this->outsource_employee_model->state_list();
		$cit			=	$this->outsource_employee_model->city_list();
		$branch			=	$this->outsource_employee_model->branch_list();
		$location		=	$this->outsource_employee_model->location_list();
		$designation	=	$this->outsource_employee_model->designation_list();
		//$s = '1-4 6-7 9-10';
		$s = '0-99';
		$mark	=	$this->outsource_employee_model->srange($s);
		
		if($this->form_validation->run()==false){
			$this->load->view('hr/outsource_employee/add_outsource_employee',['country_list'=>$cont,'state_list'=>$stat,'city_list'=>$cit,'branch_list'=>$branch,
			'location_list'=>$location,'designation_list'=>$designation]);
		}
		else{
			//print_r($_POST);die;
			$data_outsource_employee=array(
												'emp_code'				=> strtolower($this->input->post('emp_code')),
												'emp_business_letter'	=> $this->input->post('emp_business_letter'),
												'emp_name'				=> strtolower($this->input->post('emp_name')),
												'date_of_birth'			=> $this->input->post('date_of_birth'),
												'office_email'			=> $this->input->post('office_email'),
												'gender'				=> $this->input->post('gender'),   
												'marital_status'		=> $this->input->post('marital_status'),
												'referred_by'			=> $this->input->post('referred_by'),
												'referrer_contact'		=> $this->input->post('referrer_contact'),
												'refferer_email'		=> $this->input->post('refferer_email'),
												'experience_yr'			=> $this->input->post('experience_yr'),
												'experience_month'		=> $this->input->post('experience_month'),
												'joining_date'			=> $this->input->post('joining_date'),
												'salary'				=> $this->input->post('salary'),
												'lda'					=> $this->input->post('lda'),
												'oda'					=> $this->input->post('oda'),
												'father_name'			=> $this->input->post('father_name'),
												'mother_name'			=> $this->input->post('mother_name'),
												'current_location'		=> $this->input->post('current_location'),
												'designation'			=> $this->input->post('designation'),
												'original_doc_status'	=> $this->input->post('original_doc_status'),
												'original_doc_details'	=> $this->input->post('original_doc_details'),
												'recieved_by'			=> $this->input->post('recieved_by'),
												'recieved_store_branch'	=> $this->input->post('recieved_store_branch'),
												'bank_holder_name'		=> $this->input->post('bank_holder_name'),
												'bank_account_no'		=> $this->input->post('bank_account_no'),
												'bank_name'				=> $this->input->post('bank_name'),
												'bank_ifsc'				=> $this->input->post('bank_ifsc'),
												'bank_branch'			=> $this->input->post('bank_branch')
								);
			$data_outsource_employee_address=array(
												'local_country'			=> $this->input->post('local_country'),
												'local_state'			=> $this->input->post('local_state'),
												'local_city'			=> $this->input->post('local_city'),
												'local_address'			=> $this->input->post('local_address'),
												'local_pin'				=> $this->input->post('local_pin'),
												'local_contact'			=> $this->input->post('local_contact'),
												'local_mobile'			=> $this->input->post('local_mobile'),
												'local_email'			=> $this->input->post('local_email'),
												'permanent_country'		=> $this->input->post('permanent_country'),
												'permanent_state'		=> $this->input->post('permanent_state'),
												'permanent_city'		=> $this->input->post('permanent_city'),
												'permanent_address'		=> $this->input->post('permanent_address'),
												'permanent_pin'			=> $this->input->post('permanent_pin'),
												'permanent_contact'		=> $this->input->post('permanent_contact'),
												'permanent_mobile'		=> $this->input->post('permanent_mobile'),
												'permanent_email'		=> $this->input->post('permanent_email'),
												
								);
			$data_outsource_employee_qualification=array(
												'qualification'			=> $this->input->post('qualification'),
												'university'			=> $this->input->post('university'),
												'marks'					=> $this->input->post('marks'),
												'passing_year'			=> $this->input->post('passing_year')
								);
			$insert_id=$this->outsource_employee_model->insert_outsource_employee($data_outsource_employee,$data_outsource_employee_address,
			$data_outsource_employee_qualification);
			$this->session->set_flashdata('message', "<p>outsource_employee added successfully.</p>");
			return redirect("outsource_employee/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	
	public function edit($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->outsource_employee_model->view_outsource_employee($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('emp_code'				, 'Employee Code'			, 'required|xss_clean');
		$this->form_validation->set_rules('emp_business_letter'		, 'Gender code'				, 'required|xss_clean');
		$this->form_validation->set_rules('emp_name'				, 'Employee Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('date_of_birth'			, 'Date of birth'			, 'required|xss_clean');
		$this->form_validation->set_rules('office_email'			, 'Official Email'			, 'required|xss_clean');
		$this->form_validation->set_rules('gender'					, 'Gender '					, 'required|xss_clean');
		$this->form_validation->set_rules('marital_status'			, 'Marriage Status'			, 'required|xss_clean');
		$this->form_validation->set_rules('referred_by'				, 'Reffered By'				, 'required|xss_clean');
		$this->form_validation->set_rules('referrer_contact'		, 'Refferer Contact'		, 'required|xss_clean');
		$this->form_validation->set_rules('refferer_email'			, 'Refferer Email'			, 'required|xss_clean');
		$this->form_validation->set_rules('experience_yr'			, 'Experience In Year'		, 'required|xss_clean');
		$this->form_validation->set_rules('experience_month'		, 'Experienec in Field'		, 'required|xss_clean');
		$this->form_validation->set_rules('joining_date'			, 'Joining Date'			, 'required|xss_clean');
		$this->form_validation->set_rules('salary'					, 'Salary Amount'			, 'required|xss_clean');
		$this->form_validation->set_rules('lda'						, 'LDA'						, 'required|xss_clean');
		$this->form_validation->set_rules('oda'						, 'ODA '					, 'required|xss_clean');
		$this->form_validation->set_rules('father_name'				, 'Father Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('mother_name'				, 'Mother Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('current_location'		, 'Current Location'		, 'required|xss_clean');
		$this->form_validation->set_rules('designation'				, 'Designation'				, 'required|xss_clean');
		$this->form_validation->set_rules('original_doc_status'		, 'Orginal Document Status'	, 'required|xss_clean');
		$this->form_validation->set_rules('original_doc_details'	, 'Orginal doc Details'		, 'required|xss_clean');
		$this->form_validation->set_rules('recieved_by'				, 'Document recieved by'	, 'required|xss_clean');
		$this->form_validation->set_rules('recieved_store_branch'	, 'Store Branch'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_holder_name'		, 'Bank holder Name'		, 'required|xss_clean');
		$this->form_validation->set_rules('bank_account_no'			, 'Bank Account No'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_name'				, 'Bank Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('bank_ifsc'				, 'Bank IFSC Code'			, 'required|xss_clean');
		$this->form_validation->set_rules('bank_branch'				, 'Bank Branch'				, 'required|xss_clean');
		$this->form_validation->set_rules('address_type'			, 'Address Type'			, 'required|xss_clean');
		$this->form_validation->set_rules('country_id'				, 'Country'					, 'required|xss_clean');
		$this->form_validation->set_rules('state_id'				, 'State'					, 'required|xss_clean');
		$this->form_validation->set_rules('city_id'					, 'City'					, 'required|xss_clean');
		$this->form_validation->set_rules('address'					, 'Address'					, 'required|xss_clean');
		$this->form_validation->set_rules('pin_no'					, 'Pin No'					, 'required|xss_clean');
		$this->form_validation->set_rules('contact_no'				, 'Contact Number'			, 'required|xss_clean');
		$this->form_validation->set_rules('mobile'					, 'Mobile'					, 'required|xss_clean');
		$this->form_validation->set_rules('email'					, 'Email'					, 'required|xss_clean');
		$this->form_validation->set_rules('qualification'			, 'Mother Name'				, 'required|xss_clean');
		$this->form_validation->set_rules('university'				, 'University Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('marks'					, 'Marks Obtained'			, 'required|xss_clean');
		$this->form_validation->set_rules('passing_year'			, 'Passing Year'			, 'required|xss_clean');
		
		$cont			=	$this->outsource_employee_model->country_list();
		$stat			=	$this->outsource_employee_model->state_list();
		$cit			=	$this->outsource_employee_model->city_list();
		$branch			=	$this->outsource_employee_model->branch_list();
		$location		=	$this->outsource_employee_model->location_list();
		$designation	=	$this->outsource_employee_model->designation_list();
		
		if($this->form_validation->run()==false){
		$this->load->view('hr/outsource_employee/edit_outsource_employee',['list'=>$list,'country_list'=>$cont,'state_list'=>$stat,'city_list'=>$cit,'branch_list'=>$branch,
			'location_list'=>$location,'designation_list'=>$designation]);
		}
		else{
			$data_outsource_employee=array(
												'emp_code'				=> strtolower($this->input->post('emp_code')),
												'emp_business_letter'	=> $this->input->post('emp_business_letter'),
												'emp_name'				=> strtolower($this->input->post('emp_name')),
												'date_of_birth'			=> $this->input->post('date_of_birth'),
												'office_email'			=> $this->input->post('office_email'),
												'gender'				=> $this->input->post('gender'),   
												'marital_status'		=> $this->input->post('marital_status'),
												'referred_by'			=> $this->input->post('referred_by'),
												'referrer_contact'		=> $this->input->post('referrer_contact'),
												'refferer_email'		=> $this->input->post('refferer_email'),
												'experience_yr'			=> $this->input->post('experience_yr'),
												'experience_month'		=> $this->input->post('experience_month'),
												'joining_date'			=> $this->input->post('joining_date'),
												'salary'				=> $this->input->post('salary'),
												'lda'					=> $this->input->post('lda'),
												'oda'					=> $this->input->post('oda'),
												'father_name'			=> $this->input->post('father_name'),
												'mother_name'			=> $this->input->post('mother_name'),
												'current_location'		=> $this->input->post('current_location'),
												'designation'			=> $this->input->post('designation'),
												'original_doc_status'	=> $this->input->post('original_doc_status'),
												'original_doc_details'	=> $this->input->post('original_doc_details'),
												'recieved_by'			=> $this->input->post('recieved_by'),
												'recieved_store_branch'	=> $this->input->post('recieved_store_branch'),
												'bank_holder_name'		=> $this->input->post('bank_holder_name'),
												'bank_account_no'		=> $this->input->post('bank_account_no'),
												'bank_name'				=> $this->input->post('bank_name'),
												'bank_ifsc'				=> $this->input->post('bank_ifsc'),
												'bank_branch'			=> $this->input->post('bank_branch')
								);
			$data_outsource_employee_address=array(
												'address_type'			=> $this->input->post('address_type'),
												'country_id'			=> $this->input->post('country_id'),
												'state_id'				=> $this->input->post('state_id'),
												'city_id'				=> $this->input->post('city_id'),
												'address'				=> $this->input->post('address'),
												'pin_no'				=> $this->input->post('pin_no'),
												'contact_no'			=> $this->input->post('contact_no'),
												'mobile'				=> $this->input->post('mobile'),
												'email'					=> $this->input->post('email')
								);
			$data_outsource_employee_qualification=array(
												'qualification'			=> $this->input->post('qualification'),
												'university'			=> $this->input->post('university'),
												'marks'					=> $this->input->post('marks'),
												'passing_year'			=> $this->input->post('passing_year')
								);
			 
			$this->outsource_employee_model->update_outsource_employee($data_outsource_employee,$data_outsource_employee_address,$data_outsource_employee_qualification,$list_id);
			$this->session->set_flashdata('message', "<p>outsource_employee added successfully.</p>");
			return redirect("outsource_employee");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$cont			=	$this->outsource_employee_model->country_list();
		$stat			=	$this->outsource_employee_model->state_list();
		$cit			=	$this->outsource_employee_model->city_list();
		$branch			=	$this->outsource_employee_model->branch_list();
		$location		=	$this->outsource_employee_model->location_list();
		$designation	=	$this->outsource_employee_model->designation_list();
		
		$list=$this->outsource_employee_model->view_outsource_employee($list_id);
		
		$this->load->view('hr/outsource_employee/view_outsource_employee',['list'=>$list,'country_list'=>$cont,'state_list'=>$stat,'city_list'=>$cit,
		'branch_list'=>$branch,'location_list'=>$location,'designation_list'=>$designation]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
	
		$this->outsource_employee_model->delete_outsource_employee($list_id);
		$this->session->set_flashdata('message', '<p>outsource_employee successfully deleted!</p>');
		redirect('outsource_employee');
	}

}

?>