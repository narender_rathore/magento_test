<?php defined('BASEPATH')or exit('No Direct Access is Allowed');?>
<?php
class Expensesapproval extends MY_Controller{

	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('hr/expensesapprovalmodel','expensesapproval_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	base_url().'expensesapproval',
			'per_page'			=>	10,
			'total_rows'		=>	$this->expensesapproval_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	3,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->expensesapproval_model->expensesapproval_list($config['per_page'], $this->uri->segment(3) );
		$this->load->view('hr/expensesapproval/expensesapproval', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('expensesapproval'	, 'expensesapproval', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('hr/expensesapproval/add_expensesapproval');
		}
		else{
			$data_expensesapproval=array('expensesapproval'=> strtolower($this->input->post('expensesapproval'))	);
			$insert_id=$this->expensesapproval_model->insert_expensesapproval($data_expensesapproval);
			$this->session->set_flashdata('message', "<p>expensesapproval added successfully.</p>");
			return redirect("expensesapproval/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->expensesapproval_model->view_expensesapproval($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('expensesapproval'	, 'expensesapproval', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('hr/expensesapproval/edit_expensesapproval',['list'=>$list]);
		}
		else{
			$data_expensesapproval=array('expensesapproval'=> strtolower($this->input->post('expensesapproval')) );
			echo 
			$this->expensesapproval_model->update_expensesapproval($data_expensesapproval,$list_id);
			$this->session->set_flashdata('message', "<p>expensesapproval added successfully.</p>");
			return redirect("expensesapproval");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->expensesapproval_model->view_expensesapproval($list_id);
		$this->load->view('hr/expensesapproval/view_expensesapproval',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
		function delete($list_id) {
		$this->expensesapproval_model->delete_expensesapproval($list_id);
		$this->session->set_flashdata('message', '<p>expensesapproval successfully deleted!</p>');
		redirect('expensesapproval');
	}
}