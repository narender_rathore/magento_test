<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Interstore extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )return redirect('login');
		
		$this->load->model('inventory/interstoremodel','interstore_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');	
		$search = strtolower($this->input->post("interstore"));

		$config = [
			'base_url'			=>	site_url('inventory/interstore/index'),
			'total_rows'		=>	$this->interstore_model->num_rows(),
			'per_page'			=>	4,
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	TRUE,
			'uri_segment'		=>	4,
			'num_links'			=>	10,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$list = $this->interstore_model->interstore_list($config['per_page'],$page,$search);
		$links=	$this->pagination->create_links();
		$this->load->view('inventory/interstore/interstore', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
	
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('interstore'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('inventory/interstore/add_interstore');
		}
		else{
			$data_interstore=array('interstore'=> strtolower($this->input->post('interstore'))	);
			$insert_id=$this->interstore_model->insert_interstore($data_interstore);
			$this->session->set_flashdata('message', "<p>interstore added successfully.</p>");
			return redirect("interstore/edit/$insert_id");
		}	
		$this->load->view('panel/footer');
	}
	
	public function edit($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->interstore_model->view_interstore($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('interstore'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('inventory/interstore/edit_interstore',['list'=>$list]);
		}
		else{
			$data_interstore=array('interstore'=> strtolower($this->input->post('interstore')) );
			echo 
			$this->interstore_model->update_interstore($data_interstore,$list_id);
			$this->session->set_flashdata('message', "<p>interstore added successfully.</p>");
			return redirect("interstore");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->interstore_model->view_interstore($list_id);
		$this->load->view('inventory/interstore/view_interstore',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
	
		$this->interstore_model->delete_interstore($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('interstore');
	}	
}
?>