<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Recievestore extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )return redirect('login');
		
		$this->load->model('inventory/recievestoremodel','recievestore_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');	
		$search = strtolower($this->input->post("recievestore"));

		$config = [
			'base_url'			=>	site_url('inventory/recievestore/index'),
			'total_rows'		=>	$this->recievestore_model->num_rows(),
			'per_page'			=>	4,
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	TRUE,
			'uri_segment'		=>	4,
			'num_links'			=>	10,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$list = $this->recievestore_model->recievestore_list($config['per_page'],$page,$search);
		$links=	$this->pagination->create_links();
		$this->load->view('inventory/recievestore/recievestore', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
	
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('recievestore'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('inventory/recievestore/add_recievestore');
		}
		else{
			$data_recievestore=array('recievestore'=> strtolower($this->input->post('recievestore'))	);
			$insert_id=$this->recievestore_model->insert_recievestore($data_recievestore);
			$this->session->set_flashdata('message', "<p>recievestore added successfully.</p>");
			return redirect("recievestore/edit/$insert_id");
		}	
		$this->load->view('panel/footer');
	}
	
	public function edit($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->recievestore_model->view_recievestore($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('recievestore'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('inventory/recievestore/edit_recievestore',['list'=>$list]);
		}
		else{
			$data_recievestore=array('recievestore'=> strtolower($this->input->post('recievestore')) );
			echo 
			$this->recievestore_model->update_recievestore($data_recievestore,$list_id);
			$this->session->set_flashdata('message', "<p>recievestore added successfully.</p>");
			return redirect("recievestore");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->recievestore_model->view_recievestore($list_id);
		$this->load->view('inventory/recievestore/view_recievestore',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
	
		$this->recievestore_model->delete_recievestore($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('recievestore');
	}	
}
?>