<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Addinventory extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )return redirect('login');
		
		$this->load->model('inventory/addinventorymodel','addinventory_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');	
		$search = strtolower($this->input->post("addinventory"));

		$config = [
			'base_url'			=>	site_url('inventory/addinventory/index'),
			'total_rows'		=>	$this->addinventory_model->num_rows(),
			'per_page'			=>	4,
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	TRUE,
			'uri_segment'		=>	4,
			'num_links'			=>	10,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$list = $this->addinventory_model->addinventory_list($config['per_page'],$page,$search);
		$links=	$this->pagination->create_links();
		$this->load->view('inventory/addinventory/addinventory', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
	
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('addinventory'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('inventory/addinventory/add_addinventory');
		}
		else{
			$data_addinventory=array('addinventory'=> strtolower($this->input->post('addinventory'))	);
			$insert_id=$this->addinventory_model->insert_addinventory($data_addinventory);
			$this->session->set_flashdata('message', "<p>addinventory added successfully.</p>");
			return redirect("addinventory/edit/$insert_id");
		}	
		$this->load->view('panel/footer');
	}
	
	public function edit($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->addinventory_model->view_addinventory($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('addinventory'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('inventory/addinventory/edit_addinventory',['list'=>$list]);
		}
		else{
			$data_addinventory=array('addinventory'=> strtolower($this->input->post('addinventory')) );
			echo 
			$this->addinventory_model->update_addinventory($data_addinventory,$list_id);
			$this->session->set_flashdata('message', "<p>addinventory added successfully.</p>");
			return redirect("addinventory");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->addinventory_model->view_addinventory($list_id);
		$this->load->view('inventory/addinventory/view_addinventory',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
	
		$this->addinventory_model->delete_addinventory($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('addinventory');
	}	
}
?>