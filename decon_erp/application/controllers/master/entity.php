<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Entity extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/entitymodel','entity_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/entity/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->entity_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->entity_model->entity_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/entity/entity', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('company'	, 'Company', 'required|xss_clean|is_unique[master_entity.company]');
		$this->form_validation->set_rules('remarks'	, 'Remarks', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/entity/add_entity');
		}
		else{
			$data_entity=array('company'=> strtolower($this->input->post('company')),'remarks'=>$this->input->post('remarks')	);
			$insert_id=$this->entity_model->insert_entity($data_entity);
			$this->session->set_flashdata('message', "<p>entity added successfully.</p>");
			return redirect("entity/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->entity_model->view_entity($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('company'	, 'Company', 'required|xss_clean');
		$this->form_validation->set_rules('remarks'	, 'Remarks', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/entity/edit_entity',['list'=>$list]);
		}
		else{
			$data_entity=array('company'=> strtolower($this->input->post('company')),'remarks'=>$this->input->post('remarks')	);
			$this->entity_model->update_entity($data_entity,$list_id);
			$this->session->set_flashdata('message', "<p>entity added successfully.</p>");
			return redirect("entity");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->entity_model->view_entity($list_id);
		$this->load->view('master/entity/view_entity',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->entity_model->delete_entity($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('entity');
	}

	
}
?>