<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Region extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/regionmodel','region_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/region/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->region_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->region_model->region_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/region/region', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('region'	, 'region', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/region/add_region');
		}
		else{
			$data_region=array('region'=> strtolower($this->input->post('region'))	);
			$insert_id=$this->region_model->insert_region($data_region);
			$this->session->set_flashdata('message', "<p>region added successfully.</p>");
			return redirect("region/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->region_model->view_region($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('region'	, 'region', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/region/edit_region',['list'=>$list]);
		}
		else{
			$data_region=array('region'=> strtolower($this->input->post('region')) );
			echo 
			$this->region_model->update_region($data_region,$list_id);
			$this->session->set_flashdata('message', "<p>region added successfully.</p>");
			return redirect("region");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->region_model->view_region($list_id);
		$this->load->view('master/region/view_region',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		$this->region_model->delete_region($list_id);
		$this->session->set_flashdata('message', '<p>region successfully deleted!</p>');
		redirect('region');
	}

	
}
?>