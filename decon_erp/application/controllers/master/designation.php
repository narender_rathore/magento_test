<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Designation extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/designationmodel','designation_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/designation/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->designation_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->designation_model->designation_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/designation/designation', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('designation'	, 'designation', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/designation/add_designation');
		}
		else{
			$data_designation=array('designation'=> strtolower($this->input->post('designation'))	);
			$insert_id=$this->designation_model->insert_designation($data_designation);
			$this->session->set_flashdata('message', "<p>designation added successfully.</p>");
			return redirect("designation/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->designation_model->view_designation($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('designation'	, 'designation', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/designation/edit_designation',['list'=>$list]);
		}
		else{
			$data_designation=array('designation'=> strtolower($this->input->post('designation')) );
			echo 
			$this->designation_model->update_designation($data_designation,$list_id);
			$this->session->set_flashdata('message', "<p>designation added successfully.</p>");
			return redirect("designation");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->designation_model->view_designation($list_id);
		$this->load->view('master/designation/view_designation',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->designation_model->delete_designation($list_id);
		$this->session->set_flashdata('message', '<p>Designation successfully deleted!</p>');
		redirect('designation');
	}

	
}
?>