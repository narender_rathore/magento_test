<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Customer extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/customermodel','customer_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		## content part Start
		
		$config = [
			'base_url'			=>	site_url('master/customer/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->customer_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		//echo '<pre>';
		$this->pagination->initialize($config);
		//print_r($config);
		//echo $this->pagination->create_links();
		$this->pagination->create_links();
		$list = $this->customer_model->customer_list($config['per_page'], $this->uri->segment(4) );
		//print_r($list);
		//die;
		
		$this->load->view('master/customer/customer', ['list'=>$list]);
		## Content Part End
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('comp_code'		, 'Company Code', 'required|xss_clean|is_unique[master_customer.comp_code]');
		$this->form_validation->set_rules('comp_name'		, 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('tin'				, 'Tin Number '	, 'required|xss_clean');
		$this->form_validation->set_rules('address'			, 'Address'		, 'required|xss_clean');
		$this->form_validation->set_rules('remarks'			, 'Remarks'		, 'required|xss_clean');
		$this->form_validation->set_rules('name'			, 'Name'		, 'required|xss_clean');
		$this->form_validation->set_rules('designation'		, 'Designation'	, 'required|xss_clean');
		$this->form_validation->set_rules('department'		, 'Department'	, 'required|xss_clean');
		$this->form_validation->set_rules('mobile'			, 'Mobile No'	, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email'			, 'Email'		, 'required|xss_clean|valid_email');	
		$desig_list = $this->customer_model->designation_list();
		$depart_list = $this->customer_model->department_list();
		if($this->form_validation->run()==false){
			//set the flash data error message if there is one
			//$this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$this->load->view('master/customer/add_customer',['designation_list'=>$desig_list,'department_list'=>$depart_list]);
			//echo "xnzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
		}
		else{
		//print_r($_POST);die;
			$data_customer=array(
						'comp_code'					=> strtolower($this->input->post('comp_code')),
						'comp_name'					=> $this->input->post('comp_name'),
						'tin'						=> $this->input->post('tin'),
						'address'					=> $this->input->post('address'),
						'remarks'					=> $this->input->post('remarks')					
			);
			$data_customer_contact=array(				
						'name'						=> $this->input->post('name'),
						'designation'				=> $this->input->post('designation'),
						'department'				=> $this->input->post('department'),
						'mobile'					=> $this->input->post('mobile'),
						'email'						=> $this->input->post('email')					
			);

			$insert_id=$this->customer_model->insert_customer($data_customer,$data_customer_contact);
			//$data['message'] = 'Data Inserted Successfully';
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Customer Added Successfully");
			return redirect("customer/edit/$insert_id");
			
			//$this->load->view('insert_view', $data_customer,$data_customer_contact);
			//$this->session->set_flashdata('message', "<p>Customer added successfully.</p>");
			//redirect(base_url().'customer');
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($lists_csid){
		//echo $lists_csid;
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->customer_model->view_customer($lists_csid);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('comp_code'	, 'Company Code', 'required|xss_clean');
		$this->form_validation->set_rules('comp_name'	, 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('tin'			, 'Tin Number '	, 'required|xss_clean');
		$this->form_validation->set_rules('address'		, 'Address'		, 'required|xss_clean');
		$this->form_validation->set_rules('remarks'		, 'Remarks'		, 'required|xss_clean');
		$this->form_validation->set_rules('name'		, 'Name'		, 'required|xss_clean');
		$this->form_validation->set_rules('designation'	, 'Designation'	, 'required|xss_clean');
		$this->form_validation->set_rules('department'	, 'Department'	, 'required|xss_clean');
		$this->form_validation->set_rules('mobile'		, 'Mobile No'	, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email'		, 'Email'		, 'required|xss_clean|valid_email');	
		$desig_list = $this->customer_model->designation_list();
		$depart_list = $this->customer_model->department_list();
		if($this->form_validation->run()==false){
			//$this->load->view("master/edit_customer");
		$this->load->view('master/customer/edit_customer',['list'=>$list,'designation_list'=>$desig_list,'department_list'=>$depart_list]);
		}
		else{
			$data_customer=array(
						'comp_code'					=> strtolower($this->input->post('comp_code')),
						'comp_name'					=> $this->input->post('comp_name'),
						'tin'						=> $this->input->post('tin'),
						'address'					=> $this->input->post('address'),
						'remarks'					=> $this->input->post('remarks')					
			);
			$data_customer_contact=array(				
						'name'						=> $this->input->post('name'),
						'designation'				=> $this->input->post('designation'),
						'department'				=> $this->input->post('department'),
						'mobile'					=> $this->input->post('mobile'),
						'email'						=> $this->input->post('email')					
			);

			$this->customer_model->update_customer($data_customer,$data_customer_contact,$list_csid);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Customer updated Successfully");
			return redirect("customer");
		}
	
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($lists_csid){
		//echo $lists_csid;
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->customer_model->view_customer($lists_csid);
		$desig_list = $this->customer_model->designation_list();
		$depart_list = $this->customer_model->department_list();
		//print_r($list);
		$this->load->view('master/customer/view_customer',['list'=>$list,'designation_list'=>$desig_list,'department_list'=>$depart_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->customer_model->delete_customer($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Customer successfully deleted!");
		redirect('customer');
	}
	
}
?>