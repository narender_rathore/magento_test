<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Country extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )return redirect('login');
		
		$this->load->model('master/countrymodel','country_model');
		$this->load->helper(['form','url','html','security']);
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');	
		$search = strtolower($this->input->post("country"));

		$config = [
			'base_url'			=>	site_url('master/country/index'),
			'total_rows'		=>	$this->country_model->num_rows(),
			'per_page'			=>	4,
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	TRUE,
			'uri_segment'		=>	4,
			'num_links'			=>	10,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$list = $this->country_model->country_list($config['per_page'],$page,$search);
		$links=	$this->pagination->create_links();
		$this->load->view('master/country/country', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
	
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/country/add_country');
		}
		else{
			$data_country=array('country'=> strtolower($this->input->post('country'))	);
			$insert_id=$this->country_model->insert_country($data_country);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Country Added Successfully");
			return redirect("country/edit/$insert_id");
		}	
		$this->load->view('panel/footer');
	}
	
	public function edit($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->country_model->view_country($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country'	, 'Country', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/country/edit_country',['list'=>$list]);
		}
		else{
			$data_country=array('country'=> strtolower($this->input->post('country')) );
			echo 
			$this->country_model->update_country($data_country,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Country Updated Successfully");
			return redirect("country");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
	
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->country_model->view_country($list_id);
		$this->load->view('master/country/view_country',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->country_model->delete_country($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Country Successfully Deleted!");
		redirect('country');
	}	
}
?>