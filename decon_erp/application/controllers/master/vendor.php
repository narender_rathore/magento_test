<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Vendor extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/vendormodel','vendor_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		
		$config = [
			'base_url'			=>	site_url('master/vendor/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->vendor_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->vendor_model->vendor_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/vendor/vendor', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('vendor_code'			, 'Vendor Code'					, 'required|xss_clean');
		$this->form_validation->set_rules('vendor_name'			, 'Vendor Name'					, 'required|xss_clean');
		$this->form_validation->set_rules('tan'					, 'Tan '						, 'required|xss_clean');
		$this->form_validation->set_rules('pan'					, 'Pan'							, 'required|xss_clean');
		$this->form_validation->set_rules('service_tax'			, 'Service Tax'					, 'required|xss_clean');
		$this->form_validation->set_rules('address'				, 'Address'						, 'required|xss_clean');
		$this->form_validation->set_rules('remarks'				, 'Remarks'						, 'required|xss_clean');
		$this->form_validation->set_rules('contact_no'			, 'Contact No'					, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email'				, 'Email'						, 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('concern_name'		, 'Concern Person Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('concern_designation'	, 'Concern Person Designation'	, 'required|xss_clean');
		$this->form_validation->set_rules('concern_contact_no'	, 'Concern Person Contact No'	, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');	
		$this->form_validation->set_rules('concern_email'		, 'Concern Person Email'		, 'required|xss_clean|valid_email');	
		$this->form_validation->set_rules('prim_account_no'		, 'Primary Account No'			, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_name'			, 'Primary Account Holder Name'	, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_ifsc_code'		, 'Primary Account IFSC Code'	, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_branch'			, 'Primary Account Branch'		, 'required|xss_clean');	
		$this->form_validation->set_rules('sec_account_no'		, 'Secondary Account No'		, 'required|xss_clean');	
		$this->form_validation->set_rules('sec_name'			, 'Secondary Account Holder Name', 'required|xss_clean');	
		$this->form_validation->set_rules('sec_ifsc_code'		, 'Secondary Account IFSC Code'	 , 'required|xss_clean');	
		$this->form_validation->set_rules('sec_branch'			, 'Secondary Account Branch'	 , 'required|xss_clean');			
		$desig_list = $this->vendor_model->designation_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/vendor/add_vendor',['designation_list'=>$desig_list]);
		}
		else{
			$data_vendor=array(
						'vendor_code'									=> $this->input->post('vendor_code'),
						'vendor_name'									=> $this->input->post('vendor_name'),
						'vendor_tan'									=> $this->input->post('tan'),
						'vendor_pan'									=> $this->input->post('pan'),
						'service_tax_no'								=> $this->input->post('service_tax'),
						'address'										=> $this->input->post('address'),
						'remarks'										=> $this->input->post('remarks'),
						'concerned_person_name'							=> $this->input->post('concern_name'),
						'concerned_person_designation'					=> $this->input->post('concern_designation'),
						'concerned_person_contact'						=> $this->input->post('concern_contact_no'),
						'concerned_person_email'						=> $this->input->post('concern_email'),
						'primary_account_no'							=> $this->input->post('prim_account_no'),
						'primary_account_name'							=> $this->input->post('prim_name'),
						'primary_account_ifsc'							=> $this->input->post('prim_ifsc_code'),
						'primary_account_branch'						=> $this->input->post('prim_branch'),
						'secondary_account_no'							=> $this->input->post('sec_account_no'),
						'secondary_account_name'						=> $this->input->post('sec_name'),
						'secondary_account_ifsc'						=> $this->input->post('sec_ifsc_code'),
						'secondary_account_branch'						=> $this->input->post('sec_branch')
											
			);
			$data_vendor_contact=array(				
						'contact_no'									=> $this->input->post('contact_no'),
						'email'											=> $this->input->post('email')				
			);
			$insert_id=$this->vendor_model->insert_vendor($data_vendor,$data_vendor_contact);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Vendor Added Successfully");
			return redirect("vendor/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->vendor_model->view_vendor($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('vendor_code'			, 'Vendor Code'					, 'required|xss_clean');
		$this->form_validation->set_rules('vendor_name'			, 'Vendor Name'					, 'required|xss_clean');
		$this->form_validation->set_rules('tan'					, 'Tan '						, 'required|xss_clean');
		$this->form_validation->set_rules('pan'					, 'Pan'							, 'required|xss_clean');
		$this->form_validation->set_rules('service_tax'			, 'Service Tax'					, 'required|xss_clean');
		$this->form_validation->set_rules('address'				, 'Address'						, 'required|xss_clean');
		$this->form_validation->set_rules('remarks'				, 'Remarks'						, 'required|xss_clean');
		$this->form_validation->set_rules('contact_no'			, 'Contact No'					, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email'				, 'Email'						, 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('concern_name'		, 'Concern Person Name'			, 'required|xss_clean');
		$this->form_validation->set_rules('concern_designation'	, 'Concern Person Designation'	, 'required|xss_clean');
		$this->form_validation->set_rules('concern_contact_no'	, 'Concern Person Contact No'	, 'required|xss_clean|numeric|exact_length[10]|regex_match[/^[0-9]{10}$/]');	
		$this->form_validation->set_rules('concern_email'		, 'Concern Person Email'		, 'required|xss_clean|valid_email');	
		$this->form_validation->set_rules('prim_account_no'		, 'Primary Account No'			, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_name'			, 'Primary Account Holder Name'	, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_ifsc_code'		, 'Primary Account IFSC Code'	, 'required|xss_clean');	
		$this->form_validation->set_rules('prim_branch'			, 'Primary Account Branch'		, 'required|xss_clean');	
		$this->form_validation->set_rules('sec_account_no'		, 'Secondary Account No'		, 'required|xss_clean');	
		$this->form_validation->set_rules('sec_name'			, 'Secondary Account Holder Name', 'required|xss_clean');	
		$this->form_validation->set_rules('sec_ifsc_code'		, 'Secondary Account IFSC Code'	 , 'required|xss_clean');	
		$this->form_validation->set_rules('sec_branch'			, 'Secondary Account Branch'	 , 'required|xss_clean');	
		$desig_list = $this->vendor_model->designation_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/vendor/edit_vendor',['list'=>$list,'designation_list'=>$desig_list]);
		}
		else{
			$data_vendor=array(
						'vendor_code'									=> $this->input->post('vendor_code'),
						'vendor_name'									=> $this->input->post('vendor_name'),
						'vendor_tan'									=> $this->input->post('tan'),
						'vendor_pan'									=> $this->input->post('pan'),
						'service_tax_no'								=> $this->input->post('service_tax'),
						'address'										=> $this->input->post('address'),
						'remarks'										=> $this->input->post('remarks'),
						'concerned_person_name'							=> $this->input->post('concern_name'),
						'concerned_person_designation'					=> $this->input->post('concern_designation'),
						'concerned_person_contact'						=> $this->input->post('concern_contact_no'),
						'concerned_person_email'						=> $this->input->post('concern_email'),
						'primary_account_no'							=> $this->input->post('prim_account_no'),
						'primary_account_name'							=> $this->input->post('prim_name'),
						'primary_account_ifsc'							=> $this->input->post('prim_ifsc_code'),
						'primary_account_branch'						=> $this->input->post('prim_branch'),
						'secondary_account_no'							=> $this->input->post('sec_account_no'),
						'secondary_account_name'						=> $this->input->post('sec_name'),
						'secondary_account_ifsc'						=> $this->input->post('sec_ifsc_code'),
						'secondary_account_branch'						=> $this->input->post('sec_branch')
											
			);
			$data_vendor_contact=array(				
						'contact_no'									=> $this->input->post('contact_no'),
						'email'											=> $this->input->post('email')				
			);

			$this->vendor_model->update_vendor($data_vendor,$data_vendor_contact,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Vendor Updated Successfully");
			return redirect("vendor");
		}
	
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$desig_list = $this->vendor_model->designation_list();
		$list=$this->vendor_model->view_vendor($list_id);
		$this->load->view('master/vendor/view_vendor',['list'=>$list,'designation_list'=>$desig_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->vendor_model->delete_vendor($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Vendor successfully deleted!");
		redirect('vendor');
	}
	
}
?>