<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class User_auth extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/user_authmodel','user_auth_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$this->load->library('pagination');
		$config = [
			'base_url'			=>	site_url('master/user_auth/index'),
			'per_page'			=>	4,
			'total_rows'		=>	$this->user_auth_model->num_rows(),
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		//echo $config['total_rows'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$list = $this->user_auth_model->user_auth_list($config['per_page'],$page);
		$links=	$this->pagination->create_links();
		$this->load->view('master/user_auth/user_auth', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('email'	, 'email', 'required|xss_clean|is_unique[user_auth.user_email_id]');
		$users_list	=	$this->user_auth_model->user_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/user_auth/add_user_auth',['user_list'=>$users_list]);
		}
		else{
			$data_user_auth=array('user_email_id'=> $this->input->post('email'));
			$data_master_user_auth=array(
			'customer_master_view'					=>	$this->input->post('customer_master_view',TRUE)		==null||0 ? 0 : 1,
			'customer_master_add'					=>	$this->input->post('customer_master_add',TRUE)		==null||0 ? 0 : 1,
			'customer_master_edit'					=>	$this->input->post('customer_master_edit',TRUE)		==null||0 ? 0 : 1,
			'customer_master_delete'				=>	$this->input->post('customer_master_delete',TRUE)	==null||0 ? 0 : 1,
			'item_master_view'						=>	$this->input->post('item_master_view',TRUE)			==null||0 ? 0 : 1,
			'item_master_add'						=>	$this->input->post('item_master_add',TRUE)			==null||0 ? 0 : 1,
			'item_master_edit'						=>	$this->input->post('item_master_edit',TRUE)			==null||0 ? 0 : 1,
			'item_master_delete'					=>	$this->input->post('item_master_delete',TRUE)		==null||0 ? 0 : 1,
			'company_make_view'						=>	$this->input->post('company_master_view',TRUE)		==null||0 ? 0 : 1,
			'company_make_add'						=>	$this->input->post('company_master_add',TRUE)		==null||0 ? 0 : 1,
			'company_make_edit'						=>	$this->input->post('company_master_edit',TRUE)		==null||0 ? 0 : 1,
			'company_make_delete'					=>	$this->input->post('company_master_delete',TRUE)	==null||0 ? 0 : 1,
			'vendor_master_view'					=>	$this->input->post('vendor_master_view',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_add'						=>	$this->input->post('vendor_master_add',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_edit'					=>	$this->input->post('vendor_master_edit',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_delete'					=>	$this->input->post('vendor_master_delete',TRUE)		==null||0 ? 0 : 1,
			'store_master_view'						=>	$this->input->post('store_master_view',TRUE)		==null||0 ? 0 : 1,
			'store_master_add'						=>	$this->input->post('store_master_add',TRUE)			==null||0 ? 0 : 1,
			'store_master_edit'						=>	$this->input->post('store_master_edit',TRUE)		==null||0 ? 0 : 1,
			'store_master_delete'					=>	$this->input->post('store_master_delete',TRUE)		==null||0 ? 0 : 1,
			'country_master_view'					=>	$this->input->post('country_master_view',TRUE)		==null||0 ? 0 : 1,
			'country_master_add'					=>	$this->input->post('country_master_add',TRUE)		==null||0 ? 0 : 1,
			'country_master_edit'					=>	$this->input->post('country_master_edit',TRUE)		==null||0 ? 0 : 1,
			'country_master_delete'					=>	$this->input->post('country_master_delete',TRUE)	==null||0 ? 0 : 1,
			'state_master_view'						=>	$this->input->post('state_master_view',TRUE)		==null||0 ? 0 : 1,
			'state_master_add'						=>	$this->input->post('state_master_add',TRUE)			==null||0 ? 0 : 1,
			'state_master_edit'						=>	$this->input->post('state_master_edit',TRUE)		==null||0 ? 0 : 1,
			'state_master_delete'					=>	$this->input->post('state_master_delete',TRUE)		==null||0 ? 0 : 1,
			'city_master_view'						=>	$this->input->post('city_master_view',TRUE)			==null||0 ? 0 : 1,
			'city_master_add'						=>	$this->input->post('city_master_add',TRUE)			==null||0 ? 0 : 1,
			'city_master_edit'						=>	$this->input->post('city_master_edit',TRUE)			==null||0 ? 0 : 1,
			'city_master_delete'					=>	$this->input->post('city_master_delete',TRUE)		==null||0 ? 0 : 1,
			'region_master_view'					=>	$this->input->post('region_master_view',TRUE)		==null||0 ? 0 : 1,
			'region_master_add'						=>	$this->input->post('region_master_add',TRUE)		==null||0 ? 0 : 1,
			'region_master_edit'					=>	$this->input->post('region_master_edit',TRUE)		==null||0 ? 0 : 1,
			'region_master_delete'					=>	$this->input->post('region_master_delete',TRUE)		==null||0 ? 0 : 1,
			'location_master_view'					=>	$this->input->post('location_master_view',TRUE)		==null||0 ? 0 : 1,
			'location_master_add'					=>	$this->input->post('location_master_add',TRUE)		==null||0 ? 0 : 1,
			'location_master_edit'					=>	$this->input->post('location_master_edit',TRUE)		==null||0 ? 0 : 1,
			'location_master_delete'				=>	$this->input->post('location_master_delete',TRUE)	==null||0 ? 0 : 1,
			'branch_master_view'					=>	$this->input->post('branch_master_view',TRUE)		==null||0 ? 0 : 1,
			'branch_master_add'						=>	$this->input->post('branch_master_add',TRUE)		==null||0 ? 0 : 1,
			'branch_master_edit'					=>	$this->input->post('branch_master_edit',TRUE)		==null||0 ? 0 : 1,
			'branch_master_delete'					=>	$this->input->post('branch_master_delete',TRUE)		==null||0 ? 0 : 1,
			'entity_master_view'					=>	$this->input->post('entity_master_view',TRUE)		==null||0 ? 0 : 1,
			'entity_master_add'						=>	$this->input->post('entity_master_add',TRUE)		==null||0 ? 0 : 1,
			'entity_master_edit'					=>	$this->input->post('entity_master_edit',TRUE)		==null||0 ? 0 : 1,
			'entity_master_delete'					=>	$this->input->post('entity_master_delete',TRUE)		==null||0 ? 0 : 1,
			'work_package_master_view'				=>	$this->input->post('workpackage_master_view',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_add'				=>	$this->input->post('workpackage_master_add',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_edit'				=>	$this->input->post('workpackage_master_edit',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_delete'			=>	$this->input->post('workpackage_master_delete',TRUE)==null||0 ? 0 : 1,
			'designation_master_view'				=>	$this->input->post('designation_master_view',TRUE)	==null||0 ? 0 : 1,
			'designation_master_add'				=>	$this->input->post('designation_master_add',TRUE)	==null||0 ? 0 : 1,
			'designation_master_edit'				=>	$this->input->post('designation_master_edit',TRUE)	==null||0 ? 0 : 1,
			'designation_master_delete'				=>	$this->input->post('designation_master_delete',TRUE)==null||0 ? 0 : 1,
			'department_master_view'				=>	$this->input->post('department_master_view',TRUE)	==null||0 ? 0 : 1,
			'department_master_add'					=>	$this->input->post('department_master_add',TRUE)	==null||0 ? 0 : 1,
			'department_master_edit'				=>	$this->input->post('department_master_edit',TRUE)	==null||0 ? 0 : 1,
			'department_master_delete'				=>	$this->input->post('department_master_delete',TRUE)	==null||0 ? 0 : 1,
			'user_registration_master_view'			=>	$this->input->post('new_user_master_view',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_add'			=>	$this->input->post('new_user_master_add',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_edit'			=>	$this->input->post('new_user_master_edit',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_delete'		=>	$this->input->post('new_user_master_delete',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_view'				=>	$this->input->post('user_auth_master_view',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_add'				=>	$this->input->post('user_auth_master_add',TRUE)		==null||0 ? 0 : 1,
			'usermenu_auth_master_edit'				=>	$this->input->post('user_auth_master_edit',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_delete'			=>	$this->input->post('user_auth_master_delete',TRUE)	==null||0 ? 0 : 1,
			);
			$data_hr_user_auth=array(
			'employee_hr_view'						=>	$this->input->post('employee_hr_view',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_add'						=>	$this->input->post('employee_hr_add',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_edit'						=>	$this->input->post('employee_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_delete'					=>	$this->input->post('employee_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'outs_employee_hr_view'					=>	$this->input->post('outsrc_employee_hr_view',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_add'					=>	$this->input->post('outsrc_employee_hr_add',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_edit'					=>	$this->input->post('outsrc_employee_hr_edit',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_delete'				=>	$this->input->post('outsrc_employee_hr_delete',TRUE)==null||0 ? 0 : 1,
			'expensemaster_hr_view'					=>	$this->input->post('expenses_hr_view',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_add'					=>	$this->input->post('expenses_hr_add',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_edit'					=>	$this->input->post('expenses_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_delete'				=>	$this->input->post('expenses_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'expenseapproval_hr_view'				=>	$this->input->post('expenses_apprvl_hr_view',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_add'				=>	$this->input->post('expenses_apprvl_hr_add',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_edit'				=>	$this->input->post('expenses_apprvl_hr_edit',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_delete'				=>	$this->input->post('expenses_apprvl_hr_delete',TRUE)==null||0 ? 0 : 1,
			'accountstatus_hr_view'					=>	$this->input->post('acc_stat_hr_view',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_add'					=>	$this->input->post('acc_stat_hr_add',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_edit'					=>	$this->input->post('acc_stat_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_delete'				=>	$this->input->post('acc_stat_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_view'						=>	$this->input->post('daily_info_hr_view',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_add'						=>	$this->input->post('daily_info_hr_add',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_edit'						=>	$this->input->post('daily_info_hr_edit',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_delete'					=>	$this->input->post('daily_info_hr_delete',TRUE)		==null||0 ? 0 : 1,
			);
			$data_project_user_auth=array(
			'projects_project_view'					=>	$this->input->post('project_prjt_view',TRUE)		==null||0 ? 0 : 1,
			'projects_project_add'					=>	$this->input->post('project_prjt_add',TRUE)			==null||0 ? 0 : 1,
			'projects_project_edit'					=>	$this->input->post('project_prjt_edit',TRUE)		==null||0 ? 0 : 1,
			'projects_project_delete'				=>	$this->input->post('project_prjt_delete',TRUE)		==null||0 ? 0 : 1,
			'customerpo_project_view'				=>	$this->input->post('customer_po_prjt_view',TRUE)	==null||0 ? 0 : 1,
			'customerpo_project_add'				=>	$this->input->post('customer_po_prjt_add',TRUE)		==null||0 ? 0 : 1,
			'customerpo_project_edit'				=>	$this->input->post('customer_po_prjt_edit',TRUE)	==null||0 ? 0 : 1,
			'customerpo_project_delete'				=>	$this->input->post('customer_po_prjt_delete',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_view'				=>	$this->input->post('work_status_prjt_view',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_add'				=>	$this->input->post('work_status_prjt_add',TRUE)		==null||0 ? 0 : 1,
			'workstatus_project_edit'				=>	$this->input->post('work_status_prjt_edit',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_delete'				=>	$this->input->post('work_status_prjt_delete',TRUE)	==null||0 ? 0 : 1,
			);
			$data_invoice_user_auth=array(
			'paymenttrans_invoice_view'				=>	$this->input->post('pay_trans_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'paymenttrans_invoice_add'				=>	$this->input->post('pay_trans_invacc_add',TRUE)		==null||0 ? 0 : 1,
			'paymenttrans_invoice_edit'				=>	$this->input->post('pay_trans_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'paymenttrans_invoice_delete'			=>	$this->input->post('pay_trans_invacc_delete',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_view'			=>	$this->input->post('poiss_toven_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_add'			=>	$this->input->post('poiss_toven_invacc_add',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_edit'			=>	$this->input->post('poiss_toven_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_delete'		=>	$this->input->post('poiss_toven_invacc_delete',TRUE)==null||0 ? 0 : 1,
			'poissue_toemp_invoice_view'			=>	$this->input->post('poiss_toemp_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_add'				=>	$this->input->post('poiss_toemp_invacc_add',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_edit'			=>	$this->input->post('poiss_toemp_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_delete'			=>	$this->input->post('poiss_toemp_invacc_delete',TRUE)==null||0 ? 0 : 1,
			);
			$data_inventory_user_auth=array(
			'purchaseinv_inventory_view'			=>	$this->input->post('purchase_inv_view',TRUE)		==null||0 ? 0 : 1,
			'purchaseinv_inventory_add'				=>	$this->input->post('purchase_inv_add',TRUE)			==null||0 ? 0 : 1,
			'purchaseinv_inventory_edit'			=>	$this->input->post('purchase_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'purchaseinv_inventory_delete'			=>	$this->input->post('purchase_inv_delete',TRUE)		==null||0 ? 0 : 1,
			'addinv_inventory_view'					=>	$this->input->post('add_inv_view',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_add'					=>	$this->input->post('add_inv_add',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_edit'					=>	$this->input->post('add_inv_edit',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_delete'				=>	$this->input->post('add_inv_delete',TRUE)			==null||0 ? 0 : 1,
			'storetransfer_inventory_view'			=>	$this->input->post('store_trans_inv_view',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_add'			=>	$this->input->post('store_trans_inv_add',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_edit'			=>	$this->input->post('store_trans_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_delete'		=>	$this->input->post('store_trans_inv_delete',TRUE)	==null||0 ? 0 : 1,
			'receive_bystore_inventory_view'		=>	$this->input->post('reciv_store_inv_view',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_add'			=>	$this->input->post('reciv_store_inv_add',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_edit'		=>	$this->input->post('reciv_store_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_delete'		=>	$this->input->post('reciv_store_inv_delete',TRUE)	==null||0 ? 0 : 1,
			'issuetoemp_inventory_view'				=>	$this->input->post('iss_emp_inv_view',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_add'				=>	$this->input->post('iss_emp_inv_add',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_edit'				=>	$this->input->post('iss_emp_inv_edit',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_delete'			=>	$this->input->post('iss_emp_inv_delete',TRUE)		==null||0 ? 0 : 1,
			'receive_byemp_inventory_view'			=>	$this->input->post('rcv_emp_inv_view',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_add'			=>	$this->input->post('rcv_emp_inv_add',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_edit'			=>	$this->input->post('rcv_emp_inv_edit',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_delete'		=>	$this->input->post('rcv_emp_inv_delete',TRUE)		==null||0 ? 0 : 1,
			
			);
			$insert_id=$this->user_auth_model->insert_user_auth($data_user_auth,$data_master_user_auth,$data_hr_user_auth,$data_invoice_user_auth,
			$data_project_user_auth,$data_inventory_user_auth);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Authentication To User Added Successfully");
			return redirect("user_auth/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->user_auth_model->view_user_auth($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('email'	, 'email', 'xss_clean');
		$users_list	=	$this->user_auth_model->user_list();
		
		if($this->form_validation->run()==false){
		$this->load->view('master/user_auth/edit_user_auth',['list'=>$list,'user_list'=>$users_list]);
		}
		else{
			
			$data_user_auth=array('user_email_id'=> $this->input->post('email'));
			$data_master_user_auth=array(
			'customer_master_view'					=>	$this->input->post('customer_master_view',TRUE)		==null||0 ? 0 : 1,
			'customer_master_add'					=>	$this->input->post('customer_master_add',TRUE)		==null||0 ? 0 : 1,
			'customer_master_edit'					=>	$this->input->post('customer_master_edit',TRUE)		==null||0 ? 0 : 1,
			'customer_master_delete'				=>	$this->input->post('customer_master_delete',TRUE)	==null||0 ? 0 : 1,
			'item_master_view'						=>	$this->input->post('item_master_view',TRUE)			==null||0 ? 0 : 1,
			'item_master_add'						=>	$this->input->post('item_master_add',TRUE)			==null||0 ? 0 : 1,
			'item_master_edit'						=>	$this->input->post('item_master_edit',TRUE)			==null||0 ? 0 : 1,
			'item_master_delete'					=>	$this->input->post('item_master_delete',TRUE)		==null||0 ? 0 : 1,
			'company_make_view'						=>	$this->input->post('company_master_view',TRUE)		==null||0 ? 0 : 1,
			'company_make_add'						=>	$this->input->post('company_master_add',TRUE)		==null||0 ? 0 : 1,
			'company_make_edit'						=>	$this->input->post('company_master_edit',TRUE)		==null||0 ? 0 : 1,
			'company_make_delete'					=>	$this->input->post('company_master_delete',TRUE)	==null||0 ? 0 : 1,
			'vendor_master_view'					=>	$this->input->post('vendor_master_view',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_add'						=>	$this->input->post('vendor_master_add',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_edit'					=>	$this->input->post('vendor_master_edit',TRUE)		==null||0 ? 0 : 1,
			'vendor_master_delete'					=>	$this->input->post('vendor_master_delete',TRUE)		==null||0 ? 0 : 1,
			'store_master_view'						=>	$this->input->post('store_master_view',TRUE)		==null||0 ? 0 : 1,
			'store_master_add'						=>	$this->input->post('store_master_add',TRUE)			==null||0 ? 0 : 1,
			'store_master_edit'						=>	$this->input->post('store_master_edit',TRUE)		==null||0 ? 0 : 1,
			'store_master_delete'					=>	$this->input->post('store_master_delete',TRUE)		==null||0 ? 0 : 1,
			'country_master_view'					=>	$this->input->post('country_master_view',TRUE)		==null||0 ? 0 : 1,
			'country_master_add'					=>	$this->input->post('country_master_add',TRUE)		==null||0 ? 0 : 1,
			'country_master_edit'					=>	$this->input->post('country_master_edit',TRUE)		==null||0 ? 0 : 1,
			'country_master_delete'					=>	$this->input->post('country_master_delete',TRUE)	==null||0 ? 0 : 1,
			'state_master_view'						=>	$this->input->post('state_master_view',TRUE)		==null||0 ? 0 : 1,
			'state_master_add'						=>	$this->input->post('state_master_add',TRUE)			==null||0 ? 0 : 1,
			'state_master_edit'						=>	$this->input->post('state_master_edit',TRUE)		==null||0 ? 0 : 1,
			'state_master_delete'					=>	$this->input->post('state_master_delete',TRUE)		==null||0 ? 0 : 1,
			'city_master_view'						=>	$this->input->post('city_master_view',TRUE)			==null||0 ? 0 : 1,
			'city_master_add'						=>	$this->input->post('city_master_add',TRUE)			==null||0 ? 0 : 1,
			'city_master_edit'						=>	$this->input->post('city_master_edit',TRUE)			==null||0 ? 0 : 1,
			'city_master_delete'					=>	$this->input->post('city_master_delete',TRUE)		==null||0 ? 0 : 1,
			'region_master_view'					=>	$this->input->post('region_master_view',TRUE)		==null||0 ? 0 : 1,
			'region_master_add'						=>	$this->input->post('region_master_add',TRUE)		==null||0 ? 0 : 1,
			'region_master_edit'					=>	$this->input->post('region_master_edit',TRUE)		==null||0 ? 0 : 1,
			'region_master_delete'					=>	$this->input->post('region_master_delete',TRUE)		==null||0 ? 0 : 1,
			'location_master_view'					=>	$this->input->post('location_master_view',TRUE)		==null||0 ? 0 : 1,
			'location_master_add'					=>	$this->input->post('location_master_add',TRUE)		==null||0 ? 0 : 1,
			'location_master_edit'					=>	$this->input->post('location_master_edit',TRUE)		==null||0 ? 0 : 1,
			'location_master_delete'				=>	$this->input->post('location_master_delete',TRUE)	==null||0 ? 0 : 1,
			'branch_master_view'					=>	$this->input->post('branch_master_view',TRUE)		==null||0 ? 0 : 1,
			'branch_master_add'						=>	$this->input->post('branch_master_add',TRUE)		==null||0 ? 0 : 1,
			'branch_master_edit'					=>	$this->input->post('branch_master_edit',TRUE)		==null||0 ? 0 : 1,
			'branch_master_delete'					=>	$this->input->post('branch_master_delete',TRUE)		==null||0 ? 0 : 1,
			'entity_master_view'					=>	$this->input->post('entity_master_view',TRUE)		==null||0 ? 0 : 1,
			'entity_master_add'						=>	$this->input->post('entity_master_add',TRUE)		==null||0 ? 0 : 1,
			'entity_master_edit'					=>	$this->input->post('entity_master_edit',TRUE)		==null||0 ? 0 : 1,
			'entity_master_delete'					=>	$this->input->post('entity_master_delete',TRUE)		==null||0 ? 0 : 1,
			'work_package_master_view'				=>	$this->input->post('workpackage_master_view',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_add'				=>	$this->input->post('workpackage_master_add',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_edit'				=>	$this->input->post('workpackage_master_edit',TRUE)	==null||0 ? 0 : 1,
			'work_package_master_delete'			=>	$this->input->post('workpackage_master_delete',TRUE)==null||0 ? 0 : 1,
			'designation_master_view'				=>	$this->input->post('designation_master_view',TRUE)	==null||0 ? 0 : 1,
			'designation_master_add'				=>	$this->input->post('designation_master_add',TRUE)	==null||0 ? 0 : 1,
			'designation_master_edit'				=>	$this->input->post('designation_master_edit',TRUE)	==null||0 ? 0 : 1,
			'designation_master_delete'				=>	$this->input->post('designation_master_delete',TRUE)==null||0 ? 0 : 1,
			'department_master_view'				=>	$this->input->post('department_master_view',TRUE)	==null||0 ? 0 : 1,
			'department_master_add'					=>	$this->input->post('department_master_add',TRUE)	==null||0 ? 0 : 1,
			'department_master_edit'				=>	$this->input->post('department_master_edit',TRUE)	==null||0 ? 0 : 1,
			'department_master_delete'				=>	$this->input->post('department_master_delete',TRUE)	==null||0 ? 0 : 1,
			'user_registration_master_view'			=>	$this->input->post('new_user_master_view',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_add'			=>	$this->input->post('new_user_master_add',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_edit'			=>	$this->input->post('new_user_master_edit',TRUE)		==null||0 ? 0 : 1,
			'user_registration_master_delete'		=>	$this->input->post('new_user_master_delete',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_view'				=>	$this->input->post('user_auth_master_view',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_add'				=>	$this->input->post('user_auth_master_add',TRUE)		==null||0 ? 0 : 1,
			'usermenu_auth_master_edit'				=>	$this->input->post('user_auth_master_edit',TRUE)	==null||0 ? 0 : 1,
			'usermenu_auth_master_delete'			=>	$this->input->post('user_auth_master_delete',TRUE)	==null||0 ? 0 : 1,
			);
			$data_hr_user_auth=array(
			'employee_hr_view'						=>	$this->input->post('employee_hr_view',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_add'						=>	$this->input->post('employee_hr_add',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_edit'						=>	$this->input->post('employee_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'employee_hr_delete'					=>	$this->input->post('employee_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'outs_employee_hr_view'					=>	$this->input->post('outsrc_employee_hr_view',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_add'					=>	$this->input->post('outsrc_employee_hr_add',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_edit'					=>	$this->input->post('outsrc_employee_hr_edit',TRUE)	==null||0 ? 0 : 1,
			'outs_employee_hr_delete'				=>	$this->input->post('outsrc_employee_hr_delete',TRUE)==null||0 ? 0 : 1,
			'expensemaster_hr_view'					=>	$this->input->post('expenses_hr_view',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_add'					=>	$this->input->post('expenses_hr_add',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_edit'					=>	$this->input->post('expenses_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'expensemaster_hr_delete'				=>	$this->input->post('expenses_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'expenseapproval_hr_view'				=>	$this->input->post('expenses_apprvl_hr_view',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_add'				=>	$this->input->post('expenses_apprvl_hr_add',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_edit'				=>	$this->input->post('expenses_apprvl_hr_edit',TRUE)	==null||0 ? 0 : 1,
			'expenseapproval_hr_delete'				=>	$this->input->post('expenses_apprvl_hr_delete',TRUE)==null||0 ? 0 : 1,
			'accountstatus_hr_view'					=>	$this->input->post('acc_stat_hr_view',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_add'					=>	$this->input->post('acc_stat_hr_add',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_edit'					=>	$this->input->post('acc_stat_hr_edit',TRUE)			==null||0 ? 0 : 1,
			'accountstatus_hr_delete'				=>	$this->input->post('acc_stat_hr_delete',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_view'						=>	$this->input->post('daily_info_hr_view',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_add'						=>	$this->input->post('daily_info_hr_add',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_edit'						=>	$this->input->post('daily_info_hr_edit',TRUE)		==null||0 ? 0 : 1,
			'dailyinfo_hr_delete'					=>	$this->input->post('daily_info_hr_delete',TRUE)		==null||0 ? 0 : 1,
			);
			$data_project_user_auth=array(
			'projects_project_view'					=>	$this->input->post('project_prjt_view',TRUE)		==null||0 ? 0 : 1,
			'projects_project_add'					=>	$this->input->post('project_prjt_add',TRUE)			==null||0 ? 0 : 1,
			'projects_project_edit'					=>	$this->input->post('project_prjt_edit',TRUE)		==null||0 ? 0 : 1,
			'projects_project_delete'				=>	$this->input->post('project_prjt_delete',TRUE)		==null||0 ? 0 : 1,
			'customerpo_project_view'				=>	$this->input->post('customer_po_prjt_view',TRUE)	==null||0 ? 0 : 1,
			'customerpo_project_add'				=>	$this->input->post('customer_po_prjt_add',TRUE)		==null||0 ? 0 : 1,
			'customerpo_project_edit'				=>	$this->input->post('customer_po_prjt_edit',TRUE)	==null||0 ? 0 : 1,
			'customerpo_project_delete'				=>	$this->input->post('customer_po_prjt_delete',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_view'				=>	$this->input->post('work_status_prjt_view',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_add'				=>	$this->input->post('work_status_prjt_add',TRUE)		==null||0 ? 0 : 1,
			'workstatus_project_edit'				=>	$this->input->post('work_status_prjt_edit',TRUE)	==null||0 ? 0 : 1,
			'workstatus_project_delete'				=>	$this->input->post('work_status_prjt_delete',TRUE)	==null||0 ? 0 : 1,
			);
			$data_invoice_user_auth=array(
			'paymenttrans_invoice_view'				=>	$this->input->post('pay_trans_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'paymenttrans_invoice_add'				=>	$this->input->post('pay_trans_invacc_add',TRUE)		==null||0 ? 0 : 1,
			'paymenttrans_invoice_edit'				=>	$this->input->post('pay_trans_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'paymenttrans_invoice_delete'			=>	$this->input->post('pay_trans_invacc_delete',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_view'			=>	$this->input->post('poiss_toven_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_add'			=>	$this->input->post('poiss_toven_invacc_add',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_edit'			=>	$this->input->post('poiss_toven_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'poissue_tovendor_invoice_delete'		=>	$this->input->post('poiss_toven_invacc_delete',TRUE)==null||0 ? 0 : 1,
			'poissue_toemp_invoice_view'			=>	$this->input->post('poiss_toemp_invacc_view',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_add'				=>	$this->input->post('poiss_toemp_invacc_add',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_edit'			=>	$this->input->post('poiss_toemp_invacc_edit',TRUE)	==null||0 ? 0 : 1,
			'poissue_toemp_invoice_delete'			=>	$this->input->post('poiss_toemp_invacc_delete',TRUE)==null||0 ? 0 : 1,
			);
			$data_inventory_user_auth=array(
			'purchaseinv_inventory_view'			=>	$this->input->post('purchase_inv_view',TRUE)		==null||0 ? 0 : 1,
			'purchaseinv_inventory_add'				=>	$this->input->post('purchase_inv_add',TRUE)			==null||0 ? 0 : 1,
			'purchaseinv_inventory_edit'			=>	$this->input->post('purchase_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'purchaseinv_inventory_delete'			=>	$this->input->post('purchase_inv_delete',TRUE)		==null||0 ? 0 : 1,
			'addinv_inventory_view'					=>	$this->input->post('add_inv_view',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_add'					=>	$this->input->post('add_inv_add',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_edit'					=>	$this->input->post('add_inv_edit',TRUE)				==null||0 ? 0 : 1,
			'addinv_inventory_delete'				=>	$this->input->post('add_inv_delete',TRUE)			==null||0 ? 0 : 1,
			'storetransfer_inventory_view'			=>	$this->input->post('store_trans_inv_view',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_add'			=>	$this->input->post('store_trans_inv_add',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_edit'			=>	$this->input->post('store_trans_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'storetransfer_inventory_delete'		=>	$this->input->post('store_trans_inv_delete',TRUE)	==null||0 ? 0 : 1,
			'receive_bystore_inventory_view'		=>	$this->input->post('reciv_store_inv_view',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_add'			=>	$this->input->post('reciv_store_inv_add',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_edit'		=>	$this->input->post('reciv_store_inv_edit',TRUE)		==null||0 ? 0 : 1,
			'receive_bystore_inventory_delete'		=>	$this->input->post('reciv_store_inv_delete',TRUE)	==null||0 ? 0 : 1,
			'issuetoemp_inventory_view'				=>	$this->input->post('iss_emp_inv_view',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_add'				=>	$this->input->post('iss_emp_inv_add',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_edit'				=>	$this->input->post('iss_emp_inv_edit',TRUE)			==null||0 ? 0 : 1,
			'issuetoemp_inventory_delete'			=>	$this->input->post('iss_emp_inv_delete',TRUE)		==null||0 ? 0 : 1,
			'receive_byemp_inventory_view'			=>	$this->input->post('rcv_emp_inv_view',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_add'			=>	$this->input->post('rcv_emp_inv_add',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_edit'			=>	$this->input->post('rcv_emp_inv_edit',TRUE)			==null||0 ? 0 : 1,
			'receive_byemp_inventory_delete'		=>	$this->input->post('rcv_emp_inv_delete',TRUE)		==null||0 ? 0 : 1,
			
			);

			$this->user_auth_model->update_user_auth($data_master_user_auth,$data_hr_user_auth,$data_invoice_user_auth,
			$data_project_user_auth,$data_inventory_user_auth,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>User Authentication Updated Successfully");
			return redirect("user_auth");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$users_list	=	$this->user_auth_model->user_list();
		$list=$this->user_auth_model->view_user_auth($list_id);
		$this->load->view('master/user_auth/view_user_auth',['list'=>$list,'user_list'=>$users_list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->user_auth_model->delete_user_auth($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>User Authenticity successfully deleted!");
		redirect('user_auth');
	}

	
}
?>