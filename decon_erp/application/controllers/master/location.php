<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Location extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/locationmodel','location_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$config = [
			'base_url'			=>	site_url('master/location/index'),
			'per_page'			=>	1,
			'total_rows'		=>	$this->location_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$list = $this->location_model->location_list($config['per_page'], $page );
		$links=	$this->pagination->create_links();
		$this->load->view('master/location/location', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('city_id'	, 'City', 'required|xss_clean');
		$this->form_validation->set_rules('location'	, 'location', 'required|xss_clean|is_unique[master_location.location]');	
		$cont_list = $this->location_model->city_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/location/add_location',['contlist'=>$cont_list]);
		}
		else{
			$data_location=array(
						'city_id'					=> $this->input->post('city_id'),
						'location'							=> strtolower($this->input->post('location'))				
			);
			$insert_id=$this->location_model->insert_location($data_location);
			$this->session->set_flashdata('message', "<p>location added successfully.</p>");
			return redirect("location/edit/$insert_id");
			
		}	
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->location_model->view_location($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('city_id'	, 'City', 'required|xss_clean');
		$this->form_validation->set_rules('location'	, 'location', 'required|xss_clean');
		$cont_list = $this->location_model->city_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/location/edit_location',['list'=>$list,'contlist'=>$cont_list]);
		}
		else{
			$data_location=array(
						'city_id'					=> $this->input->post('city_id'),
						'location'							=> strtolower($this->input->post('location'))				
			);

			$this->location_model->update_location($data_location,$list_id);
			$this->session->set_flashdata('message', "<p>location added successfully.</p>");
			return redirect("location");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$cont_list = $this->location_model->city_list();
		$list=$this->location_model->view_location($list_id);
		$this->load->view('master/location/view_location',['list'=>$list,'contlist'=>$cont_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		$this->location_model->delete_location($list_id);
		$this->session->set_flashdata('message', '<p>region successfully deleted!</p>');
		redirect('location');
	}
	
}
?>