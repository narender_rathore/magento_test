<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class City extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/citymodel','city_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$config = [
			'base_url'			=>	site_url('master/city/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->city_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->city_model->city_list($config['per_page'], $this->uri->segment(4) );
		
		$this->load->view('master/city/city', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country_id'	, 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('state_id'	, 'State', 'required|xss_clean');
		$this->form_validation->set_rules('city'	, 'city', 'required|xss_clean|is_unique[master_city.city]');	
		$cont_list = $this->city_model->country_list();
		$state_list = $this->city_model->state_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/city/add_city',['contlist'=>$cont_list,'statlist'=>$state_list]);
		}
		else{
			$data_city=array(
						'country_id'					=> $this->input->post('country_id'),
						'state_id'						=> $this->input->post('state_id'),
						'city'							=> strtolower($this->input->post('city'))				
			);
			$insert_id=$this->city_model->insert_city($data_city);
			$this->session->set_flashdata('message', "<p>city added successfully.</p>");
			return redirect("city/edit/$insert_id");
			
		}	
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->city_model->view_city($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country_id'	, 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('state_id'	, 'State', 'required|xss_clean');
		$this->form_validation->set_rules('city'	, 'city', 'required|xss_clean');
		$cont_list = $this->city_model->country_list();
		$state_list = $this->city_model->state_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/city/edit_city',['list'=>$list,'contlist'=>$cont_list,'statlist'=>$state_list]);
		}
		else{
			$data_city=array(
						'country_id'					=> $this->input->post('country_id'),
						'state_id'						=> $this->input->post('state_id'),
						'city'							=> strtolower($this->input->post('city'))				
			);

			$this->city_model->update_city($data_city,$list_id);
			$this->session->set_flashdata('message', "<p>city added successfully.</p>");
			return redirect("city");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$cont_list = $this->city_model->country_list();
		$state_list = $this->city_model->state_list();
		$list=$this->city_model->view_city($list_id);
		$this->load->view('master/city/view_city',['list'=>$list,'contlist'=>$cont_list,'statlist'=>$state_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->city_model->delete_city($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('city');
	}
	
}
?>