<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Store extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/storemodel','store_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/store/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->store_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->store_model->store_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/store/store', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('store_code'	, 'Store Code'	, 'required|xss_clean');
		$this->form_validation->set_rules('lead_person'	, 'Lead Person'	, 'required|xss_clean');
		$this->form_validation->set_rules('location'	, 'Location'	, 'required|xss_clean');
		$location_list = $this->store_model->location_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/store/add_store',['loc_list'=>$location_list]);
		}
		else{
			$data_store=array('store_code'=> $this->input->post('store_code'),'lead_person'=> $this->input->post('lead_person'),'location'=> $this->input->post('location'));
			$insert_id=$this->store_model->insert_store($data_store);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Store Added Successfully");
			return redirect("store/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->store_model->view_store($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('store_code'	, 'Store Code'	, 'required|xss_clean');
		$this->form_validation->set_rules('lead_person'	, 'Lead Person'	, 'required|xss_clean');
		$this->form_validation->set_rules('location'	, 'Location'	, 'required|xss_clean');
		$location_list = $this->store_model->location_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/store/edit_store',['list'=>$list,'loc_list'=>$location_list]);
		}
		else{
			$data_store=array('store_code'=> $this->input->post('store_code'),'lead_person'=> $this->input->post('lead_person'),'location'=> $this->input->post('location'));
			echo 
			$this->store_model->update_store($data_store,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Store Updated Successfully");
			return redirect("store");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$location_list = $this->store_model->location_list();
		$list=$this->store_model->view_store($list_id);
		$this->load->view('master/store/view_store',['list'=>$list,'loc_list'=>$location_list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		$this->store_model->delete_store($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Store Successfully Deleted!");
		redirect('store');
	}

	
}
?>