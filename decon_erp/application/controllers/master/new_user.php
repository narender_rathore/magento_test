<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class new_user extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/new_usermodel','new_user_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
		$this->load->library('bcrypt');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/new_user/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->new_user_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->new_user_model->new_user_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/new_user/new_user', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('email'	, 'Email', 'required|xss_clean|valid_email|is_unique[master_user_register.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|matches[cf_pass]');
		$this->form_validation->set_rules('cf_pass', 'Confirm Password', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/new_user/add_new_user');
		}
		else{
			$password=$this->input->post('password');
			$data_new_user=array('email'=> strtolower($this->input->post('email')),'password'=>	$this->bcrypt->hash_password($password));
			$insert_id=$this->new_user_model->insert_new_user($data_new_user);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>User Added Successfully");
			return redirect("new_user/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->new_user_model->view_new_user($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		//$this->form_validation->set_rules('email'	, 'Email', 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|matches[cf_pass]');
		$this->form_validation->set_rules('cf_pass', 'Confirm Password', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/new_user/edit_new_user',['list'=>$list]);
		}
		else{
			//$data_new_user=array('email'=> strtolower($this->input->post('email')),'password'=>$this->input->post('password')	);
			$password=$this->input->post('password');
			$data_new_user=array('password'=>$this->bcrypt->hash_password($password));
			$this->new_user_model->update_new_user($data_new_user,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>User Updated Successfully");
			return redirect("new_user");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->new_user_model->view_new_user($list_id);
		$this->load->view('master/new_user/view_new_user',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->new_user_model->delete_new_user($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>User successfully deleted!");
		redirect('new_user');
	}

	
}
?>