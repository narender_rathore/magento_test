<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Branch extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/branchmodel','branch_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$config = [
			'base_url'			=>	site_url('master/branch/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->branch_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->branch_model->branch_list($config['per_page'], $this->uri->segment(4) );
		
		$this->load->view('master/branch/branch', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('location_id'	, 'location', 'required|xss_clean');
		$this->form_validation->set_rules('branch'	, 'branch', 'required|xss_clean|is_unique[master_branch.branch]');	
		$cont_list = $this->branch_model->location_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/branch/add_branch',['contlist'=>$cont_list]);
		}
		else{
			$data_branch=array(
						'location_id'					=> $this->input->post('location_id'),
						'branch'						=> strtolower($this->input->post('branch'))				
			);
			$insert_id=$this->branch_model->insert_branch($data_branch);
			$this->session->set_flashdata('message', "<p>branch added successfully.</p>");
			return redirect("branch/edit/$insert_id");
			
		}	
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->branch_model->view_branch($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('location_id'	, 'Location', 'required|xss_clean');
		$this->form_validation->set_rules('branch'	, 'Branch', 'required|xss_clean');
		$cont_list = $this->branch_model->location_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/branch/edit_branch',['list'=>$list,'contlist'=>$cont_list]);
		}
		else{
			$data_branch=array(
						'location_id'					=> $this->input->post('location_id'),
						'branch'							=> strtolower($this->input->post('branch'))				
			);

			$this->branch_model->update_branch($data_branch,$list_id);
			$this->session->set_flashdata('message', "<p>branch added successfully.</p>");
			return redirect("branch");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$cont_list = $this->branch_model->location_list();
		$list=$this->branch_model->view_branch($list_id);
		$this->load->view('master/branch/view_branch',['list'=>$list,'contlist'=>$cont_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->branch_model->delete_branch($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('branch');
	}
	
}
?>