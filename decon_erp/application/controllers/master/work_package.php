<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Work_package extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/work_packagemodel','work_package_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/work_package/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->work_package_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->work_package_model->work_package_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/work_package/work_package', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('package_name'	, 'Work Package', 'required|xss_clean|is_unique[master_work_package.package_name]');
		$this->form_validation->set_rules('description'	, 'Description', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/work_package/add_work_package');
		}
		else{
			$data_work_package=array('package_name'=> strtolower($this->input->post('package_name')),'description'=>$this->input->post('description')	);
			$insert_id=$this->work_package_model->insert_work_package($data_work_package);
			$this->session->set_flashdata('message', "<p>Work Package added successfully.</p>");
			return redirect("work_package/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->work_package_model->view_work_package($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('package_name'	, 'Work Package', 'required|xss_clean');
		$this->form_validation->set_rules('description'	, 'Description', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/work_package/edit_work_package',['list'=>$list]);
		}
		else{
			$data_work_package=array('package_name'=> strtolower($this->input->post('package_name')),'description'=>$this->input->post('description')	);
			$this->work_package_model->update_work_package($data_work_package,$list_id);
			$this->session->set_flashdata('message', "<p>Work Package added successfully.</p>");
			return redirect("work_package");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->work_package_model->view_work_package($list_id);
		$this->load->view('master/work_package/view_work_package',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->work_package_model->delete_work_package($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('work_package');
	}

	
}
?>