<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Company extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/companymodel','company_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/company/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->company_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->company_model->company_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/company/company', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('make_name', 'Company Make name', 'required|xss_clean|is_unique[master_company.make_name]');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/company/add_company');
		}
		else{
			$data_company=array('make_name'=> strtolower($this->input->post('make_name'))	);
			$insert_id=$this->company_model->insert_company($data_company);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Company Added Successfully");
			return redirect("company/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->company_model->view_company($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('make_name'	, 'Company Make name', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/company/edit_company',['list'=>$list]);
		}
		else{
			$data_company=array('make_name'=> strtolower($this->input->post('make_name')) );
			echo 
			$this->company_model->update_company($data_company,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Company Updated Successfully");
			return redirect("company");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->company_model->view_company($list_id);
		$this->load->view('master/company/view_company',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->company_model->delete_company($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Company Successfully Deleted!");
		redirect('company');
	}

	
}
?>