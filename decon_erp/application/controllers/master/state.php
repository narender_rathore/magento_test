<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class State extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/statemodel','state_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$config = [
			'base_url'			=>	site_url('master/state/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->state_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->state_model->state_list($config['per_page'], $this->uri->segment(4) );
		
		$this->load->view('master/state/state', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country_id'	, 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('state'	, 'State', 'required|xss_clean|is_unique[master_state.state]');	
		$cont_list = $this->state_model->country_list();
		if($this->form_validation->run()==false){
			$this->load->view('master/state/add_state',['contlist'=>$cont_list]);
		}
		else{
			$data_state=array(
						'country_id'					=> $this->input->post('country_id'),
						'state'							=> strtolower($this->input->post('state'))				
			);
			$insert_id=$this->state_model->insert_state($data_state);
			$this->session->set_flashdata('message', "<p>state added successfully.</p>");
			return redirect("state/edit/$insert_id");
			
		}	
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->state_model->view_state($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('country_id'	, 'Country', 'required|xss_clean');
		$this->form_validation->set_rules('state'	, 'State', 'required|xss_clean');
		$cont_list = $this->state_model->country_list();
		if($this->form_validation->run()==false){
		$this->load->view('master/state/edit_state',['list'=>$list,'contlist'=>$cont_list]);
		}
		else{
			$data_state=array(
						'country_id'					=> $this->input->post('country_id'),
						'state'							=> strtolower($this->input->post('state'))				
			);

			$this->state_model->update_state($data_state,$list_id);
			$this->session->set_flashdata('message', "<p>state added successfully.</p>");
			return redirect("state");
		}
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$cont_list = $this->state_model->country_list();
		$list=$this->state_model->view_state($list_id);
		$this->load->view('master/state/view_state',['list'=>$list,'contlist'=>$cont_list]);
		$this->load->view('panel/footer');;
	}
	public function delete($list_id) {
		
		$this->state_model->delete_state($list_id);
		$this->session->set_flashdata('message', '<p>Country successfully deleted!</p>');
		redirect('state');
	}
	
}
?>