<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Item extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/itemmodel','item_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$this->load->library('pagination');
		$config = [
			'base_url'			=>	site_url('master/item/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->item_model->num_rows(),
			'display_pages'		=>	TRUE,
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		//echo $config['total_rows'];
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$list = $this->item_model->item_list($config['per_page'],$page);
		$links=	$this->pagination->create_links();
		$this->load->view('master/item/item', ['list'=>$list,'link'=>$links]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('item_name'	, 'Item Name', 'required|xss_clean|is_unique[master_new_inventory.item_name]');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/item/add_item');
		}
		else{
			$data_item=array('item_name'=> strtolower($this->input->post('item_name')),'model_no'=>$this->input->post('model_no',TRUE)==null ? 0 : 1,
			'ram'=>$this->input->post('ram',TRUE)==null ? 0 : 1,'battery'=>$this->input->post('battery',TRUE)==null ? 0 : 1,
			'laptop_charger'=>$this->input->post('laptop_charger',TRUE)==null ? 0 : 1,'antivirus_key'=>$this->input->post('antivirus_key',TRUE)==null ? 0 : 1,
			'hard_disk'=>$this->input->post('hard_disk',TRUE)==null ? 0 : 1,'serial_no'=>$this->input->post('serial_no',TRUE)==null ? 0 : 1,
			'warranty_date'=>$this->input->post('warranty_date',TRUE)==null ? 0 : 1,'software'=>$this->input->post('software',TRUE)==null ? 0 : 1,
			'chargable'=>$this->input->post('chargable',TRUE)==null ? 0 : 1,'zoom'=>$this->input->post('zoom',TRUE)==null ? 0 : 1,
			'pixel'=>$this->input->post('pixel',TRUE)==null ? 0 : 1,'length'=>$this->input->post('length',TRUE)==null ? 0 : 1,
			'imei_no'=>$this->input->post('imei_no',TRUE)==null ? 0 : 1,'memory_card'=>$this->input->post('memory_card',TRUE)==null ? 0 : 1,
			'calibration_info'=>$this->input->post('calibration_info',TRUE)==null ? 0 : 1,'dongle'=>$this->input->post('dongle',TRUE)==null ? 0 : 1,
			'ear_plug'=>$this->input->post('ear_plug',TRUE)==null ? 0 : 1,'goggles'=>$this->input->post('goggles',TRUE)==null ? 0 : 1,
			'gloves'=>$this->input->post('gloves',TRUE)==null ? 0 : 1,'shoes'=>$this->input->post('shoes',TRUE)==null ? 0 : 1,
			'helmet'=>$this->input->post('helmet',TRUE)==null ? 0 : 1,'full_body'=>$this->input->post('full_body',TRUE)==null ? 0 : 1,
			'half_body'=>$this->input->post('half_body',TRUE)==null ? 0 : 1,'lan_card'=>$this->input->post('lan_card',TRUE)==null ? 0 : 1,
			'fall_arrestor'=>$this->input->post('fall_arrestor',TRUE)==null ? 0 : 1,'first_aid_box'=>$this->input->post('first_aid_box',TRUE)==null ? 0 : 1,
			'reflective_jacket'=>$this->input->post('reflective_jacket',TRUE)==null ? 0 : 1	);
			$insert_id=$this->item_model->insert_item($data_item);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Item Added Successfully");
			return redirect("item/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->item_model->view_item($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('item_name'	, 'Item Name', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/item/edit_item',['list'=>$list]);
		}
		else{
			$data_item=array('item_name'=> strtolower($this->input->post('item_name')),'model_no'=>$this->input->post('model_no',TRUE)==null ? 0 : 1,
			'ram'=>$this->input->post('ram',TRUE)==null ? 0 : 1,'battery'=>$this->input->post('battery',TRUE)==null ? 0 : 1,
			'laptop_charger'=>$this->input->post('laptop_charger',TRUE)==null ? 0 : 1,'antivirus_key'=>$this->input->post('antivirus_key',TRUE)==null ? 0 : 1,
			'hard_disk'=>$this->input->post('hard_disk',TRUE)==null ? 0 : 1,'serial_no'=>$this->input->post('serial_no',TRUE)==null ? 0 : 1,
			'warranty_date'=>$this->input->post('warranty_date',TRUE)==null ? 0 : 1,'software'=>$this->input->post('software',TRUE)==null ? 0 : 1,
			'chargable'=>$this->input->post('chargable',TRUE)==null ? 0 : 1,'zoom'=>$this->input->post('zoom',TRUE)==null ? 0 : 1,
			'pixel'=>$this->input->post('pixel',TRUE)==null ? 0 : 1,'length'=>$this->input->post('length',TRUE)==null ? 0 : 1,
			'imei_no'=>$this->input->post('imei_no',TRUE)==null ? 0 : 1,'memory_card'=>$this->input->post('memory_card',TRUE)==null ? 0 : 1,
			'calibration_info'=>$this->input->post('calibration_info',TRUE)==null ? 0 : 1,'dongle'=>$this->input->post('dongle',TRUE)==null ? 0 : 1,
			'ear_plug'=>$this->input->post('ear_plug',TRUE)==null ? 0 : 1,'goggles'=>$this->input->post('goggles',TRUE)==null ? 0 : 1,
			'gloves'=>$this->input->post('gloves',TRUE)==null ? 0 : 1,'shoes'=>$this->input->post('shoes',TRUE)==null ? 0 : 1,
			'helmet'=>$this->input->post('helmet',TRUE)==null ? 0 : 1,'full_body'=>$this->input->post('full_body',TRUE)==null ? 0 : 1,
			'half_body'=>$this->input->post('half_body',TRUE)==null ? 0 : 1,'lan_card'=>$this->input->post('lan_card',TRUE)==null ? 0 : 1,
			'fall_arrestor'=>$this->input->post('fall_arrestor',TRUE)==null ? 0 : 1,'first_aid_box'=>$this->input->post('first_aid_box',TRUE)==null ? 0 : 1,
			'reflective_jacket'=>$this->input->post('reflective_jacket',TRUE)==null ? 0 : 1	);
			$this->item_model->update_item($data_item,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Item Updated Successfully");
			return redirect("item");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->item_model->view_item($list_id);
		$this->load->view('master/item/view_item',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->item_model->delete_item($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Item successfully deleted!");
		redirect('item');
	}

	
}
?>