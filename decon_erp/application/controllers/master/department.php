<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<?php
class Department extends MY_Controller{
	
	public function __construct(){
	
		parent::__construct();
		if( ! $this->session->userdata('user_id') )
			return redirect('login');
		$this->load->model('master/departmentmodel','department_model');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('security');
		$this->load->library('pagination');
		$this->load->library('form_validation');
	}
	
	public function index(){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');		
		$config = [
			'base_url'			=>	site_url('master/department/index'),
			'per_page'			=>	2,
			'total_rows'		=>	$this->department_model->num_rows(),
			'use_page_numbers'	=> 	true,
			'uri_segment'		=>	4,
			'num_links'			=>	4,
			'full_tag_open'		=>	"<ul class='pagination'>",
			'full_tag_close'	=>	"</ul>",
			'first_tag_open'	=>	'<li>',
			'first_tag_close'	=>	'</li>',
			'last_tag_open'		=>	'<li>',
			'last_tag_close'	=>	'</li>',
			'next_tag_open'		=>	'<li>',
			'next_tag_close'	=>	'</li>',
			'prev_tag_open'		=>	'<li>',
			'prev_tag_close'	=>	'</li>',
			'num_tag_open'		=>	'<li>',
			'num_tag_close'		=>	'</li>',
			'cur_tag_open'		=>	"<li class='active'><a>",
			'cur_tag_close'		=>	'</a></li>',
		];
		$this->pagination->initialize($config);
		$this->pagination->create_links();
		$list = $this->department_model->department_list($config['per_page'], $this->uri->segment(4) );
		$this->load->view('master/department/department', ['list'=>$list]);
		$this->load->view('panel/footer');
	}
	
	public function create(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		############################################
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('department'	, 'department', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
			$this->load->view('master/department/add_department');
		}
		else{
			$data_department=array('department'=> strtolower($this->input->post('department'))	);
			$insert_id=$this->department_model->insert_department($data_department);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Department Added Successfully");
			return redirect("department/edit/$insert_id");
		}	
		###############################################
		$this->load->view('panel/footer');
	}
	public function edit($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->department_model->view_department($list_id);
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
		$this->form_validation->set_rules('department'	, 'department', 'required|xss_clean');
		
		if($this->form_validation->run()==false){
		$this->load->view('master/department/edit_department',['list'=>$list]);
		}
		else{
			$data_department=array('department'=> strtolower($this->input->post('department')) );
			echo 
			$this->department_model->update_department($data_department,$list_id);
			$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Department Updated Successfully");
			return redirect("department");
		}
		########################################################
		$this->load->view('panel/footer');
	}
	public function view($list_id){
		$this->load->view('panel/header');
		$this->load->view('panel/sidebar');
		$list=$this->department_model->view_department($list_id);
		$this->load->view('master/department/view_department',['list'=>$list]);
		$this->load->view('panel/footer');;
	}
	
	public function delete($list_id) {
		
		$this->department_model->delete_department($list_id);
		$this->session->set_flashdata('message', "<i class='icon fa fa-check'></i>Department successfully deleted!");
		redirect('department');
	}

	
}
?>
