<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Customermodel extends CI_Model{
	function __construct() {
        parent::__construct();
    }
	public function customer_list($limit, $offset){
	
		$query=$this->db->select(['cus.id as csid','cus.comp_code','cus.comp_name','cus.tin','cus.address','cus.remarks','cus_det.name','cus_det.designation',
		'cus_det.department','cus_det.mobile','cus_det.email'])->from('master_customer as cus')->join('master_customer_contact_detail as cus_det ',
		'cus.id=cus_det.customer_id','inner')
		->order_by("cus.comp_code", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function designation_list(){
	
		$query=$this->db->select(['id','designation'])->from('master_designation')
		->order_by("designation", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['designation']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	public function department_list(){
	
		$query=$this->db->select(['id','department'])->from('master_department')
		->order_by("department", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['department']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['cus.id as csid','cus.comp_code','cus.comp_name','cus.tin','cus.address','cus.remarks','cus_det.name','cus_det.designation',
				'cus_det.department','cus_det.mobile','cus_det.email'])
				->from('master_customer as cus')
				->join('master_customer_contact_detail as cus_det ','cus.id=cus_det.customer_id','inner')
				->get();
		return $query->num_rows();
	}
	
	public function insert_customer($data_customer,$data_customer_contact){
	
		$this->db->insert('master_customer', $data_customer);
		$data_customer_contact['customer_id'] = $this->db->insert_id();
		//sprint_r($data_customer_contact);

		$this->db->insert('master_customer_contact_detail', $data_customer_contact);
		//$id = $this->db->insert_id();
    	return $data_customer_contact['customer_id'];	
		//return (isset($data_customer['id'])) ? $data_customer['id'] : FALSE;
	}
	
	public function update_customer($data_customer,$data_customer_contact,$lists_csid){
		$this->db->where('id',$lists_csid);
		$this->db->update('master_customer', $data_customer);
		$id=fetch_by_customer_id($lists_csid);
		$this->db->where('id',$id);
		$this->db->update('master_customer_contact_detail', $data_customer_contact);
    	return true;
	}
	
	public function fetch_by_customer_id($lists_csid){
		$query=$this->db->select('id')->where('customer_id',$lists_csid)->get();
		return $query->result();
	}
	
	public function view_customer($lists_csid){
	
		$query=$this->db->select(['cus.id as csid','cus.comp_code','cus.comp_name','cus.tin','cus.address','cus.remarks','cus_det.name','cus_det.designation',
		'cus_det.department','cus_det.mobile','cus_det.email'])->from('master_customer as cus')->join('master_customer_contact_detail as cus_det ',
		'cus.id=cus_det.customer_id','inner')->where('cus.id',$lists_csid)
		->get();
		return $query->row();
	}
	public function delete_customer($list_id){
	
		$this->db->where('id', $list_id);
		$this->db->delete('master_customer');
		
		$this->db->where('customer_id', $list_id);
		$this->db->delete('master_customer_contact_detail');	
		
	}
}
?>