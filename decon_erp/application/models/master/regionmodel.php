<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Regionmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function region_list($limit, $offset){
	
		$query=$this->db->select(['id','region'])->from('master_region')
		->order_by("region", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','region'])->from('master_region')->get();
		return $query->num_rows();
	}
	public function insert_region($data_region){
		$this->db->insert('master_region', $data_region);
		$data_region['id'] = $this->db->insert_id();
    	return $data_region['id'];	
	}
	
	public function update_region($data_region,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_region', $data_region);
		return true;
	}
	
	public function view_region($list_id){
		$query=$this->db->select(['id','region'])->from('master_region')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_region($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_region');
	}
}
?>