<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Entitymodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function entity_list($limit, $offset){
	
		$query=$this->db->select(['id','company','remarks'])->from('master_entity')
		->order_by("id", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','company','remarks'])->from('master_entity')->get();
		return $query->num_rows();
	}
	public function insert_entity($data_entity){
		$this->db->insert('master_entity', $data_entity);
		$data_entity['id'] = $this->db->insert_id();
    	return $data_entity['id'];	
	}
	
	public function update_entity($data_entity,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_entity', $data_entity);
		return true;
	}
	
	public function view_entity($list_id){
		$query=$this->db->select(['id','company','remarks'])->from('master_entity')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_entity($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_entity');
	}
}
?>