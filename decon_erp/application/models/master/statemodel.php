<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Statemodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	
	public function state_list($limit, $offset){
	
		$query=$this->db->select(['master_state.id','master_state.state','master_state.country_id','master_country.country'])->from('master_state')
		->join('master_country','master_country.id=master_state.country_id','inner')
		->order_by("state", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function country_list(){
	
		$query=$this->db->select(['id','country'])->from('master_country')
		->order_by("country", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['country']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['master_state.id','master_state.state','master_state.country_id','master_country.country'])->from('master_state')
		->join('master_country','master_country.id=master_state.country_id','inner')
		->get();
		return $query->num_rows();
	}
	public function insert_state($data_state){
		$this->db->insert('master_state', $data_state);
		$data_state['id'] = $this->db->insert_id();
    	return $data_state['id'];	
	}
	
	public function update_state($data_state,$list_id){
		$this->db->where('master_state.id',$list_id);
		$this->db->update('master_state', $data_state);
		return true;
	}
	
	public function view_state($list_id){
		$query=$this->db->select(['master_state.id','master_state.state','master_state.country_id','master_country.country'])->from('master_state')
		->join('master_country','master_country.id=master_state.country_id','inner')
		->where('master_state.id',$list_id)->get();
		return $query->row();
	}
	public function delete_state($list_id){
	
		$this->db->where('id', $list_id);
		$this->db->delete('master_state');
	}
}
?>