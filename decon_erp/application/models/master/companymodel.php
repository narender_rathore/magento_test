<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Companymodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function company_list($limit, $offset){
	
		$query=$this->db->select(['id','make_name'])->from('master_company')
		->order_by("make_name", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','make_name'])->from('master_company')->get();
		return $query->num_rows();
	}
	public function insert_company($data_company){
		$this->db->insert('master_company', $data_company);
		$data_company['id'] = $this->db->insert_id();
    	return $data_company['id'];	
	}
	
	public function update_company($data_company,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_company', $data_company);
		return true;
	}
	
	public function view_company($list_id){
		$query=$this->db->select(['id','make_name'])->from('master_company')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_company($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_company');
	}
}
?>