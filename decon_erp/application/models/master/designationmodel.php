<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class designationmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function designation_list($limit, $offset){
	
		$query=$this->db->select(['id','designation'])->from('master_designation')
		->order_by("designation", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','designation'])->from('master_designation')->get();
		return $query->num_rows();
	}
	public function insert_designation($data_designation){
		$this->db->insert('master_designation', $data_designation);
		$data_designation['id'] = $this->db->insert_id();
    	return $data_designation['id'];	
	}
	
	public function update_designation($data_designation,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_designation', $data_designation);
		return true;
	}
	
	public function view_designation($list_id){
		$query=$this->db->select(['id','designation'])->from('master_designation')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_designation($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_designation');
	}
}
?>