<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Countrymodel extends CI_Model{

	public function __construct() {
        parent::__construct();
    }
	public function country_list($limit, $offset){
	
		$query=$this->db->select(['id','country'])->from('master_country')
		//->order_by("country", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function search($limit, $offset,$search){
	
		$query=$this->db->select(['id','country'])->from('master_country')->or_like('country',$search,'both')
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows(){
	
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','country'])->from('master_country')->get();
		return $query->num_rows();
	}
	public function insert_country($data_country){
	
		$this->db->insert('master_country', $data_country);
		$data_country['id'] = $this->db->insert_id();
    	return $data_country['id'];	
	}
	
	public function update_country($data_country,$list_id){
	
		$this->db->where('id',$list_id);
		$this->db->update('master_country', $data_country);
		return true;
	}
	
	public function view_country($list_id){
	
		$query=$this->db->select(['id','country'])->from('master_country')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_country($list_id){
	
		$this->db->where('id', $list_id);
		$this->db->delete('master_country');
	}
}
?>