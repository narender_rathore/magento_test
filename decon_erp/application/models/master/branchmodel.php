<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Branchmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	
	public function branch_list($limit, $offset){
	
		$query=$this->db->select(['master_branch.id','master_branch.branch','master_branch.location_id','master_location.location'])->from('master_branch')
		->join('master_location','master_location.id=master_branch.location_id','inner')
		->order_by("branch", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function location_list(){
	
		$query=$this->db->select(['id','location'])->from('master_location')
		->order_by("location", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['location']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['master_branch.id','master_branch.branch','master_branch.location_id','master_location.location'])->from('master_branch')
		->join('master_location','master_location.id=master_branch.location_id','inner')
		->get();
		return $query->num_rows();
	}
	public function insert_branch($data_branch){
		$this->db->insert('master_branch', $data_branch);
		$data_branch['id'] = $this->db->insert_id();
    	return $data_branch['id'];	
	}
	
	public function update_branch($data_branch,$list_id){
		$this->db->where('master_branch.id',$list_id);
		$this->db->update('master_branch', $data_branch);
		return true;
	}
	
	public function view_branch($list_id){
		$query=$this->db->select(['master_branch.id','master_branch.branch','master_branch.location_id','master_location.location'])->from('master_branch')
		->join('master_location','master_location.id=master_branch.location_id','inner')
		->where('master_branch.id',$list_id)->get();
		return $query->row();
	}
	public function delete_branch($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_branch');
	}
}
?>