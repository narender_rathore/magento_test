<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Locationmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	
	public function location_list($limit, $offset){
	
		$query=$this->db->select(['master_location.id','master_location.location','master_location.city_id','master_city.city'])->from('master_location')
		->join('master_city','master_city.id=master_location.city_id','inner')
		->order_by("location", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function city_list(){
	
		$query=$this->db->select(['id','city'])->from('master_city')
		->order_by("city", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['city']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['master_location.id','master_location.location','master_location.city_id','master_city.city'])->from('master_location')
		->join('master_city','master_city.id=master_location.city_id','inner')
		->get();
		return $query->num_rows();
	}
	public function insert_location($data_location){
		$this->db->insert('master_location', $data_location);
		$data_location['id'] = $this->db->insert_id();
    	return $data_location['id'];	
	}
	
	public function update_location($data_location,$list_id){
		$this->db->where('master_location.id',$list_id);
		$this->db->update('master_location', $data_location);
		return true;
	}
	
	public function view_location($list_id){
		$query=$this->db->select(['master_location.id','master_location.location','master_location.city_id','master_city.city'])->from('master_location')
		->join('master_city','master_city.id=master_location.city_id','inner')
		->where('master_location.id',$list_id)->get();
		return $query->row();
	}
	public function delete_location($list_id){
		$this->db->where('id', $list_id);
		$this->db->delete('master_location');
	}
}
?>