<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Itemmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function item_list($limit, $offset){
	
		$query=$this->db->select(['id','item_name'])->from('master_new_inventory')
		->order_by("item_name", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','item_name'])->from('master_new_inventory')->get();
		return $query->num_rows();
	}
	public function insert_item($data_item){
		$this->db->insert('master_new_inventory', $data_item);
		$data_item['id'] = $this->db->insert_id();
    	return $data_item['id'];	
	}
	
	public function update_item($data_item,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_new_inventory', $data_item);
		return true;
	}
	
	public function view_item($list_id){
		$query=$this->db->select('*')->from('master_new_inventory')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_item($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_new_inventory');
	}
}
?>