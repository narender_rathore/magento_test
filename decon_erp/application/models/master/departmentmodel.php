<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Departmentmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function department_list($limit, $offset){
	
		$query=$this->db->select(['id','department'])->from('master_department')
		->order_by("department", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','department'])->from('master_department')->get();
		return $query->num_rows();
	}
	public function insert_department($data_department){
		$this->db->insert('master_department', $data_department);
		$data_department['id'] = $this->db->insert_id();
    	return $data_department['id'];	
	}
	
	public function update_department($data_department,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_department', $data_department);
		return true;
	}
	
	public function view_department($list_id){
		$query=$this->db->select(['id','department'])->from('master_department')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_department($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_department');
	}
}
?>