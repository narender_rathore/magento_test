<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class new_usermodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function new_user_list($limit, $offset){
	
		$query=$this->db->select(['id','email','password'])->from('master_user_register')
		->order_by("id", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','email','password'])->from('master_user_register')->get();
		return $query->num_rows();
	}
	public function insert_new_user($data_new_user){
		$this->db->insert('master_user_register', $data_new_user);
		$data_new_user['id'] = $this->db->insert_id();
    	return $data_new_user['id'];	
	}
	
	public function update_new_user($data_new_user,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_user_register', $data_new_user);
		return true;
	}
	
	public function view_new_user($list_id){
		$query=$this->db->select(['id','email','password'])->from('master_user_register')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_new_user($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_user_register');
	}
}
?>