<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Citymodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	
	public function city_list($limit, $offset){
	
		$query=$this->db->select(['master_city.id','master_city.city','master_city.country_id','master_city.state_id','master_country.country','master_state.state'])->from('master_city')
		->join('master_country','master_country.id=master_city.country_id','inner')->join('master_state','master_state.id=master_city.state_id','inner')
		->order_by("city", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function country_list(){
	
		$query=$this->db->select(['id','country'])->from('master_country')
		->order_by("country", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['country']);
		}
		}

        return $return;
	}
	public function state_list(){
	
		$query=$this->db->select(['id','state'])->from('master_state')
		->order_by("state", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['state']);
		}
		}

        return $return;
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['master_city.id','master_city.city','master_city.country_id','master_city.state_id','master_country.country','master_state.state'])->from('master_city')
		->join('master_country','master_country.id=master_city.country_id','inner')->join('master_state','master_state.id=master_city.state_id','inner')
		->get();
		return $query->num_rows();
	}
	public function insert_city($data_city){
		$this->db->insert('master_city', $data_city);
		$data_city['id'] = $this->db->insert_id();
    	return $data_city['id'];	
	}
	
	public function update_city($data_city,$list_id){
		$this->db->where('master_city.id',$list_id);
		$this->db->update('master_city', $data_city);
		return true;
	}
	
	public function view_city($list_id){
		$query=$this->db->select(['master_city.id','master_city.city','master_city.country_id','master_city.state_id','master_country.country','master_state.state'])->from('master_city')
		->join('master_country','master_country.id=master_city.country_id','inner')->join('master_state','master_state.id=master_city.state_id','inner')
		->where('master_city.id',$list_id)->get();
		return $query->row();
	}
	public function delete_city($list_id){
	
		$this->db->where('id', $list_id);
		$this->db->delete('master_city');
	}
}
?>