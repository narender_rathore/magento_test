<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Work_packagemodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function work_package_list($limit, $offset){
	
		$query=$this->db->select(['id','package_name','description'])->from('master_work_package')
		->order_by("id", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','package_name','description'])->from('master_work_package')->get();
		return $query->num_rows();
	}
	public function insert_work_package($data_work_package){
		$this->db->insert('master_work_package', $data_work_package);
		$data_work_package['id'] = $this->db->insert_id();
    	return $data_work_package['id'];	
	}
	
	public function update_work_package($data_work_package,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_work_package', $data_work_package);
		return true;
	}
	
	public function view_work_package($list_id){
		$query=$this->db->select(['id','package_name','description'])->from('master_work_package')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_work_package($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_work_package');
	}
}
?>