<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class User_authmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function user_auth_list($limit, $offset){
	
		$query=$this->db->select(['user_auth.*','master_user_register.email as us_email','master_user_register.id as us_id','master_user_auth.*','hr_user_auth.*',
		'inventory_user_auth.*','invoice_user_auth.*','project_user_auth.*'])->from('user_auth')
		->join('master_user_auth','user_auth.id=master_user_auth.auth_id')->join('hr_user_auth','user_auth.id=hr_user_auth.auth_id')
		->join('invoice_user_auth','user_auth.id=invoice_user_auth.auth_id')->join('inventory_user_auth','user_auth.id=inventory_user_auth.auth_id')
		->join('project_user_auth','user_auth.id=project_user_auth.auth_id')->join('master_user_register','user_auth.user_email_id=master_user_register.id')
		->order_by("user_auth.id", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['user_auth.*','master_user_register.email as us_email','master_user_register.id','master_user_auth.*','hr_user_auth.*',
		'inventory_user_auth.*','invoice_user_auth.*','project_user_auth.*'])->from('user_auth')
		->join('master_user_auth','user_auth.id=master_user_auth.auth_id')->join('hr_user_auth','user_auth.id=hr_user_auth.auth_id')
		->join('invoice_user_auth','user_auth.id=invoice_user_auth.auth_id')->join('inventory_user_auth','user_auth.id=inventory_user_auth.auth_id')
		->join('project_user_auth','user_auth.id=project_user_auth.auth_id')->join('master_user_register','user_auth.user_email_id=master_user_register.id')
		->get();
		return $query->num_rows();
	}
	
	public function user_list(){
	
		$query=$this->db->select(['id','email'])->from('master_user_register')
		->order_by("email", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = ($row['email']);
		}
		}

        return $return;
	}
	public function insert_user_auth($data_user_auth,$data_master_user_auth,$data_hr_user_auth,$data_invoice_user_auth,$data_project_user_auth,$data_inventory_user_auth){
		$this->db->insert('user_auth', $data_user_auth);
		$data_user_auth['id'] = $data_master_user_auth['auth_id']	=	$data_hr_user_auth['auth_id']	=	$data_invoice_user_auth['auth_id']	=	
		$data_project_user_auth['auth_id']  =	$data_inventory_user_auth['auth_id']	= 	$this->db->insert_id();
		//echo $data_master_user_auth['auth_id'];die;
		$this->db->insert('master_user_auth'	, $data_master_user_auth);
		$this->db->insert('hr_user_auth'		, $data_hr_user_auth);
		$this->db->insert('invoice_user_auth'	, $data_invoice_user_auth);
		$this->db->insert('project_user_auth'	, $data_project_user_auth);
		$this->db->insert('inventory_user_auth'	, $data_inventory_user_auth);
    	return $data_user_auth['id'];	
	}
	
	public function update_user_auth($data_master_user_auth,$data_hr_user_auth,$data_invoice_user_auth,$data_project_user_auth,$data_inventory_user_auth,$list_id){
		$this->db->where('auth_id'				, $list_id);
		$this->db->update('master_user_auth'	, $data_master_user_auth);
		$this->db->update('hr_user_auth'		, $data_hr_user_auth);
		$this->db->update('invoice_user_auth'	, $data_invoice_user_auth);
		$this->db->update('project_user_auth'	, $data_project_user_auth);
		$this->db->update('inventory_user_auth'	, $data_inventory_user_auth);
		return true;
	}
	
	public function view_user_auth($list_id){
		//$query=$this->db->select('*')->from('user_auth')->where('id',$list_id)->get();
		$query=$this->db->select(['user_auth.*','master_user_register.email as us_email','master_user_register.id as us_id','master_user_auth.*','hr_user_auth.*',
		'inventory_user_auth.*','invoice_user_auth.*','project_user_auth.*'])->from('user_auth')
		->join('master_user_auth','user_auth.id=master_user_auth.auth_id')->join('hr_user_auth','user_auth.id=hr_user_auth.auth_id')
		->join('invoice_user_auth','user_auth.id=invoice_user_auth.auth_id')->join('inventory_user_auth','user_auth.id=inventory_user_auth.auth_id')
		->join('project_user_auth','user_auth.id=project_user_auth.auth_id')->join('master_user_register','user_auth.user_email_id=master_user_register.id')->
		where('user_auth.id',$list_id)->get();
		return $query->row();
	}
	public function delete_user_auth($list_id){
		$this->db->trans_begin();
	
		$this->db->where('id', $list_id);
		$db=$this->db->delete('user_auth');
		if($db==true){
			$this->db->where('auth_id', $list_id);
			$this->db->delete(['master_user_auth','hr_user_auth','project_user_auth','invoice_user_auth','inventory_user_auth']);
		
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();
			}
		}
	}
}
?>