<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Vendormodel extends CI_Model{
	function __construct() {
        parent::__construct();
    }
	public function vendor_list($limit, $offset){
	
		$query=$this->db->select(['ven.id as venid','ven.vendor_code','ven.vendor_name','ven.address','ven.concerned_person_name',
		'ven.concerned_person_designation','ven.concerned_person_contact','ven.concerned_person_email','ven_cnt.contact_no','ven_cnt.email'])
		->from('master_vendor as ven')->join('master_vendor_contact_detail as ven_cnt ','ven.id=ven_cnt.vendor_id','inner')
		->order_by("ven.id", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows(){
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['ven.id as venid','ven.vendor_code','ven.vendor_name','ven.address','ven.concerned_person_name',
		'ven.concerned_person_designation','ven.concerned_person_contact','ven.concerned_person_email','ven_cnt.contact_no','ven_cnt.email'])
		->from('master_vendor as ven')->join('master_vendor_contact_detail as ven_cnt ','ven.id=ven_cnt.vendor_id','inner')
		->get();
		return $query->num_rows();
	}
	public function designation_list(){
	
		$query=$this->db->select(['id','designation'])->from('master_designation')
		->order_by("designation", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['designation']);
		}
		}

        return $return;
		//return $query->result_array();
	}
	public function insert_vendor($data_vendor,$data_vendor_contact){
	
		$this->db->insert('master_vendor', $data_vendor);
		$data_vendor_contact['vendor_id'] = $this->db->insert_id();
		$this->db->insert('master_vendor_contact_detail', $data_vendor_contact);
		$id = $this->db->insert_id();
    	return $data_vendor_contact['vendor_id'];	
		//return (isset($data_vendor['id'])) ? $data_vendor['id'] : FALSE;
	}
	
	public function update_vendor($data_vendor,$data_vendor_contact,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_vendor', $data_vendor);
		$id=$this->fetch_by_vendor_id($list_id);
		$this->db->where('id',$id);
		$this->db->update('master_vendor_contact_detail', $data_vendor_contact);
    	return true;
	}
	
	public function fetch_by_vendor_id($list_id){
		$query=$this->db->select('id')->from('master_vendor_contact_detail')->where('vendor_id',$list_id)->get();
		return $query->result();
	}
	
	public function view_vendor($list_id){
	
		$query=$this->db->select(['ven.*','ven_cnt.*'])
		->from('master_vendor as ven')->join('master_vendor_contact_detail as ven_cnt ','ven.id=ven_cnt.vendor_id','inner')->where('ven.id',$list_id)
		->get();
		return $query->row();
	}
	public function delete_vendor($list_id){
		
		$this->db->trans_begin();
	
		$this->db->where('id', $list_id);
		$db=$this->db->delete('master_vendor');
		if($db==true){
			$this->db->where('vendor_id', $list_id);
			$this->db->delete(['master_vendor_contact_detail']);
		
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}
			else{
				$this->db->trans_commit();
			}
		}
	}
		
	
}
?>