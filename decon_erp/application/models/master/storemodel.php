<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Storemodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function store_list($limit, $offset){
	
		$query=$this->db->select(['id','store_code','lead_person','location'])->from('master_store')
		->order_by("id", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	public function location_list(){
	
		$query=$this->db->select(['id','location'])->from('master_location')
		->order_by("id", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['location']);
		}
		}

        return $return;
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['id','store_code','lead_person','location'])->from('master_store')->get();
		return $query->num_rows();
	}
	public function insert_store($data_store){
		$this->db->insert('master_store', $data_store);
		$data_store['id'] = $this->db->insert_id();
    	return $data_store['id'];	
	}
	
	public function update_store($data_store,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('master_store', $data_store);
		return true;
	}
	
	public function view_store($list_id){
		$query=$this->db->select(['id','store_code','lead_person','location'])->from('master_store')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_store($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('master_store');
	}
}
?>