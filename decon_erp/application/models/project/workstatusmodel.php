<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Workstatusmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function workstatus_list($limit, $offset){}
	
	public function num_rows(){}
	public function insert_workstatus($data_workstatus){}
	
	public function update_workstatus($data_workstatus,$list_id){}
	
	public function view_workstatus($list_id){}
	public function delete_workstatus($list_id){}
}
?>