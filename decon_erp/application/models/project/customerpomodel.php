<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Customerpomodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function customerpo_list($limit, $offset){
	
		$query=$this->db->select('*')->from('projects_customer_po')
		//->order_by("customerpo", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select('*')->from('projects_customer_po')->get();
		return $query->num_rows();
	}
	public function insert_customerpo($data_customerpo){
		$this->db->insert('projects_customer_po', $data_customerpo);
		$data_customerpo['id'] = $this->db->insert_id();
    	return $data_customerpo['id'];	
	}
	
	public function update_customerpo($data_customerpo,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('projects_customer_po', $data_customerpo);
		return true;
	}
	
	public function view_customerpo($list_id){
		$query=$this->db->select(['id','customerpo'])->from('projects_customer_po')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_customerpo($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('projects_customer_po');
	}
}
?>