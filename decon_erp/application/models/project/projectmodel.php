<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php
class Projectmodel extends CI_Model{
	public function __construct() {
        parent::__construct();
    }
	public function project_list($limit, $offset){
	
		$query=$this->db->select('*')->from('projects_project_master')
		//->order_by("project", "asc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows()
	{
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select('*')->from('projects_project_master')->get();
		return $query->num_rows();
	}
	public function insert_project($data_project){
		$this->db->insert('projects_project_master', $data_project);
		$data_project['id'] = $this->db->insert_id();
    	return $data_project['id'];	
	}
	
	public function update_project($data_project,$list_id){
		$this->db->where('id',$list_id);
		$this->db->update('projects_project_master', $data_project);
		return true;
	}
	
	public function view_project($list_id){
		$query=$this->db->select(['id','project'])->from('projects_project_master')->where('id',$list_id)->get();
		return $query->row();
	}
	public function delete_project($list_id)
	{
		$this->db->where('id', $list_id);
		$this->db->delete('projects_project_master');
	}
}
?>