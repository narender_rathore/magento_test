<?php defined('BASEPATH') or exit('NO Direct Access is Allowed');?>

<?php
class Employeemodel extends CI_Model{

	public function __construct() {
	
        parent::__construct();
    }
	
	public function employee_list($limit, $offset){
	
		$query=$this->db->select(['hr_employee.*','hr_employee_address.*','hr_employee_qualification.*'])->from('hr_employee')
		->join('hr_employee_address','hr_employee.id=hr_employee_address.employee_id')->join('hr_employee_qualification','hr_employee.id=hr_employee_qualification.employee_id')
		->order_by("hr_employee.id", "desc")
    	->limit($limit, $offset)
		->get();
		return $query->result();
	}
	
	public function num_rows(){
	
		$user_id = $this->session->userdata('user_id');
		$query=$this->db->select(['hr_employee.*','hr_employee_address.*','hr_employee_qualification.*'])->from('hr_employee')
		->join('hr_employee_address','hr_employee.id=hr_employee_address.employee_id')->join('hr_employee_qualification','hr_employee.id=hr_employee_qualification.employee_id')
		->order_by("hr_employee.id", "desc")->get();
		return $query->num_rows();
	}
	
	public function country_list(){
	
		$query=$this->db->select(['id','country'])->from('master_country')
		->order_by("country", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['country']);
		}
		}

        return $return;
	}
	
	public function designation_list(){
	
		$query=$this->db->select(['id','designation'])->from('master_designation')
		->order_by("designation", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['designation']);
		}
		}

        return $return;
	}
	
	public function branch_list(){
	
		$query=$this->db->select(['id','branch'])->from('master_branch')
		->order_by("branch", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['branch']);
		}
		}

        return $return;
	}
	
	public function location_list(){
	
		$query=$this->db->select(['id','location'])->from('master_location')
		->order_by("location", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['location']);
		}
		}

        return $return;
	}
	
	public function state_list(){
	
		$query=$this->db->select(['id','state'])->from('master_state')
		->order_by("state", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['state']);
		}
		}

        return $return;
	}
	
	public function city_list(){
	
		$query=$this->db->select(['id','city'])->from('master_city')
		->order_by("city", "asc")
		->get();
		$return = array();
		if($query->num_rows() > 0) {
		foreach($query->result_array() as $row) {
		$return[$row['id']] = strtoupper($row['city']);
		}
		}

        return $return;
	}
	              
	public function srange ($s) {
	
	  preg_match_all("/([0-9]{1,2})-?([0-9]{0,2}) ?,?;?/", $s, $a);
	  $n = array ();
	  foreach ($a[1] as $k => $v) {
		$n  = array_merge ($n, range ($v, (empty($a[2][$k])?$v:$a[2][$k])));
	  }
	  return ($n);
	}
	
	//$s = '1-4 6-7 9-10';
	//print_r(srange($s));

	public function insert_employee($data_employee,$data_employee_address,$data_employee_qualification){
	
		$this->db->insert('hr_employee', $data_employee);
		$data_employee_address['employee_id'] = $data_employee_qualification['employee_id']= $this->db->insert_id();		
		$this->db->insert('hr_employee_address', $data_employee_address);
		$this->db->insert('hr_employee_qualification', $data_employee_qualification);
    	return $data_employee_address['employee_id'];	
	}
	
	public function update_employee($data_employee,$data_employee_address,$data_employee_qualification,$list_id){
	
		$this->db->where('id',$list_id);
		$this->db->update('hr_employee', $data_employee);
		
		$this->db->where('employee_id',$list_id);
		$this->db->update('hr_employee_address', $data_employee_address);
		
		$this->db->where('employee_id',$list_id);
		$this->db->update('hr_employee_qualification', $data_employee_qualification);
		return true;
	}
	
	public function view_employee($list_id){
	
		$query=$this->db->select(['hr_emp.*','emp_add.*','emp_qual.*'])->from('hr_employee as hr_emp')
		->join('hr_employee_address as emp_add','hr_emp.id=emp_add.employee_id','inner')
		->join('hr_employee_qualification as emp_qual','hr_emp.id=emp_qual.employee_id','inner')
		->where('hr_emp.id',$list_id)->get();
		return $query->row();
	}
	
	public function delete_employee($list_id){
	
		$this->db->where('id', $list_id);
		$this->db->delete('hr_employee');
		
		$this->db->where('employee_id', $list_id);
		$this->db->delete('hr_employee_address');
		
		$this->db->where('employee_id', $list_id);
		$this->db->delete('hr_employee_qualification');
	}
}

?>