<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit Project</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/project','Project')?></li>
        <li class="active">Edit Project</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("project/edit/{$list->id}",['name'=>'project_form_edit','id'=>'project_form_edit']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('project/create','<i class="fa fa-fw fa-plus-square"></i> Add Project',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('project','<i class="fa fa-fw fa-arrows"></i> Project List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Country','project',['for'=>'project']);?><?php echo form_error('project'); ?>
                    				<?php echo form_input(['name'=>'project','class'=>'form-control','id'=>'project','placeholder'=>'Enter Country  here',
									'value'=>set_value('project',strtoupper($list->project))]);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>