<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Project</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/project','Project')?></li>
        <li class="active">Add Project</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('project/create',['name'=>'project_form_add','id'=>'project_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('project','<i class="fa fa-fw fa-arrows"></i> Project List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Entity Name','entity_name',['for'=>'entity_name']);?><?php echo form_error('entity_name'); ?>
                    				<?php $options=array(''=>'Select Entity','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('entity_name',$options,set_value('entity_name'),['class'=>'form-control']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Project Code','project_code',['for'=>'project_code']);?><?php echo form_error('project_code'); ?>
                                    <?php $options=array(''=>'Select Project','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('project_code',$options,set_value('project_code'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Customer Code','customer_code',['for'=>'customer_code']);?><?php echo form_error('customer_code'); ?>
                                    <?php $options=array(''=>'Select Customer Code','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('customer_code',$options,set_value('customer_code'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Zonal Head','zonal_head',['for'=>'zonal_head']);?><?php echo form_error('zonal_head'); ?>
                                    <?php $options=array(''=>'Select Zonal Head','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('zonal_head',$options,set_value('zonal_head'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Zonal Head Contact','zonal_contact',['for'=>'zonal_contact']);?><?php echo form_error('zonal_contact'); ?>
                    				<?php echo form_input(['name'=>'zonal_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Zonal head Contact Here',
									'value'=>set_value('zonal_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Zonal Head Email','zonal_email',['for'=>'zonal_email']);?><?php echo form_error('zonal_email'); ?>
                    				<?php echo form_input(['name'=>'zonal_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Zonal Head Email Here',
									'value'=>set_value('zonal_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Team Leader','team_leader',['for'=>'team_leader']);?><?php echo form_error('team_leader'); ?>
                                    <?php $options=array(''=>'Select Team Leader','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('team_leader',$options,set_value('team_leader'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Team Leader Contact','leader_contact',['for'=>'leader_contact']);?><?php echo form_error('leader_contact'); ?>
                    				<?php echo form_input(['name'=>'leader_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Team Leader Contact Here',
									'value'=>set_value('zonal_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Team Leader Email','leader_email',['for'=>'leader_email']);?><?php echo form_error('leader_email'); ?>
                    				<?php echo form_input(['name'=>'leader_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Team Leader Email Here',
									'value'=>set_value('leader_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Project Admin','project_admin',['for'=>'project_admin']);?><?php echo form_error('project_admin'); ?>
                                    <?php $options=array(''=>'Select Project Admin','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('project_admin',$options,set_value('project_admin'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Project Admin Contact','project_admin_contact',['for'=>'project_admin_contact']);?>
									<?php echo form_error('project_admin_contact'); ?>
                    				<?php echo form_input(['name'=>'project_admin_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Project Admin Contact Here',
									'value'=>set_value('project_admin_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Project Admin Email','project_admin_email',['for'=>'project_admin_email']);?>
									<?php echo form_error('project_admin_email'); ?>
                    				<?php echo form_input(['name'=>'project_admin_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Project Admin Email Here',
									'value'=>set_value('project_admin_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Account Admin','account_admin',['for'=>'account_admin']);?><?php echo form_error('account_admin'); ?>
                                    <?php $options=array(''=>'Select Project Admin','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('account_admin',$options,set_value('account_admin'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Account Admin Contact','account_admin_contact',['for'=>'account_admin_contact']);?>
									<?php echo form_error('account_admin_contact'); ?>
                    				<?php echo form_input(['name'=>'account_admin_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Project Admin Contact Here',
									'value'=>set_value('account_admin_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Account Admin Email','account_admin_email',['for'=>'account_admin_email']);?>
									<?php echo form_error('account_admin_email'); ?>
                    				<?php echo form_input(['name'=>'account_admin_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Project Admin Email Here',
									'value'=>set_value('account_admin_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Circle Head','circle_head',['for'=>'circle_head']);?><?php echo form_error('circle_head'); ?>
                                    <?php $options=array(''=>'Select Circle Head','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('circle_head',$options,set_value('circle_head'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Circle Head Contact','circle_head_contact',['for'=>'circle_head_contact']);?>
									<?php echo form_error('circle_head_contact'); ?>
                    				<?php echo form_input(['name'=>'circle_head_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Circle Head Contact Here',
									'value'=>set_value('circle_head_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Circle Head Email','account_admin_email',['for'=>'account_admin_email']);?>
									<?php echo form_error('circle_head_email'); ?>
                    				<?php echo form_input(['name'=>'circle_head_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Circle Head Email Here',
									'value'=>set_value('circle_head_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('NPO Manager','npo_manager',['for'=>'npo_manager']);?><?php echo form_error('npo_manager'); ?>
                                    <?php $options=array(''=>'Select NPO MANAGER','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('npo_manager',$options,set_value('npo_manager'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('NPO Manager Contact','npo_manager_contact',['for'=>'circle_head_contact']);?>
									<?php echo form_error('npo_manager_contact'); ?>
                    				<?php echo form_input(['name'=>'npo_manager_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter NPO Manager Contact Here',
									'value'=>set_value('npo_manager_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('NPO Manager Email','npo_manager_email',['for'=>'npo_manager_email']);?>
									<?php echo form_error('npo_manager_email'); ?>
                    				<?php echo form_input(['name'=>'npo_manager_email','class'=>'form-control','id'=>'','placeholder'=>'Enter NPO Manager Email Here',
									'value'=>set_value('npo_manager_email')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('RE Leader','re_leader',['for'=>'re_leader']);?><?php echo form_error('re_leader'); ?>
                                    <?php $options=array(''=>'Select RE Leader','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('re_leader',$options,set_value('re_leader'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('RE Leader Contact','re_leader_contact',['for'=>'re_leader_contact']);?>
									<?php echo form_error('re_leader_contact'); ?>
                    				<?php echo form_input(['name'=>'re_leader_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter RE Lead Contact Here',
									'value'=>set_value('re_leader_contact')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('RE Leader Email','re_lead_email',['for'=>'re_lead_email']);?>
									<?php echo form_error('re_leader_email'); ?>
                    				<?php echo form_input(['name'=>'re_leader_email','class'=>'form-control','id'=>'','placeholder'=>'Enter RE Leader Email Here',
									'value'=>set_value('re_leader_email')]);?>
                                </div>
                                <div class="well well-sm">Projects Work Package</div>
                               	<table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'bb_emf_survey','id'=>'','value'=>set_value('bb_emf_survey','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('BB EMF Survey','bb_emf_survey',['for'=>'bb_emf_survey','class'=>'control-label']);?>
										<?php echo form_error('bb_emf_survey'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'cluster_drive','id'=>'','value'=>set_value('cluster_drive','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Cluster Drive','cluster_drive',['for'=>'cluster_drive','class'=>'control-label']);?>
										<?php echo form_error('cluster_drive'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'cm_emf_survey','id'=>'','value'=>set_value('cm_emf_survey','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('CM EMF Survey','cm_emf_survey',['for'=>'cm_emf_survey','class'=>'control-label']);?>
										<?php echo form_error('cm_emf_survey'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'los_survey','id'=>'los_survey','value'=>set_value('los_survey','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('LOS Survey','los_survey',['for'=>'los_survey','class'=>'control-label']);?>
										<?php echo form_error('los_survey'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'man_month','id'=>'','value'=>set_value('man_month','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Man Month','man_month',['for'=>'man_month','class'=>'control-label']);?>
										<?php echo form_error('man_month'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'mro_testing_ms1','id'=>'','value'=>set_value('mro_testing_ms1','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('MRO Testing Ms1','mro_testing_ms1',['for'=>'mro_testing_ms1','class'=>'control-label']);?>
										<?php echo form_error('mro_testing_ms1'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'mro_testing_ms2','id'=>'','value'=>set_value('mro_testing_ms2','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('MRO Testing Ms2','mro_testing_ms2',['for'=>'mro_testing_ms2','class'=>'control-label']);?>
										<?php echo form_error('mro_testing_ms2'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'nb_emf_survey','id'=>'','value'=>set_value('nb_emf_survey','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('NB EMF Survey','nb_emf_survey',['for'=>'nb_emf_survey','class'=>'control-label']);?>
										<?php echo form_error('nb_emf_survey'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'rf_survey','id'=>'','value'=>set_value('rf_survey','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('RF Survey','rf_survey',['for'=>'rf_survey','class'=>'control-label']);?><?php echo form_error('rf_survey'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'scft_drive','id'=>'','value'=>set_value('scft_drive','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('SCFT Drive','scft_drive',['for'=>'scft_drive','class'=>'control-label']);?><?php echo form_error('scft_drive'); ?>
                                    </td>
                                </tr>
                                
                             </table>
                                        
        </div>
    </div>
</div>
<hr />
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>