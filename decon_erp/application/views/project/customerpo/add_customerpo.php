<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add CustomerPO</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('project/customerpo','Project')?></li>
        <li class="active">Add CustomerPO</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('customerpo/create',['name'=>'project_form_add','id'=>'project_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('customerpo','<i class="fa fa-fw fa-arrows"></i> CustomerPO List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        	<div class="well well-sm"><strong>Customer Contact Details</strong></div>
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                                <div><a href="#" id="addNew">Add New Item</a></div>            
                                    <table id="dataTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <th>Project Code</th>
                                            <th>Internal Po NO</th>
                                            <th>Customer PO NO</th>
                                            <th>Date</th>
                                            <th>Site Id</th>
                                            <th>Work Package</th>
                                            <th>Po Value</th>
                                            <th>Payment Terms</th>
                                            <th>Remarks</th>
                                            <th></th>
                                        </tr>
                                        <tr style="border:1px solid black">
                                            <td>
												<?php $options=array(''=>'Select Project Code','1'=>'Purchase','2'=>'Rent'); ?> 
                                                <?php echo form_dropdown('designation',$options,set_value('designation'),['class'=>'form-control','style'=>'width:200px;']);?>
                                            </td>
                                            <td>
                                            	<?php echo form_input(['name'=>'name','class'=>'form-control','id'=>'name','style'=>'width:200px;',
												'placeholder'=>'Enter Internal PO No','value'=>set_value('name')]);?>
                                            </td>
                                            <td>
                                                <?php echo form_input(['name'=>'name','class'=>'form-control','id'=>'name','style'=>'width:200px;',
												'placeholder'=>'Enter Customer PO No','value'=>set_value('name')]);?>
                                            </td>
                                            <td>
                                                <?php echo form_input(['name'=>'Date','class'=>'form-control','id'=>'mobile','style'=>'width:200px;',
												'placeholder'=>'Select Date Here','value'=>set_value('mobile')]);?>
                                            </td>
                                            <td><?php echo form_input(['name'=>'siteid','class'=>'form-control','id'=>'email','style'=>'width:200px;',
												'placeholder'=>'Enter Site Id','value'=>set_value('email')]);?>
                                            </td>
                                            <td>
                                            	<?php $options=array(''=>'Select WorkPackage','1'=>'Purchase','2'=>'Rent'); ?> 
                                            	<?php echo form_dropdown('workpackage',$options,set_value('designation'),['class'=>'form-control','style'=>'width:200px;']);?>
                                            </td>
                                            <td>
                                            	<?php echo form_input(['name'=>'po_value','class'=>'form-control','id'=>'email','style'=>'width:200px;',
												'placeholder'=>'Enter PO Value Here','value'=>set_value('email')]);?>
                                            </td>
                                            <td>
                                            	<?php echo form_input(['name'=>'email','class'=>'form-control','id'=>'email','style'=>'width:200px;',
												'placeholder'=>'Enter Payment Terms here','value'=>set_value('email')]);?>
                                            </td>
                                            <td>
                                            	<?php echo form_input(['name'=>'remarks','class'=>'form-control','id'=>'email','style'=>'width:200px;',
												'placeholder'=>'Give Remarks Here','value'=>set_value('email')]);?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo form_error('project_code'); ?></td>
                                            <td><?php echo form_error('internal po no'); ?></td>
                                            <td><?php echo form_error('customer po no'); ?></td>
                                            <td><?php echo form_error('date'); ?></td>
                                            <td><?php echo form_error('siteid'); ?></td>
                                            <td><?php echo form_error('workpackage'); ?></td>
                                            <td><?php echo form_error('po_value'); ?></td>
                                            <td><?php echo form_error('email'); ?></td>
                                            <td><?php echo form_error('remarks'); ?></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>  	
                        </div>
                    </div>
                </div>
            	<hr />
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>