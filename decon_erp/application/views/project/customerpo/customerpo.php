<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Customer PO</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Customer PO</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
     
          <?php echo form_button('','<i class="fa fa-fw fa-trash"></i> Delete',['type'=>'submit','class'=>'btn btn-default pull-right',
		  'style'=>'margin-left:5px;background-color: #ffffff;','value'=>'Delete_Project'],$js); ?>
          <?php echo anchor('customerpo/create','<i class="fa fa-fw fa-plus-square"></i> Add CustomerPO',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          <?php echo form_open();?>
          <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Project Code</th>
              <th>Internal PO No</th>
              <th>Customer PO No</th>
              <th>Site Code</th>
              <th>Work Packages</th>
              <th>PO Value</th>
              <th>Date Of PO</th>
              <th>Payment Terms</th>
              <th>Remarks</th>
              <th>Action</th>
              <th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(4);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  strtoupper($lists->project);?></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>
					<?php echo anchor("customerpo/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Customer']); ?><?php nbs(3);?>
                    <?php echo anchor("customerpo/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Customer']); ?>
              </td>
              <td><?php echo form_checkbox(['name'=>'customerpo_to_delete[]','type'=>'checkbox','class'=>'checkboxes','value'=>$lists->id]);?></td>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Project Code</th>
              <th>Internal PO No</th>
              <th>Customer PO No</th>
              <th>Site Code</th>
              <th>Work Packages</th>
              <th>PO Value</th>
              <th>Date Of PO</th>
              <th>Payment Terms</th>
              <th>Remarks</th>

              <th>Action</th>
              <th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox','disabled'=>'disabled']);?></th>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php /*?><?php $count++; echo  $this->pagination->create_links(); ?><?php */?>
        <?php $count++; echo  $link; ?>
      
        </div>
    </div>
  </div>
</div>
</section>
</div>
