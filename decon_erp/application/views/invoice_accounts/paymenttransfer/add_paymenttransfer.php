<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Payment Transfer</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('invoice_accounts/Paymenttransfer','Payment Transfer')?></li>
        <li class="active">Add Payment Transfer</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('paymenttransfer/create',['name'=>'paymenttransfer_form_add','id'=>'customer_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('paymenttransfer','<i class="fa fa-fw fa-arrows"></i> Payment Transfer List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Transaction Done By','transaction_done_by',['for'=>'transaction_done_by']);?>
									<?php echo form_error('transaction_done_by'); ?>
                    				<?php echo form_input(['name'=>'transaction_done_by','class'=>'form-control','id'=>'','placeholder'=>'Enter Transaaction done by here',
									'value'=>set_value('transaction_done_by')]);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Paid To','paid_to',['for'=>'paid_to']);?><?php echo form_error('paid_to'); ?>
                                    <?php $options=array(''=>'Select Paid TO','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('paid_to',$options,set_value('paid_to'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('Paidto_name','Paidto_name',['for'=>'Paidto_name']);?><?php echo form_error('Paidto_name'); ?>
									 <?php echo form_input(['name'=>'Paidto_name','class'=>'form-control','id'=>'','placeholder'=>'Paid To Name Here',
									 'value'=>set_value('Paidto_name')]);?>
                                </div>
                                <div class="form-group">
                                     <?php echo form_label('code','code',['for'=>'code']);?><?php echo form_error('code'); ?>
									 <?php echo form_input(['name'=>'code','class'=>'form-control','id'=>'','placeholder'=>'Enter Code Here',
									 'value'=>set_value('code')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Mode of Payment','mode_payment',['for'=>'mode_payment']);?><?php echo form_error('mode_payment'); ?>
                                    <?php $options=array(''=>'Select Mode Of Payment','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('mode_payment',$options,set_value('mode_payment'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Details','details',['for'=>'details']);?><?php echo form_error('details'); ?>
                                    <?php echo form_textarea(['name'=>'details','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Details Here','value'=>set_value('details')]);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('Date OF Payment','payment_date',['for'=>'payment_date']);?><?php echo form_error('payment_date'); ?>
									 <?php echo form_input(['name'=>'payment_date','class'=>'form-control','id'=>'','placeholder'=>'Enter Payment Date Here',
									 'value'=>set_value('payment_date')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Payment Against','payment_against',['for'=>'payment_against']);?><?php echo form_error('payment_against'); ?>
                                    <?php $options=array(''=>'Select Payment Against','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('payment_against',$options,set_value('payment_against'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('PO NO','po_no',['for'=>'po_no']);?><?php echo form_error('po_no'); ?>
									 <?php echo form_input(['name'=>'po_no','class'=>'form-control','id'=>'','placeholder'=>'Enter PO NO Here',
									 'value'=>set_value('po_no')]);?>
                                </div>
                                <div class="form-group">
                                     <?php echo form_label('Amount','amount',['for'=>'amount']);?><?php echo form_error('amount'); ?>
									 <?php echo form_input(['name'=>'amount','class'=>'form-control','id'=>'','placeholder'=>'Enter Amount Here',
									 'value'=>set_value('amount')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Remarks Here','value'=>set_value('remarks')]);?>
                              	</div>                
                        </div>
                    </div>
                </div>
                <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>

