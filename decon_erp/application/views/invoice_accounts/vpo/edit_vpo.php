 
<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit Payment Transfer</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('vpo','Payment Transfer')?></li>
        <li class="active">Edit Payment Transfer</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("vpo/edit/{$list->id}",['name'=>'vpo_form_edit','id'=>'vpo_form_edit']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor('vpo/create','<i class="fa fa-fw fa-plus-square"></i> Add Payment Transfer',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('vpo','<i class="fa fa-fw fa-arrows"></i> Payemnt Transfer List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>					    				
                </div><!-- /.box-header -->
				<div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('PO No','po_no',['for'=>'po_no']);?>
									<?php echo form_error('po_no'); ?>
                    				<?php echo form_input(['name'=>'po_no','class'=>'form-control','id'=>'','placeholder'=>'Enter PO NO here',
									'value'=>set_value('po_no')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('PO Date','po_date',['for'=>'po_date']);?>
									<?php echo form_error('po_date'); ?>
                    				<?php echo form_input(['name'=>'po_date','class'=>'form-control','id'=>'','placeholder'=>'Enter PO Date here',
									'value'=>set_value('po_date')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('PO Expiry Date','po_expiry_date',['for'=>'po_expiry_date']);?>
									<?php echo form_error('po_expiry_date'); ?>
                    				<?php echo form_input(['name'=>'po_expiry_date','class'=>'form-control','id'=>'','placeholder'=>'Enter PO Expiry Date here',
									'value'=>set_value('po_expiry_date')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Project Code','project_code',['for'=>'project_code']);?>
									<?php echo form_error('project_code'); ?>
                                    <?php $options=array(''=>'Select Project Code','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('project_code',$options,set_value('project_code'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('PO Type','po_type',['for'=>'po_type']);?>
									<?php echo form_error('po_type'); ?>
                    				<?php echo form_input(['name'=>'po_type','class'=>'form-control','id'=>'','placeholder'=>'Enter PO Type here',
									'value'=>set_value('po_type')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('PO Particulars','po_particular',['for'=>'po_particular']);?>
									<?php echo form_error('po_particular'); ?>
                    				<?php echo form_input(['name'=>'po_particular','class'=>'form-control','id'=>'','placeholder'=>'Enter PO Particular here',
									'value'=>set_value('po_particular')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Vendor Code','vendor_code',['for'=>'vendor_code']);?>
									<?php echo form_error('vendor_code'); ?>
                                    <?php $options=array(''=>'Select Vendor Code','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('vendor_code',$options,set_value('vendor_code'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Vendor Details','vendor_detail',['for'=>'vendor_detail']);?><?php echo form_error('vendor_detail'); ?>
                                    <?php echo form_textarea(['name'=>'vendor_detail','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Vendor Details Here','value'=>set_value('vendor_detail')]);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('PO Value','po_value',['for'=>'po_value']);?><?php echo form_error('po_value'); ?>
									 <?php echo form_input(['name'=>'po_value','class'=>'form-control','id'=>'','placeholder'=>'PO Value Here',
									 'value'=>set_value('po_value')]);?>
                                </div>
                                <div class="form-group">
                                     <?php echo form_label('Other Taxes','other_taxes',['for'=>'other_taxes']);?><?php echo form_error('other_taxes'); ?>
									 <?php echo form_input(['name'=>'other_taxes','class'=>'form-control','id'=>'','placeholder'=>'Enter Other Taxes Here',
									 'value'=>set_value('other_taxes')]);?>
                                </div>
                                <div class="form-group">
                                     <?php echo form_label('Total PO Value','total_po',['for'=>'total_po']);?><?php echo form_error('total_po'); ?>
									 <?php echo form_input(['name'=>'total_po','class'=>'form-control','id'=>'','placeholder'=>'Enter Total PO Here',
									 'value'=>set_value('total_po')]);?>
                                </div>
                                <div class="form-group">
                                     <?php echo form_label('Payment Terms','payment_terms',['for'=>'payment_terms']);?><?php echo form_error('payment_terms'); ?>
									 <?php echo form_input(['name'=>'payment_terms','class'=>'form-control','id'=>'','placeholder'=>'Enter Payment Terms Here',
									 'value'=>set_value('payment_terms')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Remarks Here','value'=>set_value('remarks')]);?>
                              	</div>  
                                 <div class="well well-sm"><strong>No OF Installment</strong></div>
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                                <div><a href="#" id="addNew">Add New Item</a></div>            
                                    <table id="dataTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <th>Date</th>
                                            <th>Percentage</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                        <tr style="border:1px solid black">
                                            <td>
												<?php echo form_input(['name'=>'date','class'=>'form-control','id'=>'','style'=>'width:200px;',
                                                'placeholder'=>'Enter Date here','value'=>set_value('date')]);?>
                                            </td>
                                            <td>
												<?php echo form_input(['name'=>'percentage','class'=>'form-control','id'=>'','style'=>'width:200px;',
                                                'placeholder'=>'Enter Percentage here','value'=>set_value('percentage')]);?>
                                            </td>
                                            <td>
												<?php echo form_input(['name'=>'amount','class'=>'form-control','id'=>'','style'=>'width:200px;',
                                                'placeholder'=>'Enter Amount here','value'=>set_value('amount')]);?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo form_error('date'); ?></td>
                                            <td><?php echo form_error('percentage'); ?></td>
                                            <td><?php echo form_error('amount'); ?></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>              
                            </div>
                        </div>
                    </div>
                
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
