<?php /*?><?php print_r($list);?><?php */?>
<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View Payment Transfer</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('paymenttransfer','Payment Transfer')?></li>
        <li class="active">View Payment Transfer</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
				  <?php echo anchor('customer','<i class="fa fa-fw fa-arrows"></i> Payment Transfer List',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor('paymenttransfer/create','<i class="fa fa-fw fa-plus-square"></i> Add Payment Transfer',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("paymenttransfer/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Payment Transfer',['class'=>'btn btn-default pull-right',
				  'style'=>'background-color: #ffffff;']); ?>    

                </div><!-- /.box-header -->

                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Employee','employee',['for'=>'employee']);?>
									<?php echo form_error('employee'); ?>
                                    <?php $options=array(''=>'Select Employee ','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('employee',$options,set_value('employee'),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>
                        		<div class="form-group">
									<?php echo form_label('PO No','po_no',['for'=>'po_no']);?>
									<?php echo form_error('po_no'); ?>
                    				<?php echo form_input(['name'=>'po_no','class'=>'form-control','id'=>'','placeholder'=>'Enter PO NO here',
									'value'=>set_value('po_no'),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Project','project',['for'=>'project']);?>
									<?php echo form_error('project'); ?>
                                    <?php $options=array(''=>'Select Project ','1'=>'Purchase','2'=>'Rent');  ?>
                            		<?php echo form_dropdown('project',$options,set_value('project'),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Remarks Here','value'=>set_value('remarks'),'disabled'=>'disabled']);?>
                              	</div>  
                                            
                            </div>
                        </div>
                    </div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php //echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
