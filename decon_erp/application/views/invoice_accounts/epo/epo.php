<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Po Issue To Employee</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Po Issue To Employee</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
     
          <?php echo form_button('','<i class="fa fa-fw fa-trash"></i> Delete',['type'=>'submit','class'=>'btn btn-default pull-right',
		  'style'=>'margin-left:5px;background-color: #ffffff;','value'=>'Delete_epo'],$js); ?>
          <?php echo anchor('epo/create','<i class="fa fa-fw fa-plus-square"></i> Add EPO',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          <!-- /.box-header -->
          <!--<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" name="inventory_form_manage" id="inventory_form_manage" method="post" />-->
          <?php echo form_open();?>
          <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Project Code</th>
              <th>Po No</th>
              <th>Employee</th>
              <th>Remarks</th>
              <th>Action</th>
              <th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(3);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  $lists->comp_name;?></td>
              <td><?php echo  $lists->tin;?></td>
              <td><?php echo  $lists->address;?></td>
              <td><?php echo  $lists->department;?></td>
              <td><?php echo  $lists->designation;?></td>
              <td>
					<?php echo anchor("customer/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Customer']); ?>
                    <?php echo anchor("customer/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Customer']); ?>     		
              </td>
              <td><?php echo form_checkbox(['name'=>'customer_to_delete[]','type'=>'checkbox','class'=>'checkboxes','value'=>$lists->csid]);?></td>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
               <th>Sr. No</th>
              <th>Project Code</th>
              <th>Po No</th>
              <th>Employee</th>
              <th>Remarks</th>
              <th>Action</th>
              <th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox','disabled'=>'disabled']);?></th>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php $count++; echo  $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
