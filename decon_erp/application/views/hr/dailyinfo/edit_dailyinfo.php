	<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit Daily Information</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('hr/employee','Daily Info')?></li>
        <li class="active">Edit Daily Information</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("dailyinfo/edit/{$list->id}",['name'=>'dailyinfo_form_add','id'=>'dailyinfo_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('dailyinfo/create','<i class="fa fa-fw fa-plus-square"></i> Add Daily Information',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('dailyinfo','<i class="fa fa-fw fa-arrows"></i>Daily Information List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        <div class="well well-sm"><strong>Project</strong></div>
                        		<div class="form-group">
									<?php echo form_label('Project','project',['for'=>'project']);?><?php echo form_error('project'); ?>
                                    <?php $option=[''=>'Select Project','1'=>'project1','2'=>'project2'] ?>
                    				<?php echo form_dropdown('project',$option,set_value('project',$list->project),['class'=>'form-control']);?>
                                </div>	

                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks',$list->remarks)]);?>
                              	</div><!-- /.form-group -->   
                            </div>
                        </div>
                    </div>
                <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>
