<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Employee</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('hr/employee','Employee')?></li>
        <li class="active">Add Employee</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('employee/create',['name'=>'employee_form_add','id'=>'employee_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('employee','<i class="fa fa-fw fa-arrows"></i> Employee List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        <div class="well well-sm"><strong>Employees Details</strong></div>
                        		<div class="form-group">
									<?php echo form_label('Employee Code','emp_code',['for'=>'emp_code']);?><?php echo form_error('emp_code'); ?>
                    				<?php echo form_input(['name'=>'emp_code','class'=>'form-control','id'=>'emp_code','placeholder'=>'Enter Employee Code here',
									'value'=>set_value('emp_code')]);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Employee Name','emp_business_letter',['for'=>'emp_business_letter']);?><?php echo form_error('emp_business_letter'); ?>
                                    <?php $options=array('mr'=>'Mr.','mrs'=>'MRS.','miss'=>'Miss');  ?>
                            		<?php echo form_dropdown('emp_business_letter',$options,set_value('emp_business_letter'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Employee Name','emp_name',['for'=>'emp_name']);?><?php echo form_error('emp_name'); ?>
                                    <?php echo form_input(['name'=>'emp_name','class'=>'form-control','id'=>'emp_name','placeholder'=>'Enter Employee Name here',
									'value'=>set_value('emp_name')]);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('Date of Birth','date_of_birth',['for'=>'date_of_birth']);?><?php echo form_error('date_of_birth'); ?>
									 <?php echo form_input(['name'=>'date_of_birth','class'=>'form-control','id'=>'reservation',
									 'placeholder'=>'Choose Date Of Birth Here','value'=>set_value('date_of_birth')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Office Email','office_email',['for'=>'office_email']);?><?php echo form_error('office_email'); ?>
                                    <?php echo form_input(['name'=>'office_email','class'=>'form-control','id'=>'office_email','placeholder'=>'Enter Official Email here',
									'value'=>set_value('office_email')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('Gender','gender',['for'=>'gender']);?><?php echo form_error('gender'); ?>
                                    <?php $options=array('male'=>'Male','female'=>'Female');  ?>
                            		<?php echo form_dropdown('gender',$options,set_value('gender'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Merital Status','',['for'=>'marital_status']);?><?php echo form_error('marital_status'); ?>
                                    <?php $options=array('married'=>'Married','unmarried'=>'Unmarried','divorced'=>'Divorced');  ?>
                            		<?php echo form_dropdown('marital_status',$options,set_value('marital_status'),['class'=>'form-control']);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Reffered By','referred_by',['for'=>'referred_by']);?><?php echo form_error('referred_by'); ?>
                                    <?php echo form_input(['name'=>'referred_by','class'=>'form-control','id'=>'','placeholder'=>'Enter Reffreer name here',
									'value'=>set_value('referred_by')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('Refferer Contact','referrer_contact',['for'=>'referrer_contact']);?><?php echo form_error('referrer_contact'); ?>
                                    <?php echo form_input(['name'=>'referrer_contact','class'=>'form-control','id'=>'','placeholder'=>'Enter Reffreer Contact here',
									'value'=>set_value('referrer_contact')]);?>                              	
                                </div>
                                
                                <div class="form-group">
									<?php echo form_label('Refferer Email','refferer_email',['for'=>'refferer_email']);?><?php echo form_error('refferer_email'); ?>
                                    <?php echo form_input(['name'=>'refferer_email','class'=>'form-control','id'=>'','placeholder'=>'Enter Reffreer Email here',
									'value'=>set_value('refferer_email')]);?>                              	
                                </div>
                                
                                <div class="form-group">
									<?php echo form_label('Experience In Year','experience_yr',['for'=>'Experience_yr']);?><?php echo form_error('Experience_yr'); ?>
                                    <?php $options=array('0'=>'Fresher','1'=>'1 Year','2'=>'2 Year','3'=>'3 Year');  ?>
                            		<?php echo form_dropdown('experience_yr',$options,set_value('experience_yr'),['class'=>'form-control']);?>
                              	</div>
                                
                                <div class="form-group">
									<?php echo form_label('Experience In Month','experience_month',['for'=>'experience_month']);?><?php echo form_error('experience_month'); ?>
                                    <?php $options=array('0'=>'0 Month','1'=>'1 Month','2'=>'2 Month','3'=>'3 Month');  ?>
                            		<?php echo form_dropdown('experience_month',$options,set_value('experience_month'),['class'=>'form-control']);?>
                              	</div>
								<div class="form-group">
									<?php echo form_label('Joining Date','joining_date',['for'=>'joining_date']);?><?php echo form_error('joining_date'); ?>
                                    <?php echo form_input(['name'=>'joining_date','class'=>'form-control','id'=>'','placeholder'=>'Enter Joining Date here',
									'value'=>set_value('joining_date')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('Salary','salary',['for'=>'salary']);?><?php echo form_error('salary'); ?>
                                    <?php echo form_input(['name'=>'salary','class'=>'form-control','id'=>'','placeholder'=>'Enter Salary Amount Here',
									'value'=>set_value('salary')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('LDA','lda',['for'=>'lda']);?><?php echo form_error('lda'); ?>
                                    <?php echo form_input(['name'=>'lda','class'=>'form-control','id'=>'','placeholder'=>'Enter LDA Here',
									'value'=>set_value('lda')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('ODA','oda',['for'=>'oda']);?><?php echo form_error('oda'); ?>
                                    <?php echo form_input(['name'=>'oda','class'=>'form-control','id'=>'','placeholder'=>'Enter ODA Here',
									'value'=>set_value('oda')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('Father\'s Name','lda',['for'=>'father_name']);?><?php echo form_error('father_name'); ?>
                                    <?php echo form_input(['name'=>'father_name','class'=>'form-control','id'=>'','placeholder'=>'Enter Father\'s Name Here',
									'value'=>set_value('father_name')]);?>                              	
                                </div>
                                <div class="form-group">
									<?php echo form_label('Mother\'s Name','mother_name',['for'=>'mother_name']);?><?php echo form_error('mother_name'); ?>
                                    <?php echo form_input(['name'=>'mother_name','class'=>'form-control','id'=>'','placeholder'=>'Enter Mother\'s Name Here',
									'value'=>set_value('mother_name')]);?>                              	
                                </div>
								<div class="form-group">
									<?php echo form_label('Current Location','current_location',['for'=>'current_location']);?><?php echo form_error('current_location'); ?>
                                    <?php $options=array('0'=>'Fresher','1'=>'1 Year','2'=>'2 Year','3'=>'3 Year');  ?>
                            		<?php echo form_dropdown('current_location',$location_list,set_value('current_location'),['class'=>'form-control']);?>
                              	</div>
                                
                                <div class="form-group">
									<?php echo form_label('Designation','designation',['for'=>'designation']);?><?php echo form_error('designation'); ?>
                                    <?php $options=array('0'=>'0 Month','1'=>'1 Month','2'=>'2 Month','3'=>'3 Month');  ?>
                            		<?php echo form_dropdown('designation',$designation_list,set_value('designation'),['class'=>'form-control']);?>
                              	</div>
								<div class="form-group">
									<?php echo form_label('Original Documents Status','original_doc_status',['for'=>'original_doc_status']);?>
									<?php echo form_error('original_doc_status'); ?>
                                     <?php $options=array('yes'=>'Yes','no'=>'No',);  ?>
                            		<?php echo form_dropdown('original_doc_status',$options,set_value('original_doc_status'),['class'=>'form-control']);?>                             	
                                </div>

                                <div class="form-group">
									<?php echo form_label('Recived Document Details','original_doc_details',['for'=>'original_doc_details']);?>
									<?php echo form_error('original_doc_details'); ?>
                                    <?php echo form_textarea(['name'=>'original_doc_details','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;',
									'cols'=>'40','rows'=>'10','placeholder'=>'Give Document Details Here','value'=>set_value('original_doc_details')]);?>
                              	</div><!-- /.form-group -->
                                <div class="form-group">
									<?php echo form_label('Document Recived By','recieved_by',['for'=>'recieved_by']);?><?php echo form_error('recieved_by'); ?>
                                    <?php echo form_input(['name'=>'recieved_by','class'=>'form-control','id'=>'','placeholder'=>'Enter Document Reciver Name Here',
									'value'=>set_value('recieved_by')]);?>                              	
                                </div>

                                <div class="form-group">
									<?php echo form_label('Recieved Store Branch','recieved_store_branch',['for'=>'recieved_store_branch']);?>
									<?php echo form_error('recieved_store_branch'); ?>
                                     <?php $options=array('yes'=>'Yes','no'=>'No',);  ?>
                            		<?php echo form_dropdown('recieved_store_branch',$branch_list,set_value('recieved_store_branch'),['class'=>'form-control']);?>                             	
                                </div>
                                <div class="well well-sm"><strong>Bank Details</strong></div>
                                <div class="form-group">
									<?php echo form_label('Name','bank_holder_name',['for'=>'bank_holder_name']);?><?php echo form_error('bank_holder_name'); ?>
                                    <?php echo form_input(['name'=>'bank_holder_name','class'=>'form-control','id'=>'bank_holder_name','placeholder'=>'Enter Bank Holder here',
                                    'value'=>set_value('bank_holder_name')]);?>
                                </div>
                                 <div class="form-group">
									<?php echo form_label('Account No.','bank_account_no',['for'=>'bank_account_no']);?><?php echo form_error('bank_account_no'); ?>
                                    <?php echo form_input(['name'=>'bank_account_no','class'=>'form-control','id'=>'bank_account_no','placeholder'=>'Enter Account No. here',
                                    'value'=>set_value('bank_account_no')]);?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label(' Bank Name','bank_name',['for'=>'bank_name']);?><?php echo form_error('bank_name'); ?>
                                    <?php echo form_input(['name'=>'bank_name','class'=>'form-control','id'=>'bank_name','placeholder'=>'Enter Bank Name here',
                                    'value'=>set_value('bank_name')]);?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('IFSC Code','bank_ifsc',['for'=>'bank_ifsc']);?><?php echo form_error('bank_ifsc'); ?>
                                    <?php echo form_input(['name'=>'bank_ifsc','class'=>'form-control','id'=>'bank_ifsc','placeholder'=>'Enter banks\' IFSC Code here',
                                    'value'=>set_value('bank_ifsc')]);?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('Branch','bank_branch',['for'=>'bank_branch']);?><?php echo form_error('bank_branch'); ?>
                                    <?php $options=array(''=>'Select Bank Branch','1'=>'Purchase','2'=>'Rent'); ?> 
                                    <?php echo form_dropdown('bank_branch',$options,set_value('bank_branch'),['class'=>'form-control']);?>
                                </div>
                                
                                
                                <div class="well well-sm"><strong>Employee Address Details</strong></div>
              					<div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                                    <table  class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                        	<th>Address Type</th>
                                            <th>Coutry</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Pin No</th>
                                            <th>Contact No</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                        </tr>
                                        <tr> 
                                        	<td><?php echo form_input(['class'=>'form-control','style'=>'width:130px;','value'=>'Local','disabled'=>'disabled']);?></td>  
                                            <td><?php echo form_dropdown('local_country',$country_list,set_value('local_country'),['class'=>'form-control',
                                                'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_dropdown('local_state',$state_list,set_value('local_state'),['class'=>'form-control',
                                                'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_dropdown('local_city',$city_list,set_value('local_city'),['class'=>'form-control'
                                                ,'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_input(['name'=>'local_address','class'=>'form-control','style'=>'width:200px;','placeholder'=>
                                                'Enter Address here','value'=>set_value('local_address')]);?></td>
                                            <td><?php echo form_input(['name'=>'local_pin','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Pin No here','value'=>set_value('local_pin')]);?></td>
                                            <td><?php echo form_input(['name'=>'local_contact','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Contact No here','value'=>set_value('local_contact')]);?></td>
                                            <td><?php echo form_input(['name'=>'local_mobile','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Mobile No here','value'=>set_value('local_mobile')]);?></td>
                                            <td><?php echo form_input(['name'=>'local_email','class'=>'form-control','style'=>'width:200px;','placeholder'=>
                                                'Enter Email here','value'=>set_value('local_email')]);?></td>
                                        </tr>
                                        <tr>
                                        	<td></td>
                                            <td><?php echo form_error('local_country'); ?></td>
                                            <td><?php echo form_error('local_state'); ?></td>
                                            <td><?php echo form_error('local_city'); ?></td>
                                            <td><?php echo form_error('local_address'); ?></td>
                                            <td><?php echo form_error('local_pin'); ?></td>
                                            <td><?php echo form_error('local_contact'); ?></td>
                                            <td><?php echo form_error('local_mobile'); ?></td>
                                            <td><?php echo form_error('local_email'); ?></td>
                                        </tr>
                                        
                                         <tr>
                                         	<td><?php echo form_input(['class'=>'form-control','style'=>'width:130px;','value'=>'Permanent','disabled'=>'disabled']);?></td>     
                                            <td><?php echo form_dropdown('permanent_country',$country_list,set_value('permanent_country'),['class'=>'form-control',
                                                'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_dropdown('permanent_state',$state_list,set_value('permanent_state'),['class'=>'form-control',
                                                'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_dropdown('permanent_city',$city_list,set_value('permanent_city'),['class'=>'form-control',
                                                'style'=>'width:130px;']);?></td>
                                            <td><?php echo form_input(['name'=>'permanent_address','class'=>'form-control','style'=>'width:200px;','placeholder'=>
                                                'Enter Address here','value'=>set_value('permanent_address')]);?></td>
                                            <td><?php echo form_input(['name'=>'permanent_pin','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Pin No here','value'=>set_value('permanent_pin')]);?></td>
                                            <td><?php echo form_input(['name'=>'permanent_contact','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Contact No here','value'=>set_value('permanent_contact')]);?></td>
                                            <td><?php echo form_input(['name'=>'permanent_mobile','class'=>'form-control','style'=>'width:130px;','placeholder'=>
                                                'Enter Mobile No here','value'=>set_value('permanent_mobile')]);?></td>
                                            <td><?php echo form_input(['name'=>'permanent_email','class'=>'form-control','style'=>'width:200px;','placeholder'=>
                                                'Enter Email here','value'=>set_value('permanent_email')]);?></td>
                                        </tr>
                                        <tr>
                                        	<td></td>
                                            <td><?php echo form_error('permanent_country'); ?></td>
                                            <td><?php echo form_error('permanent_state'); ?></td>
                                            <td><?php echo form_error('permanent_city'); ?></td>
                                            <td><?php echo form_error('permanent_address'); ?></td>
                                            <td><?php echo form_error('permanent_pin'); ?></td>
                                            <td><?php echo form_error('permanent_contact'); ?></td>
                                            <td><?php echo form_error('permanent_mobile'); ?></td>
                                            <td><?php echo form_error('permanent_email'); ?></td>
                                        </tr>
                                        </tr>
                                    </table>
                   				</div>
                                <div class="clearfix"></div>
 
                                <div class="well well-sm"><strong>Qualification Details</strong></div>
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                                <?php echo form_button('Add','<i class="fa fa-fw fa-plus-square"></i> Add Qualification',['class'=>'btn btn-block btn-success add',
                                    'style'=>'width:180px;']); ?>               
                <table id="participantTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Qualification</th>
                        <th>University</th>
                        <th>Marks</th>
                        <th>Passing Year</th>
                        <th></th>
                    </tr>
                    <tr  class="participantRow">
                        <td><?php echo form_input(['name'=>'qualification','class'=>'form-control','id'=>'qualification','style'=>'width:250px;',
						'placeholder'=>'Enter Qualification here','value'=>set_value('qualification')]);?></td>
                        <td><?php $options=array(''=>'Select University','1'=>'Purchase','2'=>'Rent'); ?> 
                            <?php //echo form_dropdown('university',$options,set_value('university'),['class'=>'form-control','style'=>'width:250px;']);?>
                            <?php echo form_input(['name'=>'university','class'=>'form-control','id'=>'','style'=>'width:250px;','placeholder'=>'Enter University Name Here',
							'value'=>set_value('university')]);?>
                        </td>
                        <td>
                            <?php $options=array(''=>'Select Marks','1'=>'Purchase','2'=>'Rent');  ?>
                            <?php echo form_input(['name'=>'marks','class'=>'form-control','id'=>'','style'=>'width:250px;','placeholder'=>'Enter Percentage obtained here',
							'value'=>set_value('marks')]);?>
                            <?php //echo form_dropdown('marks',$marks,set_value('marks'),['class'=>'form-control','style'=>'width:250px;']);?>
                        </td>
                        <td>
                            <?php echo form_input(['name'=>'passing_year','class'=>'form-control','id'=>'mobile','style'=>'width:250px;','placeholder'=>'Enter Passing Year here',
							'value'=>set_value('passing_year')]);?>
                        </td>
                        <td>
							<?php echo form_button('Remove','<i class="fa fa-fw fa-remove"></i>',['class'=>'btn btn-block btn-danger remove','style'=>'width:50px;']); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo form_error('qualification'); ?></td>
                        <td><?php echo form_error('university'); ?></td>
                        <td><?php echo form_error('marks'); ?></td>
                        <td><?php echo form_error('passing_year'); ?></td>
                        <td></td>
                    </tr>
                </table>
            </div>        
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
