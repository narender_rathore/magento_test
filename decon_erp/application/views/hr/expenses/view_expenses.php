<?php defined('BASEPATH') or exit('No Direct Access is Allowed'); ?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View Employee</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('hr/employee','Employee')?></li>
        <li class="active">View Employee</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("employee/view/{$list->id}",['name'=>'employee_form_view','id'=>'employee_form_view']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor("employee/edit/{$list->id}",'<i class="fa fa-fw fa-plus-square"></i> Edit Employee',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor('employee/create','<i class="fa fa-fw fa-plus-square"></i> Add Employee',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('employee','<i class="fa fa-fw fa-arrows"></i> Employee List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        <div class="well well-sm"><strong>Site Work Details</strong></div>
                      
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
            					     
								<?php /*?><?php echo form_button('Add','<i class="fa fa-fw fa-plus-square"></i> Add New Site Work',['class'=>'btn btn-block btn-success add',
                                                'style'=>'width:180px;']); ?> <?php */?>      
                            <table id="participantTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Date</th>
                                    <th>SiteID</th>
                                    <th>WorkPackage</th>
                                    <th></th>
                                </tr>
                                <tr class="participantRow">
                                    <td><?php echo form_input(['name'=>'date_added','class'=>'form-control','style'=>'width:200px;','placeholder'=>'Choose Date here',
                                    'value'=>set_value('date_added',$list->date_added),'disabled'=>'disabled']);?></td>
                                    <td>
                                        <?php $options=array(''=>'Select Site Id','1'=>'Purchase','2'=>'Rent'); ?> 
                                        <?php echo form_dropdown('site_id',$options,set_value('site_id',$list->site_id),['class'=>'form-control','style'=>'width:200px;',
										'disabled'=>'disabled']);?>
                                    </td>
                                    <td>
                                        <?php $options=array(''=>'Select Work Package','1'=>'Purchase','2'=>'Rent');  ?>
                                        <?php echo form_dropdown('work_packages',$options,set_value('work_packages',$list->work_packages),['class'=>'form-control',
										'style'=>'width:200px;','disabled'=>'disabled']);?>
                                    </td>
                                    <td><?php /*?><?php echo form_button('Remove','<i class="fa fa-fw fa-remove"></i>',['class'=>'btn btn-block btn-danger remove',
									'style'=>'width:50px;']); ?><?php */?></td>
                                </tr>
                                <tr>
                                    <td><?php echo form_error('date_added'); ?></td>
                                    <td><?php echo form_error('site_id'); ?></td>
                                    <td><?php echo form_error('work_packages'); ?></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div> 				
            					<div class="form-group">
									<?php echo form_label('Vehicle Used','vehicle',['for'=>'vehicle']);?><?php echo form_error('vehicle'); ?>
                                    <?php $options=array(''=>'Select Vehicle','1'=>'Monthly','2'=>'Daily','3'=>'Co. Own','4'=>'No');  ?>
                            		<?php echo form_dropdown('vehicle',$options,set_value('vehicle',$list->vehicle),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>
                                
                                <div class="form-group">
									<?php echo form_label('Travel Details','travel_details',['for'=>'travel_details']);?><?php echo form_error('travel_detail'); ?>
                                    <?php echo form_textarea(['name'=>'travel_detail','class'=>'form-control','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Travel Details Here','value'=>set_value('travel_detail',$list->travel_detail),'disabled'=>'disabled']);?>
                              	</div>
                                
                        		<div class="form-group">
									<?php echo form_label('Start Date & Time','start_date',['for'=>'start_date']);?><?php echo form_error('start_date'); ?>
                    				<?php echo form_input(['name'=>'start_date','class'=>'form-control','placeholder'=>'choose Start Date & Time here',
									'value'=>set_value('start_date',$list->start_date),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('End Date & Time','end_date',['for'=>'end_date']);?><?php echo form_error('end_date'); ?>
                    				<?php echo form_input(['name'=>'end_date','class'=>'form-control','id'=>'end_date','placeholder'=>'End Date & Time here',
									'value'=>set_value('end_date',$list->end_date),'disabled'=>'disabled']);?>
                                </div>
                                
                                <div class="form-group">
									<?php echo form_label('Start Reading','start_reading',['for'=>'start_reading']);?><?php echo form_error('start_reading'); ?>
                    				<?php echo form_input(['name'=>'start_reading','class'=>'form-control','placeholder'=>'Enter Start Reading here',
									'value'=>set_value('start_reading',$list->start_reading),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('End Reading','end_reading',['for'=>'end_reading']);?><?php echo form_error('end_reading'); ?>
                    				<?php echo form_input(['name'=>'end_reading','class'=>'form-control','id'=>'end_reading','placeholder'=>'Enter End Reading here',
									'value'=>set_value('end_reading',$list->end_reading),'disabled'=>'disabled']);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Work Approved By','work_approved_by',['for'=>'work_approved_by']);?><?php echo form_error('work_approved_by'); ?>
                                    <?php $options=array('mr'=>'Mr.','mrs'=>'MRS.','miss'=>'Miss');  ?>
                            		<?php echo form_dropdown('work_approved_by',$options,set_value('work_approved_by',$list->work_approved_by),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>
                                
                                <div class="form-group">
									<?php echo form_label('Vehicle No','vehicle_no',['for'=>'vehicle_no']);?><?php echo form_error('vehicle_no'); ?>
                    				<?php echo form_input(['name'=>'vehicle_no','class'=>'form-control','id'=>'vehicle_no','placeholder'=>'Enter Vehicle No here',
									'value'=>set_value('vehicle_no',$list->vehicle_no),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Driver Name','driver_name',['for'=>'driver_name']);?><?php echo form_error('driver_name'); ?>
                    				<?php echo form_input(['name'=>'driver_name','class'=>'form-control','placeholder'=>'Enter Driver Name here',
									'value'=>set_value('driver_name',$list->driver_name),'disabled'=>'disabled']);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Driver Contact','driver_no',['for'=>'driver_no']);?><?php echo form_error('driver_no'); ?>
                    				<?php echo form_input(['name'=>'driver_no','class'=>'form-control','id'=>'driver_no','placeholder'=>'Enter Driver Mobile here',
									'value'=>set_value('driver_no',$list->driver_no),'disabled'=>'disabled']);?>
                                </div>		
                                 <div class="form-group">
									<?php echo form_label('PO No','po_no',['for'=>'po_no']);?><?php echo form_error('po_no'); ?>
                    				<?php echo form_input(['name'=>'po_no','class'=>'form-control','id'=>'po_no','placeholder'=>'Enter PO No here',
									'value'=>set_value('po_no',$list->po_no),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Extra Mode of Transport','extra_mode_transport',['for'=>'extra_mode_transport']);?>
									<?php echo form_error('extra_mode_transport'); ?>
                    				<?php echo form_input(['name'=>'extra_mode_transport','class'=>'form-control',
									'placeholder'=>'Enter Extra Mode OF Transport here','value'=>set_value('extra_mode_transport',$list->extra_mode_transport),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Engineer DA','engineer_da',['for'=>'engineer_da']);?><?php echo form_error('engineer_da'); ?>
                                    <?php $options=array('local'=>'Local DA','outstation'=>'Outstation DA','no'=>'No DA');  ?>
                            		<?php echo form_dropdown('engineer_da',$options,set_value('engineer_da',$list->engineer_da),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>		
                                <div class="form-group">
									<?php echo form_label('Rigger','rigger_id',['for'=>'rigger_id']);?><?php echo form_error('rigger_id'); ?>
                                    <?php $options=array('local'=>'Local DA','outstation'=>'Outstation DA','no'=>'No DA');  ?>
                            		<?php echo form_dropdown('rigger_id',$options,set_value('rigger_id',$list->rigger_id),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>	
                                <div class="form-group">
									<?php echo form_label('Rigger DA','rigger_da',['for'=>'rigger_da']);?><?php echo form_error('rigger_da'); ?>
                                    <?php $options=array('local'=>'Local DA','outstation'=>'Outstation DA','no'=>'No DA');  ?>
                            		<?php echo form_dropdown('rigger_da',$options,set_value('rigger_da',$list->rigger_da),['class'=>'form-control','disabled'=>'disabled']);?>
                              	</div>	
                                <div class="form-group">
									<?php echo form_label('Vehicle Payment','vehicle_payment',['for'=>'vehicle_payment']);?><?php echo form_error('vehicle_payment'); ?>
                    				<?php echo form_input(['name'=>'vehicle_payment','class'=>'form-control','id'=>'vehicle_payment','placeholder'=>'Enter Vehicle Payment here',
									'value'=>set_value('vehicle_payment',$list->vehicle_payment),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Other Expenses','other_expense',['for'=>'other_expense']);?><?php echo form_error('other_expense'); ?>
                    				<?php echo form_input(['name'=>'other_expense','class'=>'form-control','placeholder'=>'Enter Other Expenses here',
									'value'=>set_value('other_expense',$list->other_expense),'disabled'=>'disabled']);?>
                                </div>
                                 <div class="form-group">
									<?php echo form_label('Total Expenses','total_expenses',['for'=>'total_expenses']);?><?php echo form_error('total_expenses'); ?>
                    				<?php echo form_input(['name'=>'total_expenses','class'=>'form-control','id'=>'total_expenses','placeholder'=>'Enter Total Expenses here',
									'value'=>set_value('total_expenses',$list->total_expenses),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Bill Upload','bill_upload',['for'=>'bill_upload']);?><?php echo form_error('bill_upload'); ?>
                    				<?php echo form_upload(['name'=>'bill_upload','class'=>'','id'=>'bill_upload','placeholder'=>'Enter Billl here',
									'value'=>set_value('bill_upload',$list->bill_upload),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks',$list->remarks),'disabled'=>'disabled']);?>
                              	</div>
                            </div>
                        </div>
                    </div>
                <hr />

<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>