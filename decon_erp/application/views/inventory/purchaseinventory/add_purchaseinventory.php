<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Purchase Inventory</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('inventory/purchaseinventory','Purchase Inventory')?></li>
        <li class="active">Add Purchase Inventory</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('purchaseinventory/create',['name'=>'purchaseinventory_form_add','id'=>'purchaseinventory_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('purchaseinventory','<i class="fa fa-fw fa-arrows"></i> Purchased Inventory List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('PO No','po_no',['for'=>'po_no']);?><?php echo form_error('po_no'); ?>
                    				<?php echo form_input(['name'=>'po_no','class'=>'form-control','id'=>'','placeholder'=>'Enter PO NO here',
									'value'=>set_value('po_no')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Inventory Type','inventory_type',['for'=>'inventory_type']);?><?php echo form_error('inventory_type'); ?>
                    				<?php $options=array(''=>'Select Inventory Type','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('inventory_type',$options,set_value('inventory_type'),['class'=>'form-control']);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Date','date',['for'=>'date']);?><?php echo form_error('date'); ?>
                                    <?php echo form_input(['name'=>'date','class'=>'form-control','id'=>'','placeholder'=>'Enter Date here',
									'value'=>set_value('date')]);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Vendor','vendor',['for'=>'vendor']);?><?php echo form_error('vendor'); ?>
                    				<?php $options=array(''=>'Select Vendor','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('vendor',$options,set_value('vendor'),['class'=>'form-control']);?>
                                </div>
                                <div class="well well-sm"><strong>Add Item</strong></div>
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
            <div><a href="#" id="addNew">Add New Item</a></div>            
                <table id="dataTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Item</th>
                        <th>Make</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Rental</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <tr style="border:1px solid black">
                        <td><?php $options=array(''=>'Select Item','1'=>'Purchase','2'=>'Rent'); ?> 
                            <?php echo form_dropdown('item',$options,set_value('item'),['class'=>'form-control','style'=>'width:200px;']);?>
                        </td>
                        <td>
                            <?php $options=array(''=>'Select Make','1'=>'Purchase','2'=>'Rent');  ?>
                            <?php echo form_dropdown('make',$options,set_value('make'),['class'=>'form-control','style'=>'width:200px;']);?>
                        </td>
                        <td>
                            <?php echo form_input(['name'=>'quantity','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Quantity here',
							'value'=>set_value('quantity')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'unit_price','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Unit Price here',
						'value'=>set_value('email')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'rental','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Rental here',
						'value'=>set_value('email')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'amount','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Amount here',
						'value'=>set_value('name')]);?>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                    	<td><?php echo form_error('item'); ?></td>
                        <td><?php echo form_error('make'); ?></td>
                        <td><?php echo form_error('quantity'); ?></td>
                        <td><?php echo form_error('unit_price'); ?></td>
                        <td><?php echo form_error('rental'); ?></td>
                        <td><?php echo form_error('amount'); ?></td>
                        <td></td>
                    </tr>
                </table>
            </div>        
            <div class="form-group">
				<?php echo form_label('Tax','tax',['for'=>'tax']);?><?php echo form_error('tax'); ?>
                <?php echo form_input(['name'=>'tax','class'=>'form-control','id'=>'','placeholder'=>'Enter Tax here',
                'value'=>set_value('tax')]);?>
            </div>
            <div class="form-group">
                <?php echo form_label('Total Amount','total_amount',['for'=>'vendor']);?><?php echo form_error('total_amount'); ?>
                <?php echo form_input(['name'=>'total_amount','class'=>'form-control','id'=>'','placeholder'=>'Total Amount here',
                'value'=>set_value('total_amount')]);?>
            </div>
            <div class="well well-sm"><strong>Payment Terms</strong></div>
            <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
            <div><a href="#" id="addNew">Add Payment Terms</a></div>            
                <table id="dataTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Date</th>
                        <th>Percentage</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <tr style="border:1px solid black">
                        <td><?php echo form_input(['name'=>'date','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Date here',
						'value'=>set_value('email')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'percentage','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Percentage here',
						'value'=>set_value('email')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'amount','class'=>'form-control','id'=>'','style'=>'width:200px;','placeholder'=>'Enter Amount here',
						'value'=>set_value('name')]);?>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><?php echo form_error('date'); ?></td>
                        <td><?php echo form_error('percentage'); ?></td>
                        <td><?php echo form_error('amount'); ?></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>
