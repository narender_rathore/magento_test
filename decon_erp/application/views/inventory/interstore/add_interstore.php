<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Inter Store Transfer</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('inventory/interstore','Inter Store Transfer')?></li>
        <li class="active">Add Inter Store Transfer</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('interstore/create',['name'=>'addinventory_form_add','id'=>'addinventory_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('interstore','<i class="fa fa-fw fa-arrows"></i>Inter Store Transfer List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Date','date',['for'=>'date']);?><?php echo form_error('date'); ?>
                                    <?php echo form_input(['name'=>'date','class'=>'form-control','id'=>'','placeholder'=>'Enter Date here',
									'value'=>set_value('date')]);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('From Store','from_store',['for'=>'from_store']);?><?php echo form_error('from_store'); ?>
                    				<?php $options=array(''=>'Select From Store','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('from_store',$options,set_value('from_store'),['class'=>'form-control']);?>
                                </div>	
                                
                                <div class="form-group">
									<?php echo form_label('To Store','make',['for'=>'to_store']);?><?php echo form_error('to_store'); ?>
                    				<?php $options=array(''=>'Select To Store','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('to_store',$options,set_value('to_store'),['class'=>'form-control']);?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks')]);?>
                                </div>
                                <div class="well well-sm"><strong>Item Details</strong></div>
                                <button id="btnAdd" type="button">+ Add Item</button>
                                <div class="form-group">
									<?php echo form_label('Item','item',['for'=>'item']);?><?php echo form_error('item'); ?>
                                    <?php echo form_input(['name'=>'item','class'=>'form-control','id'=>'','placeholder'=>'Enter Item here',
									'value'=>set_value('item')]);?>
                              	</div>
                            </div>
                        </div>
                    </div>
                    <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>
