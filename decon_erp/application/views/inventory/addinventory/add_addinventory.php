<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Inventory</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('inventory/addinventory','Inventory')?></li>
        <li class="active">Add Inventory</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('addinventory/create',['name'=>'addinventory_form_add','id'=>'addinventory_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('addinventory','<i class="fa fa-fw fa-arrows"></i>Inventory List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('PO No','po_no',['for'=>'po_no']);?><?php echo form_error('po_no'); ?>
                            		<?php echo form_dropdown('po_no',$options,set_value('po_no'),['class'=>'form-control']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Item','item',['for'=>'item']);?><?php echo form_error('item'); ?>
                    				<?php $options=array(''=>'Select Item','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('item',$options,set_value('item'),['class'=>'form-control']);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Date','date',['for'=>'date']);?><?php echo form_error('date'); ?>
                                    <?php echo form_input(['name'=>'date','class'=>'form-control','id'=>'','placeholder'=>'Enter Date here',
									'value'=>set_value('date')]);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Make','make',['for'=>'make']);?><?php echo form_error('make'); ?>
                    				<?php $options=array(''=>'Select Vendor','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('make',$options,set_value('make'),['class'=>'form-control']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Vendor','vendor',['for'=>'vendor']);?><?php echo form_error('vendor'); ?>
                    				<?php $options=array(''=>'Select Vendor','1'=>'Purchase','2'=>'Rent'); ?> 
                            		<?php echo form_dropdown('vendor',$options,set_value('vendor'),['class'=>'form-control']);?>
                                </div>                       
                                <div class="form-group">
                                    <?php echo form_label('Model No','model',['for'=>'model']);?><?php echo form_error('model'); ?>
                                    <?php echo form_input(['name'=>'tax','class'=>'form-control','id'=>'','placeholder'=>'Enter Model here',
                                    'value'=>set_value('tax')]);?>
                                </div>
                                <div class="form-group">
                                    <?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks')]);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>
