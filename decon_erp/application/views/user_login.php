<?php defined('BASEPATH') or exit('No direct Access is Allowed');?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DECON ERP | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <?=link_tag('assets/bootstrap/css/bootstrap.min.css'); ?>
    <?=link_tag('assets/dist/css/AdminLTE.min.css'); ?>
    <?=link_tag('assets/plugins/iCheck/square/blue.css'); ?>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>DECON</b>ERP</a>
      </div><!-- /.login-logo -->
      <?php if($error=$this->session->flashdata('login_failed')): ?>
    <div class="row">
    	<div class="col-lg-12">
            <div class="alert alert-dismissible alert-danger">
              <?=$error?>
            </div>
        </div>
    </div>
	<?php endif;?>
        	
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?=form_open('login/user_login')?>
          <?php echo form_error('email','<p class="text-danger">','</p>'); ?>
          <div class="form-group has-feedback">
            <?=form_input('email',set_value('email'),array('class'=>'form-control','placeholder'=>'Email'))?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <?php echo form_error('password','<p class="text-danger">','</p>'); ?>
          <div class="form-group has-feedback">
            <?=form_password('password','',array('class'=>'form-control','placeholder'=>'password'))?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <?=form_checkbox('remember_me','')?><?=nbs(1);?>Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <?=form_submit('signin','Sign In','class="btn btn-primary btn-block btn-flat"')?>
            </div><!-- /.col -->
          </div>
        <?=form_close();?>
        <?=anchor('login/forget_pass','I forgot my password');?><?=br();?>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?=base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js')?>"></script>
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
