<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>
<?php include_once('panel/header.php');?>
<?php include_once('panel/sidebar.php');?>

 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User Profile
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage Profile</a></li>
            <li class="active">Admin profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <?=$img_property_logo=['src'=>'assets/dist/img/logo.png','alt'=>'User profile picture','class'=>'profile-user-img img-responsive img-circle'];?>
          	  	  <?=img($img_property_logo);?>
                  <h3 class="profile-username text-center"><?=ucfirst($admin_data['fname'])."&nbsp;&nbsp;".ucfirst($admin_data['lname'])?></h3>
                  <p class="text-muted text-center"><?=ucfirst($admin_data['type'])?></p>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->

          
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li ><a href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                  
            

            		
                     <span class="error"><?php echo $msg;?></span>
                  <div class="active tab-pane" id="settings">
                    <form class="form-horizontal" name="" id="" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                      <span class="error"><?php echo $fnameErr;?></span>
                        <label for="inputName" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="<?=$admin_data['fname']?>" id="inputName" name="fname" id="fname" placeholder="First Name">
                        </div>
                      </div>
                      
                      <div class="form-group">
                      <span class="error"><?php echo $lnameErr;?></span>
                        <label for="inputName" class="col-sm-2 control-label">Last name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="<?=$admin_data['lname']?>" id="inputName" name="lname" id="lname" placeholder="Last Name">
                        </div>
                      </div>
                      
                      <div class="form-group">
                      <span class="error"><?php echo $unameErr;?></span>
                        <label for="inputName" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="<?=$admin_data['username']?>" id="inputName" name="username" id="username"  placeholder="Username">
                        </div>
                      </div>
                      
                      <div class="form-group">
                      <span class="error"><?php echo $emailErr;?></span>
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" value="<?=$admin_data['email']?>" id="inputEmail" name="email" id="email"  placeholder="Email">
                        </div>
                      </div>
                      
                      
                      <div class="form-group">
                      <span class="error"><?php echo $passErr;?></span>
                        <label for="inputSkills" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="<?=$admin_data['password']?>" id="inputSkills" name="password" id="password" placeholder="Password">
                        </div>
                      </div>
                     
                     <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label">Profile Image</label>
                        <div class="col-sm-10">
                          <input type="file" name="manage_picture" id="manage_picture" >
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" name="manage_profile" id="manage_profile" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
                
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
        
      
      </div><!-- /.content-wrapper -->
      
  
      
 <script src="<?=DOCUMENT_ROOT?>plugins/fastclick/fastclick.min.js"></script>
<?php include_once('panel/footer.php');?>