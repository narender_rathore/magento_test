<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add User</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/new_user','User')?></li>
        <li class="active">Add User</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('new_user/create',['name'=>'new_user_form_add','id'=>'new_user_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('new_user','<i class="fa fa-fw fa-arrows"></i>User List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('User Email','email',['for'=>'email']);?><?php echo form_error('email'); ?>
                    				<?php echo form_input(['name'=>'email','class'=>'form-control','id'=>'email','placeholder'=>'Enter Email Here',
									'value'=>set_value('email')]);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('User Password','password',['for'=>'password']);?><?php echo form_error('password'); ?>
                    				<?php echo form_password(['name'=>'password','class'=>'form-control','id'=>'password','placeholder'=>'Enter Password Here',
									'value'=>set_value('password')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Confirm Password','cf_pass',['for'=>'cf_pass']);?><?php echo form_error('cf_pass'); ?>
                    				<?php echo form_password(['name'=>'cf_pass','class'=>'form-control','id'=>'cf_pass','placeholder'=>'Confirm Password',
									'value'=>set_value('cf_pass')]);?>
                                </div>
                            </div>
                        </div>
                    </div>
               <hr />
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
