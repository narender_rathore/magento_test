<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit User Authentication</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/user_auth','User Authentication')?></li>
        <li class="active">Edit User Authentication</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("user_auth/edit/{$list->id}",['name'=>'userauth_form_add','id'=>'userauth_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('user_auth/create','<i class="fa fa-fw fa-plus-square"></i> Add Authenticated User',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?> 
				  <?php echo anchor('user_auth','<i class="fa fa-fw fa-arrows"></i>Authenticated User List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_label('User Email','email',['for'=>'email']);?><?php echo form_error('email'); ?>
                                <?php /*?><?php echo form_input(['name'=>'email','class'=>'form-control','id'=>'email','placeholder'=>'Enter User Email Here',
                                'value'=>set_value('email',$list->email)]);?><?php */?>
                                <?php echo form_dropdown('email',$user_list,set_value('email',$list->user_email_id),['class'=>'form-control','disabled'=>'disabled']);?>
                            </div>
                            <div class="well well-sm">Masters</div>
                            <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'customer_master_view','id'=>'','value'=>set_value('customer_master_view')]);?>
                                        <input type="checkbox" name="customer_master_view" <?php if($list->customer_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer Master View','customer_master_view',['for'=>'customer_master_view','class'=>'control-label']);?>
										<?php echo form_error('customer_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_master_add','id'=>'','value'=>set_value('customer_master_add')]);?>
                                        <input type="checkbox" name="customer_master_add" <?php if($list->customer_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer Master Add','customer_master_add',['for'=>'customer_master_add','class'=>'control-label']);?>
										<?php echo form_error('customer_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_master_edit','id'=>'','value'=>set_value('customer_master_edit')]);?>
                                        <input type="checkbox" name="customer_master_edit" <?php if($list->customer_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer Master Edit','customer_master_edit',['for'=>'customer_master_edit','class'=>'control-label']);?>
										<?php echo form_error('customer_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_master_delete','value'=>set_value('customer_master_delete')]);?>
                                        <input type="checkbox" name="customer_master_delete" <?php if($list->customer_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer Master Delete','customer_master_delete',['for'=>'customer_master_delete','class'=>'control-label']);?>
										<?php echo form_error('customer_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'item_master_view','id'=>'','value'=>set_value('item_master_view')]);?>
                                        <input type="checkbox" name="item_master_view" <?php if($list->item_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Item Master View','item_master_view',['for'=>'item_master_view','class'=>'control-label']);?>
										<?php echo form_error('item_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'item_master_add','id'=>'','value'=>set_value('item_master_add')]);?>
                                        <input type="checkbox" name="item_master_add" <?php if($list->item_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Item Master Add','item_master_add',['for'=>'item_master_add','class'=>'control-label']);?>
										<?php echo form_error('item_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'item_master_edit','id'=>'','value'=>set_value('item_master_edit')]);?>
                                        <input type="checkbox" name="item_master_edit" <?php if($list->item_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Item Master Edit','item_master_edit',['for'=>'item_master_edit','class'=>'control-label']);?>
										<?php echo form_error('item_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'item_master_delete','id'=>'','value'=>set_value('item_master_delete')]);?>
                                        <input type="checkbox" name="item_master_delete" <?php if($list->item_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Item Master Delete','item_master_delete',['for'=>'item_master_delete','class'=>'control-label']);?>
										<?php echo form_error('item_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'company_master_view','id'=>'','value'=>set_value('company_master_view')]);?>
                                        <input type="checkbox" name="company_master_view" <?php if($list->company_make_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Company Master View','company_master_view',['for'=>'company_master_view','class'=>'control-label']);?>
										<?php echo form_error('company_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'company_master_add','id'=>'','value'=>set_value('company_master_add')]);?>
                                        <input type="checkbox" name="company_master_add" <?php if($list->company_make_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Company Master Add','company_master_add',['for'=>'company_master_add','class'=>'control-label']);?>
										<?php echo form_error('company_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'company_master_edit','id'=>'','value'=>set_value('company_master_edit')]);?>
                                        <input type="checkbox" name="company_master_edit" <?php if($list->company_make_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Company Master Edit','company_master_edit',['for'=>'company_master_edit','class'=>'control-label']);?>
										<?php echo form_error('company_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'company_master_delete','id'=>'','value'=>set_value('company_master_delete')]);?>
                                        <input type="checkbox" name="company_master_delete" <?php if($list->company_make_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Company Master Delete','company_master_delete',['for'=>'company_master_delete','class'=>'control-label']);?>
										<?php echo form_error('company_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'vendor_master_view','id'=>'','value'=>set_value('vendor_master_view')]);?>
                                        <input type="checkbox" name="vendor_master_view" <?php if($list->vendor_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Vendor Master View','vendor_master_view',['for'=>'vendor_master_view','class'=>'control-label']);?>
										<?php echo form_error('vendor_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'vendor_master_add','id'=>'','value'=>set_value('vendor_master_add')]);?>
                                        <input type="checkbox" name="vendor_master_add" <?php if($list->vendor_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Vendor Master Add','vendor_master_add',['for'=>'vendor_master_add','class'=>'control-label']);?>
										<?php echo form_error('vendor_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'vendor_master_edit','id'=>'','value'=>set_value('vendor_master_edit')]);?>
                                        <input type="checkbox" name="vendor_master_edit" <?php if($list->vendor_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Vendor Master Edit','vendor_master_edit',['for'=>'vendor_master_edit','class'=>'control-label']);?>
										<?php echo form_error('vendor_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'vendor_master_delete','id'=>'','value'=>set_value('vendor_master_delete')]);?>
                                        <input type="checkbox" name="vendor_master_delete" <?php if($list->vendor_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Vendor Master Delete','vendor_master_delete',['for'=>'vendor_master_delete','class'=>'control-label']);?>
										<?php echo form_error('vendor_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'store_master_view','id'=>'','value'=>set_value('store_master_view')]);?>
                                        <input type="checkbox" name="store_master_view" <?php if($list->store_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Master View','store_master_view',['for'=>'store_master_view','class'=>'control-label']);?>
										<?php echo form_error('store_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_master_add','id'=>'','value'=>set_value('store_master_add')]);?>
                                        <input type="checkbox" name="store_master_add" <?php if($list->store_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Master Add','store_master_add',['for'=>'store_master_add','class'=>'control-label']);?>
										<?php echo form_error('store_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_master_edit','id'=>'','value'=>set_value('store_master_edit')]);?>
                                        <input type="checkbox" name="store_master_edit" <?php if($list->store_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Master Edit','store_master_edit',['for'=>'store_master_edit','class'=>'control-label']);?>
										<?php echo form_error('store_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_master_delete','id'=>'','value'=>set_value('store_master_delete')]);?>
                                        <input type="checkbox" name="store_master_delete" <?php if($list->store_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Master Delete','store_master_delete',['for'=>'store_master_delete','class'=>'control-label']);?>
										<?php echo form_error('store_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'country_master_view','id'=>'','value'=>set_value('country_master_view')]);?>
                                        <input type="checkbox" name="country_master_view" <?php if($list->country_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Country Master View','country_master_view',['for'=>'country_master_view','class'=>'control-label']);?>
										<?php echo form_error('country_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'country_master_add','id'=>'','value'=>set_value('country_master_add')]);?>
                                        <input type="checkbox" name="country_master_add" <?php if($list->country_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Country Master Add','country_master_add',['for'=>'country_master_add','class'=>'control-label']);?>
										<?php echo form_error('country_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'country_master_edit','id'=>'','value'=>set_value('country_master_edit')]);?>
                                        <input type="checkbox" name="country_master_edit" <?php if($list->country_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Country Master Edit','country_master_edit',['for'=>'country_master_edit','class'=>'control-label']);?>
										<?php echo form_error('country_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'country_master_delete','id'=>'','value'=>set_value('country_master_delete')]);?>
                                        <input type="checkbox" name="country_master_delete" <?php if($list->country_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Country Master Delete','country_master_delete',['for'=>'country_master_delete','class'=>'control-label']);?>
										<?php echo form_error('country_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'state_master_view','id'=>'','value'=>set_value('state_master_view')]);?>
                                        <input type="checkbox" name="state_master_view" <?php if($list->state_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('State Master View','state_master_view',['for'=>'state_master_view','class'=>'control-label']);?>
										<?php echo form_error('state_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'state_master_add','id'=>'','value'=>set_value('state_master_add')]);?>
                                        <input type="checkbox" name="state_master_add" <?php if($list->state_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('State Master Add','state_master_add',['for'=>'state_master_add','class'=>'control-label']);?>
										<?php echo form_error('state_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'state_master_edit','id'=>'','value'=>set_value('state_master_edit')]);?>
                                        <input type="checkbox" name="state_master_edit" <?php if($list->state_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('State Master Edit','state_master_edit',['for'=>'state_master_edit','class'=>'control-label']);?>
										<?php echo form_error('state_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'state_master_delete','id'=>'','value'=>set_value('state_master_delete')]);?>
                                        <input type="checkbox" name="state_master_delete" <?php if($list->state_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('State Master Delete','state_master_delete',['for'=>'state_master_delete','class'=>'control-label']);?>
										<?php echo form_error('state_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'city_master_view','id'=>'','value'=>set_value('city_master_view')]);?>
                                        <input type="checkbox" name="city_master_view" <?php if($list->city_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('City Master View','city_master_view',['for'=>'city_master_view','class'=>'control-label']);?>
										<?php echo form_error('city_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'city_master_add','id'=>'','value'=>set_value('city_master_add')]);?>
                                        <input type="checkbox" name="city_master_add" <?php if($list->city_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('City Master Add','city_master_add',['for'=>'city_master_add','class'=>'control-label']);?>
										<?php echo form_error('city_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'city_master_edit','id'=>'','value'=>set_value('city_master_edit')]);?>
                                        <input type="checkbox" name="city_master_edit" <?php if($list->city_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('City Master Edit','city_master_edit',['for'=>'city_master_edit','class'=>'control-label']);?>
										<?php echo form_error('city_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'city_master_delete','id'=>'','value'=>set_value('city_master_delete')]);?>
                                        <input type="checkbox" name="city_master_delete" <?php if($list->city_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('City Master Delete','city_master_delete',['for'=>'city_master_delete','class'=>'control-label']);?>
										<?php echo form_error('city_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'region_master_view','id'=>'','value'=>set_value('region_master_view')]);?>
                                        <input type="checkbox" name="region_master_view" <?php if($list->region_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Region Master View','region_master_view',['for'=>'region_master_view','class'=>'control-label']);?>
										<?php echo form_error('region_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'region_master_add','id'=>'','value'=>set_value('region_master_add')]);?>
                                        <input type="checkbox" name="region_master_add" <?php if($list->region_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Region Master Add','region_master_add',['for'=>'region_master_add','class'=>'control-label']);?>
										<?php echo form_error('region_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'region_master_edit','id'=>'','value'=>set_value('region_master_edit')]);?>
                                        <input type="checkbox" name="region_master_edit" <?php if($list->region_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Region Master Edit','region_master_edit',['for'=>'region_master_edit','class'=>'control-label']);?>
										<?php echo form_error('region_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'region_master_delete','id'=>'','value'=>set_value('region_master_delete')]);?>
                                        <input type="checkbox" name="region_master_delete" <?php if($list->region_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Region Master Delete','region_master_delete',['for'=>'region_master_delete','class'=>'control-label']);?>
										<?php echo form_error('region_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'location_master_view','id'=>'','value'=>set_value('location_master_view')]);?>
                                        <input type="checkbox" name="location_master_view" <?php if($list->location_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Location Master View','location_master_view',['for'=>'location_master_view','class'=>'control-label']);?>
										<?php echo form_error('location_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'location_master_add','id'=>'','value'=>set_value('location_master_add')]);?>
                                        <input type="checkbox" name="location_master_add" <?php if($list->location_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Location Master Add','location_master_add',['for'=>'location_master_add','class'=>'control-label']);?>
										<?php echo form_error('location_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'location_master_edit','id'=>'','value'=>set_value('location_master_edit')]);?>
                                        <input type="checkbox" name="location_master_edit" <?php if($list->location_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Location Master Edit','location_master_edit',['for'=>'location_master_edit','class'=>'control-label']);?>
										<?php echo form_error('location_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'location_master_delete','id'=>'','value'=>set_value('location_master_delete')]);?>
                                        <input type="checkbox" name="location_master_delete" <?php if($list->location_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Location Master Delete','location_master_delete',['for'=>'location_master_delete','class'=>'control-label']);?>
										<?php echo form_error('location_master_delete'); ?>
                                    </td>
                                </tr>   
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'branch_master_view','id'=>'','value'=>set_value('branch_master_view')]);?>
                                        <input type="checkbox" name="branch_master_view" <?php if($list->branch_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Branch Master View','branch_master_view',['for'=>'branch_master_view','class'=>'control-label']);?>
										<?php echo form_error('branch_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'branch_master_add','id'=>'','value'=>set_value('branch_master_add')]);?>
                                        <input type="checkbox" name="branch_master_add" <?php if($list->branch_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Branch Master Add','branch_master_add',['for'=>'branch_master_add','class'=>'control-label']);?>
										<?php echo form_error('branch_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'branch_master_edit','id'=>'','value'=>set_value('branch_master_edit')]);?>
                                        <input type="checkbox" name="branch_master_edit" <?php if($list->branch_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Branch Master Edit','branch_master_edit',['for'=>'branch_master_edit','class'=>'control-label']);?>
										<?php echo form_error('branch_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'branch_master_delete','id'=>'','value'=>set_value('branch_master_delete')]);?>
                                        <input type="checkbox" name="branch_master_delete" <?php if($list->branch_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Branch Master Delete','branch_master_delete',['for'=>'branch_master_delete','class'=>'control-label']);?>
										<?php echo form_error('branch_master_delete'); ?>
                                    </td>
                                </tr> 
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'entity_master_view','id'=>'','value'=>set_value('entity_master_view')]);?>
                                        <input type="checkbox" name="entity_master_view" <?php if($list->entity_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Entity Master View','entity_master_view',['for'=>'entity_master_view','class'=>'control-label']);?>
										<?php echo form_error('entity_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'entity_master_add','id'=>'','value'=>set_value('entity_master_add')]);?>
                                        <input type="checkbox" name="entity_master_add" <?php if($list->entity_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Entity Master Add','entity_master_add',['for'=>'entity_master_add','class'=>'control-label']);?>
										<?php echo form_error('entity_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'entity_master_edit','id'=>'','value'=>set_value('entity_master_edit')]);?>
                                        <input type="checkbox" name="entity_master_edit" <?php if($list->entity_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Entity Master Edit','entity_master_edit',['for'=>'entity_master_edit','class'=>'control-label']);?>
										<?php echo form_error('entity_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'entity_master_delete','id'=>'','value'=>set_value('entity_master_delete')]);?>
                                        <input type="checkbox" name="entity_master_delete" <?php if($list->entity_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Entity Master Delete','entity_master_delete',['for'=>'entity_master_delete','class'=>'control-label']);?>
										<?php echo form_error('entity_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
									<?php //echo form_checkbox(['name'=>'workpackage_master_view','id'=>'','value'=>set_value('workpackage_master_view')]);?>
                                    <input type="checkbox" name="workpackage_master_view" <?php if($list->work_package_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Workpackage Master View','workpackage_master_view',['for'=>'workpackage_master_view','class'=>'control-label']);?>
										<?php echo form_error('workpackage_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'workpackage_master_add','id'=>'','value'=>set_value('workpackage_master_add')]);?>
                                        <input type="checkbox" name="workpackage_master_add" <?php if($list->work_package_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Workpackage Master Add','workpackage_master_add',['for'=>'workpackage_master_add','class'=>'control-label']);?>
										<?php echo form_error('workpackage_master_add'); ?>
                                    </td>
                                    <td>                                   
									<?php //echo form_checkbox(['name'=>'workpackage_master_edit','id'=>'','value'=>set_value('workpackage_master_edit')]);?>
                                    <input type="checkbox" name="workpackage_master_edit" <?php if($list->work_package_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Workpackage Master Edit','workpackage_master_edit',['for'=>'workpackage_master_edit','class'=>'control-label']);?>
										<?php echo form_error('workpackage_master_edit'); ?>
                                    </td>
                                    <td>                                   
								<?php //echo form_checkbox(['name'=>'workpackage_master_delete','id'=>'','value'=>set_value('workpackage_master_delete')]);?>
                                <input type="checkbox" name="workpackage_master_delete" <?php if($list->work_package_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                     <?php echo form_label('Workpackage Master Delete','workpackage_master_delete',['for'=>'workpackage_master_delete','class'=>'control-label']);?>
										<?php echo form_error('workpackage_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'designation_master_view','id'=>'','value'=>set_value('designation_master_view')]);?>
                                        <input type="checkbox" name="designation_master_view" <?php if($list->designation_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Designation Master View','designation_master_view',['for'=>'designation_master_view','class'=>'control-label']);?>
										<?php echo form_error('designation_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'designation_master_add','id'=>'','value'=>set_value('designation_master_add')]);?>
                                        <input type="checkbox" name="designation_master_add" <?php if($list->designation_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Designation Master Add','designation_master_add',['for'=>'designation_master_add','class'=>'control-label']);?>
										<?php echo form_error('designation_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'designation_master_edit','id'=>'','value'=>set_value('designation_master_edit')]);?>
                                        <input type="checkbox" name="designation_master_edit" <?php if($list->designation_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Designation Master Edit','designation_master_edit',['for'=>'designation_master_edit','class'=>'control-label']);?>
										<?php echo form_error('designation_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'designation_master_delete','id'=>'','value'=>set_value('designation_master_delete')]);?>
                                        <input type="checkbox" name="designation_master_delete" <?php if($list->designation_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Designation Master Delete','designation_master_delete',['for'=>'designation_master_delete','class'=>'control-label']);?>
										<?php echo form_error('designation_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'department_master_view','id'=>'','value'=>set_value('department_master_view')]);?>
                                        <input type="checkbox" name="department_master_view" <?php if($list->department_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Department Master View','department_master_view',['for'=>'department_master_view','class'=>'control-label']);?>
										<?php echo form_error('department_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'department_master_add','id'=>'','value'=>set_value('department_master_add')]);?>
                                        <input type="checkbox" name="department_master_add" <?php if($list->department_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Department Master Add','department_master_add',['for'=>'department_master_add','class'=>'control-label']);?>
										<?php echo form_error('department_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'department_master_edit','id'=>'','value'=>set_value('department_master_edit')]);?>
                                        <input type="checkbox" name="department_master_edit" <?php if($list->department_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Department Master Edit','department_master_edit',['for'=>'department_master_edit','class'=>'control-label']);?>
										<?php echo form_error('department_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'department_master_delete','id'=>'','value'=>set_value('department_master_delete')]);?>
                                        <input type="checkbox" name="department_master_delete" <?php if($list->department_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Department Master Delete','department_master_delete',['for'=>'department_master_delete','class'=>'control-label']);?>
										<?php echo form_error('department_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'new_user_master_view','id'=>'','value'=>set_value('new_user_master_view')]);?>
                                        <input type="checkbox" name="new_user_master_view" <?php if($list->user_registration_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('New User Master View','new_user_master_view',['for'=>'new_user_master_view','class'=>'control-label']);?>
										<?php echo form_error('new_user_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'new_user_master_add','id'=>'','value'=>set_value('new_user_master_add')]);?>
                                        <input type="checkbox" name="new_user_master_add" <?php if($list->user_registration_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('New User Master Add','new_user_master_add',['for'=>'new_user_master_add','class'=>'control-label']);?>
										<?php echo form_error('new_user_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'new_user_master_edit','id'=>'','value'=>set_value('new_user_master_edit')]);?>
                                        <input type="checkbox" name="new_user_master_edit" <?php if($list->user_registration_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('New User Master Edit','new_user_master_edit',['for'=>'new_user_master_edit','class'=>'control-label']);?>
										<?php echo form_error('new_user_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'new_user_master_delete','id'=>'','value'=>set_value('new_user_master_delete')]);?>
                                        <input type="checkbox" name="new_user_master_delete" <?php if($list->user_registration_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('New User Master Delete','new_user_master_delete',['for'=>'new_user_master_delete','class'=>'control-label']);?>
										<?php echo form_error('new_user_master_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'user_auth_master_view','id'=>'','value'=>set_value('user_auth_master_view')]);?>
                                        <input type="checkbox" name="user_auth_master_view" <?php if($list->usermenu_auth_master_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('User Authenticate Master View','user_auth_master_view',['for'=>'user_auth_master_view','class'=>'control-label']);?>
										<?php echo form_error('user_auth_master_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'user_auth_master_add','id'=>'','value'=>set_value('user_auth_master_add')]);?>
                                        <input type="checkbox" name="user_auth_master_add" <?php if($list->usermenu_auth_master_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('User Authenticate Master Add','user_auth_master_add',['for'=>'user_auth_master_add','class'=>'control-label']);?>
										<?php echo form_error('user_auth_master_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'user_auth_master_edit','id'=>'','value'=>set_value('user_auth_master_edit')]);?>
                                        <input type="checkbox" name="user_auth_master_edit" <?php if($list->usermenu_auth_master_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('User Authenticate Master Edit','user_auth_master_edit',['for'=>'user_auth_master_edit','class'=>'control-label']);?>
										<?php echo form_error('user_auth_master_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'user_auth_master_delete','id'=>'','value'=>set_value('user_auth_master_delete')]);?>
                                        <input type="checkbox" name="user_auth_master_delete" <?php if($list->usermenu_auth_master_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('User Authenticate Master Delete','user_auth_master_delete',['for'=>'user_auth_master_delete','class'=>'control-label']);?>
										<?php echo form_error('user_auth_master_delete'); ?>
                                    </td>
                                </tr>
                           </table>	
                           <div class="well well-sm">HR</div>
                           <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'employee_hr_view','id'=>'','value'=>set_value('employee_hr_view')]);?>
                                        <input type="checkbox" name="employee_hr_view" <?php if($list->employee_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Employee View','employee_hr_view',['for'=>'employee_hr_view','class'=>'control-label']);?>
										<?php echo form_error('employee_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'employee_hr_add','id'=>'','value'=>set_value('employee_hr_add')]);?>
                                        <input type="checkbox" name="employee_hr_add" <?php if($list->employee_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Employee Add','employee_hr_add',['for'=>'employee_hr_add','class'=>'control-label']);?>
										<?php echo form_error('employee_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'employee_hr_edit','id'=>'','value'=>set_value('employee_hr_edit')]);?>
                                        <input type="checkbox" name="employee_hr_edit" <?php if($list->employee_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Employee Edit','employee_hr_edit',['for'=>'employee_hr_edit','class'=>'control-label']);?>

										<?php echo form_error('employee_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'employee_hr_delete','id'=>'','value'=>set_value('employee_hr_delete')]);?>
                                        <input type="checkbox" name="employee_hr_delete" <?php if($list->employee_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Employee Delete','employee_hr_delete',['for'=>'employee_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('empployee_hr_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'outsrc_employee_hr_view','id'=>'','value'=>set_value('outsrc_employee_hr_view')]);?>
                                        <input type="checkbox" name="outsrc_employee_hr_view" <?php if($list->outs_employee_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Outsource Employee View','outsrc_employee_hr_view',['for'=>'outsrc_employee_hr_view','class'=>'control-label']);?>
										<?php echo form_error('outsrc_employee_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'outsrc_employee_hr_add','id'=>'','value'=>set_value('outsrc_employee_hr_add')]);?>
                                        <input type="checkbox" name="outsrc_employee_hr_add" <?php if($list->outs_employee_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Outsource Employee Add','outsrc_employee_hr_add',['for'=>'outsrc_employee_hr_add','class'=>'control-label']);?>
										<?php echo form_error('outsrc_employee_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'outsrc_employee_hr_edit','id'=>'','value'=>set_value('outsrc_employee_hr_edit')]);?>
                                        <input type="checkbox" name="outsrc_employee_hr_edit" <?php if($list->outs_employee_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Outsource Employee Edit','outsrc_employee_hr_edit',['for'=>'outsrc_employee_hr_edit','class'=>'control-label']);?>
										<?php echo form_error('outsrc_employee_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'outsrc_employee_hr_delete','id'=>'','value'=>set_value('outsrc_employee_hr_delete')]);?>
                                        <input type="checkbox" name="outsrc_employee_hr_delete" <?php if($list->outs_employee_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Outsource Employee Delete','outsrc_employee_hr_delete',['for'=>'outsrc_employee_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('empployee_hr_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_hr_view','id'=>'','value'=>set_value('expenses_hr_view')]);?>
                                        <input type="checkbox" name="expenses_hr_view" <?php if($list->expensemaster_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses View','expenses_hr_view',['for'=>'expenses_hr_view','class'=>'control-label']);?>
										<?php echo form_error('expenses_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_hr_add','id'=>'','value'=>set_value('expenses_hr_add')]);?>
                                        <input type="checkbox" name="expenses_hr_add" <?php if($list->expensemaster_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Add','expenses_hr_add',['for'=>'expenses_hr_add','class'=>'control-label']);?>
										<?php echo form_error('expenses_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_hr_edit','id'=>'','value'=>set_value('expenses_hr_edit')]);?>
                                        <input type="checkbox" name="expenses_hr_edit" <?php if($list->expensemaster_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Edit','expenses_hr_edit',['for'=>'expenses_hr_edit','class'=>'control-label']);?>
										<?php echo form_error('expenses_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_hr_delete','id'=>'','value'=>set_value('expenses_hr_delete')]);?>
                                        <input type="checkbox" name="expenses_hr_delete" <?php if($list->expensemaster_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Delete','expenses_hr_delete',['for'=>'expenses_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('expenses_hr_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_apprvl_hr_view','id'=>'','value'=>set_value('expenses_apprvl_hr_view')]);?>
                                        <input type="checkbox" name="expenses_apprvl_hr_view" <?php if($list->expenseapproval_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Approval View','expenses_apprvl_hr_view',['for'=>'expenses_apprvl_hr_view','class'=>'control-label']);?>
										<?php echo form_error('expenses_apprvl_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_apprvl_hr_add','id'=>'','value'=>set_value('expenses_apprvl_hr_add')]);?>
                                        <input type="checkbox" name="expenses_apprvl_hr_add" <?php if($list->expenseapproval_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Approval Add','expenses_apprvl_hr_add',['for'=>'expenses_apprvl_hr_add','class'=>'control-label']);?>
										<?php echo form_error('expenses_apprvl_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_apprvl_hr_edit','id'=>'','value'=>set_value('expenses_apprvl_hr_edit')]);?>
                                        <input type="checkbox" name="expenses_apprvl_hr_edit" <?php if($list->expenseapproval_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Approval Edit','expenses_apprvl_hr_edit',['for'=>'expenses_apprvl_hr_edit','class'=>'control-label']);?>
										<?php echo form_error('expenses_apprvl_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'expenses_apprvl_hr_delete','id'=>'','value'=>set_value('expenses_apprvl_hr_delete')]);?>
                                        <input type="checkbox" name="expenses_apprvl_hr_delete" <?php if($list->expenseapproval_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Expenses Approval Delete','expenses_apprvl_hr_delete',['for'=>'expenses_apprvl_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('expenses_apprvl_hr_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'acc_stat_hr_view','id'=>'','value'=>set_value('acc_stat_hr_view')]);?>
                                        <input type="checkbox" name="acc_stat_hr_view" <?php if($list->accountstatus_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Account Status View','acc_stat_hr_view',['for'=>'acc_stat_hr_view','class'=>'control-label']);?>
										<?php echo form_error('acc_stat_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'acc_stat_hr_add','id'=>'','value'=>set_value('acc_stat_hr_add')]);?>
                                        <input type="checkbox" name="acc_stat_hr_add" <?php if($list->accountstatus_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Account Status Add','acc_stat_hr_add',['for'=>'acc_stat_hr_add','class'=>'control-label']);?>
										<?php echo form_error('acc_stat_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'acc_stat_hr_edit','id'=>'','value'=>set_value('acc_stat_hr_edit')]);?>
                                        <input type="checkbox" name="acc_stat_hr_edit" <?php if($list->accountstatus_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Account Status Edit','acc_stat_hr_edit',['for'=>'acc_stat_hr_edit','class'=>'control-label']);?>
										<?php echo form_error('acc_stat_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'acc_stat_hr_delete','id'=>'','value'=>set_value('acc_stat_hr_delete')]);?>
                                        <input type="checkbox" name="acc_stat_hr_delete" <?php if($list->accountstatus_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Account Status Delete','acc_stat_hr_delete',['for'=>'acc_stat_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('acc_stat_hr_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'daily_info_hr_view','id'=>'','value'=>set_value('daily_info_hr_view')]);?>
                                        <input type="checkbox" name="daily_info_hr_view" <?php if($list->dailyinfo_hr_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Daily Information View','daily_info_hr_view',['for'=>'daily_info_hr_view','class'=>'control-label']);?>
										<?php echo form_error('daily_info_hr_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'daily_info_hr_add','id'=>'','value'=>set_value('daily_info_hr_add')]);?>
                                        <input type="checkbox" name="daily_info_hr_add" <?php if($list->dailyinfo_hr_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Daily Information Add','daily_info_hr_add',['for'=>'daily_info_hr_add','class'=>'control-label']);?>
										<?php echo form_error('daily_info_hr_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'daily_info_hr_edit','id'=>'','value'=>set_value('daily_info_hr_edit')]);?>
                                        <input type="checkbox" name="daily_info_hr_edit" <?php if($list->dailyinfo_hr_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Daily Information Edit','daily_info_hr_edit',['for'=>'daily_info_hr_edit','class'=>'control-label']);?>
										<?php echo form_error('daily_info_hr_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'daily_info_hr_delete','id'=>'','value'=>set_value('daily_info_hr_delete')]);?>
                                        <input type="checkbox" name="daily_info_hr_delete" <?php if($list->dailyinfo_hr_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Daily Information Delete','daily_info_hr_delete',['for'=>'daily_info_hr_delete','class'=>'control-label']);?>
										<?php echo form_error('daily_info_hr_delete'); ?>
                                    </td>
                                </tr>   
                           </table>
                           <div class="well well-sm">Project</div>
                           <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'project_prjt_view','id'=>'','value'=>set_value('project_prjt_view')]);?>
                                        <input type="checkbox" name="project_prjt_view" <?php if($list->projects_project_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Project View','project_prjt_view',['for'=>'project_prjt_view','class'=>'control-label']);?>
										<?php echo form_error('project_prjt_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'project_prjt_add','id'=>'','value'=>set_value('project_prjt_add')]);?>
                                        <input type="checkbox" name="project_prjt_add" <?php if($list->projects_project_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Project Add','project_prjt_add',['for'=>'project_prjt_add','class'=>'control-label']);?>
										<?php echo form_error('project_prjt_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'project_prjt_edit','id'=>'','value'=>set_value('project_prjt_edit')]);?>
                                        <input type="checkbox" name="project_prjt_edit" <?php if($list->projects_project_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Project Edit','project_prjt_edit',['for'=>'project_prjt_edit','class'=>'control-label']);?>
										<?php echo form_error('project_prjt_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'project_prjt_delete','id'=>'','value'=>set_value('project_prjt_delete')]);?>
                                        <input type="checkbox" name="project_prjt_delete" <?php if($list->projects_project_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Project Delete','project_prjt_delete',['for'=>'project_prjt_delete','class'=>'control-label']);?>
										<?php echo form_error('project_prjt_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'customer_po_prjt_view','id'=>'','value'=>set_value('customer_po_prjt_view')]);?>
                                        <input type="checkbox" name="customer_po_prjt_view" <?php if($list->customerpo_project_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer PO View','customer_po_prjt_view',['for'=>'customer_po_prjt_view','class'=>'control-label']);?>
										<?php echo form_error('customer_po_prjt_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_po_prjt_add','id'=>'','value'=>set_value('customer_po_prjt_add')]);?>
                                        <input type="checkbox" name="customer_po_prjt_add" <?php if($list->customerpo_project_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer PO Add','customer_po_prjt_add',['for'=>'customer_po_prjt_add','class'=>'control-label']);?>
										<?php echo form_error('customer_po_prjt_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_po_prjt_edit','id'=>'','value'=>set_value('customer_po_prjt_edit')]);?>
                                        <input type="checkbox" name="customer_po_prjt_edit" <?php if($list->customerpo_project_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer PO Edit','customer_po_prjt_edit',['for'=>'customer_po_prjt_edit','class'=>'control-label']);?>
										<?php echo form_error('customer_po_prjt_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'customer_po_prjt_delete','id'=>'','value'=>set_value('customer_po_prjt_delete')]);?>
                                        <input type="checkbox" name="customer_po_prjt_delete" <?php if($list->customerpo_project_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Customer PO Delete','customer_po_prjt_delete',['for'=>'customer_po_prjt_delete','class'=>'control-label']);?>
										<?php echo form_error('customer_po_prjt_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'work_status_prjt_view','id'=>'','value'=>set_value('work_status_prjt_view')]);?>
                                        <input type="checkbox" name="work_status_prjt_view" <?php if($list->workstatus_project_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Work Status View','work_status_prjt_view',['for'=>'work_status_prjt_view','class'=>'control-label']);?>
										<?php echo form_error('work_status_prjt_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'work_status_prjt_add','id'=>'','value'=>set_value('work_status_prjt_add')]);?>
                                        <input type="checkbox" name="work_status_prjt_add" <?php if($list->workstatus_project_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Work Status Add','work_status_prjt_add',['for'=>'work_status_prjt_add','class'=>'control-label']);?>
										<?php echo form_error('work_status_prjt_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'work_status_prjt_edit','id'=>'','value'=>set_value('work_status_prjt_edit')]);?>
                                        <input type="checkbox" name="work_status_prjt_edit" <?php if($list->workstatus_project_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Work Status Edit','work_status_prjt_edit',['for'=>'work_status_prjt_edit','class'=>'control-label']);?>
										<?php echo form_error('work_status_prjt_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'work_status_prjt_delete','id'=>'','value'=>set_value('work_status_prjt_delete')]);?>
                                        <input type="checkbox" name="work_status_prjt_delete" <?php if($list->workstatus_project_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Work Status Delete','work_status_prjt_delete',['for'=>'work_status_prjt_delete','class'=>'control-label']);?>
										<?php echo form_error('work_status_prjt_delete'); ?>
                                    </td>
                                </tr>  
                           </table>
                           <div class="well well-sm">Invoice & Accounts</div>
                           <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'pay_trans_invacc_view','id'=>'','value'=>set_value('pay_trans_invacc_view')]);?>
                                        <input type="checkbox" name="pay_trans_invacc_view" <?php if($list->paymenttrans_invoice_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Payment Transfer View','pay_trans_invacc_view',['for'=>'pay_trans_invacc_view','class'=>'control-label']);?>
										<?php echo form_error('pay_trans_invacc_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'pay_trans_invacc_add','id'=>'','value'=>set_value('pay_trans_invacc_add')]);?>
                                        <input type="checkbox" name="pay_trans_invacc_add" <?php if($list->paymenttrans_invoice_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Payment Transfer Add','pay_trans_invacc_add',['for'=>'pay_trans_invacc_add','class'=>'control-label']);?>
										<?php echo form_error('pay_trans_invacc_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'pay_trans_invacc_edit','id'=>'','value'=>set_value('pay_trans_invacc_edit')]);?>
                                        <input type="checkbox" name="pay_trans_invacc_edit" <?php if($list->paymenttrans_invoice_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Payment Transfer Edit','pay_trans_invacc_edit',['for'=>'pay_trans_invacc_edit','class'=>'control-label']);?>
										<?php echo form_error('pay_trans_invacc_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'pay_trans_invacc_delete','id'=>'','value'=>set_value('pay_trans_invacc_delete')]);?>
                                        <input type="checkbox" name="pay_trans_invacc_delete" <?php if($list->paymenttrans_invoice_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Payment Transfer Delete','pay_trans_invacc_delete',['for'=>'pay_trans_invacc_delete','class'=>'control-label']);?>
										<?php echo form_error('pay_trans_invacc_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toven_view','id'=>'','value'=>set_value('poiss_toven_invacc_view')]);?>
                                        <input type="checkbox" name="poiss_toven_view" <?php if($list->poissue_tovendor_invoice_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to vendor View','poiss_toven_invacc_view',['for'=>'poiss_toven_invacc_view','class'=>'control-label']);?>
										<?php echo form_error('poiss_toven_invacc_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toven_invacc_add','id'=>'','value'=>set_value('poiss_toven_invacc_add')]);?>
                                        <input type="checkbox" name="poiss_toven_invacc_add" <?php if($list->poissue_tovendor_invoice_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to vendor Add','poiss_toven_invacc_add',['for'=>'poiss_toven_invacc_add','class'=>'control-label']);?>
										<?php echo form_error('poiss_toven_invacc_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toven_invacc_edit','id'=>'','value'=>set_value('poiss_toven_invacc_edit')]);?>
                                        <input type="checkbox" name="poiss_toven_invacc_edit" <?php if($list->poissue_tovendor_invoice_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to vendor Edit','poiss_toven_invacc_edit',['for'=>'poiss_toven_invacc_edit','class'=>'control-label']);?>
										<?php echo form_error('poiss_toven_invacc_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toven_invacc_delete','id'=>'','value'=>set_value('poiss_toven_invacc_delete')]);?>
                                        <input type="checkbox" name="poiss_toven_invacc_delete" <?php if($list->poissue_tovendor_invoice_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to vendor Delete','poiss_toven_invacc_delete',['for'=>'poiss_toven_invacc_delete','class'=>'control-label']);?>
										<?php echo form_error('poiss_toven_invacc_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toemp_view','id'=>'','value'=>set_value('poiss_toemp_invacc_view')]);?>
                                        <input type="checkbox" name="poiss_toemp_view" <?php if($list->poissue_toemp_invoice_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to Employee View','poiss_toemp_invacc_view',['for'=>'poiss_toemp_invacc_view','class'=>'control-label']);?>
										<?php echo form_error('poiss_toemp_invacc_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toemp_invacc_add','id'=>'','value'=>set_value('poiss_toemp_invacc_add')]);?>
                                        <input type="checkbox" name="poiss_toemp_invacc_add" <?php if($list->poissue_toemp_invoice_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to Employee Add','poiss_toemp_invacc_add',['for'=>'poiss_toemp_invacc_add','class'=>'control-label']);?>
										<?php echo form_error('poiss_toemp_invacc_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toemp_invacc_edit','id'=>'','value'=>set_value('poiss_toemp_invacc_edit')]);?>
                                        <input type="checkbox" name="poiss_toemp_invacc_edit" <?php if($list->poissue_toemp_invoice_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to Employee Edit','poiss_toemp_invacc_edit',['for'=>'poiss_toemp_invacc_edit','class'=>'control-label']);?>
										<?php echo form_error('poiss_toemp_invacc_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'poiss_toemp_invacc_delete','id'=>'','value'=>set_value('poiss_toemp_invacc_delete')]);?>
                                        <input type="checkbox" name="poiss_toemp_invacc_delete" <?php if($list->poissue_toemp_invoice_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Po issue to Employee Delete','poiss_toemp_invacc_delete',['for'=>'poiss_toemp_invacc_delete','class'=>'control-label']);?>
										<?php echo form_error('poiss_toemp_invacc_delete'); ?>
                                    </td>
                                </tr>   
                           </table>
                           <div class="well well-sm">Inventory</div>
                           <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'purchase_inv_view','id'=>'','value'=>set_value('purchase_inv_view')]);?>
                                        <input type="checkbox" name="purchase_inv_view" <?php if($list->purchaseinv_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Purchase Inventory View','purchase_inv_view',['for'=>'purchase_inv_view','class'=>'control-label']);?>
										<?php echo form_error('purchase_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'purchase_inv_add','id'=>'','value'=>set_value('purchase_inv_add')]);?>
                                        <input type="checkbox" name="purchase_inv_add" <?php if($list->purchaseinv_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Purchase Inventory Add','purchase_inv_add',['for'=>'purchase_inv_add','class'=>'control-label']);?>
										<?php echo form_error('purchase_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'purchase_inv_edit','id'=>'','value'=>set_value('purchase_inv_edit')]);?>
                                        <input type="checkbox" name="purchase_inv_edit" <?php if($list->purchaseinv_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Purchase Inventory Edit','purchase_inv_edit',['for'=>'purchase_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('purchase_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'purchase_inv_delete','id'=>'','value'=>set_value('purchase_inv_delete')]);?>
                                        <input type="checkbox" name="purchase_inv_delete" <?php if($list->purchaseinv_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Purchase Inventory Delete','purchase_inv_delete',['for'=>'purchase_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('purchase_inv_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'add_inv_view','id'=>'','value'=>set_value('add_inv_view')]);?>
                                        <input type="checkbox" name="add_inv_view" <?php if($list->addinv_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Add Inventory View','add_inv_view',['for'=>'add_inv_view','class'=>'control-label']);?>
										<?php echo form_error('add_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'add_inv_add','id'=>'','value'=>set_value('add_inv_add')]);?>
                                        <input type="checkbox" name="add_inv_add" <?php if($list->addinv_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Add Inventory Add','add_inv_add',['for'=>'add_inv_add','class'=>'control-label']);?>
										<?php echo form_error('add_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'add_inv_edit','id'=>'','value'=>set_value('add_inv_edit')]);?>
                                        <input type="checkbox" name="add_inv_edit" <?php if($list->addinv_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Add Inventory Edit','add_inv_edit',['for'=>'add_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('add_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'add_inv_delete','id'=>'','value'=>set_value('add_inv_delete')]);?>
                                        <input type="checkbox" name="add_inv_delete" <?php if($list->addinv_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Add Inventory Delete','add_inv_delete',['for'=>'add_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('add_inv_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'store_trans_inv_view','id'=>'','value'=>set_value('store_trans_inv_view')]);?>
                                        <input type="checkbox" name="store_trans_inv_view" <?php if($list->storetransfer_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Transfer View','store_trans_inv_view',['for'=>'store_trans_inv_view','class'=>'control-label']);?>
										<?php echo form_error('store_trans_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_trans_inv_add','id'=>'','value'=>set_value('store_trans_inv_add')]);?>
                                        <input type="checkbox" name="store_trans_inv_add" <?php if($list->storetransfer_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Transfer Add','store_trans_inv_add',['for'=>'store_trans_inv_add','class'=>'control-label']);?>
										<?php echo form_error('store_trans_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_trans_inv_edit','id'=>'','value'=>set_value('store_trans_inv_edit')]);?>
                                        <input type="checkbox" name="store_trans_inv_edit" <?php if($list->storetransfer_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Transfer Edit','store_trans_inv_edit',['for'=>'store_trans_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('store_trans_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'store_trans_inv_delete','id'=>'','value'=>set_value('store_trans_inv_delete')]);?>
                                        <input type="checkbox" name="store_trans_inv_delete" <?php if($list->storetransfer_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Store Transfer Delete','store_trans_inv_delete',['for'=>'store_trans_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('store_trans_inv_delete'); ?>
                                    </td>
                                </tr> 
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'reciv_store_inv_view','id'=>'','value'=>set_value('reciv_store_inv_view')]);?>
                                        <input type="checkbox" name="reciv_store_inv_view" <?php if($list->receive_bystore_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Recive BY Store View','reciv_store_inv_view',['for'=>'reciv_store_inv_view','class'=>'control-label']);?>
										<?php echo form_error('reciv_store_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'reciv_store_inv_add','id'=>'','value'=>set_value('reciv_store_inv_add')]);?>
                                        <input type="checkbox" name="reciv_store_inv_add" <?php if($list->receive_bystore_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Recive BY Store Add','reciv_store_inv_add',['for'=>'reciv_store_inv_add','class'=>'control-label']);?>
										<?php echo form_error('reciv_store_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'reciv_store_inv_edit','id'=>'','value'=>set_value('reciv_store_inv_edit')]);?>
                                        <input type="checkbox" name="reciv_store_inv_edit" <?php if($list->receive_bystore_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Recive BY Store Edit','reciv_store_inv_edit',['for'=>'reciv_store_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('reciv_store_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'reciv_store_inv_delete','id'=>'','value'=>set_value('reciv_store_inv_delete')]);?>
                                        <input type="checkbox" name="reciv_store_inv_delete" <?php if($list->receive_bystore_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Recive BY Store Delete','reciv_store_inv_delete',['for'=>'reciv_store_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('reciv_store_inv_delete'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'iss_emp_inv_view','id'=>'iss_emp_inv_view','value'=>set_value('iss_emp_inv_view')]);?>
                                        <input type="checkbox" name="iss_emp_inv_view" <?php if($list->issuetoemp_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Issue to Employee View','iss_emp_inv_view',['for'=>'iss_emp_inv_view','class'=>'control-label']);?>
										<?php echo form_error('iss_emp_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'iss_emp_inv_add','id'=>'iss_emp_inv_add','value'=>set_value('iss_emp_inv_add')]);?>
                                        <input type="checkbox" name="iss_emp_inv_add" <?php if($list->issuetoemp_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Issue to Employee Add','iss_emp_inv_add',['for'=>'iss_emp_inv_add','class'=>'control-label']);?>
										<?php echo form_error('iss_emp_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'iss_emp_inv_edit','id'=>'iss_emp_inv_edit','value'=>set_value('iss_emp_inv_edit')]);?>
                                        <input type="checkbox" name="iss_emp_inv_edit" <?php if($list->issuetoemp_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Issue to Employee Edit','iss_emp_inv_edit',['for'=>'iss_emp_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('iss_emp_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'iss_emp_inv_delete','id'=>'iss_emp_inv_delete','value'=>set_value('iss_emp_inv_delete')]);?>
                                        <input type="checkbox" name="iss_emp_inv_delete" <?php if($list->issuetoemp_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Issue to Employee Delete','iss_emp_inv_delete',['for'=>'iss_emp_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('iss_emp_inv_delete'); ?>
                                    </td>
                                </tr> 
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php //echo form_checkbox(['name'=>'rcv_emp_inv_view','id'=>'','value'=>set_value('rcv_emp_inv_view')]);?>
                                        <input type="checkbox" name="rcv_emp_inv_view" <?php if($list->receive_byemp_inventory_view==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Receive By to Employee View','rcv_emp_inv_view',['for'=>'rcv_emp_inv_view','class'=>'control-label']);?>
										<?php echo form_error('rcv_emp_inv_view'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'rcv_emp_inv_add','id'=>'','value'=>set_value('rcv_emp_inv_add')]);?>
                                        <input type="checkbox" name="rcv_emp_inv_add" <?php if($list->receive_byemp_inventory_add==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Receive By Employee Add','rcv_emp_inv_add',['for'=>'rcv_emp_inv_add','class'=>'control-label']);?>
										<?php echo form_error('rcv_emp_inv_add'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'rcv_emp_inv_edit','id'=>'','value'=>set_value('rcv_emp_inv_edit')]);?>
                                        <input type="checkbox" name="rcv_emp_inv_edit" <?php if($list->receive_byemp_inventory_edit==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Receive By Employee Edit','rcv_emp_inv_edit',['for'=>'rcv_emp_inv_edit','class'=>'control-label']);?>
										<?php echo form_error('rcv_emp_inv_edit'); ?>
                                    </td>
                                    <td>                                   
										<?php //echo form_checkbox(['name'=>'rcv_emp_inv_delete','id'=>'','value'=>set_value('rcv_emp_inv_delete')]);?>
                                        <input type="checkbox" name="rcv_emp_inv_delete" <?php if($list->receive_byemp_inventory_delete==1){ echo "checked"; }?> />
										<?=nbs(2);?>
                                        <?php echo form_label('Receive By Employee Delete','rcv_emp_inv_delete',['for'=>'rcv_emp_inv_delete','class'=>'control-label']);?>
										<?php echo form_error('rcv_emp_inv_delete'); ?>
                                    </td>
                                </tr>   
                           </table>
                    </div>
                </div>
            </div>
        <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>