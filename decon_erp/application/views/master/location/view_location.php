<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View Location</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/location','Location')?></li>
        <li class="active">View Location</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('location/view',['name'=>'location_form_view','id'=>'location_form_view']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('location/create','<i class="fa fa-fw fa-plus-square"></i> Add Location',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("location/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Location',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('location','<i class="fa fa-fw fa-arrows"></i> Location List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('city','city',['for'=>'city']);?><?php echo form_error('city_id'); ?>
                    				<?php $options=array(''=>'Select City','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('city_id',$contlist,set_value('city_id',$list->id),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>
                                
                        		<div class="form-group">
									<?php echo form_label('Location','location',['for'=>'location']);?><?php echo form_error('location'); ?>
                    				<?php echo form_input(['name'=>'location','class'=>'form-control','id'=>'location','placeholder'=>'Enter Location here',
									'value'=>set_value('location',$list->location),'disabled'=>'disabled']);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>