<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View Company Make</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/company','Company')?></li>
        <li class="active">View Company Make</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("company/view/{$list->id}",['name'=>'company_form_add','id'=>'company_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('company/create','<i class="fa fa-fw fa-plus-square"></i> Add Company',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("company/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Company',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('company','<i class="fa fa-fw fa-arrows"></i> Company List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Company Make Name','make_name',['for'=>'make_name']);?><?php echo form_error('make_name'); ?>
                    				<?php echo form_input(['name'=>'make_name','class'=>'form-control','id'=>'make_name','placeholder'=>'Enter Company Make Name here',
									'value'=>set_value('make_name',strtoupper($list->make_name)),'disabled'=>'disabled']);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>