<?php print_r(json_decode(file_get_contents('php://input'))); ?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Customer</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/customer','Customer')?></li>
        <li class="active">Add Customer</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('customer/create',['name'=>'customer_form_add','id'=>'customer_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('customer','<i class="fa fa-fw fa-arrows"></i> Customer List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <?php /*?><?php echo form_open('add_customer',['name'=>'customer_form_add','id'=>'customer_form_add']);?>
          		<?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?><?php */?>
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Company Code','comp_code',['for'=>'comp_code']);?><?php echo form_error('comp_code'); ?>
                    				<?php echo form_input(['name'=>'comp_code','class'=>'form-control','id'=>'company_code','placeholder'=>'Enter Company Code here',
									'value'=>set_value('comp_code')]);?>
                                </div>	
                                <div class="form-group">
									<?php echo form_label('Company Name','comp_name',['for'=>'comp_name']);?><?php echo form_error('comp_name'); ?>
                                    <?php echo form_input(['name'=>'comp_name','class'=>'form-control','id'=>'company_name','placeholder'=>'Enter Company Name here',
									'value'=>set_value('comp_name')]);?>
                              	</div>
                                <div class="form-group">
                                     <?php echo form_label('Tin','tin',['for'=>'tin']);?><?php echo form_error('tin'); ?>
									 <?php echo form_input(['name'=>'tin','class'=>'form-control','id'=>'tin','placeholder'=>'Enter Tin Here','value'=>set_value('tin')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Address','address',['for'=>'address']);?><?php echo form_error('address'); ?>
                                    <?php echo form_textarea(['name'=>'address','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Address Here','value'=>set_value('address')]);?>
                              	</div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                    <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks')]);?>
                              	</div><!-- /.form-group -->
                                <div class="well well-sm"><strong>Customer Contact Details</strong></div>
                                <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                                <div ng-app="productsTableApp">
                                  <div ng-controller="ProductsController">
            						<?php echo form_button('Add','<i class="fa fa-fw fa-plus-square"></i> Add Qualification',['class'=>'btn btn-block btn-success add',
                                    'style'=>'width:180px;']); ?>           
                <table id="participantTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Department</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                    <tr class="participantRow">
                        <td><?php echo form_input(['name'=>'name','class'=>'form-control','id'=>'name','style'=>'width:200px;','placeholder'=>'Enter Name here',
						'value'=>set_value('name')]);?></td>
                        <td><?php $options=array(''=>'Select Designation','1'=>'Purchase','2'=>'Rent'); ?> 
                            <?php echo form_dropdown('designation',$designation_list,set_value('designation'),['class'=>'form-control','style'=>'width:200px;']);?>
                        </td>
                        <td>
                            <?php $options=array(''=>'Select Department','1'=>'Purchase','2'=>'Rent');  ?>
                            <?php echo form_dropdown('department',$department_list,set_value('department'),['class'=>'form-control','style'=>'width:200px;']);?>
                        </td>
                        <td>
                            <?php echo form_input(['name'=>'mobile','class'=>'form-control','id'=>'mobile','style'=>'width:200px;','placeholder'=>'Enter 10 digit mobile no here',
							'value'=>set_value('mobile')]);?>
                        </td>
                        <td><?php echo form_input(['name'=>'email','class'=>'form-control','id'=>'email','style'=>'width:200px;','placeholder'=>'Enter Email here',
						'value'=>set_value('email')]);?></td>
                        <td> <?php echo form_button('Remove','<i class="fa fa-fw fa-remove"></i>',['class'=>'btn btn-block btn-danger remove',
                            'style'=>'width:50px;']); ?> 
                        </td>
                      
                    </tr>
                    <tr>
                        <td><?php echo form_error('name'); ?></td>
                        <td><?php echo form_error('designation'); ?></td>
                        <td><?php echo form_error('department'); ?></td>
                        <td><?php echo form_error('mobile'); ?></td>
                        <td><?php echo form_error('email'); ?></td>
                        <td></td>
                    </tr>
                </table>
            </div> </div></div>
        </div>
    </div>
</div>
<hr />

<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>

