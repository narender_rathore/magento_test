<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Region Master</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Region Master</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
        
          <?php echo form_open('region/delete');?>
          <?php echo form_hidden('region_id',$list->id);?>
      
          <?php /*?><?php echo form_submit(['name'=>'submit','value'=>'Delete','class'=>'btn btn-default pull-right',
		  style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
          <?php echo anchor('region/create','<i class="fa fa-fw fa-plus-square"></i> Add Region',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Region</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th><?php */?>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(3);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  strtoupper($lists->region);?></td>
              <td>
					<?php echo anchor("region/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Region']); ?><?php nbs(3);?>
                    <?php echo anchor("region/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Region']); ?>	
                    <?php echo anchor("region/delete/{$lists->id}",'<i class="fa fa-fw fa-scissors"></i>',['title'=>'Delete Region','onClick' => "return confirm('Are you sure you want to delete?')"]); ?>
              </td>
              <?php /*?><td><?php echo form_checkbox(['name'=>'checkbox[]','id'=>'checkbox[]','type'=>'checkbox','class'=>'checkboxes','value'=>$lists->id]);?></td><?php */?>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Region</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th><?php */?>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php $count++; echo  $this->pagination->create_links(); ?>
      </div>
    </div>
  </div>
</div>
</section>
</div>

