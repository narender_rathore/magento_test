<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Inventory Item</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/item','Inventory Item')?></li>
        <li class="active">Add Inventory Item</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('item/create',['name'=>'item_form_add','id'=>'item_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('item','<i class="fa fa-fw fa-arrows"></i> Item List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_label('Item Name','item_name',['for'=>'item_name']);?><?php echo form_error('item_name'); ?>
                                <?php echo form_input(['name'=>'item_name','class'=>'form-control','id'=>'item_name','placeholder'=>'Enter Item Name here',
                                'value'=>set_value('item_name')]);?>
                            </div>
                            <div class="well well-sm">Check which you want to show</div>
                            <table width="100%">
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'model_no','id'=>'model_no','value'=>set_value('model_no','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Model No.','model_no',['for'=>'model_no','class'=>'control-label']);?><?php echo form_error('model_no'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'ram','id'=>'ram','value'=>set_value('ram','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Ram.','ram',['for'=>'ram','class'=>'control-label']);?><?php echo form_error('ram'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'battery','id'=>'battery','value'=>set_value('battery','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Battery','battery',['for'=>'battery','class'=>'control-label']);?><?php echo form_error('battery'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'laptop_charger','id'=>'laptop_charger','value'=>set_value('laptop_charger','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Laptop Charger','laptop_charger',['for'=>'laptop_charger','class'=>'control-label']);?>
										<?php echo form_error('laptop_charger'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'antivirus_key','id'=>'antivirus_key','value'=>set_value('antivirus_key','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Antivirus Key','antivirus_key',['for'=>'antivirus_key','class'=>'control-label']);?>
										<?php echo form_error('antivirus_key'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'hard_disk','id'=>'hard_disk','value'=>set_value('hard_disk','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Hard Disk','hard_disk',['for'=>'hard_disk','class'=>'control-label']);?><?php echo form_error('hard_disk'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'serial_no','id'=>'serial_no','value'=>set_value('serial_no','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Serial No.','serial_no',['for'=>'serial_no','class'=>'control-label']);?><?php echo form_error('serial_no'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'warranty_date','id'=>'warranty_date','value'=>set_value('warranty_date','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Warranty Date','warranty_date',['for'=>'warranty_date','class'=>'control-label']);?>
										<?php echo form_error('warranty_date'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'software','id'=>'software','value'=>set_value('software','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Software','software',['for'=>'software','class'=>'control-label']);?><?php echo form_error('software'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'chargable','id'=>'chargable','value'=>set_value('chargable','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Chargable','chargable',['for'=>'chargable','class'=>'control-label']);?><?php echo form_error('chargable'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'zoom','id'=>'zoom','value'=>set_value('zoom','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Zoom','zoom',['for'=>'zoom','class'=>'control-label']);?><?php echo form_error('zoom'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'pixel','id'=>'pixel','value'=>set_value('pixel','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Pixel','model_no',['for'=>'pixel','class'=>'control-label']);?><?php echo form_error('pixel'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'length','id'=>'length','value'=>set_value('length','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Length','length',['for'=>'length','class'=>'control-label']);?><?php echo form_error('length'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'imei_no','id'=>'imei_no','value'=>set_value('imei_no','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('IMEI No','imei_no',['for'=>'imei_no','class'=>'control-label']);?>
										<?php echo form_error('imei_no'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'memory_card','id'=>'memory_card','value'=>set_value('memory_card','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Memory Card','memory_card',['for'=>'memory_card','class'=>'control-label']);?>
										<?php echo form_error('memory_card'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'calibration_info','id'=>'calibration_info','value'=>set_value('calibration_info','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Calibration Info','calibration_info',['for'=>'calibration_info','class'=>'control-label']);?>
										<?php echo form_error('calibration_info'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'dongle','id'=>'dongle','value'=>set_value('dongle','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Dongle','dongle',['for'=>'dongle','class'=>'control-label']);?><?php echo form_error('dongle'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'ear_plug','id'=>'ear_plug','value'=>set_value('ear_plug','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Ear Plug','ear_plug',['for'=>'ear_plug','class'=>'control-label']);?><?php echo form_error('ear_plug'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'goggles','id'=>'goggles','value'=>set_value('goggles','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Goggles','goggles',['for'=>'goggles','class'=>'control-label']);?>
										<?php echo form_error('goggles'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'gloves','id'=>'gloves','value'=>set_value('gloves','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Gloves','gloves',['for'=>'gloves','class'=>'control-label']);?>
										<?php echo form_error('gloves'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'shoes','id'=>'shoes','value'=>set_value('shoes','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Shoes','shoes',['for'=>'shoes','class'=>'control-label']);?><?php echo form_error('shoes'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'helmet','id'=>'helmet','value'=>set_value('helmet','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Helmet','helmet',['for'=>'helmet','class'=>'control-label']);?><?php echo form_error('helmet'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'full_body','id'=>'full_body','value'=>set_value('full_body','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Full Body','full_body',['for'=>'full_body','class'=>'control-label']);?><?php echo form_error('full_body'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'half_body','id'=>'half_body','value'=>set_value('half_body','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Half Body','half_body',['for'=>'half_body','class'=>'control-label']);?>
										<?php echo form_error('half_body'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'lan_card','id'=>'lan_card','value'=>set_value('lan_card','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Lan Card','lan_card',['for'=>'lan_card','class'=>'control-label']);?>
										<?php echo form_error('lan_card'); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td><?=nbs(10);?></td>
                                	<td>                                   
										<?php echo form_checkbox(['name'=>'fall_arrestor','id'=>'fall_arrestor','value'=>set_value('fall_arrestor','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Fall Arrestor','fall_arrestor',['for'=>'fall_arrestor','class'=>'control-label']);?>
										<?php echo form_error('fall_arrestor'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'first_aid_box','id'=>'first_aid_box','value'=>set_value('first_aid_box','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('First Aid Boxs','first_aid_box',['for'=>'first_aid_box','class'=>'control-label']);?>
										<?php echo form_error('first_aid_boxam'); ?>
                                    </td>
                                    <td>                                   
										<?php echo form_checkbox(['name'=>'reflective_jacket','id'=>'reflective_jacket','value'=>set_value('reflective_jacket','1')]);?><?=nbs(2);?>
                                        <?php echo form_label('Refractive Jacket','reflective_jacket',['for'=>'reflective_jacket','class'=>'control-label']);?>
										<?php echo form_error('reflective_jacket'); ?>
                                    </td>
                                    <td>                                   
										<?php /*?><?php echo form_checkbox(['name'=>'laptop_charger','id'=>'laptop_charger','value'=>set_value('laptop_charger')]);?><?=nbs(2);?>
                                        <?php echo form_label('Laptop Charger','laptop_charger',['for'=>'laptop_charger','class'=>'control-label']);?>
										<?php echo form_error('laptop_charger'); ?><?php */?>
                                    </td>
                                    <td>                                   
										<?php /*?><?php echo form_checkbox(['name'=>'antivirus_key','id'=>'antivirus_key','value'=>set_value('antivirus_key')]);?><?=nbs(2);?>
                                        <?php echo form_label('Antivirus Key','antivirus_key',['for'=>'antivirus_key','class'=>'control-label']);?>
										<?php echo form_error('antivirus_key'); ?><?php */?>
                                    </td>
                                </tr>
                             </table>             
        				</div>
                    </div>
                </div>
<hr />
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>