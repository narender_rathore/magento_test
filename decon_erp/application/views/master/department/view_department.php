<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View Department</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/department','Department')?></li>
        <li class="active">view Department</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('department/create',['name'=>'department_form_add','id'=>'department_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('department/create','<i class="fa fa-fw fa-plus-square"></i> Add Department',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("department/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Department',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?> 
				  <?php echo anchor('department','<i class="fa fa-fw fa-arrows"></i> Department List',['class'=>'btn btn-default pull-right',
				  'style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Department','department',['for'=>'department']);?><?php echo form_error('department'); ?>
                    				<?php echo form_input(['name'=>'department','class'=>'form-control','id'=>'department','placeholder'=>'Enter department Code here',
									'value'=>set_value('department',ucwords($list->department)),'disabled'=>'disabled']);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>