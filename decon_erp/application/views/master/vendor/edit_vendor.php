<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit Vendor</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/vendor','Vendor')?></li>
        <li class="active">Edit Vendor</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("vendor/edit/{$list->id}",['name'=>'vendor_form_add','id'=>'vendor_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('vendor/create','<i class="fa fa-fw fa-plus-square"></i> Add Vendor',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('vendor','<i class="fa fa-fw fa-arrows"></i> Vendor List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_label('Vendor Code','vendor_code',['for'=>'vendor_code']);?><?php echo form_error('vendor_code'); ?>
                                <?php echo form_input(['name'=>'vendor_code','class'=>'form-control','id'=>'vendor_code','placeholder'=>'Enter vendor Code here',
                                'value'=>set_value('vendor_code',$list->id)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Vendor Name','vendor_name',['for'=>'vendor_name']);?><?php echo form_error('vendor_name'); ?>
                                <?php echo form_input(['name'=>'vendor_name','class'=>'form-control','id'=>'vendor_code','placeholder'=>'Enter vendor Name here',
                                'value'=>set_value('vendor_name',$list->vendor_name)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Tan','tan',['for'=>'tan']);?><?php echo form_error('tan'); ?>
                                <?php echo form_input(['name'=>'tan','class'=>'form-control','id'=>'tan','placeholder'=>'Enter Tan  here',
                                'value'=>set_value('tan',$list->vendor_tan)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Pan','pan',['for'=>'pan']);?><?php echo form_error('pan'); ?>
                                <?php echo form_input(['name'=>'pan','class'=>'form-control','id'=>'pan','placeholder'=>'Enter Pan Code here',
                                'value'=>set_value('pan',$list->vendor_pan)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Service Tax','sevice_tax',['for'=>'service_tax']);?><?php echo form_error('service_tax'); ?>
                                <?php echo form_input(['name'=>'service_tax','class'=>'form-control','id'=>'service_tax','placeholder'=>'Enter Service Tax here',
                                'value'=>set_value('service_tax',$list->service_tax_no)]);?>
                            </div>
                            <div class="form-group">
									<?php echo form_label('Address','address',['for'=>'address']);?><?php echo form_error('address'); ?>
                                    <?php echo form_textarea(['name'=>'address','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
									'rows'=>'10','placeholder'=>'Enter Address Here','value'=>set_value('address',$list->address)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                                <?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;','cols'=>'40',
                                'rows'=>'10','placeholder'=>'Give Remarks Here','value'=>set_value('remarks',$list->remarks)]);?>
                            </div>
                            
                             <div class="well well-sm"><strong>Vendor Contact Details</strong></div>
                            <div ng-app="productsTableApp">
                                  <div ng-controller="ProductsController">
            						<?php echo form_button('Add','<i class="fa fa-fw fa-plus-square"></i> Add Contact Details',['class'=>'btn btn-block btn-success add',
										'style'=>'width:180px;']); ?>
                                            <table id="participantTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th>Contact Number</th>
                                                    <th>Email</th>
                                                    <th></th>
                                                </tr>
                                                <tr class="participantRow">
                                                    <td>
                                                        <?php echo form_input(['name'=>'contact_no','class'=>'form-control','id'=>'contact_no',
                                                        'placeholder'=>'Enter Contact Number here','value'=>set_value('contact_no',$list->contact_no),'style'=>'width:450px;',]);?>
                                                    </td>
                                                    <td>
                                                        <?php echo form_input(['name'=>'email','class'=>'form-control','id'=>'email',
                                                        'placeholder'=>'Enter Email Address here','value'=>set_value('email',$list->email),'style'=>'width:450px;',]);?>
                                                    </td>
                                                    <td>
                                                        <?php echo form_button('Remove','<i class="fa fa-fw fa-remove"></i>',
                                                            ['class'=>'btn btn-block btn-danger remove','style'=>'width:50px;']); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo form_error('contact_no'); ?></td>
                                                    <td><?php echo form_error('email'); ?></td>
                                                    <td></td>
                                                </tr>
                                             </table>
                            	</div>
                            </div><div class="clearfix"></br></div>                            
                            <div class="well well-sm"><strong>Concerned Person Details</strong></div>
                            <div class="form-group">
                                <?php echo form_label('Name','concern_name',['for'=>'concern_name']);?><?php echo form_error('concern_name'); ?>
                                <?php echo form_input(['name'=>'concern_name','class'=>'form-control','id'=>'concern_name','placeholder'=>'Enter Name here',
								'value'=>set_value('concern_name',$list->concerned_person_name)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Designation','concern_designation',['for'=>'concern_designation']);?><?php echo form_error('concern_designation'); ?>
                                <?php $options=array(''=>'Select Designation','1'=>'Purchase','2'=>'Rent'); ?> 
                            	<?php echo form_dropdown('concern_designation',$designation_list,set_value('concern_designation',$list->concerned_person_designation),
								['class'=>'form-control']);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Contact No','concern_contact_no',['for'=>'concern_contact_no']);?><?php echo form_error('concern_contact_no'); ?>
                                <?php echo form_input(['name'=>'concern_contact_no','class'=>'form-control','id'=>'concern_contact_no','placeholder'=>'Enter Contact Number here',
                                'value'=>set_value('concern_contact_no',$list->concerned_person_contact)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Email','concern_email',['for'=>'concern_email']);?><?php echo form_error('concern_email'); ?>
                                <?php echo form_input(['name'=>'concern_email','class'=>'form-control','id'=>'concern_email','placeholder'=>'Enter Email Address here',
                                'value'=>set_value('concern_email',$list->concerned_person_email)]);?>
                            </div>
                            <div class="well well-sm"><strong>Primary Account Details</strong></div>
                            <div class="form-group">
                                <?php echo form_label('Account No.','prim_account_no',['for'=>'prim_account_no']);?><?php echo form_error('prim_account_no'); ?>
                                <?php echo form_input(['name'=>'prim_account_no','class'=>'form-control','id'=>'prim_account_no','placeholder'=>'Enter Account No. here',
								'value'=>set_value('prim_account_no',$list->primary_account_no)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Name','prim_name',['for'=>'prim_name']);?><?php echo form_error('prim_name'); ?>
                                <?php echo form_input(['name'=>'prim_name','class'=>'form-control','id'=>'prim_name','placeholder'=>'Enter Name here',
								'value'=>set_value('prim_name',$list->primary_account_name)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('IFSC Code','prim_ifsc_code',['for'=>'prim_ifsc_code']);?><?php echo form_error('prim_ifsc_code'); ?>
                                <?php echo form_input(['name'=>'prim_ifsc_code','class'=>'form-control','id'=>'prim_ifsc_code','placeholder'=>'Enter banks\' IFSC Code here',
                                'value'=>set_value('prim_ifsc_code',$list->primary_account_ifsc)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Branch','prim_branch',['for'=>'prim_branch']);?><?php echo form_error('prim_branch'); ?>
                                <?php $options=array(''=>'Select Bank Branch','1'=>'Purchase','2'=>'Rent'); ?> 
                            	<?php echo form_dropdown('prim_branch',$options,set_value('prim_branch',$list->primary_account_branch),['class'=>'form-control']);?>
                            </div>
                            <div class="well well-sm"><strong>Secondary Account Details</strong></div>
                            <div class="form-group">
                                <?php echo form_label('Account No.','sec_account_no',['for'=>'sec_account_no']);?><?php echo form_error('sec_account_no'); ?>
                                <?php echo form_input(['name'=>'sec_account_no','class'=>'form-control','id'=>'sec_account_no','placeholder'=>'Enter Account No. here',
								'value'=>set_value('sec_account_no',$list->secondary_account_no)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Name','sec_name',['for'=>'sec_name']);?><?php echo form_error('sec_name'); ?>
                                <?php echo form_input(['name'=>'sec_name','class'=>'form-control','id'=>'sec_name','placeholder'=>'Enter Name here',
								'value'=>set_value('sec_name',$list->secondary_account_name)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('IFSC Code','sec_ifsc_code',['for'=>'sec_ifsc_code']);?><?php echo form_error('sec_ifsc_code'); ?>
                                <?php echo form_input(['name'=>'sec_ifsc_code','class'=>'form-control','id'=>'sec_ifsc_code','placeholder'=>'Enter banks\' IFSC Code here',
                                'value'=>set_value('ifsc_code',$list->secondary_account_ifsc)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Branch','sec_branch',['for'=>'sec_branch']);?><?php echo form_error('sec_branch'); ?>
                                <?php $options=array(''=>'Select Bank Branch','1'=>'Purchase','2'=>'Rent'); ?> 
                            	<?php echo form_dropdown('sec_branch',$options,set_value('sec_branch',$list->secondary_account_branch),['class'=>'form-control']);?>
                            </div>                            
                        </div>
                    </div>
                </div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>