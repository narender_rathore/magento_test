<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View store</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/store','store')?></li>
        <li class="active">View Store</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("store/view/{$list->id}",['name'=>'store_form_view','id'=>'store_form_view']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('store/create','<i class="fa fa-fw fa-plus-square"></i> Add Store',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("store/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Store',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('store','<i class="fa fa-fw fa-arrows"></i> Store List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Store Name','store',['for'=>'store_code']);?><?php echo form_error('store_code'); ?>
                    				<?php echo form_input(['name'=>'store_code','class'=>'form-control','id'=>'store_code','placeholder'=>'Enter Store Code here',
									'value'=>set_value('store_code',$list->store_code),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Lead Person','lead_person',['for'=>'lead_person']);?><?php echo form_error('lead_person'); ?>
                    				<?php $options=array(''=>'Select Lead person','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('lead_person',$options,set_value('lead_person',$list->lead_person),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Location','location',['for'=>'location']);?><?php echo form_error('location'); ?>
                    				<?php $options=array(''=>'Select Location','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('location',$loc_list,set_value('location',$list->location),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>	          
                            </div>
                        </div>
                    </div>
               	<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>