<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add store</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/store','Store')?></li>
        <li class="active">Add Store</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('store/create',['name'=>'store_form_add','id'=>'store_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('store','<i class="fa fa-fw fa-arrows"></i> Store List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Store Name','store',['for'=>'store_code']);?><?php echo form_error('store_code'); ?>
                    				<?php echo form_input(['name'=>'store_code','class'=>'form-control','id'=>'store_code','placeholder'=>'Enter Store Code here',
									'value'=>set_value('store_code')]);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Lead Person','lead_person',['for'=>'lead_person']);?><?php echo form_error('lead_person'); ?>
                    				<?php $options=array(''=>'Select Lead person','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('lead_person',$options,set_value('lead_person'),['class'=>'form-control']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Location','location',['for'=>'location']);?><?php echo form_error('location'); ?>
                    				<?php $options=array(''=>'Select Location','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('location',$loc_list,set_value('location'),['class'=>'form-control']);?>
                                </div>	                              
                            </div>
                        </div>
                    </div>
               <hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>