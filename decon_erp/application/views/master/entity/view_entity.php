<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Entity</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/entity','Entity')?></li>
        <li class="active">Add Entity</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("entity/view/{$list->id}",['name'=>'entity_form_add','id'=>'entity_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('entity/create','<i class="fa fa-fw fa-plus-square"></i> Add Entity',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("entity/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit Entity',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>  
				  <?php echo anchor('entity','<i class="fa fa-fw fa-arrows"></i> Entity List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Company Name','company',['for'=>'company']);?><?php echo form_error('company'); ?>
                    				<?php echo form_input(['name'=>'company','class'=>'form-control','id'=>'company','placeholder'=>'Enter Company Title Here',
									'value'=>set_value('company',ucwords($list->company)),'disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('Remarks','remarks',['for'=>'remarks']);?><?php echo form_error('remarks'); ?>
                    				<?php echo form_textarea(['name'=>'remarks','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;',
									'cols'=>'40','rows'=>'10','placeholder'=>'Enter Remarks Here','value'=>set_value('remarks',$list->remarks),'disabled'=>'disabled']);?>
                                </div>	                                        
                            </div>
                        </div>
                    </div>
               <hr />
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>