<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Branch Master</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Branch Master</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
     
          <?php /*?><?php echo form_button('','<i class="fa fa-fw fa-trash"></i> Delete',['type'=>'submit','class'=>'btn btn-default pull-right',
		  'style'=>'margin-left:5px;background-color: #ffffff;','value'=>'Delete_branch'],$js); ?><?php */?>
          <?php echo anchor('branch/create','<i class="fa fa-fw fa-plus-square"></i> Add Branch',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          
          <!--<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" name="inventory_form_manage" id="inventory_form_manage" method="post" />-->
          <?php echo form_open();?>
          <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Branch</th>
              <th>Location</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th><?php */?>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(3);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  strtoupper($lists->branch);?></td>
              <td><?php echo  strtoupper($lists->location);?></td>
              <td>
					<?php echo anchor("branch/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Branch']); ?><?php nbs(3);?>
                    <?php echo anchor("branch/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Branch']); ?>  
                    <?php echo anchor("branch/delete/{$lists->id}",'<i class="fa fa-fw fa-scissors"></i>',['title'=>'Delete Branch','onClick' => "return confirm('Are you sure you want to delete?')"]); ?>  		
              </td>
              <?php /*?><td><?php echo form_checkbox(['name'=>'branch_to_delete[]','type'=>'checkbox','class'=>'checkboxes','value'=>$lists->id]);?></td><?php */?>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Branch</th>
              <th>Location</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox','disabled'=>'disabled']);?></th><?php */?>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php $count++; echo  $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>
