<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Add Branch</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/branch','branch')?></li>
        <li class="active">Add Branch</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('branch/create',['name'=>'branch_form_add','id'=>'branch_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('branch','<i class="fa fa-fw fa-arrows"></i> Branch List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Location','location',['for'=>'location']);?><?php echo form_error('location_id'); ?>
                    				<?php $options=array(''=>'Select Location','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('location_id',$contlist,set_value('location_id'),['class'=>'form-control']);?>
                                </div>
                        		<div class="form-group">
									<?php echo form_label('Branch','branch',['for'=>'branch']);?><?php echo form_error('branch'); ?>
                    				<?php echo form_input(['name'=>'branch','class'=>'form-control','id'=>'branch','placeholder'=>'Enter Branch here',
									'value'=>set_value('branch')]);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>