<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit Country</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/country','Country')?></li>
        <li class="active">Edit Country</li>
      </ol>
    </section> 
    
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">    
				  <?php $msg = $this->session->flashdata('message'); if((isset($msg)) && (!empty($msg))) { ?>
                   		<div class="alert alert-success pull-left" style="height:35px;width:600px;padding:4px;margin:2px;text-align:center;">
                    	<a href="#" class="close" data-dismiss="alert">&times;</a> <?php echo($msg); ?></div>
                  <?php } ?>              
                  <?php echo form_open("country/edit/{$list->id}",['name'=>'country_form_edit','id'=>'country_form_edit']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('country/create','<i class="fa fa-fw fa-plus-square"></i> Add Country',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('country','<i class="fa fa-fw fa-arrows"></i> Country List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>
                  
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Country','country',['for'=>'country']);?><?php echo form_error('country'); ?>
                    				<?php echo form_input(['name'=>'country','class'=>'form-control','id'=>'country','placeholder'=>'Enter Country  here',
									'value'=>set_value('country',strtoupper($list->country))]);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>