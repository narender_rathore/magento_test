<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Country Master</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Country Master</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
     	  <?php echo form_open('country/search');?>
          <?php echo form_input('country',set_value('country'),['class'=>'','sstyle'=>'width:400px;']);?>
          <?php echo form_submit('search','search');?>
          <?php /*?><?php echo form_button('','<i class="fa fa-fw fa-trash"></i> Delete',['type'=>'submit','class'=>'btn btn-default pull-right',
		  'style'=>'margin-left:5px;background-color: #ffffff;','value'=>'Delete_inventory'],$js); ?><?php */?>
          <?php echo anchor('country/create','<i class="fa fa-fw fa-plus-square"></i> Add Country',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          <?php //echo $message;?>
          <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Country</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th><?php */?>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(4);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  strtoupper($lists->country);?></td>
              <td>
					<?php echo anchor("country/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Country']); ?><?php nbs(3);?>
                    <?php echo anchor("country/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Country']); ?>
					<?php echo anchor("country/delete/{$lists->id}",'<i class="fa fa-fw fa-scissors"></i>',['title'=>'Delete Country','onClick' => "return confirm('Are you sure you want to delete?')"]); ?>
               </td>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Country</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox','disabled'=>'disabled']);?></th><?php */?>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php /*?><?php $count++; echo  $this->pagination->create_links(); ?><?php */?>
        <?php $count++; echo  $link; ?>
      	
    	<?php $msg = $this->session->flashdata('message'); if((isset($msg)) && (!empty($msg))) { ?>
              <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a> <?php print_r($msg); ?></div>
        <?php } ?>
        </div>
    </div>
  </div>
</div>
</section>
</div>
