<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View State</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/state','State')?></li>
        <li class="active">View State</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open('state/view',['name'=>'state_form_view','id'=>'state_form_edit']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor("state/edit/{$list->id}",'<i class="fa fa-fw fa-plus-square"></i> Edit State',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor('state/create','<i class="fa fa-fw fa-plus-square"></i> Add State',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
				  <?php echo anchor('state','<i class="fa fa-fw fa-arrows"></i> State List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        
                        		<div class="form-group">
									<?php echo form_label('Country','country',['for'=>'country']);?><?php echo form_error('country'); ?>
                    				<?php $options=array(''=>'Select Country','1'=>'India','2'=>'Pakistan'); ?> 
                            		<?php echo form_dropdown('country_id',$contlist,set_value('country_id',$list->country_id),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>	
                        		<div class="form-group">
									<?php echo form_label('State','state',['for'=>'state']);?><?php echo form_error('state'); ?>
                    				<?php echo form_input(['name'=>'state','class'=>'form-control','id'=>'state','placeholder'=>'Enter State here',
									'value'=>set_value('state',strtoupper($list->state)),'disabled'=>'disabled']);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php /*?><?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>