<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>View City</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/company','Company')?></li>
        <li class="active">View City</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("city/view{$list->id}",['name'=>'city_form_view','id'=>'city_form_view']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
                  
                  <?php echo anchor('city/create','<i class="fa fa-fw fa-plus-square"></i> Add City',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  <?php echo anchor("city/edit/{$list->id}",'<i class="fa fa-edit"></i> Edit City',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>  
				  <?php echo anchor('city','<i class="fa fa-fw fa-arrows"></i>City List',['class'=>'btn btn-default pull-right','style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                        		<div class="form-group">
									<?php echo form_label('Country Name','country',['for'=>'country']);?><?php echo form_error('country_id'); ?>
                    				<?php $options=array(''=>'Select Country','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('country_id',$contlist,set_value('country_id',$list->country_id),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>
                                <div class="form-group">
									<?php echo form_label('State Name','country',['for'=>'state']);?><?php echo form_error('state_id'); ?>
                    				<?php $options=array(''=>'Select State','1'=>'Purchase','2'=>'Rent');  ?>
                  					<?php echo form_dropdown('state_id',$statlist,set_value('state_id',$list->state_id),['class'=>'form-control','disabled'=>'disabled']);?>
                                </div>
                        		<div class="form-group">
									<?php echo form_label('City Name','city',['for'=>'city']);?><?php echo form_error('city'); ?>
                    				<?php echo form_input(['name'=>'city','class'=>'form-control','id'=>'city','placeholder'=>'Enter City here',
									'value'=>set_value('city',strtoupper($list->city)),'disabled'=>'disabled']);?>
                                </div>	
                                        
        </div>
    </div>
</div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php /*?><?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?><?php */?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>