<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard<small>Edit WorkPackage</small></h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Dashboard')?></li>
        <li><?php echo anchor('master/work_package','WorkPackage')?></li>
        <li class="active">Edit Workpackage</li>
      </ol>
    </section> 
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add Products</h3>-->
                  <?php echo form_open("work_package/edit/{$list->id}",['name'=>'workpackage_form_add','id'=>'workpackage_form_add']);?>
          		  <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
                  <?php echo $message;?>
                  <?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
                  
                  <?php echo anchor('work_package/create','<i class="fa fa-fw fa-plus-square"></i> Add WorkPackage',['class'=>'btn btn-default pull-right',
				  'style'=>'margin-left:5px;background-color: #ffffff;']); ?>
				  <?php echo anchor('work_package','<i class="fa fa-fw fa-arrows"></i> WorkPackage List',['class'=>'btn btn-default pull-right',
				  'style'=>'background-color: #ffffff;']); ?>    
                </div><!-- /.box-header -->
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_label('WorkPackage Name','package_name',['for'=>'package_name']);?><?php echo form_error('package_name'); ?>
                                <?php echo form_input(['name'=>'package_name','class'=>'form-control','id'=>'package_name','placeholder'=>'Enter WorkPackage Here',
                                'value'=>set_value('package_name',$list->package_name)]);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_label('Description','description',['for'=>'description']);?><?php echo form_error('description'); ?>
                                <?php echo form_textarea(['name'=>'description','class'=>'form-control','id'=>'editor1','style'=>'width:100%; height:75px;',
                                'cols'=>'40','rows'=>'10','placeholder'=>'Enter Description Here','value'=>set_value('description',$list->description)]);?>
                            </div>	                          
                        </div>
                    </div>
                </div>
<hr />
<?php $js=['onClick'=>'return inventory_validate();'];?>
<?php echo form_submit(['name'=>'save','value'=>'Save','class'=>'btn btn-default pull-right','style'=>'margin-left:5px;background-color: #ffffff;']); ?>
<?php echo  form_close();?>
</div>
</div>
</div>
</section>
</div>
<script>
	$(function () {
	CKEDITOR.replace('editor1');
	$(".textarea").wysihtml5();
	});
</script>