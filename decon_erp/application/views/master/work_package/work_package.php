<?php defined('BASEPATH') or exit('No direct Access is Allowed'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>WorkPackage Master</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">WorkPackage Master</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage inventorys</h3>-->
     
         <?php /*?> <?php echo form_button('','<i class="fa fa-fw fa-trash"></i> Delete',['type'=>'submit','class'=>'btn btn-default pull-right',
		  'style'=>'margin-left:5px;background-color: #ffffff;','value'=>'Delete_inventory'],$js); ?><?php */?>
          <?php echo anchor('work_package/create','<i class="fa fa-fw fa-plus-square"></i> Add WorkPackage',['class'=>'btn btn-default pull-right',
		  'style'=>'background-color: #ffffff;']); ?>
          </div>
          <!-- /.box-header -->
          <!--<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" name="inventory_form_manage" id="inventory_form_manage" method="post" />-->
          <?php echo form_open("work_package");?>
          <?php echo form_hidden('my_array',['name'=>'doaction','id'=>'doaction']);?>
        <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>WorkPackage Name</th>
              <th>Description</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox']);?></th><?php */?>
            </tr>
          </thead>
          <tbody>
			<?php if( count($list) ):$sn=1;
			$count = $this->uri->segment(3);
			foreach($list as $lists ): ?>
            <tr>
              <td><?php echo  $sn; ?></td>
              <td><?php echo  $lists->package_name;?></td>
              <td><?php echo  $lists->description;?></td>
              <td>
					<?php echo anchor("work_package/edit/{$lists->id}",'<i class="fa fa-edit"></i>',['title'=>'Edit Work Package']); ?><?php nbs(3);?>
                    <?php echo anchor("work_package/view/{$lists->id}",'<i class="fa fa-fw fa-eye"></i>',['title'=>'View Work Package']); ?>
                    <?php echo anchor("work_package/delete/{$lists->id}",'<i class="fa fa-fw fa-scissors"></i>',['title'=>'Delete Work Package','onClick' => "return confirm('Are you sure you want to delete?')"]); ?>
              </td>
              <?php /*?><td><?php echo form_checkbox(['name'=>'work_package_to_delete[]','type'=>'checkbox','class'=>'checkboxes','value'=>$lists->id]);?></td><?php */?>
            </tr>
            <?php $sn++;?>
			<?php endforeach; ?>
            <?php else : ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php endif; ?>

          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>WorkPackage Name</th>
              <th>Description</th>
              <th>Action</th>
              <?php /*?><th><?php echo form_checkbox(['name'=>'selectall','id'=>'selectall','type'=>'checkbox','disabled'=>'disabled']);?></th><?php */?>
            </tr>
          </tfoot>
        <?php echo form_close();?>
        </table>
        <?php $count++; echo  $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
