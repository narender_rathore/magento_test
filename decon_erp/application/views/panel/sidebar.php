
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <?=$img_property_logo=['src'=>'assets/dist/img/logo.png','alt'=>'User Image','class'=>'img-circle'];?>
          	  <?=img($img_property_logo);?>
            </div>
            <div class="pull-left info">
              <p><?php //echo ucfirst($admin_data['fname'])."&nbsp;&nbsp;".ucfirst($admin_data['lname'])?></p>
              <!-- Status -->
             <!-- <a href=""><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
          </div>



          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
                <li class="header">Inventory Master</li>
                <!-- Optionally, you can add icons to the links -->
                <!-- <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Manage Product</span></a></li>
                <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>-->
                <li class="treeview">
                      <a href="#"><i class="fa fa-dashboard"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?=base_url('customer');?>"><i class="fa fa-circle-o text-aqua"></i>Customer</a></li>
                        <li><a href="<?=base_url('item');?>"><i class="fa fa-circle-o text-aqua"></i>New Inventory Type</a></li>
                        <li><a href="<?=base_url('company');?>"><i class="fa fa-circle-o text-aqua"></i>Company Make</a></li>
                        <li><a href="<?=base_url('vendor');?>"><i class="fa fa-circle-o text-aqua"></i>Vendor</a></li>
                        <li><a href="<?=base_url('store');?>"><i class="fa fa-circle-o text-aqua"></i>Store</a></li>
                        <li><a href="<?=base_url('country');?>"><i class="fa fa-circle-o text-aqua"></i>Country Master</a></li>
                        <li><a href="<?=base_url('state');?>"><i class="fa fa-circle-o text-aqua"></i>State Master</a></li>
                        <li><a href="<?=base_url('city');?>"><i class="fa fa-circle-o text-aqua"></i>City Master</a></li>
                        <li><a href="<?=base_url('region');?>"><i class="fa fa-circle-o text-aqua"></i>Region</a></li>
                        <li><a href="<?=base_url('location');?>"><i class="fa fa-circle-o text-aqua"></i>Location</a></li>
                        <li><a href="<?=base_url('branch');?>"><i class="fa fa-circle-o text-aqua"></i>Branch</a></li>
                        <li><a href="<?=base_url('entity');?>"><i class="fa fa-circle-o text-aqua"></i>Entity Master</a></li>
                        <li><a href="<?=base_url('work_package');?>"><i class="fa fa-circle-o text-aqua"></i>Work Package Master</a></li>
                        <li><a href="<?=base_url('designation');?>"><i class="fa fa-circle-o text-aqua"></i>Designation</a></li>
                        <li><a href="<?=base_url('department');?>"><i class="fa fa-circle-o text-aqua"></i>Department Master</a></li>
                        <li id="newuser"><a href="<?=base_url('new_user');?>" class="menu-button"><i class="fa fa-circle-o text-aqua"></i>New User Register</a></li>
                        <li id="usermenuauth"><a href="<?=base_url('user_auth');?>" class="menu-button"><i class="fa fa-circle-o text-aqua"></i>Menu Authentication</a></li>
                      </ul>
                </li>
                <li class="treeview">
                      <a href="#"><i class="fa fa-files-o"></i> <span>HR</span> <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?=base_url('employee');?>"><i class="fa fa-circle-o text-aqua"></i>Employee</a></li>
                        <li><a href="<?=base_url('outsource_employee');?>"><i class="fa fa-circle-o text-aqua"></i>Outsource Employee</a></li>
                        <li><a href="<?=base_url('expenses');?>"><i class="fa fa-circle-o text-aqua"></i>Employee Attandance & Expenses</a></li>
                        <li><a href="<?=base_url('expensesapproval');?>"><i class="fa fa-circle-o text-aqua"></i>Expenses Approval</a></li>
                        <li><a href="<?=base_url('accountstatus');?>"><i class="fa fa-circle-o text-aqua"></i>Account Status</a></li>
                        <li><a href="<?=base_url('dailyinfo');?>"><i class="fa fa-circle-o text-aqua"></i>Daily Information</a></li>
                      </ul>
                </li>
                <li class="treeview">
                      <a href="#"><i class="fa fa-pie-chart"></i> <span>Projects</span> <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?=base_url('project');?>"><i class="fa fa-circle-o text-aqua"></i>Project Master</a></li>
                        <li><a href="<?=base_url('customerpo');?>"><i class="fa fa-circle-o text-aqua"></i>Customer PO</a></li>
                        <li><a href="<?=base_url('workstatus');?>"><i class="fa fa-circle-o text-aqua"></i>Work Status</a></li>
                      </ul>
                </li>
                <li class="treeview">
                      <a href="#"><i class="fa fa-laptop"></i> <span>Invoice & Accounts</span> <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?=base_url('paymenttransfer');?>"><i class="fa fa-circle-o text-aqua"></i>Payment Transfers</a></li>
                        <li><a href="<?=base_url('vpo');?>"><i class="fa fa-circle-o text-aqua"></i>Service Po Issue To vendor</a></li>
                        <li><a href="<?=base_url('epo');?>"><i class="fa fa-circle-o text-aqua"></i>Po Issue To Employee</a></li>
                      </ul>
                </li>
                <li class="treeview">
                      <a href="#"><i class="fa fa-edit"></i> <span>Inventory</span> <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?=base_url('purchaseinventory');?>"><i class="fa fa-circle-o text-aqua"></i>Inventory Purchase Information</a></li>
                        <li><a href="<?=base_url('addinventory');?>"><i class="fa fa-circle-o text-aqua"></i>Inventory</a></li>
                        <li><a href="<?=base_url('interstore');?>"><i class="fa fa-circle-o text-aqua"></i>Inter Store Transfer</a></li>
                        <li><a href="<?=base_url('recievestore');?>"><i class="fa fa-circle-o text-aqua"></i>Receive By Store</a></li>
                        <li><a href="<?=base_url('itemtoemployee');?>"><i class="fa fa-circle-o text-aqua"></i>Issue Item to Employee</a></li>
                        <li><a href="<?=base_url('recievebyemployee');?>"><i class="fa fa-circle-o text-aqua"></i>Receive By Employee</a></li>
                      </ul>
                </li>
                <li class="active" ><a href="<?=base_url('userprofile');?>"><i class="fa fa-fw fa-user-secret"></i> <span>Admin Profile</span></a></li>
                
                 <?php /*?><li class="active"><a href="subscriber_list.php"><i class="fa fa-fw fa-motorcycle"></i> <span>Subscriber</span></a></li><?php */?>
                

          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
 