-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2016 at 11:42 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `decon_erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_session`
--

CREATE TABLE `ci_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_session`
--

INSERT INTO `ci_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('422c4083d65c662a943abb0418650500473128dc', '::1', 1456182517, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138323531373b),
('9c2310d662c58d9eb92bfcbc6b8ade9df2afc9f9', '::1', 1456182943, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138323934333b),
('4e009fa07c49f3397e3b8d102da7af54a8847439', '::1', 1456183378, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138333337383b),
('edf5d7c7739f16c9c0495fd9a6bd5c18b10752eb', '::1', 1456184232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138343233323b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('a87cc96e5e3976827f34b34b3fc33c98605826fc', '::1', 1456183891, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138333839313b),
('1fcbfab3ec2bc17e2516240e8b44d412c24afc80', '::1', 1456185868, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138353836383b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('a889eaf2ddb559a58e56f7e8e4b4fe832a2df72f', '::1', 1456186272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138363237323b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('b71d1a590554adbea2e9aaba0c2d7416a539223b', '::1', 1456186718, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138363731383b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('8cb9897bd77d0a7da46749e59ea9a9d8222a6a44', '::1', 1456188179, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138383137393b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('7ccd2d6d7328cf3aa871b2e4a2e3151dc70577ca', '::1', 1456188582, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138383538323b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('62eab0d55181c75f5de6d62de9c1d22fef98b2f7', '::1', 1456188587, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363138383538323b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('3ac11844001cb4594cc40710095185142d785b42', '::1', 1456250884, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363235303838343b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('a5770095ce11f217149da4d9d7a090d71bbd47fe', '::1', 1456251588, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363235313538383b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('222f03150fca74ea744bb596135c05976ab53842', '::1', 1456252717, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363235323731373b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('f5a53e63ba44eeb503d6e236a8ea6ba63844f74d', '::1', 1456252717, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363235323731373b757365725f69647c693a303b757365726e616d657c733a303a22223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('ffb65a711408824dece1cfd74f9285512708097d', '::1', 1456260419, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363236303234323b);

-- --------------------------------------------------------

--
-- Table structure for table `hr_daily_info`
--

CREATE TABLE `hr_daily_info` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `daily_work_info` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_daily_info`
--

INSERT INTO `hr_daily_info` (`id`, `project_id`, `daily_work_info`) VALUES
(1, 1, 'c sc nas cna sn asn dhsahdsahdhjsa cnsa chasch as');

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee`
--

CREATE TABLE `hr_employee` (
  `id` int(11) NOT NULL,
  `emp_code` varchar(100) DEFAULT NULL,
  `emp_business_letter` varchar(20) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `date_of_birth` varchar(50) DEFAULT NULL,
  `office_email` varchar(100) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `marital_status` varchar(30) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `referrer_contact` varchar(15) DEFAULT NULL,
  `refferer_email` varchar(100) DEFAULT NULL,
  `experience_month` varchar(100) DEFAULT NULL,
  `experience_yr` varchar(100) DEFAULT NULL,
  `last_working_date` varchar(50) DEFAULT NULL,
  `joining_date` varchar(50) DEFAULT NULL,
  `salary` varchar(50) DEFAULT NULL,
  `lda` varchar(255) DEFAULT NULL,
  `oda` varchar(255) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `current_location` int(11) DEFAULT NULL,
  `designation` int(11) DEFAULT NULL,
  `original_doc_status` varchar(50) DEFAULT NULL,
  `original_doc_details` text,
  `recieved_by` varchar(100) DEFAULT NULL,
  `recieved_store_branch` varchar(50) DEFAULT NULL,
  `bank_holder_name` varchar(100) DEFAULT NULL,
  `bank_account_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_branch` varchar(100) DEFAULT NULL,
  `bank_ifsc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_employee`
--

INSERT INTO `hr_employee` (`id`, `emp_code`, `emp_business_letter`, `emp_name`, `date_of_birth`, `office_email`, `gender`, `marital_status`, `referred_by`, `referrer_contact`, `refferer_email`, `experience_month`, `experience_yr`, `last_working_date`, `joining_date`, `salary`, `lda`, `oda`, `father_name`, `mother_name`, `current_location`, `designation`, `original_doc_status`, `original_doc_details`, `recieved_by`, `recieved_store_branch`, `bank_holder_name`, `bank_account_no`, `bank_name`, `bank_branch`, `bank_ifsc`) VALUES
(1, 'emp-001', 'mr', 'narender', '14-02-2016', 'nrathore.web@gmail.com', 'male', 'unmarried', 'sudhir', '1234567890', 'nrathore.web@gmail.com', '2', '2', NULL, '14-02-2016', '25000', '1200', '14000', 'Rajveer singh', 'Shanti', 1, 1, 'yes', 'mark sheet 10th', 'aarti ', '1', 'Narender', 'acc-121321', 'Bank of india', '1', '18372187');

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_address`
--

CREATE TABLE `hr_employee_address` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `local_country` tinyint(3) DEFAULT NULL,
  `local_state` tinyint(3) DEFAULT NULL,
  `local_city` tinyint(3) DEFAULT NULL,
  `local_address` varchar(255) DEFAULT NULL,
  `local_pin` int(11) DEFAULT NULL,
  `local_contact` int(11) DEFAULT NULL,
  `local_mobile` int(11) DEFAULT NULL,
  `local_email` varchar(100) DEFAULT NULL,
  `permanent_country` tinyint(3) DEFAULT NULL,
  `permanent_state` tinyint(3) DEFAULT NULL,
  `permanent_city` tinyint(3) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `permanent_pin` int(11) DEFAULT NULL,
  `permanent_contact` int(11) DEFAULT NULL,
  `permanent_mobile` int(11) DEFAULT NULL,
  `permanent_email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_employee_address`
--

INSERT INTO `hr_employee_address` (`id`, `employee_id`, `local_country`, `local_state`, `local_city`, `local_address`, `local_pin`, `local_contact`, `local_mobile`, `local_email`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_address`, `permanent_pin`, `permanent_contact`, `permanent_mobile`, `permanent_email`) VALUES
(1, 1, 1, 10, 3, 'sdfsdf', 121004, 2147483647, 2147483647, 'nrathore.web@mail.com', 1, 2, 3, 'dvmsnb vns d', 121004, 2147483647, 2147483647, 'nrathore.web@mail.com');

-- --------------------------------------------------------

--
-- Table structure for table `hr_employee_qualification`
--

CREATE TABLE `hr_employee_qualification` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `university` varchar(255) DEFAULT NULL,
  `marks` varchar(20) DEFAULT NULL,
  `passing_year` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_employee_qualification`
--

INSERT INTO `hr_employee_qualification` (`id`, `employee_id`, `qualification`, `university`, `marks`, `passing_year`) VALUES
(1, 1, 'B.tech', 'mdu rohtak', '60', '2010');

-- --------------------------------------------------------

--
-- Table structure for table `hr_expenses`
--

CREATE TABLE `hr_expenses` (
  `id` int(11) NOT NULL,
  `vehicle` tinyint(2) DEFAULT NULL,
  `travel_detail` text,
  `start_date` varchar(50) DEFAULT NULL,
  `end_date` varchar(50) DEFAULT NULL,
  `start_reading` varchar(50) DEFAULT NULL,
  `end_reading` varchar(20) DEFAULT NULL,
  `work_approved_by` int(11) NOT NULL,
  `vehicle_no` varchar(100) DEFAULT NULL,
  `driver_name` varchar(50) DEFAULT NULL,
  `driver_no` int(11) DEFAULT NULL,
  `po_no` varchar(50) DEFAULT NULL,
  `extra_mode_transport` varchar(100) DEFAULT NULL,
  `engineer_da` varchar(20) DEFAULT NULL,
  `rigger_id` int(11) NOT NULL,
  `rigger_da` varchar(20) DEFAULT NULL,
  `vehicle_payment` varchar(100) DEFAULT NULL,
  `other_expense` varchar(100) DEFAULT NULL,
  `total_expenses` varchar(100) DEFAULT NULL,
  `bill_upload` varchar(255) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hr_expenses_detail`
--

CREATE TABLE `hr_expenses_detail` (
  `id` int(11) NOT NULL,
  `expense_id` int(11) NOT NULL,
  `date_added` varchar(50) DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `work_packages` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hr_outsource_employee`
--

CREATE TABLE `hr_outsource_employee` (
  `id` int(11) NOT NULL,
  `emp_code` varchar(100) DEFAULT NULL,
  `emp_business_letter` varchar(20) DEFAULT NULL,
  `emp_name` varchar(100) DEFAULT NULL,
  `date_of_birth` varchar(50) DEFAULT NULL,
  `office_email` varchar(100) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `marital_status` varchar(30) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `referrer_contact` varchar(15) DEFAULT NULL,
  `refferer_email` varchar(100) DEFAULT NULL,
  `experience_month` varchar(100) DEFAULT NULL,
  `experience_yr` varchar(100) DEFAULT NULL,
  `last_working_date` varchar(50) DEFAULT NULL,
  `joining_date` varchar(50) DEFAULT NULL,
  `salary` varchar(50) DEFAULT NULL,
  `lda` varchar(255) DEFAULT NULL,
  `oda` varchar(255) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `current_location` int(11) DEFAULT NULL,
  `designation` int(11) DEFAULT NULL,
  `original_doc_status` varchar(50) DEFAULT NULL,
  `original_doc_details` text,
  `recieved_by` varchar(100) DEFAULT NULL,
  `recieved_store_branch` varchar(50) DEFAULT NULL,
  `bank_holder_name` varchar(100) DEFAULT NULL,
  `bank_account_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_branch` varchar(100) DEFAULT NULL,
  `bank_ifsc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_outsource_employee`
--

INSERT INTO `hr_outsource_employee` (`id`, `emp_code`, `emp_business_letter`, `emp_name`, `date_of_birth`, `office_email`, `gender`, `marital_status`, `referred_by`, `referrer_contact`, `refferer_email`, `experience_month`, `experience_yr`, `last_working_date`, `joining_date`, `salary`, `lda`, `oda`, `father_name`, `mother_name`, `current_location`, `designation`, `original_doc_status`, `original_doc_details`, `recieved_by`, `recieved_store_branch`, `bank_holder_name`, `bank_account_no`, `bank_name`, `bank_branch`, `bank_ifsc`) VALUES
(2, 'out-src-jdshfvhds', 'mrs', 'sadaskldn', '14-02-2016', 'nrathore.web@gmail.com', 'male', 'unmarried', 'sudhir', '1234567890', 'nrathore.web@gmail.com', '2', '1', NULL, '14-02-2016', '211231', '1200', '14000', 'Rajveer singh', 'Shanti', 1, 2, 'yes', 'fgdgfdgfdgdfgdf', 'dasd', '1', 'asdasdasd', 'asd', 'asdas', '1', 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `hr_outsource_employee_address`
--

CREATE TABLE `hr_outsource_employee_address` (
  `id` int(11) NOT NULL,
  `outsource_employee_id` int(11) NOT NULL,
  `local_country` tinyint(3) DEFAULT NULL,
  `local_state` tinyint(3) DEFAULT NULL,
  `local_city` tinyint(3) DEFAULT NULL,
  `local_address` varchar(255) DEFAULT NULL,
  `local_pin` int(11) DEFAULT NULL,
  `local_contact` int(11) DEFAULT NULL,
  `local_mobile` int(11) DEFAULT NULL,
  `local_email` varchar(100) DEFAULT NULL,
  `permanent_country` tinyint(3) DEFAULT NULL,
  `permanent_state` tinyint(3) DEFAULT NULL,
  `permanent_city` tinyint(3) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `permanent_pin` int(11) DEFAULT NULL,
  `permanent_contact` int(11) DEFAULT NULL,
  `permanent_mobile` int(11) DEFAULT NULL,
  `permanent_email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_outsource_employee_address`
--

INSERT INTO `hr_outsource_employee_address` (`id`, `outsource_employee_id`, `local_country`, `local_state`, `local_city`, `local_address`, `local_pin`, `local_contact`, `local_mobile`, `local_email`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_address`, `permanent_pin`, `permanent_contact`, `permanent_mobile`, `permanent_email`) VALUES
(2, 2, 11, 12, 2, 'asdas', 1213, 1234567890, 1234567890, 'nrathore.web@mail.com', 11, 12, 2, 'fdds', 121004, 1234567890, 1234567890, 'nrathore.web@mail.com');

-- --------------------------------------------------------

--
-- Table structure for table `hr_outsource_employee_qualification`
--

CREATE TABLE `hr_outsource_employee_qualification` (
  `id` int(11) NOT NULL,
  `outsource_employee_id` int(11) NOT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `university` varchar(255) DEFAULT NULL,
  `marks` varchar(20) DEFAULT NULL,
  `passing_year` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_outsource_employee_qualification`
--

INSERT INTO `hr_outsource_employee_qualification` (`id`, `outsource_employee_id`, `qualification`, `university`, `marks`, `passing_year`) VALUES
(2, 2, '12th', 'mdu rohtak', '67', '2010');

-- --------------------------------------------------------

--
-- Table structure for table `hr_user_auth`
--

CREATE TABLE `hr_user_auth` (
  `id` int(11) NOT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `employee_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `employee_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `employee_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `employee_hr_delete` tinyint(1) NOT NULL DEFAULT '0',
  `outs_employee_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `outs_employee_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `outs_employee_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `outs_employee_hr_delete` tinyint(1) NOT NULL DEFAULT '0',
  `expensemaster_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `expensemaster_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `expensemaster_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `expensemaster_hr_delete` tinyint(1) NOT NULL DEFAULT '0',
  `expenseapproval_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `expenseapproval_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `expenseapproval_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `expenseapproval_hr_delete` tinyint(1) NOT NULL DEFAULT '0',
  `accountstatus_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `accountstatus_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `accountstatus_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `accountstatus_hr_delete` tinyint(1) NOT NULL DEFAULT '0',
  `dailyinfo_hr_view` tinyint(1) NOT NULL DEFAULT '0',
  `dailyinfo_hr_add` tinyint(1) NOT NULL DEFAULT '0',
  `dailyinfo_hr_edit` tinyint(1) NOT NULL DEFAULT '0',
  `dailyinfo_hr_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_user_auth`
--

INSERT INTO `hr_user_auth` (`id`, `auth_id`, `employee_hr_view`, `employee_hr_add`, `employee_hr_edit`, `employee_hr_delete`, `outs_employee_hr_view`, `outs_employee_hr_add`, `outs_employee_hr_edit`, `outs_employee_hr_delete`, `expensemaster_hr_view`, `expensemaster_hr_add`, `expensemaster_hr_edit`, `expensemaster_hr_delete`, `expenseapproval_hr_view`, `expenseapproval_hr_add`, `expenseapproval_hr_edit`, `expenseapproval_hr_delete`, `accountstatus_hr_view`, `accountstatus_hr_add`, `accountstatus_hr_edit`, `accountstatus_hr_delete`, `dailyinfo_hr_view`, `dailyinfo_hr_add`, `dailyinfo_hr_edit`, `dailyinfo_hr_delete`) VALUES
(2, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 3, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inter_store_transfer`
--

CREATE TABLE `inter_store_transfer` (
  `id` int(11) NOT NULL,
  `on_date` varchar(100) DEFAULT NULL,
  `from_store` int(11) DEFAULT NULL,
  `to_store` int(11) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inter_store_transfer_detail`
--

CREATE TABLE `inter_store_transfer_detail` (
  `id` int(11) NOT NULL,
  `inter_store_id` int(11) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `po_no` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `item_code` varchar(100) DEFAULT NULL,
  `on_date` varchar(100) DEFAULT NULL,
  `make` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  `model_no` varchar(100) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_purchase`
--

CREATE TABLE `inventory_purchase` (
  `id` int(11) NOT NULL,
  `po_no` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `add_date` varchar(20) NOT NULL,
  `vendor` int(11) DEFAULT NULL,
  `tax` double NOT NULL,
  `total_amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_purchase_detail`
--

CREATE TABLE `inventory_purchase_detail` (
  `id` int(11) NOT NULL,
  `inventory_id` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `make` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` varchar(20) DEFAULT NULL,
  `rental` varchar(20) DEFAULT NULL,
  `amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_purchase_payment_terms`
--

CREATE TABLE `inventory_purchase_payment_terms` (
  `id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `percentage` varchar(20) DEFAULT NULL,
  `amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_user_auth`
--

CREATE TABLE `inventory_user_auth` (
  `id` int(11) NOT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `purchaseinv_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `purchaseinv_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `purchaseinv_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `purchaseinv_inventory_delete` tinyint(1) NOT NULL DEFAULT '0',
  `addinv_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `addinv_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `addinv_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `addinv_inventory_delete` tinyint(1) NOT NULL DEFAULT '0',
  `storetransfer_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `storetransfer_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `storetransfer_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `storetransfer_inventory_delete` tinyint(1) NOT NULL DEFAULT '0',
  `receive_bystore_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `receive_bystore_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `receive_bystore_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `receive_bystore_inventory_delete` tinyint(1) NOT NULL DEFAULT '0',
  `issuetoemp_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `issuetoemp_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `issuetoemp_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `issuetoemp_inventory_delete` tinyint(1) NOT NULL DEFAULT '0',
  `receive_byemp_inventory_view` tinyint(1) NOT NULL DEFAULT '0',
  `receive_byemp_inventory_add` tinyint(1) NOT NULL DEFAULT '0',
  `receive_byemp_inventory_edit` tinyint(1) NOT NULL DEFAULT '0',
  `receive_byemp_inventory_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory_user_auth`
--

INSERT INTO `inventory_user_auth` (`id`, `auth_id`, `purchaseinv_inventory_view`, `purchaseinv_inventory_add`, `purchaseinv_inventory_edit`, `purchaseinv_inventory_delete`, `addinv_inventory_view`, `addinv_inventory_add`, `addinv_inventory_edit`, `addinv_inventory_delete`, `storetransfer_inventory_view`, `storetransfer_inventory_add`, `storetransfer_inventory_edit`, `storetransfer_inventory_delete`, `receive_bystore_inventory_view`, `receive_bystore_inventory_add`, `receive_bystore_inventory_edit`, `receive_bystore_inventory_delete`, `issuetoemp_inventory_view`, `issuetoemp_inventory_add`, `issuetoemp_inventory_edit`, `issuetoemp_inventory_delete`, `receive_byemp_inventory_view`, `receive_byemp_inventory_add`, `receive_byemp_inventory_edit`, `receive_byemp_inventory_delete`) VALUES
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_accounts_employee_po`
--

CREATE TABLE `invoice_accounts_employee_po` (
  `id` int(11) NOT NULL,
  `employee` int(11) DEFAULT NULL,
  `po_no` varchar(100) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_accounts_paymenttransfer`
--

CREATE TABLE `invoice_accounts_paymenttransfer` (
  `id` int(11) NOT NULL,
  `transaction_doneby` varchar(100) DEFAULT NULL,
  `paid_to_type` varchar(100) DEFAULT NULL,
  `paid_to_name` varchar(100) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `mode_of_payment` varchar(100) DEFAULT NULL,
  `details` text,
  `date_of_payment` varchar(100) DEFAULT NULL,
  `payment_against` varchar(100) DEFAULT NULL,
  `po_number` varchar(255) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_accounts_po_tovendor`
--

CREATE TABLE `invoice_accounts_po_tovendor` (
  `id` int(11) NOT NULL,
  `po_no` varchar(100) DEFAULT NULL,
  `po_date` varchar(100) DEFAULT NULL,
  `po_expiary_date` varchar(100) DEFAULT NULL,
  `project_code` int(11) DEFAULT NULL,
  `po_type` varchar(255) DEFAULT NULL,
  `po_particular` varchar(100) DEFAULT NULL,
  `vendor_code` int(11) DEFAULT NULL,
  `vendor_detail` varchar(255) DEFAULT NULL,
  `po_value` varchar(255) DEFAULT NULL,
  `other_taxes` varchar(255) DEFAULT NULL,
  `total_po_value` varchar(255) DEFAULT NULL,
  `payment_terms` varchar(255) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_accounts_po_tovendor_detail`
--

CREATE TABLE `invoice_accounts_po_tovendor_detail` (
  `id` int(11) NOT NULL,
  `po_tovendor_id` int(11) DEFAULT NULL,
  `date_added` varchar(100) DEFAULT NULL,
  `percentage` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_user_auth`
--

CREATE TABLE `invoice_user_auth` (
  `id` int(11) NOT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `paymenttrans_invoice_view` tinyint(1) NOT NULL DEFAULT '0',
  `paymenttrans_invoice_add` tinyint(1) NOT NULL DEFAULT '0',
  `paymenttrans_invoice_edit` tinyint(1) NOT NULL DEFAULT '0',
  `paymenttrans_invoice_delete` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_tovendor_invoice_view` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_tovendor_invoice_add` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_tovendor_invoice_edit` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_tovendor_invoice_delete` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_toemp_invoice_view` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_toemp_invoice_add` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_toemp_invoice_edit` tinyint(1) NOT NULL DEFAULT '0',
  `poissue_toemp_invoice_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_user_auth`
--

INSERT INTO `invoice_user_auth` (`id`, `auth_id`, `paymenttrans_invoice_view`, `paymenttrans_invoice_add`, `paymenttrans_invoice_edit`, `paymenttrans_invoice_delete`, `poissue_tovendor_invoice_view`, `poissue_tovendor_invoice_add`, `poissue_tovendor_invoice_edit`, `poissue_tovendor_invoice_delete`, `poissue_toemp_invoice_view`, `poissue_toemp_invoice_add`, `poissue_toemp_invoice_edit`, `poissue_toemp_invoice_delete`) VALUES
(2, 2, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1),
(3, 3, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `make`
--

CREATE TABLE `make` (
  `id` int(11) NOT NULL,
  `make` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `make`
--

INSERT INTO `make` (`id`, `make`) VALUES
(1, 'SONY'),
(2, 'MICROMAX'),
(3, 'INTEX'),
(4, 'NOKIA'),
(5, 'SAMSUNG'),
(6, 'MI'),
(7, 'XOLO'),
(8, 'SPICE'),
(9, 'LENOVO'),
(10, 'DELL'),
(11, 'COMPAQ'),
(12, 'ASUS'),
(13, 'LAPTOP'),
(14, 'ANTENNA'),
(15, 'Binoculars'),
(16, 'NIKON'),
(17, 'CANNON'),
(18, 'kodak'),
(19, 'Car Inverter'),
(20, 'Decon'),
(21, 'NARDA'),
(22, 'ASCOM'),
(23, 'PCTEL');

-- --------------------------------------------------------

--
-- Table structure for table `master_branch`
--

CREATE TABLE `master_branch` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `branch` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_branch`
--

INSERT INTO `master_branch` (`id`, `location_id`, `branch`) VALUES
(1, 1, 'ballabgarh'),
(2, 2, 'n c sd c');

-- --------------------------------------------------------

--
-- Table structure for table `master_city`
--

CREATE TABLE `master_city` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_city`
--

INSERT INTO `master_city` (`id`, `country_id`, `state_id`, `city`) VALUES
(2, 1, 2, 'delhi'),
(3, 3, 1, 'faridabad');

-- --------------------------------------------------------

--
-- Table structure for table `master_company`
--

CREATE TABLE `master_company` (
  `id` int(11) NOT NULL,
  `make_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_company`
--

INSERT INTO `master_company` (`id`, `make_name`) VALUES
(1, 'nb b b '),
(2, 'decon telecom solution pvt ltd');

-- --------------------------------------------------------

--
-- Table structure for table `master_country`
--

CREATE TABLE `master_country` (
  `id` int(11) NOT NULL,
  `country` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_country`
--

INSERT INTO `master_country` (`id`, `country`) VALUES
(1, 'india'),
(4, 'england'),
(5, 'rusia'),
(6, 'china'),
(7, 'japan'),
(8, 'nepal'),
(9, 'shri lanka'),
(10, 'bangladesh'),
(11, 'afganistan'),
(12, 'malasia'),
(13, 'australia'),
(14, 'pakistan'),
(15, 'united state of america'),
(16, 'australia'),
(17, 'north america'),
(18, 'zoker'),
(19, 'vcbc'),
(20, 'cbc'),
(22, 'xcccccccccccccccc'),
(23, 'gfdgd');

-- --------------------------------------------------------

--
-- Table structure for table `master_customer`
--

CREATE TABLE `master_customer` (
  `id` int(11) NOT NULL,
  `comp_code` varchar(50) DEFAULT NULL,
  `comp_name` varchar(100) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `address` text,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_customer`
--

INSERT INTO `master_customer` (`id`, `comp_code`, `comp_name`, `tin`, `address`, `remarks`) VALUES
(1, 'aircel', 'AIRCEL', 'AIRCEL', 'AIRCEL', 'AIRCEL'),
(4, 'huawei', 'HUAWEI', 'HUAWEI-113', 'dewrwr', 'b'),
(5, 'di-2001', 'AIRTEL', 'AIRTEL-3763', 'fdgd', 'bjk'),
(6, ' bcvb', 'bcvbcv', 'bcvbcvb', 'cvbcv', 'bcvb'),
(7, 'dssf', 'dsfds', 'dsfds', 'ds', 'dsds'),
(8, 'cvxvx', 'vxcv', 'vcxv', 'cxvx', 'cxvxc'),
(9, 'dsfsd', 'fsdf', 'sdfs', 'dfsd', 'sdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `master_customer_contact_detail`
--

CREATE TABLE `master_customer_contact_detail` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `designation` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_customer_contact_detail`
--

INSERT INTO `master_customer_contact_detail` (`id`, `customer_id`, `name`, `designation`, `department`, `mobile`, `email`) VALUES
(1, 1, 'AIRCEL', 2, 1, '1234567890', 'demo@mail.com'),
(4, 4, 'hjbjbdjb', 1, 1, '8745916499', 'narender.01@mail.com'),
(5, 5, 'bcvkjbc', 1, 2, '8745916499', 'narender.01@mail.com'),
(6, 9, 'dsfsd', 2, 2, '8745916499', 'nrathore.web@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `master_department`
--

CREATE TABLE `master_department` (
  `id` int(11) NOT NULL,
  `department` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_department`
--

INSERT INTO `master_department` (`id`, `department`) VALUES
(1, 'software'),
(2, 'it');

-- --------------------------------------------------------

--
-- Table structure for table `master_designation`
--

CREATE TABLE `master_designation` (
  `id` int(11) NOT NULL,
  `designation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_designation`
--

INSERT INTO `master_designation` (`id`, `designation`) VALUES
(1, 'software engginear'),
(2, 'hr');

-- --------------------------------------------------------

--
-- Table structure for table `master_entity`
--

CREATE TABLE `master_entity` (
  `id` int(11) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_entity`
--

INSERT INTO `master_entity` (`id`, `company`, `remarks`) VALUES
(1, 'decon telecom solution', 'decon is telecom solution'),
(2, 'dnf sn', 'n dnf dsn fn ');

-- --------------------------------------------------------

--
-- Table structure for table `master_item`
--

CREATE TABLE `master_item` (
  `id` int(11) NOT NULL,
  `item` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_item`
--

INSERT INTO `master_item` (`id`, `item`) VALUES
(1, 'LAPTOP'),
(2, 'SONY X ARC S LT18I RE'),
(3, 'SONY X ARC S LT18A RE'),
(4, 'SONY X V LT25I RE '),
(5, 'W995 PHONE'),
(6, 'W995 PHONE (T)'),
(7, 'SG S4'),
(8, 'C5 PHONE'),
(9, 'Micromax PHONE'),
(10, 'XOLO PHONE'),
(11, 'Intex PHONE'),
(12, 'GPS MOUSE'),
(13, 'GPS HANDHELD'),
(14, 'CAMERA'),
(15, 'DATA CARD'),
(16, 'LASER METER'),
(17, 'SCANNER'),
(18, 'USB Hub'),
(19, 'COMPASS'),
(20, 'TILT METER'),
(21, 'BINOCULARS'),
(22, 'POWER METER'),
(23, 'GURU 1200'),
(24, 'SPICE PHONE'),
(25, 'ANTENNA'),
(26, 'CAR INVERTER'),
(27, 'CELL CHARGER'),
(28, 'CHAIR'),
(29, 'CHARGER'),
(30, 'LN KEY'),
(31, 'Trimble Juno GPS'),
(32, 'MEASURING TAPE'),
(33, 'POWER BANK'),
(34, 'USB MOUSE'),
(35, 'ASD'),
(36, 'LTE TECH OPTION RE INDIA'),
(37, 'SG S5'),
(38, 'TEMS DISCOVERY DEVICE 10.0, GLS'),
(39, 'TEMS INVESTIGATION 16.X'),
(40, 'TEMS INVESTIGATION 14.X'),
(41, 'TEMS INVESTIGATION 13.X'),
(42, 'SPECTRUM ANALYZER FEATURE'),
(43, 'PCTEL EXFLEX GSM/WCDMA 900/1800/2100'),
(44, 'SRM-3006 METER');

-- --------------------------------------------------------

--
-- Table structure for table `master_location`
--

CREATE TABLE `master_location` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_location`
--

INSERT INTO `master_location` (`id`, `city_id`, `location`) VALUES
(1, 1, 'delhi haat'),
(2, 2, 'vhjvh'),
(4, 2, 'mayapuri');

-- --------------------------------------------------------

--
-- Table structure for table `master_new_inventory`
--

CREATE TABLE `master_new_inventory` (
  `id` int(11) NOT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `model_no` tinyint(1) NOT NULL DEFAULT '0',
  `ram` tinyint(1) NOT NULL DEFAULT '0',
  `battery` tinyint(1) NOT NULL DEFAULT '0',
  `laptop_charger` tinyint(1) NOT NULL DEFAULT '0',
  `antivirus_key` tinyint(1) NOT NULL DEFAULT '0',
  `hard_disk` tinyint(1) NOT NULL DEFAULT '0',
  `serial_no` tinyint(1) NOT NULL DEFAULT '0',
  `warranty_date` tinyint(1) NOT NULL DEFAULT '0',
  `software` tinyint(1) NOT NULL DEFAULT '0',
  `chargable` tinyint(1) NOT NULL DEFAULT '0',
  `zoom` tinyint(1) NOT NULL DEFAULT '0',
  `pixel` tinyint(1) NOT NULL DEFAULT '0',
  `length` tinyint(1) NOT NULL DEFAULT '0',
  `imei_no` tinyint(1) NOT NULL DEFAULT '0',
  `memory_card` tinyint(1) NOT NULL DEFAULT '0',
  `calibration_info` tinyint(1) NOT NULL DEFAULT '0',
  `dongle` tinyint(1) NOT NULL DEFAULT '0',
  `ear_plug` tinyint(1) NOT NULL DEFAULT '0',
  `goggles` tinyint(1) NOT NULL DEFAULT '0',
  `gloves` tinyint(1) NOT NULL DEFAULT '0',
  `shoes` tinyint(1) NOT NULL DEFAULT '0',
  `helmet` tinyint(1) NOT NULL DEFAULT '0',
  `full_body` tinyint(1) NOT NULL DEFAULT '0',
  `half_body` tinyint(1) NOT NULL DEFAULT '0',
  `lan_card` tinyint(1) NOT NULL DEFAULT '0',
  `fall_arrestor` tinyint(1) NOT NULL DEFAULT '0',
  `first_aid_box` tinyint(1) NOT NULL DEFAULT '0',
  `reflective_jacket` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_new_inventory`
--

INSERT INTO `master_new_inventory` (`id`, `item_name`, `model_no`, `ram`, `battery`, `laptop_charger`, `antivirus_key`, `hard_disk`, `serial_no`, `warranty_date`, `software`, `chargable`, `zoom`, `pixel`, `length`, `imei_no`, `memory_card`, `calibration_info`, `dongle`, `ear_plug`, `goggles`, `gloves`, `shoes`, `helmet`, `full_body`, `half_body`, `lan_card`, `fall_arrestor`, `first_aid_box`, `reflective_jacket`) VALUES
(3, 'joker vjhcvsdjhvfhs', 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'asdbbasjdbjasb', 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_region`
--

CREATE TABLE `master_region` (
  `id` int(11) NOT NULL,
  `region` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_region`
--

INSERT INTO `master_region` (`id`, `region`) VALUES
(1, 'sector 2, block 5');

-- --------------------------------------------------------

--
-- Table structure for table `master_state`
--

CREATE TABLE `master_state` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_state`
--

INSERT INTO `master_state` (`id`, `country_id`, `state`) VALUES
(2, 1, 'delhi'),
(3, 1, 'rajasthan'),
(4, 1, 'himachal pardesh'),
(5, 1, 'jammu'),
(8, 1, 'jharkhand'),
(10, 1, 'uttar pardesh'),
(11, 3, 'punjab'),
(12, 2, 'california');

-- --------------------------------------------------------

--
-- Table structure for table `master_store`
--

CREATE TABLE `master_store` (
  `id` int(11) NOT NULL,
  `store_code` varchar(100) DEFAULT NULL,
  `lead_person` int(11) NOT NULL,
  `location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_store`
--

INSERT INTO `master_store` (`id`, `store_code`, `lead_person`, `location`) VALUES
(1, 'strore_code12345', 2, 1),
(3, 'sd cns d c', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `master_user_auth`
--

CREATE TABLE `master_user_auth` (
  `id` int(11) NOT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `customer_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `customer_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `customer_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `customer_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `item_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `item_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `item_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `item_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `company_make_view` tinyint(1) NOT NULL DEFAULT '0',
  `company_make_add` tinyint(1) NOT NULL DEFAULT '0',
  `company_make_edit` tinyint(1) NOT NULL DEFAULT '0',
  `company_make_delete` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `store_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `store_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `store_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `store_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `country_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `country_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `country_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `country_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `state_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `state_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `state_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `state_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `city_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `city_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `city_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `city_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `region_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `region_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `region_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `region_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `location_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `location_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `location_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `location_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `branch_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `branch_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `branch_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `branch_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `entity_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `entity_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `entity_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `entity_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `department_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `department_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `department_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `department_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `designation_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `designation_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `designation_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `designation_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `work_package_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `work_package_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `work_package_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `work_package_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `user_registration_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `user_registration_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `user_registration_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `user_registration_master_delete` tinyint(1) NOT NULL DEFAULT '0',
  `usermenu_auth_master_view` tinyint(1) NOT NULL DEFAULT '0',
  `usermenu_auth_master_add` tinyint(1) NOT NULL DEFAULT '0',
  `usermenu_auth_master_edit` tinyint(1) NOT NULL DEFAULT '0',
  `usermenu_auth_master_delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_user_auth`
--

INSERT INTO `master_user_auth` (`id`, `auth_id`, `customer_master_view`, `customer_master_add`, `customer_master_edit`, `customer_master_delete`, `item_master_view`, `item_master_add`, `item_master_edit`, `item_master_delete`, `company_make_view`, `company_make_add`, `company_make_edit`, `company_make_delete`, `vendor_master_view`, `vendor_master_add`, `vendor_master_edit`, `vendor_master_delete`, `store_master_view`, `store_master_add`, `store_master_edit`, `store_master_delete`, `country_master_view`, `country_master_add`, `country_master_edit`, `country_master_delete`, `state_master_view`, `state_master_add`, `state_master_edit`, `state_master_delete`, `city_master_view`, `city_master_add`, `city_master_edit`, `city_master_delete`, `region_master_view`, `region_master_add`, `region_master_edit`, `region_master_delete`, `location_master_view`, `location_master_add`, `location_master_edit`, `location_master_delete`, `branch_master_view`, `branch_master_add`, `branch_master_edit`, `branch_master_delete`, `entity_master_view`, `entity_master_add`, `entity_master_edit`, `entity_master_delete`, `department_master_view`, `department_master_add`, `department_master_edit`, `department_master_delete`, `designation_master_view`, `designation_master_add`, `designation_master_edit`, `designation_master_delete`, `work_package_master_view`, `work_package_master_add`, `work_package_master_edit`, `work_package_master_delete`, `user_registration_master_view`, `user_registration_master_add`, `user_registration_master_edit`, `user_registration_master_delete`, `usermenu_auth_master_view`, `usermenu_auth_master_add`, `usermenu_auth_master_edit`, `usermenu_auth_master_delete`) VALUES
(2, 2, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1),
(3, 3, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_user_register`
--

CREATE TABLE `master_user_register` (
  `id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` varchar(100) NOT NULL DEFAULT 'user',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_confirmed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_user_register`
--

INSERT INTO `master_user_register` (`id`, `email`, `password`, `user_type`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`) VALUES
(1, 'decon@mail.com', '$2a$08$uoIxglw55Snm2Ex66alp6uxrZ69cwtx.jyTkaAtbvyXPhM5YQloHm', 'user', '2016-02-22 13:00:00', '2016-02-22 13:00:00', 0, 0, 0),
(2, 'test@webmail.com', '$2a$08$YnTYdJH2HlrVbAvdVm3XB.zv54F1flax5EoyssS9IO7OF28ML251S', '', '0000-00-00 00:00:00', NULL, 0, 0, 0),
(3, 'narender.01@mail.com', '$2a$08$uoIxglw55Snm2Ex66alp6uxrZ69cwtx.jyTkaAtbvyXPhM5YQloHm', '', '0000-00-00 00:00:00', NULL, 0, 0, 0),
(5, 'test@mail.com', '$2a$08$rI5XT4V3mckr64ulHEFMb.HcsvO85HeasepMEpoTQdcQl1WGn0nxC', 'user', '2016-03-17 14:19:55', NULL, 0, 0, 0),
(6, 'joker@mail.com', '$2a$08$ERdL7Mw.5FZAhAnreRm.BunjEqaPffUKLtm1Xb86V8xZXooHygk9C', 'user', '2016-03-19 17:19:44', NULL, 0, 0, 0);

--
-- Triggers `master_user_register`
--
DELIMITER $$
CREATE TRIGGER `master_user_register_trigger` BEFORE INSERT ON `master_user_register` FOR EACH ROW SET NEW.created_at = NOW()
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `master_vendor`
--

CREATE TABLE `master_vendor` (
  `id` int(11) NOT NULL,
  `vendor_code` varchar(50) DEFAULT NULL,
  `vendor_name` varchar(100) DEFAULT NULL,
  `vendor_tan` varchar(100) DEFAULT NULL,
  `vendor_pan` varchar(100) DEFAULT NULL,
  `service_tax_no` varchar(255) DEFAULT NULL,
  `address` text,
  `remarks` text,
  `concerned_person_name` varchar(100) DEFAULT NULL,
  `concerned_person_designation` int(11) NOT NULL,
  `concerned_person_contact` varchar(15) DEFAULT NULL,
  `concerned_person_email` varchar(100) DEFAULT NULL,
  `primary_account_no` varchar(100) DEFAULT NULL,
  `primary_account_name` varchar(100) DEFAULT NULL,
  `primary_account_ifsc` varchar(100) DEFAULT NULL,
  `primary_account_branch` int(11) DEFAULT NULL,
  `secondary_account_no` varchar(100) DEFAULT NULL,
  `secondary_account_name` varchar(100) DEFAULT NULL,
  `secondary_account_ifsc` varchar(100) DEFAULT NULL,
  `secondary_account_branch` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_vendor`
--

INSERT INTO `master_vendor` (`id`, `vendor_code`, `vendor_name`, `vendor_tan`, `vendor_pan`, `service_tax_no`, `address`, `remarks`, `concerned_person_name`, `concerned_person_designation`, `concerned_person_contact`, `concerned_person_email`, `primary_account_no`, `primary_account_name`, `primary_account_ifsc`, `primary_account_branch`, `secondary_account_no`, `secondary_account_name`, `secondary_account_ifsc`, `secondary_account_branch`) VALUES
(1, '1', 'njnqjd', 'decon_tan32', 'decon_pan222', 'bsdbasjdbasj', 'knkndksnknsdknn', 'dnassssssssssssssssss', 'alok', 1, '8745916499', 'nrathore.web@gmail.com', '21321121', 'Alok', 'boi_ifsc13u83833', 2, '21333333333', 'alok', 'boi_ifsc13u83833', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_vendor_contact_detail`
--

CREATE TABLE `master_vendor_contact_detail` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_vendor_contact_detail`
--

INSERT INTO `master_vendor_contact_detail` (`id`, `vendor_id`, `contact_no`, `email`) VALUES
(1, 1, '8745916499', 'nrathore.web@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `master_work_package`
--

CREATE TABLE `master_work_package` (
  `id` int(11) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_work_package`
--

INSERT INTO `master_work_package` (`id`, `package_name`, `description`) VALUES
(1, 'n sdjc sddjb jsj j', 'jjd sj jdsnds  n  '),
(2, ' n xn ', 'n n dsn ds ');

-- --------------------------------------------------------

--
-- Table structure for table `projects_customer_po`
--

CREATE TABLE `projects_customer_po` (
  `id` int(11) NOT NULL,
  `customer_po_type` varchar(100) DEFAULT NULL,
  `project_code` int(11) DEFAULT NULL,
  `internal_po_no` varchar(100) DEFAULT NULL,
  `customer_po_no` varchar(100) DEFAULT NULL,
  `on_date` varchar(50) DEFAULT NULL,
  `site_id` varchar(50) DEFAULT NULL,
  `work_packages` int(11) DEFAULT NULL,
  `po_value` varchar(255) DEFAULT NULL,
  `payment_terms` varchar(255) DEFAULT NULL,
  `remarks` text,
  `work_status_date` varchar(100) DEFAULT NULL,
  `wcc_status_date` varchar(100) DEFAULT NULL,
  `invoice_date` varchar(100) DEFAULT NULL,
  `payment_status_date` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects_project_master`
--

CREATE TABLE `projects_project_master` (
  `id` int(11) NOT NULL,
  `entity_name` varchar(30) DEFAULT NULL,
  `project_code` varchar(50) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `zonal_head` int(11) DEFAULT NULL,
  `zonal_head_contact` int(11) DEFAULT NULL,
  `zonal_head_email` varchar(100) DEFAULT NULL,
  `team_lead` int(11) DEFAULT NULL,
  `team_lead_contact` int(11) DEFAULT NULL,
  `team_lead_email` varchar(100) DEFAULT NULL,
  `project_admin` varchar(100) DEFAULT NULL,
  `project_admin_contact` int(11) DEFAULT NULL,
  `project_admin_email` varchar(100) DEFAULT NULL,
  `account_admin` varchar(100) DEFAULT NULL,
  `account_admin_contact` int(11) DEFAULT NULL,
  `account_admin_email` varchar(100) DEFAULT NULL,
  `circle_head` varchar(100) DEFAULT NULL,
  `circle_head_contact` int(11) DEFAULT NULL,
  `circle_head_email` varchar(100) DEFAULT NULL,
  `npo_manager` varchar(100) DEFAULT NULL,
  `npo_manager_contact` int(11) DEFAULT NULL,
  `npo_manager_email` varchar(100) DEFAULT NULL,
  `re_lead` varchar(100) DEFAULT NULL,
  `re_lead_contact` int(11) DEFAULT NULL,
  `re_lead_email` varchar(100) DEFAULT NULL,
  `project_bbemp_survey` int(11) NOT NULL DEFAULT '0',
  `project_cluster_drive` int(11) NOT NULL DEFAULT '0',
  `project_cmemf_survey` int(11) NOT NULL DEFAULT '0',
  `project_los_survey` int(11) NOT NULL DEFAULT '0',
  `project_man_month` int(11) NOT NULL DEFAULT '0',
  `project_mrotesting_ms1` int(11) NOT NULL DEFAULT '0',
  `project_mrotesting_ms2` int(11) NOT NULL DEFAULT '0',
  `project_nbemf_survey` int(11) NOT NULL DEFAULT '0',
  `project_rf_survey` int(11) NOT NULL DEFAULT '0',
  `project_scft_drive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_user_auth`
--

CREATE TABLE `project_user_auth` (
  `id` int(11) NOT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `projects_project_view` int(11) NOT NULL DEFAULT '0',
  `projects_project_add` int(11) NOT NULL DEFAULT '0',
  `projects_project_edit` int(11) NOT NULL DEFAULT '0',
  `projects_project_delete` int(11) NOT NULL DEFAULT '0',
  `customerpo_project_view` int(11) NOT NULL DEFAULT '0',
  `customerpo_project_add` int(11) NOT NULL DEFAULT '0',
  `customerpo_project_edit` int(11) NOT NULL DEFAULT '0',
  `customerpo_project_delete` int(11) NOT NULL DEFAULT '0',
  `workstatus_project_view` int(11) NOT NULL DEFAULT '0',
  `workstatus_project_add` int(11) NOT NULL DEFAULT '0',
  `workstatus_project_edit` int(11) NOT NULL DEFAULT '0',
  `workstatus_project_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_user_auth`
--

INSERT INTO `project_user_auth` (`id`, `auth_id`, `projects_project_view`, `projects_project_add`, `projects_project_edit`, `projects_project_delete`, `customerpo_project_view`, `customerpo_project_add`, `customerpo_project_edit`, `customerpo_project_delete`, `workstatus_project_view`, `workstatus_project_add`, `workstatus_project_edit`, `workstatus_project_delete`) VALUES
(2, 2, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 3, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_auth`
--

CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL,
  `user_email_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_auth`
--

INSERT INTO `user_auth` (`id`, `user_email_id`) VALUES
(2, 5),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `vendor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `vendor`) VALUES
(1, 'ASCOM - V-0001'),
(2, 'ASIM NAVIGATION INDIA PVT. LTD. - V-0002'),
(3, 'KAUSHIK TRAVELS - V-0208'),
(4, 'synergy tools - V-0007'),
(5, 'RAJA REDDY - V-0202'),
(6, 'SRISHTI MOBILES - V-0006'),
(7, 'OMKAR HYGIENE&#39;S (LIFE GEAR) - V-0005'),
(8, 'DEEPTI &amp; CO - V-0201'),
(9, 'Satya Computer Solution - V-0004'),
(10, 'FASTECH COMMUNICATIONS PVT LTD - V-0003'),
(11, 'Vipul Travels - V-0203'),
(12, 'JMD.Tour &amp; Travels - V-0206'),
(13, 'Jassi Touriest Service - V-0205'),
(14, 'Kamboj Travels - V-0207'),
(15, 'AS. Tour Centre - V-0204');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hr_daily_info`
--
ALTER TABLE `hr_daily_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_employee`
--
ALTER TABLE `hr_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_employee_address`
--
ALTER TABLE `hr_employee_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_employee_qualification`
--
ALTER TABLE `hr_employee_qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_expenses`
--
ALTER TABLE `hr_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_expenses_detail`
--
ALTER TABLE `hr_expenses_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_outsource_employee`
--
ALTER TABLE `hr_outsource_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_outsource_employee_address`
--
ALTER TABLE `hr_outsource_employee_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_outsource_employee_qualification`
--
ALTER TABLE `hr_outsource_employee_qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_user_auth`
--
ALTER TABLE `hr_user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inter_store_transfer`
--
ALTER TABLE `inter_store_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inter_store_transfer_detail`
--
ALTER TABLE `inter_store_transfer_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_purchase`
--
ALTER TABLE `inventory_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_purchase_detail`
--
ALTER TABLE `inventory_purchase_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_purchase_payment_terms`
--
ALTER TABLE `inventory_purchase_payment_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_user_auth`
--
ALTER TABLE `inventory_user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_accounts_employee_po`
--
ALTER TABLE `invoice_accounts_employee_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_accounts_paymenttransfer`
--
ALTER TABLE `invoice_accounts_paymenttransfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_accounts_po_tovendor`
--
ALTER TABLE `invoice_accounts_po_tovendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_accounts_po_tovendor_detail`
--
ALTER TABLE `invoice_accounts_po_tovendor_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_user_auth`
--
ALTER TABLE `invoice_user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `make`
--
ALTER TABLE `make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_branch`
--
ALTER TABLE `master_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_city`
--
ALTER TABLE `master_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_company`
--
ALTER TABLE `master_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_country`
--
ALTER TABLE `master_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_customer`
--
ALTER TABLE `master_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_customer_contact_detail`
--
ALTER TABLE `master_customer_contact_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_department`
--
ALTER TABLE `master_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_designation`
--
ALTER TABLE `master_designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_entity`
--
ALTER TABLE `master_entity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_item`
--
ALTER TABLE `master_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_location`
--
ALTER TABLE `master_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_new_inventory`
--
ALTER TABLE `master_new_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_region`
--
ALTER TABLE `master_region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_state`
--
ALTER TABLE `master_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_store`
--
ALTER TABLE `master_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_user_auth`
--
ALTER TABLE `master_user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_user_register`
--
ALTER TABLE `master_user_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_vendor`
--
ALTER TABLE `master_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_vendor_contact_detail`
--
ALTER TABLE `master_vendor_contact_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_work_package`
--
ALTER TABLE `master_work_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects_customer_po`
--
ALTER TABLE `projects_customer_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects_project_master`
--
ALTER TABLE `projects_project_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_user_auth`
--
ALTER TABLE `project_user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_auth`
--
ALTER TABLE `user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hr_daily_info`
--
ALTER TABLE `hr_daily_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hr_employee`
--
ALTER TABLE `hr_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hr_employee_address`
--
ALTER TABLE `hr_employee_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hr_employee_qualification`
--
ALTER TABLE `hr_employee_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hr_expenses`
--
ALTER TABLE `hr_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_expenses_detail`
--
ALTER TABLE `hr_expenses_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_outsource_employee`
--
ALTER TABLE `hr_outsource_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_outsource_employee_address`
--
ALTER TABLE `hr_outsource_employee_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_outsource_employee_qualification`
--
ALTER TABLE `hr_outsource_employee_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_user_auth`
--
ALTER TABLE `hr_user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inter_store_transfer`
--
ALTER TABLE `inter_store_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inter_store_transfer_detail`
--
ALTER TABLE `inter_store_transfer_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_purchase`
--
ALTER TABLE `inventory_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_purchase_detail`
--
ALTER TABLE `inventory_purchase_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_purchase_payment_terms`
--
ALTER TABLE `inventory_purchase_payment_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_user_auth`
--
ALTER TABLE `inventory_user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoice_accounts_employee_po`
--
ALTER TABLE `invoice_accounts_employee_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_accounts_paymenttransfer`
--
ALTER TABLE `invoice_accounts_paymenttransfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_accounts_po_tovendor`
--
ALTER TABLE `invoice_accounts_po_tovendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_accounts_po_tovendor_detail`
--
ALTER TABLE `invoice_accounts_po_tovendor_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_user_auth`
--
ALTER TABLE `invoice_user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `make`
--
ALTER TABLE `make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `master_branch`
--
ALTER TABLE `master_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_city`
--
ALTER TABLE `master_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_company`
--
ALTER TABLE `master_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_country`
--
ALTER TABLE `master_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `master_customer`
--
ALTER TABLE `master_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `master_customer_contact_detail`
--
ALTER TABLE `master_customer_contact_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `master_department`
--
ALTER TABLE `master_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_designation`
--
ALTER TABLE `master_designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_entity`
--
ALTER TABLE `master_entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_item`
--
ALTER TABLE `master_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `master_location`
--
ALTER TABLE `master_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `master_new_inventory`
--
ALTER TABLE `master_new_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `master_region`
--
ALTER TABLE `master_region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_state`
--
ALTER TABLE `master_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `master_store`
--
ALTER TABLE `master_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_user_auth`
--
ALTER TABLE `master_user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_user_register`
--
ALTER TABLE `master_user_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `master_vendor`
--
ALTER TABLE `master_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_vendor_contact_detail`
--
ALTER TABLE `master_vendor_contact_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_work_package`
--
ALTER TABLE `master_work_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projects_customer_po`
--
ALTER TABLE `projects_customer_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects_project_master`
--
ALTER TABLE `projects_project_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_user_auth`
--
ALTER TABLE `project_user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_auth`
--
ALTER TABLE `user_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
