<?php include_once("classes/settings.php"); ?>
	<?php
		$invoice =new Invoice();
		require("fpdf/fpdf.php");

	?>
<?php
$invoice->update_id=$_GET['id'];
$invoice->Update_purchase_order_status();

$row=$invoice->purchase_order_invoice_listing_by_id($_GET['id']);
//print_r($row);
$po_no=$row['po_no'];
$po_date=$row['po_date'];
$site_id=$row['site_id'];
$amount=$row['amount'];
$invoice_no=$row['invoice_no'];
$invoice_date=$row['invoice_date'];
$type=$row['type'];




$pdf= new FPDF('p','mm','A4');

$pdf->SetTitle('Invoice'); 
$pdf->SetLeftMargin(20);
$pdf->AddPage();
//define('EURO',chr(128));
$pdf->SetFont("Arial","B",22);
$pdf->Setx(35);  
//$pdf->cell(40,10,"Invoice",0,0,"L",false);
$pdf->Ln();
$pdf->SetFont("Arial","B",12);

//$pdf->cell(0,10,"po_no ",0,0);


$pdf->Image("logo.jpg",80,10,-100);
 $pdf->SetDrawColor(0,80,180);
    //$pdf->SetFillColor(230,230,0);
    //$pdf->SetTextColor(220,50,50);
    //Thickness of frame (1 mm)
    $pdf->SetLineWidth();
$pdf->Line(180, 30, 40-10, 30);

$pdf->SetXY(60,30);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(102,102,0);
$pdf->cell(0,10,"DECON CONSTRUCTION COMPANY",0,1,"L");

$pdf->SetXY(75,38);
//$pdf->SetFont("Times","B",16);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(255,153,51);
$pdf->cell(0,10,"Telecom Solution Provider",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
     $pdf->SetLineWidth();
$pdf->Line(180, 48, 40-10, 48);

$pdf->SetDrawColor(0,0,0);
//$pdf->SetTextColor(255,255,255);
$pdf->SetXY(55,87);
$pdf->SetFont("Arial","B",12);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,55,180,180);
//$pdf->Line(20, 20, 200-10, 20); // 20mm from each edge
//signature, name, terms, condition end

$pdf->SetXY(180,270);
//$pdf->Line(190, 267, 30-10, 267);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont("Times","",10);
$pdf->SetXY(20,55);
$pdf->cell(0,10,"S.T.: AAEFD9836FST001",0,1,"L");
$pdf->SetXY(20,60);
$pdf->cell(0,10,"PAN: AAEFD9836F",0,1,"L");
$pdf->SetXY(20,65);
$pdf->cell(0,10,"Vendor Code: 477567",0,1,"L");
$pdf->SetXY(100,70);

$pdf->SetFont("Times","U",12);
$pdf->cell(0,10,"Invoice",0,1,"L");
$pdf->SetFont("Times","",12);

$pdf->SetLineWidth(0.5);
$pdf->Rect(20,80,90,30);
$pdf->Rect(110,80,90,30);

$pdf->SetXY(20,80);
$pdf->SetFont("Times","",12);
$pdf->cell(0,10,"To",0,1,"L");
$pdf->SetXY(20,85);
$pdf->cell(0,10,"Nokia Solutions and Networks India Pvt. Ltd.",0,1,"L");
$pdf->SetXY(20,90);
$pdf->cell(0,10,"1507, Regus Business Centre, Eros Corporate",0,1,"L");
$pdf->SetXY(20,95);
$pdf->cell(0,10,"Towers, Level 15, Nehru Place, New Delhi-110019",0,1,"L");

$pdf->SetXY(110,80);
$pdf->cell(0,10,"Purchase Order No. ",0,1,"L");
$pdf->SetXY(110,85);
$pdf->cell(0,10,"Purchase Order Date ",0,1,"L");
$pdf->SetXY(110,90);
$pdf->cell(0,10,"Invoice No. ",0,1,"L");
$pdf->SetXY(110,95);
$pdf->cell(0,10,"Invoice Date ",0,1,"L");

$pdf->SetXY(160,80);
$pdf->cell(0,10,": $po_no",0,1,"L");
$pdf->SetXY(160,85);
$pdf->cell(0,10,": $po_date",0,1,"L");
$pdf->SetXY(160,90);
$pdf->cell(0,10,": $invoice_no",0,1,"L");
$pdf->SetXY(160,95);
$pdf->cell(0,10,": $invoice_date",0,1,"L");

$pdf->SetFont("Times","B",12);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,110,25,40);
$pdf->SetXY(20,110);
$pdf->cell(0,10,"Item",0,1,"L");
$pdf->Rect(45,110,25,40);
$pdf->SetXY(45,110);
$pdf->cell(0,10,"Item Code",0,1,"L");

$pdf->Rect(70,110,40,40);
$pdf->SetXY(70,110);
$pdf->cell(0,10,"Description",0,1,"L");

$pdf->Rect(110,110,25,40);
$pdf->SetXY(110,110);
$pdf->cell(0,10,"Siteid",0,1,"L");

$pdf->Rect(135,110,15,40);
$pdf->SetXY(135,110);
$pdf->cell(0,10,"Qty",0,1,"L");

$pdf->Rect(150,110,25,40);
$pdf->SetXY(150,110);
$pdf->cell(0,10,"Unit Price",0,1,"L");

$pdf->Rect(175,110,25,68);
$pdf->SetXY(175,110);
$pdf->cell(0,10,"Amount(Rs.)",0,1,"L");


$pdf->Line(200, 120, 30-10, 120);
if($row['type']=='SCFT'){
//SCFT
$pdf->SetFont("Times","",12);
$pdf->SetXY(22,120);
$pdf->cell(0,10,"00010",0,1,"L");
$pdf->SetXY(22,135);
$pdf->cell(0,10,"00020",0,1,"L");

$pdf->SetXY(47,120);
$pdf->cell(0,10,"00077201",0,1,"L");
$pdf->SetXY(47,135);
$pdf->cell(0,10,"00007740",0,1,"L");

$pdf->SetFont("Times","",11);
$pdf->SetXY(70,120);
$pdf->cell(0,10,"Radio Network Overlay",0,1,"L");

$pdf->SetXY(70,125);
$pdf->cell(0,10,"Design",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(70,135);
$pdf->cell(0,10,"Field Drive Test",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(110,120);
$pdf->cell(0,10,"$site_id",0,1,"L");
$pdf->SetXY(110,135);
$pdf->cell(0,10,"$site_id",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(140,120);
$pdf->cell(0,10,"1",0,1,"L");
$pdf->SetXY(140,135);
$pdf->cell(0,10,"1",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(155,120);
$pdf->cell(0,10,"900",0,1,"L");
$pdf->SetXY(155,135);
$pdf->cell(0,10,"4000",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(180,120);
$pdf->cell(0,10,"900.00",0,1,"L");
$pdf->SetXY(180,135);
$pdf->cell(0,10,"4000.00",0,1,"L");

$pdf->Line(200, 150, 30-10, 150);

$pdf->Line(200, 157, 30-10, 157);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,148);
$pdf->cell(0,10,"Total(Rs.)",0,1,"L");

$pdf->Line(200, 164, 30-10, 164);
$pdf->SetFont("Times","",12);
$pdf->SetXY(20,155);
$pdf->cell(0,10,"Add : Service Tax : @ 14%",0,1,"L");

$pdf->Line(200, 171, 30-10, 171);
$pdf->SetFont("Times","",12);
$pdf->SetXY(20,162);
$pdf->cell(0,10,"Swachh Bharat Cess : @ 0.5%",0,1,"L");

$pdf->Line(200, 178, 30-10, 178);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,169);
$pdf->cell(0,10,"Total Amount(Rs.)",0,1,"L");

$pdf->Line(200, 185, 30-10, 185);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,176);
$pdf->cell(0,10,"Rs. FIVE THOUSAND SIX HUNDRED & ELEVEN ONLY.",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,148);
$pdf->cell(0,10,"4900.00",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,155);
$pdf->cell(0,10,"686.00",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,162);
$pdf->cell(0,10,"24.50",0,1,"L");


$pdf->SetFont("Times","B",12);
$pdf->SetXY(180,169);
$pdf->cell(0,10,"5611",0,1,"L");
//SCFT
}
if($row['type']=='CLUSTER'){


//CLUSTER
$pdf->SetFont("Times","",12);
$pdf->SetXY(22,120);
$pdf->cell(0,10,"00010",0,1,"L");
$pdf->SetXY(22,130);
$pdf->cell(0,10,"00020",0,1,"L");
$pdf->SetXY(22,140);
$pdf->cell(0,10,"00030",0,1,"L");

$pdf->SetXY(47,120);
$pdf->cell(0,10,"00007740",0,1,"L");
$pdf->SetXY(47,130);
$pdf->cell(0,10,"00077201",0,1,"L");
$pdf->SetXY(47,140);
$pdf->cell(0,10,"00007740",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(70,120);
$pdf->cell(0,10,"Field Drive Test",0,1,"L");

$pdf->SetFont("Times","",11);
$pdf->SetXY(70,130);
$pdf->cell(0,10,"Radio Network Overlay",0,1,"L");

$pdf->SetXY(70,135);
$pdf->cell(0,10,"Design",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(70,140);
$pdf->cell(0,10,"Field Drive Test",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(110,120);
$pdf->cell(0,10,"$site_id",0,1,"L");
$pdf->SetXY(110,130);
$pdf->cell(0,10,"$site_id",0,1,"L");
$pdf->SetXY(110,140);
$pdf->cell(0,10,"$site_id",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(140,120);
$pdf->cell(0,10,"1",0,1,"L");
$pdf->SetXY(140,130);
$pdf->cell(0,10,"1",0,1,"L");
$pdf->SetXY(140,140);
$pdf->cell(0,10,"1",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(155,120);
$pdf->cell(0,10,"700",0,1,"L");
$pdf->SetXY(155,130);
$pdf->cell(0,10,"2500",0,1,"L");
$pdf->SetXY(155,140);
$pdf->cell(0,10,"700",0,1,"L");

$pdf->SetFont("Times","",12);
$pdf->SetXY(180,120);
$pdf->cell(0,10,"700.00",0,1,"L");
$pdf->SetXY(180,130);
$pdf->cell(0,10,"2500.00",0,1,"L");
$pdf->SetXY(180,140);
$pdf->cell(0,10,"700.00",0,1,"L");

$pdf->Line(200, 150, 30-10, 150);

$pdf->Line(200, 157, 30-10, 157);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,148);
$pdf->cell(0,10,"Total(Rs.)",0,1,"L");

$pdf->Line(200, 164, 30-10, 164);
$pdf->SetFont("Times","",12);
$pdf->SetXY(20,155);
$pdf->cell(0,10,"Add : Service Tax : @ 14%",0,1,"L");

$pdf->Line(200, 171, 30-10, 171);
$pdf->SetFont("Times","",12);
$pdf->SetXY(20,162);
$pdf->cell(0,10,"Swachh Bharat Cess : @ 0.5%",0,1,"L");

$pdf->Line(200, 178, 30-10, 178);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,169);
$pdf->cell(0,10,"Total Amount(Rs.)",0,1,"L");

$pdf->Line(200, 185, 30-10, 185);
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,176);
$pdf->cell(0,10,"Rs. FOUR THOUSAND FOUR HUNDRED & SIXTY SIX ONLY.",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,148);
$pdf->cell(0,10,"3900.00",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,155);
$pdf->cell(0,10,"546.00",0,1,"L");


$pdf->SetFont("Times","",12);
$pdf->SetXY(180,162);
$pdf->cell(0,10,"19.50",0,1,"L");


$pdf->SetFont("Times","B",12);
$pdf->SetXY(180,169);
$pdf->cell(0,10,"4466",0,1,"L");
//SCFT
}
$pdf->SetFont("Times","B",12);
$pdf->SetXY(20,185);
$pdf->cell(0,10,"For Decon Construction Company",0,1,"L");

$pdf->SetFont("Times","B",12);
$pdf->SetXY(157,227);
$pdf->cell(0,10,"Authorized Signatory",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
$pdf->SetLineWidth();
$pdf->Line(180, 245, 40-10, 245);

$pdf->SetTextColor(102,102,0);

$pdf->SetFont("Times","B",12);
$pdf->SetXY(60,250);
$pdf->cell(0,10,"R-12/28, Raj Nagar, Ghaziabad (U.P.) Pin- 201002",0,1,"L");

$pdf->SetXY(57,256);
$pdf->cell(0,10,"Ph.: 0120-4901639 Fax: 0120-4901639 Mobile : 09310061400",0,1,"L");

$pdf->SetXY(54,262);
$pdf->cell(0,10,"E-mail : info@deconindia.com Website : www.deconindia.com",0,1,"L");
//$pdf->Rect(110,80,90,95);
/*
$pdf->output("invoice.pdf","D"); //Prompting user to choose where to save the PDF file

$pdf->Output('directory/yourfilename.pdf','F'); //Saving the PDF file to server (make sure you have 777 writing permissions for that folder!): 

$pdf->Output('', 'S'); //Method 4: Returning the PDF file content as a string

$pdf->output("invoice.pdf","I");// Automatically open PDF in your browser after being generated:

*/


$pdf->output("$invoice_no.pdf","D");
//$pdf->output("invoice.pdf","I");


?>