<?php include_once("classes/settings.php"); ?>
	<?php
		$invoice =new Invoice();
		require("fpdf/fpdf.php");
	?>

<?php

function number_to_word( $num = '' )
{
	$num	= ( string ) ( ( int ) $num );
	
	if( ( int ) ( $num ) && ctype_digit( $num ) )
	{
		$words	= array( );
		
		$num	= str_replace( array( ',' , ' ' ) , '' , trim( $num ) );
		
		$list1	= array('','one','two','three','four','five','six','seven',
			'eight','nine','ten','eleven','twelve','thirteen','fourteen',
			'fifteen','sixteen','seventeen','eighteen','nineteen');
		
		$list2	= array('','ten','twenty','thirty','forty','fifty','sixty',
			'seventy','eighty','ninety','hundred');
		
		$list3	= array('','thousand','million','billion','trillion',
			'quadrillion','quintillion','sextillion','septillion',
			'octillion','nonillion','decillion','undecillion',
			'duodecillion','tredecillion','quattuordecillion',
			'quindecillion','sexdecillion','septendecillion',
			'octodecillion','novemdecillion','vigintillion');
		
		$num_length	= strlen( $num );
		$levels	= ( int ) ( ( $num_length + 2 ) / 3 );
		$max_length	= $levels * 3;
		$num	= substr( '00'.$num , -$max_length );
		$num_levels	= str_split( $num , 3 );
		
		foreach( $num_levels as $num_part )
		{
			$levels--;
			$hundreds	= ( int ) ( $num_part / 100 );
			$hundreds	= ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '' );
			$tens		= ( int ) ( $num_part % 100 );
			$singles	= '';
			
			if( $tens < 20 )
			{
				$tens	= ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
			}
			else
			{
				$tens	= ( int ) ( $tens / 10 );
				$tens	= ' ' . $list2[$tens] . ' ';
				$singles	= ( int ) ( $num_part % 10 );
				$singles	= ' ' . $list1[$singles] . ' ';
			}
			$words[]	= $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
		}
		
		$commas	= count( $words );
		
		if( $commas > 1 )
		{
			$commas	= $commas - 1;
		}
		
		$words	= implode( ', ' , $words );
		
		//Some Finishing Touch
		//Replacing multiples of spaces with one space
		$words	= trim( str_replace( ' ,' , ',' , trim( ucwords( $words ) ) ) , ', ' );
		if( $commas )
		{
			$words	= str_replace( ',' , ' and' , $words );
		}
		
		return $words;
	}
	else if( ! ( ( int ) $num ) )
	{
		return 'Zero';
	}
	return '';
}

?>

<?php
$invoice->update_id=$_GET['id'];
$invoice->Update_purchase_order_status();

$row=$invoice->purchase_order_invoice_listing_by_id($_GET['id']);
//print_r($row);
$po_no=$row['po_no'];
$po_date=$row['po_date'];
$site_id=$row['site_id'];
$amount=$row['amount'];
$invoice_no=$row['invoice_no'];
$invoice_date=$row['invoice_date'];
$type=$row['type'];




$pdf= new FPDF('p','mm','A4');

$pdf->SetTitle('Invoice'); 
$pdf->SetLeftMargin(20);
$pdf->AddPage();
//define('EURO',chr(128));
$pdf->SetFont("Arial","B",22);
$pdf->Setx(35);  
//$pdf->cell(40,10,"Invoice",0,0,"L",false);
$pdf->Ln();
$pdf->SetFont("Arial","B",12);

//$pdf->cell(0,10,"po_no ",0,0);


$pdf->Image("logo.jpg",80,10,-100);
 $pdf->SetDrawColor(0,80,180);
    //$pdf->SetFillColor(230,230,0);
    //$pdf->SetTextColor(220,50,50);
    //Thickness of frame (1 mm)
    $pdf->SetLineWidth();
$pdf->Line(180, 30, 40-10, 30);

$pdf->SetXY(60,30);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(102,102,0);
$pdf->cell(0,10,"DECON CONSTRUCTION COMPANY",0,1,"L");

$pdf->SetXY(75,38);
//$pdf->SetFont("Times","B",16);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(255,153,51);
$pdf->cell(0,10,"Telecom Solution Provider",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
     $pdf->SetLineWidth();
$pdf->Line(180, 48, 40-10, 48);

$pdf->SetDrawColor(0,0,0);
//$pdf->SetTextColor(255,255,255);
$pdf->SetXY(55,87);
$pdf->SetFont("Arial","B",8);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,55,180,180);
//$pdf->Line(20, 20, 200-10, 20); // 20mm from each edge
//signature, name, terms, condition end

$pdf->SetXY(180,270);
//$pdf->Line(190, 267, 30-10, 267);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont("Times","B",8);
$pdf->SetXY(20,55);
$pdf->cell(0,10,"S.T.: AAEFD9836FST001",0,1,"L");
$pdf->SetXY(20,60);
$pdf->cell(0,10,"PAN: AAEFD9836F",0,1,"L");
$pdf->SetXY(20,65);
$pdf->cell(0,10,"Vendor Code: 477567",0,1,"L");
$pdf->SetXY(100,70);


$pdf->SetLineWidth(0.5);
$pdf->Rect(20,75,90,25);
$pdf->Rect(110,75,90,25);

$pdf->SetXY(110,75);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,10,"To",0,1,"L");
$pdf->SetXY(110,80);
$pdf->cell(0,10,"Nokia Solutions and Networks India Pvt. Ltd.",0,1,"L");
$pdf->SetXY(110,85);
$pdf->cell(0,10,"1507, Regus Business Centre, Eros Corporate",0,1,"L");
$pdf->SetXY(110,90);
$pdf->cell(0,10,"Towers, Level 15, Nehru Place, New Delhi-110019",0,1,"L");

$pdf->SetXY(20,75);
$pdf->cell(0,10,"Invoice No ",0,1,"L");
$pdf->SetXY(20,80);
$pdf->cell(0,10,"Invoice Date ",0,1,"L");
$pdf->SetXY(20,85);
$pdf->cell(0,10,"Project ",0,1,"L");
$pdf->SetXY(20,90);
$pdf->cell(0,10,"Circle ",0,1,"L");

$pdf->SetXY(60,75);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,80);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,85);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,90);
$pdf->cell(0,10,": ",0,1,"L");

$pdf->SetXY(70,75);
$pdf->cell(0,10," UENBS15161215",0,1,"L");
$pdf->SetXY(70,80);
$pdf->cell(0,10," 08-Apr-2016",0,1,"L");
$pdf->SetXY(70,85);
$pdf->cell(0,10," IN Bharti 3G",0,1,"L");
$pdf->SetXY(70,90);
$pdf->cell(0,10," UPE",0,1,"L");

$pdf->SetXY(90,100);
$pdf->SetFont("Times","U","B",8);
$pdf->cell(0,05," Item Description",0,1,"L");

//$pdf->SetLineWidth();
//$pdf->Line(180, 48, 40-10, 48);
$pdf->Rect(20,105,180,5);
$pdf->Rect(20,110,180,5);

$pdf->SetXY(20,105);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 10        Item Code :- 00077201          Description:- Radio Network Overlay Design",0,1,"L");

$pdf->SetXY(20,110);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 20        Item Code :- 00007740          Description:- Field Drive Test",0,1,"L");

$pdf->Line(200, 123, 30-10, 123);

$pdf->Line(200, 130, 30-10, 130);

$pdf->SetFont("Times","B",9);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,123,10,50);
$pdf->SetXY(20,123);
$pdf->cell(0,06,"Sr. No.",0,1,"L");
$pdf->Rect(30,123,24,50);
$pdf->SetXY(38,123);
$pdf->cell(0,06,"Po No.",0,1,"L");
$pdf->Rect(54,123,24,50);
$pdf->SetXY(58,123);
$pdf->cell(0,06,"Po Date",0,1,"L");
$pdf->Rect(78,123,24,50);
$pdf->SetXY(82,123);
$pdf->cell(0,06,"Site ID",0,1,"L");
$pdf->Rect(102,123,24,50);
$pdf->SetXY(106,123);
$pdf->cell(0,06,"WP ID",0,1,"L");
$pdf->Rect(126,123,24,50);
$pdf->SetXY(126,123);
$pdf->cell(0,06,"IA Done Date",0,1,"L");
$pdf->Rect(150,123,10,50);
$pdf->SetXY(151,123);
$pdf->cell(0,06,"Qty.",0,1,"L");
$pdf->Rect(160,123,20,50);
$pdf->SetXY(161,123);
$pdf->cell(0,06,"Unit Price",0,1,"L");
$pdf->Rect(180,123,20,69);
$pdf->SetXY(181,123);
$pdf->cell(0,06,"Amount (Rs.)",0,1,"L");
$pdf->Line(200, 173, 30-10, 173);

$pdf->Line(200, 135, 30-10, 135);
$pdf->Line(200, 140, 30-10, 140);
$pdf->Line(200, 145, 30-10, 145);
$pdf->Line(200, 150, 30-10, 150);
$pdf->Line(200, 155, 30-10, 155);
$pdf->Line(200, 160, 30-10, 160);
$pdf->Line(200, 165, 30-10, 165);
$pdf->Line(200, 170, 30-10, 170);
$srno=130;
$pdf->SetFont("Times","",9);
for($i=1;$i<=8;$i++){$pdf->SetXY(20,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(30,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(54,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(78,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(102,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(126,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(150,$srno);$pdf->cell(0,05,"1",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(160,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}$srno=130;
for($i=1;$i<=8;$i++){$pdf->SetXY(180,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;}

$pdf->Line(200, 177, 30-10, 177);
$pdf->SetFont("Times","",9);
$pdf->SetXY(20,172);
$pdf->cell(0,06,"Total",0,1,"L");
$pdf->Line(200, 182, 30-10, 182);
$pdf->SetXY(20,177);
$pdf->cell(0,06,"Add: Service Tax : @14%",0,1,"L");
$pdf->Line(200, 187, 30-10, 187);
$pdf->SetXY(20,182);
$pdf->cell(0,06,"Swacch Bharat cess : @0.5%",0,1,"L");
$pdf->SetFont("Times","B",9);
$pdf->Line(200, 192, 30-10, 192);
$pdf->SetXY(20,187);
$pdf->cell(0,06,"Total Amount",0,1,"L");
$pdf->Line(200, 197, 30-10, 197);
$pdf->SetXY(20,192);

$amountword=number_to_word(44884);
$pdf->cell(0,06,"$amountword" ."  only",0,1,"L");

$pdf->SetFont("Times","B",10);
$pdf->SetXY(20,195);
$pdf->cell(0,10,"For Decon Construction Company",0,1,"L");
$pdf->Line(200, 202, 30-10, 202);

$pdf->SetFont("Times","B",12);
$pdf->SetXY(157,227);
$pdf->cell(0,10,"Authorized Signatory",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
$pdf->SetLineWidth();
$pdf->Line(180, 245, 40-10, 245);

$pdf->SetTextColor(102,102,0);

$pdf->SetFont("Times","B",12);
$pdf->SetXY(60,250);
$pdf->cell(0,10,"R-12/28, Raj Nagar, Ghaziabad (U.P.) Pin- 201002",0,1,"L");

$pdf->SetXY(57,256);
$pdf->cell(0,10,"Ph.: 0120-4901639 Fax: 0120-4901639 Mobile : 09310061400",0,1,"L");

$pdf->SetXY(54,262);
$pdf->cell(0,10,"E-mail : info@deconindia.com Website : www.deconindia.com",0,1,"L");
//$pdf->Rect(110,80,90,95);
/*
$pdf->output("invoice.pdf","D"); //Prompting user to choose where to save the PDF file

$pdf->Output('directory/yourfilename.pdf','F'); //Saving the PDF file to server (make sure you have 777 writing permissions for that folder!): 

$pdf->Output('', 'S'); //Method 4: Returning the PDF file content as a string

$pdf->output("invoice.pdf","I");// Automatically open PDF in your browser after being generated:

*/
$pdf->AddPage();
 $pdf->SetDrawColor();
$pdf->SetLineWidth(0.4);

$pdf->SetTextColor(255,255,255);
$pdf->Rect(5,20,200,50);
$pdf->SetFont("Times","B",10);

//$pdf->Line(200, 202, 30-10, 202);
$pdf->SetFont("Times","B",7);
 $pdf->SetDrawColor();
$pdf->SetLineWidth(0.3);
$pdf->SetFillColor(153,0,153);
$pdf->Rect(5,20,200,10,'F');
 $pdf->SetDrawColor();
$pdf->Line(205, 30,15-10, 30);
$pdf->Line(10, 20,60-50, 70);
$pdf->Line(20, 20,70-50, 70);
$pdf->Line(30, 20,80-50, 70);
$pdf->Line(45, 20,95-50, 70);
$pdf->Line(60, 20,110-50, 70);
$pdf->Line(75, 20,125-50, 70);
$pdf->Line(90, 20,140-50, 70);
$pdf->Line(105, 20,155-50, 70);
$pdf->Line(120, 20,170-50, 70);
$pdf->Line(135, 20,185-50, 70);
$pdf->Line(160, 20,210-50, 70);
$pdf->Line(175, 20,225-50, 70);
$pdf->Line(190, 20,240-50, 70);
$pdf->Line(175, 20,225-50, 70);



$pdf->SetXY(5,20);
$pdf->cell(0,06,"Sr.",0,1,"L");
$pdf->SetXY(10,20);
$pdf->cell(0,06,"Project",0,1,"L");
$pdf->SetXY(20,20);
$pdf->cell(0,06,"Circle",0,1,"L");
$pdf->SetXY(30,20);
$pdf->cell(0,06,"Invoice No",0,1,"L");

$pdf->SetXY(45,20);
$pdf->cell(0,06,"Invoice Date",0,1,"L");
$pdf->SetXY(60,20);
$pdf->cell(0,06,"PO No",0,1,"L");
$pdf->SetXY(75,20);
$pdf->cell(0,06,"PO Date",0,1,"L");
$pdf->SetXY(90,20);
$pdf->cell(0,06,"SiteCustomer",0,1,"L");
$pdf->SetXY(90,25);
$pdf->cell(0,06,"Site Code",0,1,"L");

$pdf->SetXY(105,20);
$pdf->cell(0,06,"WP Work",0,1,"L");
$pdf->SetXY(105,25);
$pdf->cell(0,06,"Package ID",0,1,"L");

$pdf->SetXY(120,20);
$pdf->cell(0,06,"Item Id",0,1,"L");

$pdf->SetXY(135,20);
$pdf->cell(0,06,"Work Item/Subtype",0,1,"L");

$pdf->SetXY(160,20); 
$pdf->cell(0,06,"Complete",0,1,"L");
$pdf->SetXY(162,25);
$pdf->cell(0,02,"Date",0,1,"L");
$pdf->SetXY(161,25);
$pdf->cell(0,07,"Actual",0,1,"L");

$pdf->SetXY(175,20);
$pdf->cell(0,06,"Ia Internal",0,1,"L");
$pdf->SetXY(175,25);
$pdf->cell(0,06,"Acceptance",0,1,"L");

$pdf->SetXY(190,20);
$pdf->cell(0,06,"Goods Recipt",0,1,"L");

$pdf->Line(205, 35,15-10, 35);
$pdf->Line(205, 40,15-10, 40);
$pdf->Line(205, 45,15-10, 45);
$pdf->Line(205, 50,15-10, 50);
$pdf->Line(205, 55,15-10, 55);
$pdf->Line(205, 60,15-10, 60);
$pdf->Line(205, 65,15-10, 65);
$pdf->SetTextColor();

$pdf->output("$invoice_no.pdf","I");
//$pdf->output("invoice.pdf","D");


?>