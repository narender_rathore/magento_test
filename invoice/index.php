

<?php include_once('header.php');?>
<?php

?>
<table class="table table-striped table-hover " id="mytblId">
  <thead>
    <tr>
      <th>Sr.No</th>
      <th>PO NO</th>
      <th>PO Date</th>
      <th>Site ID</th>
      <th>Amount</th>
      <th>Invoice No</th>
      <th>Invoice Date</th>
      <th>Type</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
<?php /*?>  	<tr class="info"></tr>
    <tr class="success"></tr>
    <tr class="danger"></tr>
    <tr class="warning"></tr>
    <tr class="active"></tr><?php */?>
<?php $query=$invoice->purchase_order_invoice_listing();  ?>
<?php $query->execute(); 
	  $num_rows=$query->rowcount();
	  ?>

<?php 
		if($num_rows>0){$sn=1;
			while($row=$query->fetch()){ 			
			?>
    <tr>
      <td><?php echo $sn; ?></td>
      <td><?php echo $row['po_no']; ?></td>
      <td><?php echo $row['po_date']; ?></td>
      <td><?php echo $row['site_id']; ?></td>
      <td><?php echo $row['amount']; ?></td>
      <td><?php echo $row['invoice_no']; ?></td>
      <td><?php echo $row['invoice_date']; ?></td>
      <td><?php echo $row['type']; ?></td>
      <td><?php if($row['user_id']=='0'){echo 'pending';}elseif($row['user_id']=='1'){echo 'Done';} ?></td>
      <td>	
      		<a href="<?=DOCUMENT_ROOT?>view.php?id=<?=$row['id']?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;
      		<a href="<?=DOCUMENT_ROOT?>edit.php?id=<?=$row['id']?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
      		<a href="<?=DOCUMENT_ROOT?>delete.php?id=<?=$row['id']?>" ><input type="checkbox" value=""></a>&nbsp;&nbsp;
			<a href="<?=DOCUMENT_ROOT?>invoice.php/?id=<?=$row['id']?>" ><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
	  </td>
    </tr>
<?php 
		$sn++;
		} 
		} 
		else{ ?>
	<tr>
	  <td colspan="6"><strong>No record found</strong></td>
	</tr>
	<?php
		}  
	 ?>
  </tbody>
</table> 


</body>
<script type="text/javascript">
<!--
function myPopup2() {
window.open( "import/invoice_csv.php", "myWindow", 
"status = 1, height = 300, width = 300, resizable = 0" )
}
//-->
</script>
<script type="text/javascript">
function ExportToExcel(){
      window.location.assign("export.php");
    }
</script>
</html>