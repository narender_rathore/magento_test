<?php
include_once("connection.php");

class User extends Connection{

	public function login($username,$password){
		$sql=$this->myconn->prepare("SELECT * FROM admin  where username=:username and password=:password and type=:type");
		$sql->execute(array(':username' => $username, ':password' => $password,':type'=>'admin'));
		$result = $sql->fetch();
		return $result;
	}
	
	public function redirect($url){
		header("location:$url");
	}
	
	public function last_login($login_date,$ip){
		$sql=$this->myconn->prepare("update admin set last_login=:date,last_login_ip =:ip");
		$sql->execute(array(':date' => $login_date, ':ip' => $ip));
	}
	
	public function forget($email){
		$sql=$this->myconn->prepare("SELECT * FROM admin  where email=:email  and type=:type");
		$sql->execute(array(':email' => $email,':type'=>'admin'));
		$result = $sql->fetch();
		return $result;
	}
	
	public function admin_profile(){
		$sql=$this->myconn->prepare("SELECT * FROM admin  where  type=:type");
		$sql->execute(array(':type'=>'admin'));
		$result = $sql->fetch();
		return $result;
	}
	
	public function update_Admin(){
		$sql_query="update admin set fname=:fname,lname=:lname,email=:email,password=:password,username=:username";
		$sql_prepare=$this->myconn->prepare($sql_query);
		$sql_prepare->bindparam(":fname",$this->fname);
		$sql_prepare->bindparam(":lname",$this->lname);
		$sql_prepare->bindparam(":username",$this->username);
		$sql_prepare->bindparam(":email",$this->email);
		$sql_prepare->bindparam(":password",$this->password);
		$sql_prepare->execute();
		return true;
	}
	
	public function update_profile_image(){
		$sql_query="update admin set profile_pic=:profile_pic where type=:type";
		$sql_prepare=$this->myconn->prepare($sql_query);
		$sql_prepare->bindparam(":profile_pic",$this->profile_pic);
		$sql_prepare->bindvalue(":type",'admin');
		$sql_prepare->execute();
		return true;
	}
	
	public function subscriber_list(){
		$sql_query="select * from subscribe_email";
		$sql_prepare=$this->myconn->prepare($sql_query);
		return $sql_prepare;
	}
}


?>