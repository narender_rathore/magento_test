<?php
	require("connection.php");

	class Product extends Connection{
		
		public function product_listing(){
		
			$sql="select * from product";;
			$query=$this->myconn->prepare($sql);
			return $query;
		}
		
		public function product_listing_by_id($id){
		
			$sql="select * from product where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
	
		public function Add_product(){
		
			$sql="insert into product set brand_id=:brand_id,product=:product_name,price=:price,quantity=:quantity,meta_title=:meta_title,
			add_date=:add_date,status=:status,terms=:terms";
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":brand_id",$this->brand_id);
			$query->bindparam(":product_name",$this->product_name);
			$query->bindparam(":meta_title",$this->meta_title);
			$query->bindparam(":terms",$this->terms);
			$query->bindparam(":price",$this->price);
			$query->bindparam(":quantity",$this->quantity);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->execute();
			return $this->myconn->lastInsertId();
		}
		
		public function Update_ProductImage(){
		
			$sql ="update product set big_image='$this->product_big_image',small_image='$this->product_small_image',thumb_image='$this->product_thumb_image' where id='$this->product_id'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			return true;
		}
		
		public function Edit_product(){
		
			$sql="update product set brand_id=:brand_id,product=:product_name,price=:price,quantity=:quantity,meta_title=:meta_title,
			add_date=:add_date,status=:status,terms=:terms where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":brand_id",$this->brand_id);
			$query->bindparam(":product_name",$this->product_name);
			$query->bindparam(":meta_title",$this->meta_title);
			$query->bindparam(":terms",$this->terms);
			$query->bindparam(":price",$this->price);
			$query->bindparam(":quantity",$this->quantity);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->bindparam(":id",$this->id);
			$query->execute();
			return true;
		}
		
		public function delete_product(){
		
			$sql="delete from product where id='$this->id'";
			$query=$this->myconn->query($sql);
			$query->execute();
			return true;
		}
		
		public function order_listing(){
		
			$sql="select * from orders";;
			$query=$this->myconn->prepare($sql);
			return $query;
		}
		
		public function user_detail_by_id($id){
		
			$sql="select * from users_info where id='$id'";;
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
		}
		
		public function my_order($id,$order){
		
			$sql="select * from orders_details where user_id='$id' and order_code='$order' ";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$count=$query->rowcount();
			return $count;

		}
		
		public function get_order_details($id){
		
			$sql="select * from orders where id='$id'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;

		}
		
	public function EmailUpdateOrderStatus(){
		
			$sql="select * from orders where id='$this->order_id'";
			$query=$this->myconn->query($sql);
			$fetch=$query->fetch();
			$ord_details_msg="";
			$ordernumber=$fetch['order_code'];
			
			$sql1="select * from users_info where id='".$fetch['user_id']."'";
			$query1=$this->myconn->query($sql1);
			$fetch1=$query1->fetch();
			
			$email=$fetch1['email'];
			$fullname=$fetch1['fname']."&nbsp;".$fetch1['lname'];
			
			$sql2="select * from orders_details where user_id='".$fetch['user_id']."' and order_code='".$fetch['order_code']."' ";
			$query2=$this->myconn->query($sql2);
			
			while($fetch2=$query2->fetch()){
			
				$sql3="select * from product where id='".$fetch2['product_id']."'  ";
				$query3=$this->myconn->query($sql3);
				$fetch3=$query3->fetch();
				
				$sql4="select * from brand where id='".$fetch3['brand_id']."'  ";
				$query4=$this->myconn->query($sql4);
				$fetch4=$query4->fetch();
				
				$ord_details_msg .= "Product Name : ".$fetch4['brand']."&nbsp;".ucwords($fetch3['product'])."&nbsp;&nbsp;&nbsp;&nbsp;Price per day :
				 "."Rs. ".$fetch2['price']." &nbsp;&nbsp;&nbsp;&nbsp;Booking :". $fetch2['days']." days&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From : 
				 ".$fetch2['from_date']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To : ".$fetch2['to_date']."<br><br>";
				
			}
			
				$ord_details_msg .= "<strong> Sub Total : </strong> Rs. ".number_format($fetch['subtotal'])."<br>";
				$ord_details_msg .= "<strong>Service Tax : </strong> Rs. ".number_format($fetch['service_tax'])."<br>";
				$ord_details_msg .= "<strong>Pick & drop Charges : </strong> Rs. ".number_format($fetch['pd_charges'])."<br>";
				$ord_details_msg .= "<strong>Total Amount - Discount + Shipping Charges = Total Payable : 
				</strong> Rs. ".number_format($fetch['total_price'])."<br><br>";
				
				
				$msg="Dear ".$fullname.","."<br><br>
		
				".str_replace('{$order_number}',$ordernumber, $this->email_message_touser)."
				<br>
				
						
				Order details are as below:<br><br>
				
				<strong>Order Number</strong>: $ordernumber<br><Br>
				
				$ord_details_msg<br><br><br>
				
				Kind regards,<br>
				
				Lets Ryde Team .";
		
				/*$from="support@letsryde.com";
				
				$replyto="support@letsryde.com";*/
				
				$from="narender.rathore@websolutioncentre.com";
				
				$replyto="narender.rathore@websolutioncentre.com";
				
				$to=$email;
				
				$subject="Order details - Lets Ryde Team";
				
				$headers = "From: " . $from . "\r\nBcc: $bcc \r\n";
				
				$headers .= "Reply-To: ". strip_tags($replyto) . "\r\n";
				
				$headers .= "MIME-Version: 1.0\r\n";
				
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				
				$result= mail($to,$subject,$msg,$headers);
			
				return true;

		}
		
			public function UpdateOrderStatus(){

			 $sql = "update orders set status='$this->order_status' where id='".$this->order_id."'";
			
			$result=$this->myconn->query($sql);
			if($result){
			return true;
			}else{
			return false;
			}
		
		}
		
		

	
	}
	





?>