<?php
class Connection{
	public 	  $myconn;
	public 	  $host		='localhost';
	public	  $db		='decon_invoice';
	public	  $user		='root';
	public	  $pass		='';
	
	public function __construct(){
		try{
			$this->myconn=new PDO("mysql:host=$this->host;dbname=$this->db",$this->user,$this->pass,array(PDO::ATTR_PERSISTENT => true));
		}
		catch(Exception $e){
			echo "Error Message!".$e->getMessage();
			exit;
		}
	}
	
	public function close_Connection(){
	$this->myconn=null;
	}
	
	
}


?>