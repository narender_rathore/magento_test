<?php
include_once("../db-connection/conn.inc");
include_once("../classes/product.class.php");
require_once("../thumbnail.inc.php");
#default connection
$info = new clsSettings();
$admin_site_server   = $info->site; 
$admin_site_user     = $info->siteuser;				
$admin_site_passwd   = $info->sitepass; 
$admin_site_db 		= $info->sitedb;
$default_con = mysql_connect($admin_site_server, $admin_site_user, $admin_site_passwd)	;
mysql_select_db($admin_site_db, $default_con) or die("unable to connect");
global $default_con;

$products = new products();

$products->product_id = $_REQUEST['product_id'];

$config['Project_image_path'] = "../projects_image"; //website absolute path

$output_dir = $config['Project_image_path']."/".$_REQUEST['product_id']."/";

if(!file_exists($output_dir)){
	mkdir($output_dir,0777,TRUE);
}
	
if(isset($_FILES["myfile"]))
{
	$ret = array();

	$error =$_FILES["myfile"]["error"];
	//You need to handle  both cases
	//If Any browser does not support serializing of multiple files using FormData() 
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	//$fileName = $_FILES["myfile"]["name"];
		$name = $_FILES["myfile"]["name"];
		$tmpname1 = $_FILES["myfile"]["tmp_name"];
				##add thumb images--	
				$name=time().$name;
				
				move_uploaded_file($tmpname1,$output_dir.$name);
				
				//move_uploaded_file($tmpname1,$output_dir.$name);
				#thumb 1
				$name_small ="small-".$name;
				$thumb = new Thumbnail($output_dir.$name);
				$thumb->resize(100,115);
				$thumb->save($output_dir.$name_small);
				#thumb 2
				$name_thumb ="thumb-".$name;
				$thumb->resize(350,200);
				$thumb->save($output_dir.$name_thumb);
				
				$products->product_small_image = $name_small;
				$products->product_image_thumb = $name_thumb;
				$products->product_big_image = $name;
				
				$ret[]= $name;
				//insert 
				$products->Add_Project_gallery();

		##
		
	}
	else  //Multiple files, file[]
	{
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
	  	//$fileName = $_FILES["myfile"]["name"][$i];
		//move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName);
		
			#image upload
			$name = $_FILES["myfile"]["name"][$i];
			$tmpname1 = $_FILES["myfile"]["tmp_name"][$i];
			if($name !="")
			{  
				
				$name=time().$name;
				
				//move_uploaded_file($tmpname1,$output_dir.$name);
				#thumb 1
				$name_small ="small-".$name;
				$thumb = new Thumbnail($output_dir.$name);
				$thumb->resize(100,115);
				$thumb->save($output_dir.$name_small);
				#thumb 2
				$name_thumb ="thumb-".$name;
				$thumb->resize(350,200);
				$thumb->save($output_dir.$name_thumb);
				
				$products->product_small_image = $name_small;
				$products->product_image_thumb = $name_thumb;
				$products->product_big_image = $name;
				
				//insert 
				$products->Add_Project_gallery();
				
			
			}
		
		
	  	$ret[]= $name;
	  }
	
	}
    echo json_encode($ret);
 }
 ?>