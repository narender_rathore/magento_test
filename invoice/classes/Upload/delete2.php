<?php
include_once("../db-connection/conn.inc");
include_once("../classes/product.class.php");

#default connection
$info = new clsSettings();
$admin_site_server   = $info->site; 
$admin_site_user     = $info->siteuser;				
$admin_site_passwd   = $info->sitepass; 
$admin_site_db 		= $info->sitedb;
$default_con = mysql_connect($admin_site_server, $admin_site_user, $admin_site_passwd)	;
mysql_select_db($admin_site_db, $default_con) or die("unable to connect");
global $default_con;

$products = new products();

$products->product_id = $_REQUEST['product_id'];

$products->product_big_image = $_POST['name'];

$config['Project_image_path'] = "../projects_image"; //website absolute path
$output_dir = $config['Project_image_path']."/".$_POST['product_id']."/";
if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
{
	$fileName =$_POST['name'];
	$filePath = $output_dir. $fileName;
	if (file_exists($filePath)) 
	{
        unlink($filePath);
		$filePath_small = $output_dir."small-".$fileName;
		$filePath_thumb = $output_dir."thumb-".$fileName;
		@unlink($filePath_small);
		@unlink($filePath_thumb);
		$products->Delete_Project_gallery();
    }
	echo "Deleted File ".$fileName."<br>";
}

?>