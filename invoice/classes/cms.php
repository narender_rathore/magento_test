<?php
include_once("connection.php");

	class Cms extends Connection{
	
	
		public function cms_listing(){
		
			$sql="select * from cms";;
			$query=$this->myconn->prepare($sql);
			return $query;
		}
		
		public function cms_listing_by_id($id){
		
			$sql="select * from cms where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
	
		public function Add_cms(){
		
			$sql="insert into cms set page=:page_title,meta_title=:meta_title,meta_keyword=:meta_keyword,description=:description,add_date=:add_date,status=:status";
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":page_title",$this->page_title);
			$query->bindparam(":meta_title",$this->meta_title);
			$query->bindparam(":meta_keyword",$this->meta_keyword);
			$query->bindparam(":description",$this->description);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->execute();
			return $this->myconn->lastInsertId();
		}
		
		
		public function Edit_cms(){
		
			$sql="update cms set page=:page_title,meta_title=:meta_title,meta_keyword=:meta_keyword,description=:description,add_date=:add_date,status=:status where id=:id";
			//$sql2="update cms set page='$this->page_title',meta_title='$this->meta_title',meta_keyword='$this->meta_keyword',description='$this->description',add_date='$this->add_date',status='$this->status' where id='$this->id'";
			//echo $sql2;die;
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":page_title",$this->page_title);
			$query->bindparam(":meta_title",$this->meta_title);
			$query->bindparam(":meta_keyword",$this->meta_keyword);
			$query->bindparam(":description",$this->description);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->bindparam(":id",$this->id);
			$query->execute();
			return true;
		}
		
		public function delete_cms(){
		
			$sql="delete from cms where id='$this->id'";
			$query=$this->myconn->query($sql);
			$query->execute();
			return true;
		}
		
		public function partner_listing(){
		
			$sql="select * from partner";;
			$query=$this->myconn->prepare($sql);
			return $query;
		}
		
		public function partner_listing_by_id($id){
			
			$sql="select * from partner where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
		
		public function Add_partner(){
		
			$sql="insert into partner set partner=:partner,add_date=:add_date,status=:status";
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":partner",$this->partner_name);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->execute();
			return $this->myconn->lastInsertId();
		}
		
		public function Edit_partner(){
		
			//$sql="update partner set partner=:partner,add_date=:add_date,status=:status where id=:id";
			$sql2="update partner set partner='$this->partner_name',add_date='$this->add_date',status='$this->status' where id='$this->id'";
			$query=$this->myconn->prepare($sql2);
			/*$query->bindparam(':partner',$this->partner_name);
			$query->bindparam(':add_date',$this->add_date);
			$query->bindparam(':status',$this->status);
			$query->bindparam(':id',$this->id);*/
			$query->execute();
			return true;
		}
		
		public function Update_partner_image(){
		
			$sql ="update partner set partner_logo_small='$this->partner_logo_small',partner_logo_thumb='$this->partner_logo_thumb',
			partner_logo_big='$this->partner_logo_big' where id='$this->partner_id'";
			
			$query=$this->myconn->prepare($sql);
			$query->execute();
			return true;
		
		}
		
		public function delete_partner(){
		
			$sql="delete from partner where id='$this->id'";
			$query=$this->myconn->query($sql);
			$query->execute();
			return true;
		}
		
		public function testimonial_listing(){
		
			$sql="select * from testimonial";;
			$query=$this->myconn->prepare($sql);
			return $query;
		}
		
		public function testimonial_listing_by_id($id){
			
			$sql="select * from testimonial where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
		
		public function Add_testimonial(){
		
			$sql="insert into testimonial set name=:name,add_date=:add_date,status=:status,designation=:designation,testimonial=:testimonial";
			/*$sql2="insert into testimonial set name='$this->name',add_date='$this->add_date',status='$this->status',designation='$this->designation',
			testimonial='$this->testimonial'";*/
			//echo $sql2;die;
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":name",$this->name);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->bindparam(":designation",$this->designation);
			$query->bindparam(":testimonial",$this->testimonial);
			$query->execute();
			return $this->myconn->lastInsertId();
		}
		
		public function Edit_testimonial(){
		
			$sql="update testimonial set name=:name,add_date=:add_date,status=:status,designation=:designation,testimonial=:testimonial where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->bindparam(":name",$this->name);
			$query->bindparam(":add_date",$this->add_date);
			$query->bindparam(":status",$this->status);
			$query->bindparam(":designation",$this->designation);
			$query->bindparam(":testimonial",$this->testimonial);
			$query->bindparam(":id",$this->id);
			$query->execute();
			return true;
		}
		
		public function Update_testimonial_image(){
		
			$sql ="update testimonial set big_image='$this->big_image',small_image='$this->small_image',thumb_image='$this->thumb_image' where id='$this->testimonial_id'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			return true;
		
		}
		
		public function delete_testimonial(){
		
			$sql="delete from testimonial where id='$this->id'";
			$query=$this->myconn->query($sql);
			$query->execute();
			return true;
		}
		
		
		
	}


?>