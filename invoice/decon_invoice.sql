-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2016 at 01:29 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `decon_invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_invoice`
--

CREATE TABLE `purchase_order_invoice` (
  `id` int(11) NOT NULL,
  `po_no` varchar(50) NOT NULL,
  `po_date` date NOT NULL,
  `site_id` varchar(50) NOT NULL,
  `amount` int(30) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `invoice_date` date NOT NULL,
  `type` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_invoice`
--

INSERT INTO `purchase_order_invoice` (`id`, `po_no`, `po_date`, `site_id`, `amount`, `invoice_no`, `invoice_date`, `type`, `created_at`, `updated_at`, `user_id`) VALUES
(1, '30320416', '2016-04-14', 'JHJAM-203', 4900, ' BRNBS15160342', '2016-04-15', 'SCFT', '2016-04-21 15:30:49', '0000-00-00 00:00:00', 0),
(2, '30320416', '2016-04-15', 'JHJAM-203', 4900, ' BRNBS15160342', '2016-04-16', 'SCFT', '2016-04-21 15:30:49', '0000-00-00 00:00:00', 0),
(3, '303203463', '2016-04-16', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-17', 'SCFT', '2016-04-21 15:30:49', '0000-00-00 00:00:00', 0),
(4, '303203463', '2016-04-17', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-18', 'SCFT', '2016-04-21 15:30:50', '0000-00-00 00:00:00', 0),
(5, '303203463', '2016-04-18', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-19', 'SCFT', '2016-04-21 15:30:50', '0000-00-00 00:00:00', 0),
(6, '30320416', '2016-04-14', 'JHJAM-203', 4900, ' BRNBS15160342', '2016-04-15', 'CLUSTER', '2016-04-21 16:42:46', '0000-00-00 00:00:00', 0),
(7, '30320416', '2016-04-15', 'JHJAM-203', 4900, ' BRNBS15160342', '2016-04-16', 'CLUSTER', '2016-04-21 16:42:46', '0000-00-00 00:00:00', 0),
(8, '303203463', '2016-04-16', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-17', 'CLUSTER', '2016-04-21 16:42:46', '0000-00-00 00:00:00', 0),
(9, '303203463', '2016-04-17', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-18', 'CLUSTER', '2016-04-21 16:42:46', '0000-00-00 00:00:00', 0),
(10, '303203463', '2016-04-18', 'JHJAM-207', 4900, 'BRNBS151604534', '2016-04-19', 'CLUSTER', '2016-04-21 16:42:46', '0000-00-00 00:00:00', 0);

--
-- Triggers `purchase_order_invoice`
--
DELIMITER $$
CREATE TRIGGER `invoice_trigger_created` BEFORE INSERT ON `purchase_order_invoice` FOR EACH ROW SET NEW.created_at = NOW()
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `invoice_trigger_updated` BEFORE UPDATE ON `purchase_order_invoice` FOR EACH ROW SET NEW.updated_at = NOW()
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchase_order_invoice`
--
ALTER TABLE `purchase_order_invoice`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_order_invoice`
--
ALTER TABLE `purchase_order_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
