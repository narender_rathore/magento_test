<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	$get_id=$_REQUEST['id'];
	$fetch=$endcustomer->endcustomerListingbyid($get_id);
	
	if(isset($_REQUEST['doaction']) && ($_REQUEST['doaction']=='Edit_Endcustomer')){ 
			
		$id=$_REQUEST['bid'];
		$endcustomer->id=$id;
		$endcustomer->endcustomer=strtoupper($_REQUEST['endcustomer']);
		$endcustomer->status =  $_POST['status'];	
						
								
			$insert_id=$endcustomer->Editendcustomer();
			if($insert_id==true){					
				echo "<script>document.location.href='endcustomer.php?id=".$get_id."&message-success=endcustomer Added Successfully'</script>";
			}
			else{
				echo "<script>document.location.href='Editendcustomer.php?message-error=Error! Something is not right here.'</script>";
			}
		
	}
	
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}
			
	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Edit endcustomer</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage endcustomer</a></li>
            <li class="active">Edit endcustomer</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Edit endcustomers</h3>-->
                  
                  <button type="submit" name="add_endcustomer" onclick="return edit_endcustomer_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  onclick=""><i class="fa fa-fw fa-check-square-o"></i> Save </button>
                  
                  <button onclick="document.location.href='Addendcustomer.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add endcustomer</button>  
                  				
                  <button onclick="document.location.href='endcustomer.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-arrows"></i> Manage endcustomer</button>    
  
<?php 
	if($message!=''){ 
	 	echo $message;
 	}
	
 ?>
                </div><!-- /.box-header -->
                <form name="endcustomer_form_edit" id="endcustomer_form_edit" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <input type="hidden" name="bid" value="<?=$_REQUEST['id']?>" />
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-6">
                                <div class="form-group">
                                  <label for="">endcustomer</label>
                                  <input type="text" class="form-control" id="endcustomer" value="<?=$fetch['endcustomer']?>" name="endcustomer" placeholder="Enter endcustomer Title">
                                </div>
                                
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" name="status" id="status" style="width: 100%;">
                                  <option  value="1" <?php if($fetch['status']==1){?> selected="selected"<?php } ?> >Active</option>
                                  <option value="0"  <?php if($fetch['status']==0){?> selected="selected"<?php } ?>>Deactive</option>
                                </select>
                                </div>
                            
                 		</div>
                 	</div>
                </div><!-- /.box-body -->
              <hr />
                 <button type="submit" name="add_endcustomer" onclick="return edit_endcustomer_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  ><i class="fa fa-fw fa-check-square-o"></i> Save</button> 
                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>

     <?php include_once('footer.php');?>