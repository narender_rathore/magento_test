<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	$get_id=$_REQUEST['id'];
	$fetch=$po->poListingid($get_id);
	//print_r($fetch);
	$circles=$circle->circleListing();
	$projects=$project->projectListing();
	$customers=$customer->customerListing();
	$endcustomers=$endcustomer->endcustomerListing();
	$projectcodes=$projectcode->projectcodeListing();
	$activitys=$activity->activityListing();


	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>View po</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage po</a></li>
            <li class="active">View po</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Edit pos</h3>-->
                  
                  <button onclick="document.location.href='Addpo.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add po</button>
                  <button onclick="document.location.href='Editpo.php?id=<?=$fetch['id']?>'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Edit po</button>    			
                  <button onclick="document.location.href='po.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-arrows"></i> Manage po</button>    
            </div><!-- /.box-header -->
                <form name="po_form_edit" id="po_form_edit" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <input type="hidden" name="bid" value="<?=$_REQUEST['id']?>" />
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Circle</label>
                                  <select class="form-control select2" name="circle" id="circle" style="width: 100%;" disabled="disabled">
                                  	  <option  value="">Select Circle</option>
                                  <?php 
								  foreach($circles as $circle):?>
                                      <option  <?php if($circle['id']==$fetch['circle']){?>  selected="selected" <?php }?> value="<?=$circle['id'];?>" >
									  <?=$circle['circle'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Project</label>
                                  <select class="form-control select2" name="project" id="project" style="width: 100%;" disabled="disabled" >
                                  	  <option  value="">Select Project</option>
                                  <?php 
								  
								  foreach($projects as $project):?>5
                                      <option <?php if($project['id']==$fetch['project']){?>  selected="selected" <?php }?>  value="<?=$project['id'];?>" >
									  <?=$project['project'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Customer</label>
                                  <select class="form-control select2" name="customer" id="customer" style="width: 100%;" disabled="disabled">
                                      <option  value="">Select Customer</option>
                                  <?php 
								 
								  foreach($customers as $customer):?>
                                      <option <?php if($customer['id']==$fetch['customer']){?>  selected="selected" <?php }?>  value="<?=$customer['id'];?>" >
									  <?=$customer['customer'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">End Customer</label>
                                  <select class="form-control select2" name="endcustomer" id="endcustomer" style="width: 100%;" disabled="disabled">
                                  	<option  value="">Select End Customer</option>
                                  <?php 
								  
								  foreach($endcustomers as $endcustomer):?>
                                      <option  <?php if($endcustomer['id']==$fetch['endcustomer']){?>  selected="selected" <?php }?>  value="<?=$endcustomer['id'];?>" >
									  <?=$endcustomer['endcustomer'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Project Code</label>
                                  <select class="form-control select2" name="projectcode" id="projectcode" style="width: 100%;" disabled="disabled">
                                      <option  value="">Select Project Code</option>
                                  <?php 
								  
								  foreach($projectcodes as $projectcode):?>
                                      <option  <?php if($projectcode['id']==$fetch['projectcode']){?>  selected="selected" <?php }?>  value="<?=$projectcode['id'];?>" >
									  <?=$projectcode['projectcode'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                 </div>
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Activity</label>
                                  <select class="form-control select2" name="activity" id="activity" style="width: 100%;" disabled="disabled">
                                  	  <option  value="">Select Activity</option>
                                  <?php 
								 
								  foreach($activitys as $activity):?>
                                      <option <?php if($activity['id']==$fetch['activity']){?>  selected="selected" <?php }?>  value="<?=$activity['id'];?>" >
									  <?=$activity['activity'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Invoice date</label>
                                  <input type="text" class="form-control" value="<?=$fetch['invoicedate']?>" id="invoicedate" name="invoicedate" disabled="disabled" placeholder="Enter Invoice Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Po No</label>
                                  <input type="text" class="form-control" value="<?=$fetch['pono']?>"  id="pono" name="pono" disabled="disabled" placeholder="Enter Purchase Order number">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Po Date</label>
                                  <input type="text" class="form-control" value="<?=$fetch['podate']?>" id="podate" name="podate" disabled="disabled" placeholder="Enter Purchase order Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Site Id</label>
                                  <input type="text" class="form-control" value="<?=$fetch['siteid']?>" id="siteid" name="siteid" disabled="disabled" placeholder="Enter Site id">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Work Package Id</label>
                                  <input type="text" class="form-control" value="<?=$fetch['wpid']?>" id="wpid" name="wpid" disabled="disabled" placeholder="Enter Invoice Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Internal Acceptance Date</label>
                                  <input type="text" class="form-control" value="<?=$fetch['iadate']?>"  id="iadate" name="iadate" disabled="disabled" placeholder="Enter Invoice Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Item Id</label>
                                  <input type="text" class="form-control" value="<?=$fetch['itemid']?>" id="itemid" disabled="disabled" name="itemid" placeholder="Enter Invoice Date">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Work Item</label>
                                  <input type="text" class="form-control" value="<?=$fetch['workitem']?>" id="workitem" name="workitem" disabled="disabled" placeholder="Enter Invoice Date">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Unit Price</label>
                                  <input type="text" class="form-control"  id="itemprice" value="<?=$fetch['itemprice']?>" name="itemprice" disabled="disabled" placeholder="Enter Unit Price Amount">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Complete Date</label>
                                  <input type="text" class="form-control"  id="completedate" value="<?=$fetch['completedate']?>" name="completedate" disabled="disabled" placeholder="Enter Complete Date">
                                </div>
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" name="status" id="status" style="width: 100%;" disabled="disabled">
                                  <option  <?php if($fetch['status']==1){?>  selected="selected" <?php }?> value="1" >Active</option>
                                  <option <?php if($fetch['status']==0){?>  selected="selected" <?php }?> value="0" >Deactive</option>
                                </select>
                                </div>
                            
                 		</div>
                 	</div>
                </div><!-- /.box-body -->
              <hr />

                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>

     <?php include_once('footer.php');?>