<?php
session_start();
$wholeid	= 8;
//$row		= $_SESSION['post_data'];
$row		= $_SESSION['invoice8'];
//echo "<pre>";
//print_r($row);die;
?>

<?php include_once("classes/settings.php"); ?>
	<?php
		$po = new Po();
		$invoicecluster	= new Invoicecluster();
		require("fpdf/fpdf.php");
	?>

<?php

function number_to_word( $num = '' )
{
	$num	= ( string ) ( ( int ) $num );
	
	if( ( int ) ( $num ) && ctype_digit( $num ) )
	{
		$words	= array( );
		
		$num	= str_replace( array( ',' , ' ' ) , '' , trim( $num ) );
		
		$list1	= array('','one','two','three','four','five','six','seven',
			'eight','nine','ten','eleven','twelve','thirteen','fourteen',
			'fifteen','sixteen','seventeen','eighteen','nineteen');
		
		$list2	= array('','ten','twenty','thirty','forty','fifty','sixty',
			'seventy','eighty','ninety','hundred');
		
		$list3	= array('','thousand','million','billion','trillion',
			'quadrillion','quintillion','sextillion','septillion',
			'octillion','nonillion','decillion','undecillion',
			'duodecillion','tredecillion','quattuordecillion',
			'quindecillion','sexdecillion','septendecillion',
			'octodecillion','novemdecillion','vigintillion');
		
		$num_length	= strlen( $num );
		$levels	= ( int ) ( ( $num_length + 2 ) / 3 );
		$max_length	= $levels * 3;
		$num	= substr( '00'.$num , -$max_length );
		$num_levels	= str_split( $num , 3 );
		
		foreach( $num_levels as $num_part )
		{
			$levels--;
			$hundreds	= ( int ) ( $num_part / 100 );
			$hundreds	= ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '' );
			$tens		= ( int ) ( $num_part % 100 );
			$singles	= '';
			
			if( $tens < 20 )
			{
				$tens	= ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
			}
			else
			{
				$tens	= ( int ) ( $tens / 10 );
				$tens	= ' ' . $list2[$tens] . ' ';
				$singles	= ( int ) ( $num_part % 10 );
				$singles	= ' ' . $list1[$singles] . ' ';
			}
			$words[]	= $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
		}
		
		$commas	= count( $words );
		
		if( $commas > 1 )
		{
			$commas	= $commas - 1;
		}
		
		$words	= implode( ', ' , $words );
		
		//Some Finishing Touch
		//Replacing multiples of spaces with one space
		$words	= trim( str_replace( ' ,' , ',' , trim( ucwords( $words ) ) ) , ', ' );
		if( $commas )
		{
			$words	= str_replace( ',' , ' and' , $words );
		}
		
		return $words;
	}
	else if( ! ( ( int ) $num ) )
	{
		return 'Zero';
	}
	return '';
}

?>

<?php
$pro=(int)$row[0]['pono'];
$invoicecluster->ponum=(int)$pro;
$projectcod=$invoicecluster->projectcode_d10();
//echo $projectcod['CustomerName'];die;
$po->fcustomer=trim(strtoupper($projectcod['CustomerName']));
$getcode=$po->findcurrentcode();
//echo $getcode['currentcode'];die;
$newcurrentcode=$getcode['currentcode']+1;
$updatenewcurrentcode=$newcurrentcode;
if($newcurrentcode<10){
$newcurrentcode='000'.$newcurrentcode;
}
if($newcurrentcode<100 and $newcurrentcode>=10){
$newcurrentcode='0'.$newcurrentcode;
}
if($newcurrentcode>100){
$newcurrentcode= $newcurrentcode;
}
//echo $newcurrentcode;
//echo (var_dump((int)$row[0]['pono']));die;

//echo "<pre>";
//print_r($projectcod);die;
//$po->update_id=$_GET['id'];
//$po->Update_purchase_order_status();

//$row=$po->invoice_list1(0,8);
$current_year= date('y');
$next_year=$current_year+1;
//echo "<pre>";
//print_r($rows);

/*for($i=0;$i<=7;$i++){
$invoicedate			=	$row[$i]['invoicedate'];
$pono					=	$row[$i]['pono'];
$podate					=	$row[$i]['podate'];
$siteid					=	$row[$i]['siteid'];
$wpid					=	$row[$i]['wpid'];
$iadate					=	$row[$i]['iadate'];
$itemid					=	$row[$i]['itemid'];
$workitem				=	$row[$i]['workitem'];
$project				=	$row[$i]['project'];
$circle					=	$row[$i]['circle'];
$customer				=	$row[$i]['customer'];
$endcustomer			=	$row[$i]['endcustomer'];
$projectcode			=	$row[$i]['projectcode'];
$activity				=	$row[$i]['activity'];
//echo $activity."<br>";
}*/

$pdf= new FPDF('p','mm','A4');

$pdf->SetTitle('Invoice'); 
$pdf->SetLeftMargin(20);
$pdf->AddPage();
//define('EURO',chr(128));
$pdf->SetFont("Arial","B",22);
$pdf->Setx(35);  
//$pdf->cell(40,10,"Invoice",0,0,"L",false);
$pdf->Ln();
$pdf->SetFont("Arial","B",12);

//$pdf->cell(0,10,"po_no ",0,0);


$pdf->Image("logo.jpg",80,10,-100);
 $pdf->SetDrawColor(0,80,180);
    //$pdf->SetFillColor(230,230,0);
    //$pdf->SetTextColor(220,50,50);
    //Thickness of frame (1 mm)
    $pdf->SetLineWidth();
$pdf->Line(180, 30, 40-10, 30);

$pdf->SetXY(60,30);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(102,102,0);
$pdf->cell(0,10,"DECON CONSTRUCTION COMPANY",0,1,"L");

$pdf->SetXY(75,38);
//$pdf->SetFont("Times","B",16);
$pdf->SetFont("Times","B",16);
$pdf->SetTextColor(255,153,51);
$pdf->cell(0,10,"Telecom Solution Provider",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
     $pdf->SetLineWidth();
$pdf->Line(180, 48, 40-10, 48);

$pdf->SetDrawColor(0,0,0);
//$pdf->SetTextColor(255,255,255);
$pdf->SetXY(55,87);
$pdf->SetFont("Arial","B",8);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,55,180,180);
//$pdf->Line(20, 20, 200-10, 20); // 20mm from each edge
//signature, name, terms, condition end

$pdf->SetXY(180,270);
//$pdf->Line(190, 267, 30-10, 267);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont("Times","B",8);
$pdf->SetXY(20,55);
$pdf->cell(0,10,"S.T.: AAEFD9836FST001",0,1,"L");
$pdf->SetXY(20,60);
$pdf->cell(0,10,"PAN: AAEFD9836F",0,1,"L");
$pdf->SetXY(20,65);
$pdf->cell(0,10,"Vendor Code: 477567",0,1,"L");
$pdf->SetXY(100,70);


$pdf->SetLineWidth(0.5);
$pdf->Rect(20,75,90,25);
$pdf->Rect(110,75,90,25);

$pdf->SetXY(110,75);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,10,"To",0,1,"L");
$pdf->SetXY(110,80);
$pdf->cell(0,10,"Nokia Solutions and Networks India Pvt. Ltd.",0,1,"L");
$pdf->SetXY(110,85);
$pdf->cell(0,10,"1507, Regus Business Centre, Eros Corporate",0,1,"L");
$pdf->SetXY(110,90);
$pdf->cell(0,10,"Towers, Level 15, Nehru Place, New Delhi-110019",0,1,"L");

$pdf->SetXY(20,75);
$pdf->cell(0,10,"Invoice No ",0,1,"L");
$pdf->SetXY(20,80);
$pdf->cell(0,10,"Invoice Date ",0,1,"L");
$pdf->SetXY(20,85);
$pdf->cell(0,10,"Project ",0,1,"L");
$pdf->SetXY(20,90);
$pdf->cell(0,10,"Circle ",0,1,"L");

$pdf->SetXY(60,75);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,80);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,85);
$pdf->cell(0,10,": ",0,1,"L");
$pdf->SetXY(60,90);
$pdf->cell(0,10,": ",0,1,"L");
$invoice_date=date('d.m.Y');
$pdf->SetXY(70,75);
$invoino=trim($projectcod['ProjectCode']).$current_year.$next_year.$newcurrentcode;
$proj=$row[0]['project'];
$cir=$row[0]['circle'];
$pdf->cell(0,10," $invoino",0,1,"L");
$pdf->SetXY(70,80);
$pdf->cell(0,10," $invoice_date",0,1,"L");
$pdf->SetXY(70,85);
$pdf->cell(0,10," $proj",0,1,"L");
$pdf->SetXY(70,90);
$pdf->cell(0,10," $cir",0,1,"L");

$pdf->SetXY(90,100);
$pdf->SetFont("Times","U","B",8);
$pdf->cell(0,05," Item Description",0,1,"L");

//$pdf->SetLineWidth();
//$pdf->Line(180, 48, 40-10, 48);
$pdf->Rect(20,105,180,5);
$pdf->Rect(20,110,180,5);



//if(trim($row[0]['activity'])=='SCFT'){
/*if(strstr("$row[0]['workitemsubtype']",'SCFT')==true){
$pdf->SetXY(20,105);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 10        Item Code :- 00077201          Description:- Radio Network Overlay Design",0,1,"L");

$pdf->SetXY(20,110);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 20        Item Code :- 00007740          Description:- Field Drive Test",0,1,"L");
}*/
//if(trim($row[0]['activity'])=='CLUSTER'){

$pdf->SetXY(20,105);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 10        Item Code :- 00007740          Description:- Field Drive Test",0,1,"L");

$pdf->SetXY(20,110);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 20        Item Code :- 00077201          Description:- Radio Network Overlay Design",0,1,"L");

$pdf->SetXY(20,115);
$pdf->SetFont("Times","B",9);
$pdf->cell(0,05,"Line No:- 30        Item Code :- 00007740          Description:- Field Drive Test",0,1,"L");





$pdf->Line(200, 123, 30-10, 123);

$pdf->Line(200, 130, 30-10, 130);

$pdf->SetFont("Times","B",9);
$pdf->SetLineWidth(0.5);
$pdf->Rect(20,123,10,50);
$pdf->SetXY(20,123);
$pdf->cell(0,06,"Sr. No.",0,1,"L");
$pdf->Rect(30,123,24,50);
$pdf->SetXY(38,123);
$pdf->cell(0,06,"Po No.",0,1,"L");
$pdf->Rect(54,123,24,50);
$pdf->SetXY(58,123);
$pdf->cell(0,06,"Po Date",0,1,"L");
$pdf->Rect(78,123,24,50);
$pdf->SetXY(82,123);
$pdf->cell(0,06,"Site ID",0,1,"L");
$pdf->Rect(102,123,24,50);
$pdf->SetXY(106,123);
$pdf->cell(0,06,"WP ID",0,1,"L");
$pdf->Rect(126,123,24,50);
$pdf->SetXY(126,123);
$pdf->cell(0,06,"IA Done Date",0,1,"L");
$pdf->Rect(150,123,10,50);
$pdf->SetXY(151,123);
$pdf->cell(0,06,"Qty.",0,1,"L");
$pdf->Rect(160,123,20,50);
$pdf->SetXY(161,123);
$pdf->cell(0,06,"Unit Price",0,1,"L");
$pdf->Rect(180,123,20,69);
$pdf->SetXY(181,123);
$pdf->cell(0,06,"Amount (Rs.)",0,1,"L");
$pdf->Line(200, 173, 30-10, 173);

$pdf->Line(200, 135, 30-10, 135);
$pdf->Line(200, 140, 30-10, 140);
$pdf->Line(200, 145, 30-10, 145);
$pdf->Line(200, 150, 30-10, 150);
$pdf->Line(200, 155, 30-10, 155);
$pdf->Line(200, 160, 30-10, 160);
$pdf->Line(200, 165, 30-10, 165);
$pdf->Line(200, 170, 30-10, 170);
$srno=130;
$pdf->SetFont("Times","",9);

for($i=0;$i<=7;$i++){
$invoicedate			=	$row[$i]['invoicedate'];
$itemid					=	$row[$i]['itemid'];
$workitem				=	$row[$i]['workitem'];
$project				=	$row[$i]['project'];
$circle					=	$row[$i]['circle'];
$customer				=	$row[$i]['customer'];
$endcustomer			=	$row[$i]['endcustomer'];
$projectcode			=	$row[$i]['projectcode'];
$activity				=	$row[$i]['activity'];


for($i=1;$i<=8;$i++){													$pdf->SetXY(20,$srno);$pdf->cell(0,05,"$i",0,1,"L");$srno+=5;			}$srno=130;
for($i=0;$i<=7;$i++){		$pono	=	$row[$i]['pono'];				$pdf->SetXY(30,$srno);$pdf->cell(0,05,"$pono",0,1,"L");$srno+=5;		}$srno=130;
for($i=0;$i<=7;$i++){		$podate	=	$row[$i]['podate'];				$pdf->SetXY(54,$srno);$pdf->cell(0,05,"$podate",0,1,"L");$srno+=5;		}$srno=130;
for($i=0;$i<=7;$i++){		$siteid =	$row[$i]['site_customer'];				$pdf->SetXY(78,$srno);$pdf->cell(0,05,"$siteid",0,1,"L");$srno+=5;		}$srno=130;
for($i=0;$i<=7;$i++){		$wpid	=	$row[$i]['wpid'];				$pdf->SetXY(102,$srno);$pdf->cell(0,05,"$wpid",0,1,"L");$srno+=5;		}$srno=130;
for($i=0;$i<=7;$i++){		$iadate	=	$row[$i]['iadate'];				$pdf->SetXY(126,$srno);$pdf->cell(0,05,"$iadate",0,1,"L");$srno+=5;		}$srno=130;
for($i=0;$i<=7;$i++){													$pdf->SetXY(150,$srno);$pdf->cell(0,05,"1",0,1,"L");$srno+=5;			}$srno=130;
for($i=0;$i<=7;$i++){		$itemprice	=	$row[$i]['amount'];		$pdf->SetXY(160,$srno);$pdf->cell(0,05,"$itemprice",0,1,"L");$srno+=5;	}$srno=130;
for($i=0;$i<=7;$i++)		{$itemprice	=	$row[$i]['amount'];		$pdf->SetXY(180,$srno);$pdf->cell(0,05,"$itemprice",0,1,"L");$srno+=5;	}
}
$i=str_replace(',','',$itemprice);

$totalprice=(int)$i*8;
$tax=($totalprice/100)*14;
$swachhtax=($totalprice/100)*0.5;

$taxtotal=$totalprice+$tax+$swachhtax;

$pdf->Line(200, 177, 30-10, 177);
$pdf->SetFont("Times","",9);
$pdf->SetXY(20,172);
$pdf->cell(0,06,"Total",0,1,"L");
$pdf->Line(200, 182, 30-10, 182);
$pdf->SetXY(180,172);
$pdf->cell(0,06,"$totalprice",0,1,"L");

$pdf->SetXY(20,177);
$pdf->cell(0,06,"Add: Service Tax : @14%",0,1,"L");
$pdf->SetXY(180,177);
$pdf->cell(0,06,"$tax",0,1,"L");
$pdf->Line(200, 187, 30-10, 187);
$pdf->SetXY(20,182);
$pdf->cell(0,06,"Swacch Bharat cess : @0.5%",0,1,"L");
$pdf->SetXY(180,182);
$pdf->cell(0,06,"$swachhtax",0,1,"L");
$pdf->SetFont("Times","B",9);
$pdf->Line(200, 192, 30-10, 192);
$pdf->SetXY(20,187);
$pdf->cell(0,06,"Total Amount",0,1,"L");
$pdf->SetXY(180,187);
$pdf->cell(0,06,"$taxtotal",0,1,"L");
$pdf->Line(200, 197, 30-10, 197);
$pdf->SetXY(20,192);

$amountword=number_to_word($taxtotal);
$pdf->cell(0,06,"$amountword" ."  only",0,1,"L");

$pdf->SetFont("Times","B",10);
$pdf->SetXY(20,195);
$pdf->cell(0,10,"For Decon Construction Company",0,1,"L");
$pdf->Line(200, 202, 30-10, 202);

$pdf->SetFont("Times","B",12);
$pdf->SetXY(157,227);
$pdf->cell(0,10,"Authorized Signatory",0,1,"L");

 $pdf->SetDrawColor(0,80,180);
$pdf->SetLineWidth();
$pdf->Line(180, 245, 40-10, 245);

$pdf->SetTextColor(102,102,0);

$pdf->SetFont("Times","B",12);
$pdf->SetXY(60,250);
$pdf->cell(0,10,"R-12/28, Raj Nagar, Ghaziabad (U.P.) Pin- 201002",0,1,"L");

$pdf->SetXY(57,256);
$pdf->cell(0,10,"Ph.: 0120-4901639 Fax: 0120-4901639 Mobile : 09310061400",0,1,"L");

$pdf->SetXY(54,262);
$pdf->cell(0,10,"E-mail : info@deconindia.com Website : www.deconindia.com",0,1,"L");
//$pdf->Rect(110,80,90,95);
/*
$pdf->output("invoice.pdf","D"); //Prompting user to choose where to save the PDF file

$pdf->Output('directory/yourfilename.pdf','F'); //Saving the PDF file to server (make sure you have 777 writing permissions for that folder!): 

$pdf->Output('', 'S'); //Method 4: Returning the PDF file content as a string

$pdf->output("invoice.pdf","I");// Automatically open PDF in your browser after being generated:

*/
$pdf->AddPage();
 $pdf->SetDrawColor();
$pdf->SetLineWidth(0.4);

$pdf->SetTextColor(255,255,255);
$pdf->Rect(5,20,200,50);
$pdf->SetFont("Times","B",10);

//$pdf->Line(200, 202, 30-10, 202);
$pdf->SetFont("Times","B",7);
 $pdf->SetDrawColor();
$pdf->SetLineWidth(0.3);
$pdf->SetFillColor(153,0,153);
$pdf->Rect(5,20,200,10,'F');
 $pdf->SetDrawColor();
 
$pdf->Line(205, 30,15-10, 30);

$pdf->Line(10, 20,60-50, 70);
$pdf->Line(35, 20,85-50, 70);
$pdf->Line(50, 20,100-50, 70);
$pdf->Line(70, 20,120-50, 70);
$pdf->Line(85, 20,135-50, 70);
$pdf->Line(95, 20,145-50, 70);
$pdf->Line(115, 20,165-50, 70);
$pdf->Line(130, 20,180-50, 70);
$pdf->Line(145, 20,195-50, 70);
//$pdf->Line(135, 20,185-50, 70);
$pdf->Line(160, 20,210-50, 70);
$pdf->Line(175, 20,225-50, 70);
$pdf->Line(190, 20,240-50, 70);
$pdf->Line(175, 20,225-50, 70);



$pdf->SetXY(5,20);
$pdf->cell(0,06,"Sr.",0,1,"L");
$pdf->SetXY(10,20);
$pdf->cell(0,06,"Project",0,1,"L");
$pdf->SetXY(35,20);
$pdf->cell(0,06,"Circle",0,1,"L");
$pdf->SetXY(50,20);
$pdf->cell(0,06,"Invoice No",0,1,"L");

$pdf->SetXY(70,20);
$pdf->cell(0,06,"Invoice Date",0,1,"L");
$pdf->SetXY(85,20);
$pdf->cell(0,06,"Activity",0,1,"L");
$pdf->SetXY(95,20);
$pdf->cell(0,06,"PO No",0,1,"L");
$pdf->SetXY(115,20);
$pdf->cell(0,06,"PO Date",0,1,"L");
$pdf->SetXY(130,20);
$pdf->cell(0,06,"SiteCustomer",0,1,"L");
$pdf->SetXY(130,25);
$pdf->cell(0,06,"Site Code",0,1,"L");

$pdf->SetXY(145,20);
$pdf->cell(0,06,"WP Work",0,1,"L");
$pdf->SetXY(145,25);
$pdf->cell(0,06,"Package ID",0,1,"L");


$pdf->SetXY(160,20); 
$pdf->cell(0,06,"Complete",0,1,"L");
$pdf->SetXY(162,25);
$pdf->cell(0,02,"Date",0,1,"L");
$pdf->SetXY(161,25);
$pdf->cell(0,07,"Actual",0,1,"L");

$pdf->SetXY(175,20);
$pdf->cell(0,06,"Ia Internal",0,1,"L");
$pdf->SetXY(175,25);
$pdf->cell(0,06,"Acceptance",0,1,"L");

$pdf->SetXY(190,20);
$pdf->cell(0,06,"Goods Recipt",0,1,"L");

$pdf->Line(205, 35,15-10, 35);
$pdf->Line(205, 40,15-10, 40);
$pdf->Line(205, 45,15-10, 45);
$pdf->Line(205, 50,15-10, 50);
$pdf->Line(205, 55,15-10, 55);
$pdf->Line(205, 60,15-10, 60);
$pdf->Line(205, 65,15-10, 65);
$pdf->SetTextColor();
$y=30;
/*for($i=1;$i<=8;$i++){

$pdf->SetXY(5,$y);
$pdf->cell(0,05,"$i",0,1,"L");
$y+=5;
}*/
$srno=30;
$pdf->SetFont("Times","",7);
//echo $row[0]['customer'];die;


//print_r($getcode);die;
for($i=0;$i<=7;$i++){

$itemid					=	$row[$i]['itemid'];
$workitem				=	$row[$i]['workitem'];
$customer				=	$row[$i]['customer'];
$endcustomer			=	$row[$i]['endcustomer'];
$projectcode			=	$row[$i]['ProjectCode'];
$itemprice				=	$row[$i]['itemprice'];
$completedate			=	$row[$i]['completedate'];
$act=$row[0]['workitemsubtype'];
$invoice_no= trim($projectcod['ProjectCode']).$current_year.$next_year.$newcurrentcode;
$workitem=$activity.'/'.$row[$i]['workitem'];

for($i=1;$i<=8;$i++){														$pdf->SetXY(5,$srno);$pdf->cell(0,05,"$i",0,1,"L");				$srno+=5;}$srno=30;
$pdf->SetFont("Times","",6);
for($i=0;$i<=7;$i++){$project		=	$row[$i]['project'];				$pdf->SetXY(10,$srno);$pdf->cell(0,05,"$project",0,1,"L");		$srno+=5;}$srno=30;
$pdf->SetFont("Times","",6);
for($i=0;$i<=7;$i++){$circle		=	$row[$i]['circle'];					$pdf->SetXY(35,$srno);$pdf->cell(0,05,"$circle",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){														$pdf->SetXY(50,$srno);$pdf->cell(0,05,"$invoice_no",0,1,"L");	$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$invoicedate	=	date('d.m.Y')	;					$pdf->SetXY(70,$srno);$pdf->cell(0,05,"$invoicedate",0,1,"L");	$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$activity		=	substr($act,0,7);	$pdf->SetXY(85,$srno);$pdf->cell(0,05,"$activity",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$pono			=	$row[$i]['pono'];$pdf->SetXY(95,$srno);$pdf->cell(0,05,"$pono",0,1,"L");$srno+=5;
$invoicecluster->pono=$row[$i]['pono'];$invoicecluster->invoiceno=$invoice_no;
$invoicecluster->invoice_printed();
}$srno=30;
for($i=0;$i<=7;$i++){$podate		=	$row[$i]['podate'];					$pdf->SetXY(115,$srno);$pdf->cell(0,05,"$podate",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$siteid		=	$row[$i]['site_customer'];					$pdf->SetXY(130,$srno);$pdf->cell(0,05,"$siteid",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$wpid			=	$row[$i]['wpid'];					$pdf->SetXY(145,$srno);$pdf->cell(0,05,"$wpid",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){														$pdf->SetXY(160,$srno);$pdf->cell(0,05,"$completedate",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$iadate		=	$row[$i]['iadate'];					$pdf->SetXY(175,$srno);$pdf->cell(0,05,"$iadate",0,1,"L");		$srno+=5;}$srno=30;
for($i=0;$i<=7;$i++){$gdsdate		=	$row[$i]['gdsdate'];					$pdf->SetXY(190,$srno);$pdf->cell(0,05,"$gdsdate",0,1,"L");	$srno+=5;}
}
$po->iinvoiceno=$invoice_no;
$getinvid=$po->Addinvoice();
/*foreach($wholeid as $whole){
	$po->uid			=	$whole;
	$po->uinvoiceno		=	$invoice_no;
	$po->updateinvoiceno();
	$fetch=$po->poListingid($po->uid);
		$po->poid			=	$fetch['id'];
		$po->invoiceid		=	$getinvid;
		$po->invoiceno		=	$fetch['invoiceno'];
		$po->circle			=	$fetch['circle'];
		$po->project		=	$fetch['project'];
		$po->customer		=	$fetch['customer'];
		$po->endcustomer	=	$fetch['endcustomer'];
		$po->projectcode	=	$fetch['projectcode'];
		$po->activity		=	$fetch['activity'];
		$po->invoicedate	=	$fetch['invoicedate'];
		$po->pono			=	$fetch['pono'];
		$po->podate			=	$fetch['podate'];
		$po->siteid			=	$fetch['siteid'];
		$po->wpid			=	$fetch['wpid'];
		$po->iadate			=	$fetch['iadate'];
		$po->itemid			=	$fetch['itemid'];
		$po->workitem		=	$fetch['workitem'];
		$po->itemprice		=	$fetch['itemprice'];
		$po->completedate	=	$fetch['completedate'];
		$po->status 		=  	$fetch['status'];
	$po->Addinvoicedata();
}*/

$po->ucustomer		=trim(strtoupper($projectcod['CustomerName']));
$po->ucurrentcode	=$updatenewcurrentcode;
$po->updatecurrentcode();

$pdf->output("$invoice_no.pdf","D");
//$pdf->output("invoice.pdf","I");
/*foreach($wholeid as $whole2){
	$po->id	=	$whole2;
	$po->delete_po($po->id);

}*/
for($i=0;$i<=7;$i++){
	$po->id	=	$wholeid[$i];
	//$po->delete_po($po->id);

}

unset($_SESSION['wholeid']);
unset($_SESSION['post_data']);
session_destroy();


?>