<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 

	include_once('top_menu.php');
	include_once('sidebar.php');
	
	if(isset($_REQUEST['doaction']) && ($_POST['doaction']=='Add_Po')){  
		
		$po->circle			=	$_POST['circle'];
		$po->project		=	$_POST['project'];
		$po->customer		=	$_POST['customer'];
		$po->endcustomer	=	$_POST['endcustomer'];
		$po->projectcode	=	$_POST['projectcode'];
		$po->activity		=	$_POST['activity'];
		$po->invoicedate	=	$_POST['invoicedate'];
		$po->pono			=	$_POST['pono'];
		$po->podate			=	$_POST['podate'];
		$po->siteid			=	$_POST['siteid'];
		$po->wpid			=	$_POST['wpid'];
		$po->iadate			=	$_POST['iadate'];
		$po->itemid			=	$_POST['itemid'];
		$po->workitem		=	$_POST['workitem'];
		$po->itemprice		=	$_POST['itemprice'];
		$po->completedate	=	$_POST['completedate'];
		$po->status 		=  	$_POST['status'];
		
		/*$current_year= date('y');
		$next_year=$current_year+1;
		$code=$projectcode->projectcodeListingbyid($_POST['projectcode']);
		$invoice_no= trim($code['projectcode']).$current_year.$next_year.'001';die;*/
		
		$insert_id=$po->Addpo();
			if($insert_id!=""){	
				echo "<script>document.location.href='Editpo.php?id=".$insert_id."&message-success=po Added Successfully'</script>";
			}
			else{
				echo "<script>document.location.href='Addpo.php?message-error=Error! Something is not right here.'</script>";
			}
		
	}
	
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}	
			
	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Add Purchase Order</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage Purchase Order</a></li>
            <li class="active">Add Purchase Order</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add pos</h3>-->
                  
                  	  <button type="submit" name="add_po" onclick="return add_po_validate()" style=" margin-left:5px;background-color: #ffffff;" 
                      class="btn btn-default pull-right"  onclick=""><i class="fa fa-fw fa-check-square-o"></i> Save </button> 
                      <button onclick="document.location.href='po.php'" style="background-color: #ffffff;" class="btn btn-default pull-right">
                      <i class="fa fa-fw fa-arrows"></i> Manage po</button>    
  
<?php 

	if($message!=''){ 
	 	echo $message;
 	}
 ?>
                </div><!-- /.box-header -->
                <form name="po_form_add" id="po_form_add" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-6">
                               
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Circle</label>
                                  <select class="form-control select2" name="circle" id="circle" style="width: 100%;">
                                  	  <option  value="">Select Circle</option>
                                  <?php 
								  $circles=$circle->circleListing();foreach($circles as $circle):?>
                                      <option  value="<?=$circle['id'];?>" >
									  <?=$circle['circle'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Project</label>
                                  <select class="form-control select2" name="project" id="project" style="width: 100%;">
                                  	  <option  value="">Select Project</option>
                                  <?php 
								  $projects=$project->projectListing();
								  foreach($projects as $project):?>
                                      <option  value="<?=$project['id'];?>" >
									  <?=$project['project'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Customer</label>
                                  <select class="form-control select2" name="customer" id="customer" style="width: 100%;">
                                      <option  value="">Select Customer</option>
                                  <?php 
								  $customers=$customer->customerListing();
								  foreach($customers as $customer):?>
                                      <option  value="<?=$customer['id'];?>" >
									  <?=$customer['customer'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">End Customer</label>
                                  <select class="form-control select2" name="endcustomer" id="endcustomer" style="width: 100%;">
                                  	<option  value="">Select End Customer</option>
                                  <?php 
								  $endcustomers=$endcustomer->endcustomerListing();
								  foreach($endcustomers as $endcustomer):?>
                                      <option   value="<?=$endcustomer['id'];?>" >
									  <?=$endcustomer['endcustomer'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Project Code</label>
                                  <select class="form-control select2" name="projectcode" id="projectcode" style="width: 100%;">
                                      <option  value="">Select Project Code</option>
                                  <?php 
								  $projectcodes=$projectcode->projectcodeListing();
								  foreach($projectcodes as $projectcode):?>
                                      <option    value="<?=$projectcode['id'];?>" >
									  <?=$projectcode['projectcode'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                 </div>
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Activity</label>
                                  <select class="form-control select2" name="activity" id="activity" style="width: 100%;">
                                  	  <option  value="">Select Activity</option>
                                  <?php 
								  $activitys=$activity->activityListing();
								  foreach($activitys as $activity):?>
                                      <option  value="<?=$activity['id'];?>" >
									  <?=$activity['activity'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Invoice date</label>
                                  <input type="text" class="form-control" id="invoicedate" name="invoicedate" placeholder="Enter Invoice Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Po No</label>
                                  <input type="text" class="form-control"   id="pono" name="pono" placeholder="Enter Purchase Order number">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Po Date</label>
                                  <input type="text" class="form-control"  id="podate" name="podate" placeholder="Enter Purchase order Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Site Id</label>
                                  <input type="text" class="form-control"  id="siteid" name="siteid" placeholder="Enter Site id">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Work Package Id</label>
                                  <input type="text" class="form-control" id="wpid" name="wpid" placeholder="Enter Invoice Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Internal Acceptance Date</label>
                                  <input type="text" class="form-control"   id="iadate" name="iadate" placeholder="Enter Internal Acceptance Date">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Item Id</label>
                                  <input type="text" class="form-control"  id="itemid" name="itemid" placeholder="Enter Item Id">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Work Item</label>
                                  <input type="text" class="form-control"  id="workitem" name="workitem" placeholder="Enter Work Item ">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Unit Price</label>
                                  <input type="text" class="form-control"  id="itemprice" name="itemprice" placeholder="Enter Unit Price Amount">
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Complete Date</label>
                                  <input type="text" class="form-control"  id="completedate" name="completedate" placeholder="Enter Complete Date">
                                </div>
                                
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" name="status" id="status" style="width: 100%;">
                                  <option value="1" >Active</option>
                                  <option  value="0" >Deactive</option>
                                </select>
                                </div>
                            
                 		</div>
                 	</div>
                </div><!-- /.box-body -->
              <hr />
                 <button type="submit" name="add_po" onclick="return add_po_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  ><i class="fa fa-fw fa-check-square-o"></i> Save</button> 
                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>

     <?php include_once('footer.php');?>