<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	$get_id=$_REQUEST['id'];
	$fetch=$projectcode->projectcodeListingbyid($get_id);
	
	if(isset($_REQUEST['doaction']) && ($_REQUEST['doaction']=='Edit_Projectcode')){ 
			
		$id=$_REQUEST['bid'];
		$projectcode->id=$id;
		$projectcode->projectcode=strtoupper($_REQUEST['projectcode']);
		$projectcode->status =  $_POST['status'];	
						
								
			$insert_id=$projectcode->Editprojectcode();
			if($insert_id==true){					
				echo "<script>document.location.href='projectcode.php?id=".$get_id."&message-success=projectcode Added Successfully'</script>";
			}
			else{
				echo "<script>document.location.href='Editprojectcode.php?message-error=Error! Something is not right here.'</script>";
			}
		
	}
	
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}
			
	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Edit projectcode</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage projectcode</a></li>
            <li class="active">Edit projectcode</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Edit projectcodes</h3>-->
                  
                  <button type="submit" name="add_projectcode" onclick="return edit_projectcode_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  onclick=""><i class="fa fa-fw fa-check-square-o"></i> Save </button>
                  
                  <button onclick="document.location.href='Addprojectcode.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add projectcode</button>  
                  				
                  <button onclick="document.location.href='projectcode.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-arrows"></i> Manage projectcode</button>    
  
<?php 
	if($message!=''){ 
	 	echo $message;
 	}
	
 ?>
                </div><!-- /.box-header -->
                <form name="projectcode_form_edit" id="projectcode_form_edit" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <input type="hidden" name="bid" value="<?=$_REQUEST['id']?>" />
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-6">
                                <div class="form-group">
                                  <label for="">projectcode</label>
                                  <input type="text" class="form-control" id="projectcode" value="<?=$fetch['projectcode']?>" name="projectcode" placeholder="Enter projectcode Title">
                                </div>
                                
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" name="status" id="status" style="width: 100%;">
                                  <option  value="1" <?php if($fetch['status']==1){?> selected="selected"<?php } ?> >Active</option>
                                  <option value="0"  <?php if($fetch['status']==0){?> selected="selected"<?php } ?>>Deactive</option>
                                </select>
                                </div>
                            
                 		</div>
                 	</div>
                </div><!-- /.box-body -->
              <hr />
                 <button type="submit" name="add_projectcode" onclick="return edit_projectcode_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  ><i class="fa fa-fw fa-check-square-o"></i> Save</button> 
                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>

     <?php include_once('footer.php');?>