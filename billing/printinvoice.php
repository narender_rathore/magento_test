<?php /*?><?php
	session_start();
	//print_r($_SESSION);die;
	//error_reporting(-1); 

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
<?php 

	include_once('top_menu.php');
	include_once('sidebar.php');
	
		//$query=$po->invoice_list(0,8);
		$query=$po->invoicedoneinfo();
		$query->execute();
		$num_rows=$query->rowcount();
		
		
	?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Manage Purchase Order</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Purchase Order</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage Brands</h3>-->
        <?php 
  
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}
	if($message!=''){ 
	 	echo $message;
 	}
 ?>
        
         <!-- <button type="submit" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right" value="Delete_brand" onclick="delete_Po();"><i class="fa fa-fw fa-close"></i> Delete</button>
          <button onclick="document.location.href='Addpo.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add po</button>-->
          </div>
          <!-- /.box-header -->
          <form action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" name="po_form" id="po_form" method="post" >
          <input type="hidden"  name="doaction" id="doaction" />
          <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
          
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>Invoice No</th>
              <th>Po1</th>
              <th>Po2</th>
              <th>Po3</th>
              <th>Po4</th>
              <th>Po5</th>
              <th>Po6</th>
              <th>Po7</th>
              <th>Po8</th>
              
<!--              <th>Action</th>
              <th><input name="selectall" id="selectall" type="checkbox"  /></th>-->
            </tr>
          </thead>
          <tbody>
            <?php 
				if($num_rows>0){$sn=1;
					while($row=$query->fetch()){ 
					$ponos=explode(',', $row['pos']);	

			?>
            <tr>
              <td><?php echo $sn; ?></td>
              <td><?php echo $row['invoiceno'];?></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[0]; ?>');"class="btn"><?php echo $ponos[0];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[1]; ?>');"class="btn"><?php echo $ponos[1];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[2]; ?>');"class="btn"><?php echo $ponos[2];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[3]; ?>');"class="btn"><?php echo $ponos[3];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[4]; ?>');"class="btn"><?php echo $ponos[4];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[5]; ?>');"class="btn"><?php echo $ponos[5];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[6]; ?>');"class="btn"><?php echo $ponos[6];?></a></td>
              <td><a style="cursor:pointer;" onclick="categorycall('poinfo.php?po=<?=$ponos[7]; ?>');"class="btn"><?php echo $ponos[7];?></a></td>

           <?php /*?>   <td> 
              	<a href="Editpo.php?id=<?=$row['id']?>" title="Edit Purchase Order"> <i class="fa fa-edit"></i> </a>
                <a href="Viewpo.php?id=<?=$row['id']?>" title="Edit Purchase Order"> <i class="fa fa-eye"></i> </a>
                <a href="invoice8.php?id=<?=$row['id']?>" title="Invoice Purchase Order"> <i class="fa-file-pdf-o"></i> </a>
              </td>
              <td><input name="po_to_delete[]"  type="checkbox" value="<?=$row['id']?>"  class="checkboxes"/></td><?php */?>
            </tr>
            <?php 
				$sn++;
				} 
				} 
				else{ ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php
				}  
			 ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Invoice No</th>
              <th>Po1</th>
              <th>Po2</th>
              <th>Po3</th>
              <th>Po4</th>
              <th>Po5</th>
              <th>Po6</th>
              <th>Po7</th>
              <th>Po8</th>
             <!-- <th>Action</th>
              <th><input name="selectall" id="selectall" type="checkbox" disabled="disabled"/></th>-->
            </tr>
          </tfoot>
        </form>
        </table>
        
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

</section>


<!-- /.content -->
</div>


<?php include_once('footer.php');?>
<!-- jQuery 2.1.4 -->

<!-- DataTables -->
<script src="<?=DOCUMENT_ROOT?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/fastclick/fastclick.min.js"></script>
    <script src="<?=DOCUMENT_ROOT?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=DOCUMENT_ROOT?>bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=DOCUMENT_ROOT?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=DOCUMENT_ROOT?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?=DOCUMENT_ROOT?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=DOCUMENT_ROOT?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=DOCUMENT_ROOT?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=DOCUMENT_ROOT?>dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<!---my script----->
<script language="javascript">
	$(function(){
		$("#selectall").click(function () {
			  $('.checkboxes').attr('checked', this.checked);
		});
	 
		$(".checkboxes").click(function(){
	 
			if($(".checkboxes").length == $(".checkboxes:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}
	 
		});
	});
	</script>
    		<script type="text/javascript">
    function categorycall(url)
	{
	
	document.location.href=url;
	}
    </script>
<!---my script---->
