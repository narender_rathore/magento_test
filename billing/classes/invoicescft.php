<?php

	class Invoicescft extends Connection{
		
		public function scft_invoice_listing(){
		
			$sql="select ia.project,ia.circle,ia.site_customer,ia.site_id,ia.wpid,ia.packagename,ia.itemid,ia.workitemsubtype,ia.vendor,ia.ponumber,
			ia.company as comp,ia.completedate,ia.internal_acceptance as iadate,ia.goods_recipt as gdsdate,pds.pono,pds.podate,pds.siteid,pds.company,pds.lineitem10,
			pds.itemid10,pds.unitprice10,pds.radionetwork10,pds.lineitem20,pds.itemid20,pds.unitprice20,pds.fielddrive20,pds.deliverydate,pds.amount from po_database_scft 
			as pds join iadone as ia on pds.pono=ia.ponumber  where ia.invoice_status=0 and pds.invoice_status=0 order by pds.id asc";
			$query=$this->myconn->prepare($sql);
			//$query->execute();
			//$fetch=$query->fetch_all();
			return $query;
		}
		
		public function print_scft(){
		
			$sql="select ia.project,ia.circle,ia.site_customer,ia.site_id,ia.wpid,ia.packagename,ia.itemid,ia.workitemsubtype,ia.vendor,ia.ponumber,
			ia.company as comp,ia.completedate,ia.internal_acceptance as iadate,ia.goods_recipt as gdsdate,pds.pono,pds.podate,pds.siteid,pds.company,pds.lineitem10,
			pds.itemid10,pds.unitprice10,pds.radionetwork10,pds.lineitem20,pds.itemid20,pds.unitprice20,pds.fielddrive20,pds.deliverydate,pds.amount from po_database_scft 
			as pds join iadone as ia on pds.pono=ia.ponumber  where ia.invoice_status=0 and pds.invoice_status=0 and ia.ponumber='$this->po' and pds.pono='$this->po'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
		}

		
		public function scft_listing_by_id($id){
		
			$sql="select * from po_database_scft join iadone on po_database_scft.pono=iadone.ponumber where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
		
		public function invoice_printed(){
		
			$sql="update po_database_scft set invoice_status=1,invoiceno='$this->invoiceno' where pono='$this->pono'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			return true;
		}
		public function projectcode_d10(){
			$sql="select ProjectCode,CustomerName from d10_api where CustomerPONumber='$this->ponum'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
		}
		public function updatecurrentcode(){
		$sql="update customer set currentcode='$this->ucurrentcode' where customer='$this->ucustomer'";
		//echo $sql;die;
		$query=$this->myconn->prepare($sql);
		$query->execute();
		return true;
		}
		
		

	
	}
	





?>