<?php
//require('connection.php');
class Project extends Connection{
	
	public function project_listing(){
		$sql="select * from project";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
	public function projectListing(){
		$sql="select * from project";
		$query=$this->myconn->prepare($sql);
		$query->execute();
		$fetch=$query->fetchALL();
		return $fetch;
	}
	
	public function projectListingbyid($id){
		$sql="select * from project where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$id);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	public function Addproject(){
		$sql="insert into project set project=:project,status=:status";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":project",$this->project);
		$query->bindparam(":status",$this->status);
		$query->execute();
		return $this->myconn->lastInsertId();
	}
	public function Editproject(){
		$sql="update project set project=:project,status=:status where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":project",$this->project);
		$query->bindparam(":status",$this->status);
		$query->bindparam(":id",$this->id);
		$query->execute();
		return true;
	}
	public function Viewproject($id){
		$sql="select * from project where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$id);
		$query->execute();
	}
	
	public function delete_project(){
		
			$sql="delete from project where id='$this->id'";
			$query=$this->myconn->query($sql);
			$query->execute();
			return true;
	}
}



/*$project= new  project();
$z=$project->projectListing();
echo "<pre>";
//print_r($z);
foreach($z as $x){
echo $x['id'];
echo $x['project'];
echo $x['create_at'];
}*/
?>