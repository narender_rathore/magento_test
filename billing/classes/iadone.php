<?php
//require('connection.php');
class Iadone extends Connection{
	
	public function import_iadone(){
		
		$sql="insert into iadone set project=:project,circle=:circle,site_customer=:site_customer,site_id=:site_id,wpid=:wpid,packagename=:packagename,itemid=:itemid,workitemsubtype=:workitemsubtype,vendor=:vendor,
		ponumber=:ponumber,company=:company,completedate=:completedate,internal_acceptance=:internal_acceptance,goods_recipt=:goods_recipt";
		
		/*$sql="insert into iadone set project='$this->project',circle='$this->circle',site_customer='$this->site_customer',site_id='$this->site_id',wpid='$this->wpid',packagename='$this->packagename',workitemsubtype='$this->workitemsubtype',vendor='$this->vendor',
		ponumber='$this->ponumber',company='$this->company',completedate='$this->completedate',internal_acceptance='$this->internal_acceptance',goods_recipt='$this->goods_recipt'";
		echo $sql;die;*/
		$query=$this->myconn->prepare($sql);
		$query->bindparam(':project',		$this->project);
		$query->bindparam(':circle',		$this->circle);
		$query->bindparam(':site_customer',	$this->site_customer);
		$query->bindparam(':site_id',		$this->site_id);
		$query->bindparam(':wpid',			$this->wpid);
		$query->bindparam(':packagename',	$this->packagename);
		$query->bindparam(':itemid',		$this->itemid);
		$query->bindparam(':workitemsubtype',$this->workitemsubtype);
		$query->bindparam(':vendor',		$this->vendor);
		$query->bindparam(':ponumber',		$this->ponumber);
		$query->bindparam(':company',		$this->company);
		$query->bindparam(':completedate',	$this->completedate);
		$query->bindparam(':internal_acceptance',$this->internal_acceptance);
		$query->bindparam(':goods_recipt',	$this->goods_recipt);
		$query->execute();
		return $this->myconn->lastInsertId;
	}
	
	public function iadone_listing(){
		
		$sql="select * from  iadone";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
	
	public function fetch_iadone_listingAll(){
		
		$sql="select * from  iadone";
		$query=$this->myconn->prepare($sql);
		$query->execute();
		$fetch=$query->fetchAll();
		return $fetch;
	}
	
	
	
}
?>