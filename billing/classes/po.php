<?php
//require('connection.php');
class Po extends Connection{
	
	public function po_listing(){
		$sql="select po.id,po.invoicedate,po.pono,po.podate,po.siteid,po.wpid,po.iadate,po.itemprice,po.itemid,po.workitem,project.project,
		circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity from po 
		join circle on po.circle=circle.id join project on po.project=project.id join customer on po.customer=customer.id join endcustomer on po.endcustomer=endcustomer.id 
		join projectcode on po.projectcode=projectcode.id join activity on po.activity=activity.id ";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
	public function start_print(){
		
		$sql="select po.id,po.invoicedate,po.pono,po.podate,po.siteid,po.wpid,po.iadate,po.itemprice,po.itemid,po.completedate,po.workitem,project.project,
		circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity from po 
		join circle on po.circle=circle.id join project on po.project=project.id join customer on po.customer=customer.id join endcustomer on po.endcustomer=endcustomer.id 
		join projectcode on po.projectcode=projectcode.id join activity on po.activity=activity.id where po.id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$this->id);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	public function poinfo(){
		$sql="select invoice_data.id,invoice_data.invoicedate,invoice_data.pono,invoice_data.invoiceno,invoice_data.podate,invoice_data.siteid,invoice_data.wpid,invoice_data.iadate,invoice_data.itemprice,invoice_data.itemid,invoice_data.workitem,invoice_data.completedate,project.project, circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity from invoice_data join circle on invoice_data.circle=circle.id join project on invoice_data.project=project.id join customer on invoice_data.customer=customer.id join endcustomer on invoice_data.endcustomer=endcustomer.id join projectcode on invoice_data.projectcode=projectcode.id join activity on invoice_data.activity=activity.id where invoice_data.pono=:pono ";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":pono",$this->pono);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	public function updateinvoiceno(){
		$sql="update po set invoiceno='$this->uinvoiceno' where id='$this->uid'";
		//echo $sql;die;
		$query=$this->myconn->prepare($sql);
		$query->execute();
		return true;
	}
	public function updatecurrentcode(){
		$sql="update customer set currentcode='$this->ucurrentcode' where customer='$this->ucustomer'";
		//echo $sql;die;
		$query=$this->myconn->prepare($sql);
		$query->execute();
		return true;
	}
	public function findcurrentcode(){
		$sql="select currentcode,startcode,endcode from customer where customer=:customer";	
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":customer",$this->fcustomer);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	public function invoicedoneinfo(){
		$sql="select invoice.invoiceno,GROUP_CONCAT(invoice_data.pono SEPARATOR ', ') as pos from invoice join invoice_data on invoice.id=invoice_data.invoiceid group by 
		invoice.id ";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
		
	public function poListing(){
		$sql="select * from po";
		$query=$this->myconn->prepare($sql);
		$query->execute();
		$fetch=$query->fetchALL();
		return $fetch;
	}
	public function poListingid($id){
		$sql="select * from po where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$id);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	
	public function poListingbyid($id){
		$sql="select po.id,po.invoicedate,po.pono,po.podate,po.siteid,po.wpid,po.iadate,po.itemid,po.workitem,po.itemprice,project.project,
		circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity from po 
		join circle on po.circle=circle.id join project on po.project=project.id join customer on po.customer=customer.id join endcustomer on po.endcustomer=endcustomer.id 
		join projectcode on po.projectcode=projectcode.id join activity on po.activity=activity.id  where po.id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$id);
		$query->execute();
		$fetch=$query->fetch();
		return $fetch;
	}
	public function Addpo(){
		$sql="insert into po set circle=:circle,project=:project,customer=:customer,endcustomer=:endcustomer,itemprice=:itemprice,projectcode=:projectcode,activity=:activity,
		invoicedate=:invoicedate,pono=:pono,podate=:podate,siteid=:siteid,wpid=:wpid,iadate=:iadate,itemid=:itemid,workitem=:workitem,status=:status,completedate=:completedate";
		
		/*echo "insert into po set circle='$this->circle',project='$this->project',customer='$this->customer',endcustomer=$this->endcustomer,
		projectcode=$this->projectcode,activity=$this->activity,invoicedate=$this->invoicedate,pono=$this->pono,podate=$this->podate,siteid=$this->siteid,
		wpid=$this->wpid,iadate=$this->iadate,itemid=$this->itemid,workitem=$this->workitem,status=$this->status";die;*/
		
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":circle",		$this->circle);
		$query->bindparam(":project",		$this->project);
		$query->bindparam(":customer",		$this->customer);
		$query->bindparam(":endcustomer",	$this->endcustomer);
		$query->bindparam(":projectcode",	$this->projectcode);
		$query->bindparam(":activity",		$this->activity);
		$query->bindparam(":invoicedate",	$this->invoicedate);
		$query->bindparam(":pono",			$this->pono);
		$query->bindparam(":podate",		$this->podate);
		$query->bindparam(":siteid",		$this->siteid);
		$query->bindparam(":wpid",			$this->wpid);
		$query->bindparam(":iadate",		$this->iadate);
		$query->bindparam(":itemid",		$this->itemid);
		$query->bindparam(":workitem",		$this->workitem);
		$query->bindparam(":itemprice",		$this->itemprice);
		$query->bindparam(":completedate",	$this->completedate);
		$query->bindparam(":status",		$this->status);
		
		$query->execute();
		return $this->myconn->lastInsertId();
	}
	
	public function Addinvoice(){
		$sql="insert into invoice set invoiceno=:invoiceno";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":invoiceno",		$this->iinvoiceno);
		$query->execute();
		return $this->myconn->lastInsertId();
	}
	
	public function Addinvoicedata(){
		$sql="insert into invoice_data set poid=:poid,invoiceid=:invoiceid,invoiceno=:invoiceno,circle=:circle,project=:project,customer=:customer,
		endcustomer=:endcustomer,itemprice=:itemprice,
		projectcode=:projectcode,activity=:activity,completedate=:completedate,
		invoicedate=:invoicedate,pono=:pono,podate=:podate,siteid=:siteid,wpid=:wpid,iadate=:iadate,itemid=:itemid,workitem=:workitem,status=:status";
		
		/*echo "insert into po set circle='$this->circle',project='$this->project',customer='$this->customer',endcustomer=$this->endcustomer,
		projectcode=$this->projectcode,activity=$this->activity,invoicedate=$this->invoicedate,pono=$this->pono,podate=$this->podate,siteid=$this->siteid,
		wpid=$this->wpid,iadate=$this->iadate,itemid=$this->itemid,workitem=$this->workitem,status=$this->status";die;*/
		
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":poid",			$this->poid);
		$query->bindparam(":invoiceid",		$this->invoiceid);
		$query->bindparam(":invoiceno",		$this->invoiceno);
		$query->bindparam(":circle",		$this->circle);
		$query->bindparam(":project",		$this->project);
		$query->bindparam(":customer",		$this->customer);
		$query->bindparam(":endcustomer",	$this->endcustomer);
		$query->bindparam(":projectcode",	$this->projectcode);
		$query->bindparam(":activity",		$this->activity);
		$query->bindparam(":invoicedate",	$this->invoicedate);
		$query->bindparam(":pono",			$this->pono);
		$query->bindparam(":podate",		$this->podate);
		$query->bindparam(":siteid",		$this->siteid);
		$query->bindparam(":wpid",			$this->wpid);
		$query->bindparam(":iadate",		$this->iadate);
		$query->bindparam(":itemid",		$this->itemid);
		$query->bindparam(":workitem",		$this->workitem);
		$query->bindparam(":itemprice",		$this->itemprice);
		$query->bindparam(":completedate",	$this->completedate);
		$query->bindparam(":status",		$this->status);
		
		$query->execute();
		return $this->myconn->lastInsertId();
	}
	public function Editpo(){
		$sql="update po set circle=:circle,project=:project,customer=:customer,endcustomer=:endcustomer,itemprice=:itemprice,projectcode=:projectcode,activity=:activity,
		invoicedate=:invoicedate,pono=:pono,podate=:podate,siteid=:siteid,wpid=:wpid,iadate=:iadate,itemid=:itemid,workitem=:workitem,status=:status,completedate=:completedate where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":circle",		$this->circle);
		$query->bindparam(":project",		$this->project);
		$query->bindparam(":customer",		$this->customer);
		$query->bindparam(":endcustomer",	$this->endcustomer);
		$query->bindparam(":projectcode",	$this->projectcode);
		$query->bindparam(":activity",		$this->activity);
		$query->bindparam(":invoicedate",	$this->invoicedate);
		$query->bindparam(":pono",			$this->pono);
		$query->bindparam(":podate",		$this->podate);
		$query->bindparam(":siteid",		$this->siteid);
		$query->bindparam(":wpid",			$this->wpid);
		$query->bindparam(":iadate",		$this->iadate);
		$query->bindparam(":itemid",		$this->itemid);
		$query->bindparam(":workitem",		$this->workitem);
		$query->bindparam(":status",		$this->status);
		$query->bindparam(":itemprice",		$this->itemprice);
		$query->bindparam(":completedate",	$this->completedate);
		$query->bindparam(":id",$this->id);
		$query->execute();
		return true;
	}
	public function Viewpo($id){
		$sql="select * from po where id=:id";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(":id",$id);
		$query->execute();
	}
	
	public function delete_po(){
		
		$sql="delete from po where id='$this->id'";
		$query=$this->myconn->query($sql);
		$query->execute();
		return true;
	}
	
	public function invoice_list($start,$total){
		/*$sql="SELECT y.* FROM po as y INNER JOIN (SELECT circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf FROM po GROUP BY circle,project,customer,
				endcustomer,projectcode,activity HAVING COUNT(*)>1 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and 
				y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity limit $start,$total";*/
		$sql="SELECT y.id,y.invoicedate,y.pono,y.podate,y.siteid,y.wpid,y.iadate,y.itemid,y.itemprice,y.workitem,project.project,
		circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity FROM po as y INNER JOIN (SELECT circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf FROM po GROUP BY circle,project,customer,
				endcustomer,projectcode,activity HAVING COUNT(*)>1 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and 
				y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity join circle on y.circle=circle.id join project on y.project=project.id join customer on y.customer=customer.id join endcustomer on y.endcustomer=endcustomer.id 
		join projectcode on y.projectcode=projectcode.id join activity on y.activity=activity.id limit $start,$total";
		$query=$this->myconn->query($sql);
		return $query;
	}
	public function invoice_list1($start,$total){
		/*$sql="SELECT y.* FROM po as y INNER JOIN (SELECT circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf FROM po GROUP BY circle,project,customer,
				endcustomer,projectcode,activity HAVING COUNT(*)>1 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and 
				y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity limit $start,$total";*/
		$sql="SELECT y.id,y.invoicedate,y.pono,y.podate,y.siteid,y.wpid,y.iadate,y.itemid,y.itemprice,y.workitem,project.project,
			circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity FROM po as y 
			INNER JOIN (SELECT 	circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf FROM po GROUP BY circle,project,customer,
			endcustomer,projectcode,activity HAVING COUNT(*)>1 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and 
			y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity join circle on y.circle=circle.id join project on y.project=project.id 
			join customer on y.customer=customer.id join endcustomer on y.endcustomer=endcustomer.id 
			join projectcode on y.projectcode=projectcode.id join activity on y.activity=activity.id limit $start,$total";
		$query=$this->myconn->query($sql);
		$query->execute();
		$fetch=$query->fetchALL();
		return $fetch;
	}
	public function invoice_listing(){
		/*$sql="Select po.*, count(*) from po group by circle,project,customer,endcustomer,projectcode,activity having count(*) > 1";*/
		$sql="Select po.id,po.invoicedate,po.pono,po.podate,po.itemprice,po.siteid,po.wpid,po.iadate,po.itemid,po.workitem,project.project, circle.circle,customer.customer,endcustomer.endcustomer,projectcode.projectcode,activity.activity, count(*) from po join circle on po.circle=circle.id join project on po.project=project.id join customer on po.customer=customer.id join endcustomer on po.endcustomer=endcustomer.id join projectcode on po.projectcode=projectcode.id join activity on po.activity=activity.id group by circle,project,customer,endcustomer,projectcode,activity having count(*) > 1 ";
		$query=$this->myconn->query($sql);
		return $query;
	}
	
	/*create table d10_api(id int(11) not null AUTO_INCREMENT primary key,CustomerName varchar(50),CustomerPONumber int(11),DateOfPO varchar(50),Inovice varchar(50), InternalPONumber varchar(50),item varchar(50),ItemDescription varchar(100),PaymentStatus varchar(50),PaymentTerms varchar(50),POValue int(6),ProjectCode varchar(8),Quantity tinyint(1),Rate int(6),SiteId varchar(10),poamount int(6),Wccstatus varchar(20),Workpackages tinyint(3),Workstatus varchar(100) )*/
	public function d10_api(){
		$sql="insert into d10_api set CustomerName='$this->CustomerName',CustomerPONumber='$this->CustomerPONumber',DateOfPO='$this->DateOfPO',Inovice='$this->Inovice', InternalPONumber='$this->InternalPONumber',
		item='$this->item',ItemDescription='$this->ItemDescription',PaymentStatus='$this->PaymentStatus',PaymentTerms='$this->PaymentTerms',POValue='$this->POValue',ProjectCode='$this->ProjectCode',Quantity='$this->Quantity',
		Rate='$this->Rate',SiteId='$this->SiteId',poamount='$this->poamount',Wccstatus='$this->Wccstatus',Workpackages='$this->Workpackages',Workstatus='$this->Workstatus'";
		//echo $sql;die;
		
		$query=$this->myconn->query($sql);
		$query->execute();
		return $this->myconn->lastInsertId();
	}
	
	public function projectcode_d10(){
			$sql="select ProjectCode from d10_api where CustomerPONumber='$this->ponum'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
	}

}




/*SELECT y.* FROM po as y INNER JOIN (SELECT circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf FROM po GROUP BY circle,project,customer,endcustomer,projectcode,activity HAVING COUNT(*)>1 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity limit 0,8*/ 



/*SELECT y.* FROM po as y INNER JOIN (SELECT circle,project,customer,endcustomer,projectcode,activity, COUNT(*) AS CountOf
                        FROM po
                        GROUP BY circle,project,customer,endcustomer,projectcode,activity
                        HAVING COUNT(*)>1                 ) dt ON y.circle=dt.circle and y.project=dt.project and y.customer=dt.customer and y.endcustomer=dt.endcustomer and y.projectcode=dt.projectcode and y.activity=dt.activity*/




//Select po.*, count(*) from po group by circle,project,customer,endcustomer,projectcode,activity having count(*) > 1
//Select po.* from po group by circle,project,customer,endcustomer,projectcode,activity having count(*) > 1 
/*Select circle,project,customer,endcustomer,projectcode,activity, count(*) from po group by circle,project,customer,endcustomer,projectcode,activity having count(*) > 1 
circle 	project 	customer 	endcustomer 	projectcode 	activity 	count(*) 	
1 		1 			1 			1 				1 				1 			2*/

/*$po= new  po();
$z=$po->poListing();
echo "<pre>";
//print_r($z);
foreach($z as $x){
echo $x['id'];
echo $x['po'];
echo $x['create_at'];
}*/
?>