<?php
	class Invoicecluster extends Connection{
	
		public function cluster_invoice_listing(){
		
			$sql="select ia.project,ia.circle,ia.site_customer,ia.site_id,ia.wpid,ia.packagename,ia.itemid,ia.workitemsubtype,ia.vendor,ia.ponumber,
			ia.company as comp,ia.completedate,ia.internal_acceptance as iadate,ia.goods_recipt as gdsdate,pdc.pono,pdc.podate,pdc.siteid,pdc.company,pdc.lineitem10,
			pdc.itemid10,pdc.unitprice10,pdc.fielddrive10,pdc.lineitem20,pdc.itemid20,pdc.unitprice20,pdc.radionetwork20,pdc.lineitem30,pdc.itemid30,pdc.unitprice30,
			pdc.fielddrive30,pdc.deliverydate,pdc.amount from po_database_cluster as pdc join iadone as ia on pdc.pono=ia.ponumber where ia.invoice_status=0 
			and pdc.invoice_status=0 order by pdc.id asc";
			$query=$this->myconn->prepare($sql);
			//$query->execute();
			//$fetch=$query->fetch_all();
			return $query;
		}
		
		public function print_cluster(){
		
			$sql="select ia.project,ia.circle,ia.site_customer,ia.site_id,ia.wpid,ia.packagename,ia.itemid,ia.workitemsubtype,ia.vendor,ia.ponumber,
			ia.company as comp,ia.completedate,ia.internal_acceptance as iadate,ia.goods_recipt as gdsdate,pdc.pono,pdc.podate,pdc.siteid,pdc.company,pdc.lineitem10,
			pdc.itemid10,pdc.unitprice10,pdc.fielddrive10,pdc.lineitem20,pdc.itemid20,pdc.unitprice20,pdc.radionetwork20,pdc.lineitem30,pdc.itemid30,pdc.unitprice30,
			pdc.fielddrive30,pdc.deliverydate,pdc.amount from po_database_cluster as pdc join iadone as ia on pdc.pono=ia.ponumber where ia.invoice_status=0 
			and pdc.invoice_status=0 and ia.ponumber='$this->po' and pdc.pono='$this->po'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
		}
		
		
		public function cluster_listing_by_id($id){
		
			$sql="select * from po_database_cluster join iadone on po_database_cluster.pono=iadone.ponumber where id=:id";
			$query=$this->myconn->prepare($sql);
			$query->execute(array(":id"=>$id));
			$fetch=$query->fetch();
			return $fetch;
		}
		
		public function invoice_printed(){
		
			$sql="update po_database_cluster set invoice_status=1,invoiceno='$this->invoiceno' where pono='$this->pono'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			return true;
		}
		public function projectcode_d10(){
			$sql="select ProjectCode,CustomerName from d10_api where CustomerPONumber='$this->ponum'";
			$query=$this->myconn->prepare($sql);
			$query->execute();
			$fetch=$query->fetch();
			return $fetch;
		}
		public function updatecurrentcode(){
		$sql="update customer set currentcode='$this->ucurrentcode' where customer='$this->ucustomer'";
		//echo $sql;die;
		$query=$this->myconn->prepare($sql);
		$query->execute();
		return true;
	}
		
		
	}
?>