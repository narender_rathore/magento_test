<?php
	if($_SERVER['HTTP_HOST']==='localhost'){
		define("DOCUMENT_ROOT","http://localhost/billing/");
	}
	else{
		define("DOCUMENT_ROOT","http://localhost/billing/");
	}
	require('connection.php');
	
	define("SITE_TITLE","DASHBOARD :: DECON INVOICES");
	define("SITE_URL","/billing/");
	define("LOGO","http://localhost/invoice/bootstrap/img/logo.png");
	define("PRODUCT_IMAGE_PATH","http://localhost/invoice/images/product_images/");
	define("PROFILE_IMAGE_PATH","http://localhost/invoice/images/profile_images/");
	define("PARTNER_IMAGE_PATH","http://localhost/invoice/images/partners_images/");
	define("TESTIMONIAL_IMAGE_PATH","http://localhost/invoice/images/testimonial_images/");
	define("PER_PAGE",16);
	//error_reporting(0);
	//error_reporting(-1);
	//error_reporting(E_ALL & ~(E_NOTICE | E_STRICT | E_WARNING | E_DEPRECATED ));
		
	
	function __autoload($class){
		require("classes/".strtolower($class).".php");
	}
	//error_reporting(0);
	//error_reporting(-1);
	error_reporting(E_ALL & ~(E_ERROR | E_NOTICE | E_PARSE | E_WARNING | E_DEPRECATED));

?>