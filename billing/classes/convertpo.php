<?php
//require('connection.php');
class Convertpo extends Connection{
	
	public function import_convertpo_cluster(){
		
		//$sql="insert into po_database_cluster set pono=:pono,podate=:podate,siteid=:siteid,amount=:amount";
		$sql="insert into po_database_cluster set pono='$this->pono',podate='$this->podate',siteid='$this->siteid',amount='$this->amount',
		company='$this->company',lineitem10='$this->lineitem10',itemid10='$this->itemid10',unitprice10='$this->unitprice10',
		fielddrive10='$this->fielddrive10',lineitem20='$this->lineitem20',itemid20='$this->itemid20',unitprice20='$this->unitprice20',
		radionetwork20='$this->radionetwork20',lineitem30='$this->lineitem30',itemid30='$this->itemid30',unitprice30='$this->unitprice30',
		fielddrive30='$this->fielddrive30',deliverydate='$this->deliverydate'";
		//echo $sql;die;
		$query=$this->myconn->prepare($sql);
		/*$sql="insert into po_database_cluster set pono=:pono,podate=:podate,siteid=:siteid,amount=:amount,company=:company,lineitem10=:lineitem10,
				itemid10=:itemid10,unitprice10=:unitprice10,fielddrive10=:fielddrive10,lineitem20=:lineitem20,itemid20=:itemid20,
				unitprice20=:unitprice20,radionetwork20=:radionetwork20,lineitem30=:lineitem30,itemid30:=itemid30,unitprice30=:unitprice30,
				fielddrive30=:fielddrive30,deliverydate=:deliverydate";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(':pono',			$this->pono);
		$query->bindparam(':podate',		$this->podate);
		$query->bindparam(':siteid',		$this->siteid);
		$query->bindparam(':amount',		$this->amount);
		$query->bindparam(':company',		$this->company);
		$query->bindparam(':lineitem10',	$this->lineitem10);
		$query->bindparam(':itemid10',		$this->itemid10);
		$query->bindparam(':unitprice10',	$this->unitprice10);
		$query->bindparam(':fielddrive10',	$this->fielddrive10);
		$query->bindparam(':lineitem20',	$this->lineitem20);
		$query->bindparam(':itemid20',		$this->itemid20);
		$query->bindparam(':unitprice20',	$this->unitprice20);
		$query->bindparam(':radionetwork20',$this->radionetwork20);
		$query->bindparam(':lineitem30',	$this->lineitem30);
		$query->bindparam(':itemid30',		$this->itemid30);
		$query->bindparam(':unitprice30',	$this->unitprice30);
		$query->bindparam(':fielddrive30',	$this->fielddrive30);
		$query->bindparam(':deliverydate',	$this->deliverydate);*/
		$query->execute();
		return $this->myconn->lastInsertId;
	}
	
	public function convertpo_listing_cluster(){
		
		$sql="select * from  po_database_cluster";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
	
	public function fetch_convertpo_listingAll_cluster(){
		
		$sql="select * from  po_database_cluster";
		$query=$this->myconn->prepare($sql);
		$query->execute();
		$fetch=$query->fetchAll();
		return $fetch;
	}
	
	public function import_convertpo_scft(){
		
		//$sql="insert into po_database_scft set pono=:pono,podate=:podate,siteid=:siteid,amount=:amount";
		$sql="insert into po_database_scft set pono=:pono,podate=:podate,siteid=:siteid,amount=:amount,company=:company,lineitem10=:lineitem10,itemid10=:itemid10,unitprice10=:unitprice10,radionetwork10=:radionetwork10,lineitem20=:lineitem20,itemid20=:itemid20,unitprice20=:unitprice20,fielddrive20=:fielddrive20,deliverydate=:deliverydate";
		$query=$this->myconn->prepare($sql);
		$query->bindparam(':pono',			$this->pono);
		$query->bindparam(':podate',		$this->podate);
		$query->bindparam(':siteid',		$this->siteid);
		$query->bindparam(':amount',		$this->amount);
		$query->bindparam(':company',		$this->company);
		$query->bindparam(':lineitem10',	$this->lineitem10);
		$query->bindparam(':itemid10',		$this->itemid10);
		$query->bindparam(':unitprice10',	$this->unitprice10);
		$query->bindparam(':radionetwork10',$this->radionetwork10);
		$query->bindparam(':lineitem20',	$this->lineitem20);
		$query->bindparam(':itemid20',		$this->itemid20);
		$query->bindparam(':unitprice20',	$this->unitprice20);
		$query->bindparam(':fielddrive20',	$this->fielddrive20);
		$query->bindparam(':deliverydate',	$this->deliverydate);
		$query->execute();
		return $this->myconn->lastInsertId;
	}
	
	public function convertpo_listing_scft(){
		
		$sql="select * from  po_database_scft";
		$query=$this->myconn->prepare($sql);
		return $query;
	}
	
	public function fetch_convertpo_listingAll_scft(){
		
		$sql="select * from  po_database_scft";
		$query=$this->myconn->prepare($sql);
		$query->execute();
		$fetch=$query->fetchAll();
		return $fetch;
	}
	
	
	
}
?>