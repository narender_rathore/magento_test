
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=PROFILE_IMAGE_PATH.$admin_data['profile_pic']?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=ucfirst($admin_data['fname'])."&nbsp;&nbsp;".ucfirst($admin_data['lname'])?></p>
              <!-- Status -->
             <!-- <a href=""><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
          </div>



          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
                <li class="header">Billing Department</li>
                <!-- Optionally, you can add icons to the links -->
                <?php /*?><li <?php if($_SERVER['PHP_SELF']==SITE_URL."project.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>project.php"><i class="fa fa-fw fa-link"></i> <span>Manage Project</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."circle.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>circle.php"><i class="fa fa-fw fa-link"></i> <span>Manage Circle</span></a></li>
             	<li <?php if($_SERVER['PHP_SELF']==SITE_URL."customer.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>customer.php"><i class="fa fa-fw fa-link"></i> <span>Manage Customer</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."endcustomer.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>endcustomer.php"><i class="fa fa-fw fa-link"></i> <span>Manage EndCustomer</span></a></li>
                
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."projectcode.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>projectcode.php"><i class="fa fa-fw fa-link"></i> <span>Manage Project Code</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."activity.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>activity.php"><i class="fa fa-fw fa-link"></i> <span>Manage Activity</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."po.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>po.php"><i class="fa fa-fw fa-link"></i> <span>Manage Purchase Order</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."printinvoice.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>printinvoice.php"><i class="fa fa-fw fa-link"></i> <span>Printed Invoice</span></a></li><?php */?>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."iadone.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>iadone.php"><i class="fa fa-fw fa-link"></i> <span>Internal Acceptance</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."managepdf.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>managepdf.php"><i class="fa fa-fw fa-link"></i> <span>Pdf Conversion</span></a></li>
                <li <?php if($_SERVER['PHP_SELF']==SITE_URL."convertpo_cluster.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>convertpo_cluster.php"><i class="fa fa-fw fa-link"></i> <span>PO Database Cluster</span></a></li>
                 <li <?php if($_SERVER['PHP_SELF']==SITE_URL."convertpo_scft.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>convertpo_scft.php"><i class="fa fa-fw fa-link"></i> <span>PO Database SCFT</span></a></li>
                 
                 <li <?php if($_SERVER['PHP_SELF']==SITE_URL."invoice_cluster.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>invoice_cluster.php"><i class="fa fa-fw fa-link"></i> <span>Invoice Cluster</span></a></li>
                 <li <?php if($_SERVER['PHP_SELF']==SITE_URL."invoice_scft.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>invoice_scft.php"><i class="fa fa-fw fa-link"></i> <span>Invoice SCFT</span></a></li>
                 
                <?php /*?><li <?php if($_SERVER['PHP_SELF']==SITE_URL."profile.php"){?>class="active" <?php } ?>><a href="<?=DOCUMENT_ROOT?>profile.php"><i class="fa fa-fw fa-link"></i> <span>Admin Profile</span></a></li><?php */?>
                

                
                
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>