<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	
	if(isset($_REQUEST['doaction']) && ($_POST['doaction']=='Add_Activity')){  
		
		$activity->activity=strtoupper(($_POST['activity']));
		$activity->status =  $_POST['status'];		
		
		$insert_id=$activity->Addactivity();
			if($insert_id!=""){	
				echo "<script>document.location.href='Editactivity.php?id=".$insert_id."&message-success=activity Added Successfully'</script>";
			}
			else{
				echo "<script>document.location.href='Addactivity.php?message-error=Error! Something is not right here.'</script>";
			}
		
	}
	
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}	
			
	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Add activity</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage activity</a></li>
            <li class="active">Add activity</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add activitys</h3>-->
                  
                  <button type="submit" name="add_activity" onclick="return add_activity_validate()" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  onclick=""><i class="fa fa-fw fa-check-square-o"></i> Save </button> 
                                    <button onclick="document.location.href='activity.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-arrows"></i> Manage activity</button>    
  
<?php 

	if($message!=''){ 
	 	echo $message;
 	}
 ?>
                </div><!-- /.box-header -->
                <form name="activity_form_add" id="activity_form_add" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <div class="box-body">
                <hr />
                	<div class="row">
                		<div class="col-md-6">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">activity Name</label>
                                  <input type="text" class="form-control" id="activity" name="activity" placeholder="Enter activity Title">
                                </div>
                                
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2" name="status" id="status" style="width: 100%;">
                                  <option  value="1" selected="selected">Active</option>
                                  <option value="0" >Deactive</option>
                                </select>
                                </div>
                            
                 		</div>
                 	</div>
                </div><!-- /.box-body -->
              <hr />
                 <button type="submit" name="add_activity" onclick="return add_activity_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  ><i class="fa fa-fw fa-check-square-o"></i> Save</button> 
                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>

     <?php include_once('footer.php');?>