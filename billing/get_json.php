
<?php
//error_reporting(-1);
include_once('classes/settings.php');
include_once('classes/po.php');
$po = new Po();

set_time_limit(0);
// set HTTP header
$headers = array(
    'Content-Type: application/json',
);

// query string
$fields = array(
    'key' => '<your_api_key>',
    'format' => 'json',
    'ip' => $_SERVER['REMOTE_ADDR'],
);
$url = 'http://180.151.246.138/GetData/GetP0Details?' . http_build_query($fields);

// Open connection
$ch = curl_init();

// Set the url, number of GET vars, GET data
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Execute request
$result = curl_exec($ch);

// Close connection
curl_close($ch);

// get the result and parse to JSON
$result_arr = json_decode($result, true);
//echo "<pre>";
//print_r($result_arr);
//echo count($result_arr);


foreach($result_arr as $fetch){
	$po->CustomerName 		  	= $fetch['CustomerName'];
	$po->CustomerPONumber       = $fetch['CustomerPONumber'];
	$po->DateOfPO         		= $fetch['DateOfPO'];
	$po->Inovice         		= $fetch['Inovice'];
	$po->InternalPONumber       = $fetch['InternalPONumber'];
	$po->Item         			= $fetch['Item'];
	$po->ItemDescription        = $fetch['ItemDescription'];
	$po->PaymentStatus         	= $fetch['PaymentStatus'];
	$po->PaymentTerms         	= $fetch['PaymentTerms'];
	$po->POValue         		= $fetch['POValue'];
	$po->ProjectCode         	= $fetch['ProjectCode'];
	$po->Quantity         		= $fetch['Quantity'];
	$po->Rate         			= $fetch['Rate'];
	$po->SiteId        			= $fetch['SiteId'];
	$po->poamount        	 	= $fetch['value'];
	$po->Wccstatus         		= $fetch['Wccstatus'];
	$po->Workpackages         	= $fetch['Workpackages'];
	$po->Workstatus         	= $fetch['Workstatus'];
	$po->d10_api();
}



/*
 *  output:
 *  Array
 *  (
 *      [statusCode] => "OK",
 *      [statusMessage] => "",
 *      [ipAddress] => "123.13.123.12",
 *      [countryCode] => "MY",
 *      [countryName] => "MALAYSIA",
 *  )
 */
 ?>