<?php include_once("classes/settings.php"); ?>
	<?php
		$circle				= new Circle();
		$customer			= new Customer();
		$project			= new Project();
		$endcustomer		= new Endcustomer();
		$projectcode		= new Projectcode();
		$activity   		= new Activity();
		$po					= new Po();
		$iadone				= new Iadone();
		$convertpo  		= new Convertpo();
		$invoicecluster  	= new Invoicecluster();
		$invoicescft  		= new Invoicescft();
	?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=DOCUMENT_ROOT?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=DOCUMENT_ROOT?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
      
    <link rel="stylesheet" href="<?=DOCUMENT_ROOT?>assets/dist/css/skins/skin-blue.min.css">
	  <script src="<?=DOCUMENT_ROOT?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  
	<!--<script src="assets/dist/js/jquery.min.js"></script>
    <script src="assets/dist/js/jquery-1.10.2.min.js"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=DOCUMENT_ROOT?>assets/bootstrap/js/bootstrap.min.js"></script>
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <style>
		.alert1{
		   padding: 7px 10px !important;
		}
 		.error{
		 padding-left: 15px !important;
		 color:#ff0000;
		 line-height:normal;
 		}

	</style>
 
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">     
  
      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="<?=DOCUMENT_ROOT?>index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?=DOCUMENT_ROOT?>assets/dist/img/logo.png" height="50px"  /></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?=DOCUMENT_ROOT?>assets/dist/img/footerlogo.png" height="50px"  /> Admin</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
             

              <!-- Notifications Menu -->
              
              <!-- Tasks Menu -->
              
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
             <!-- <div class="pull-right">
                      <a href="<?=DOCUMENT_ROOT?>logout.php" class="btn btn-default">Sign out</a>
                    </div>-->
              </li>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle drose" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="<?=PROFILE_IMAGE_PATH.$admin_data['profile_pic']?>" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?=ucfirst($admin_data['fname'])."&nbsp;&nbsp;".ucfirst($admin_data['lname'])?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?=PROFILE_IMAGE_PATH.$admin_data['profile_pic']?>" class="img-circle" alt="User Image">
                    <p>
                     <?=ucfirst($admin_data1['fname'])."&nbsp;&nbsp;".ucfirst($admin_data['lname'])?> 
                      <small>Last Login  <?=$admin_data['last_login']?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=DOCUMENT_ROOT?>profile.php" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=DOCUMENT_ROOT?>logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>