<?php /*?><?php
	session_start();
	//print_r($_SESSION);die;
	//error_reporting(-1); 

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');		
		
		$valid = array(
		'pdf' => 'PDF',
		'txt' => 'TXT',
		//'doc' => 'Word'
		);
	
	$files = array();    
	$directory_path="E:/xampp/htdocs/billing/pdf/";
	$dir = new DirectoryIterator($directory_path);
	
	foreach($dir as $file)
	{
		// filter out directories
		if($file->isDot() || !$file->isFile()) continue;
	
		// Use pathinfo to get the file extension
		$info = pathinfo($file->getPathname());
	
		// Check there is an extension and it is in the whitelist
		if(isset($info['extension']) && isset($valid[$info['extension']])) 
		{
			$files[] = array(
				'filename' => $file->getFilename(),
				'size' => $file->getSize(),
				'type' => $valid[$info['extension']], // 'PDF' or 'Word'
				'created' => date('Y-m-d H:i:s', $file->getCTime())
			);
		}
	}
	//print_r($files);
	$num_rows=count($files);
	//echo $num_rows;die;
	?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Manage Pdf</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">PDF</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage Brands</h3>-->
        <?php 
  
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-3 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-3 alert1" >'.$_GET['message-error'].'</div>';
	}
	if($message!=''){ 
	 	echo $message;
 	}
 ?>
<button onclick="document.location.href='pdf/deletetxt.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i>Delete Txt</button>
<button onclick="document.location.href='pdf/readtxt_cluster.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i>Read Cluster</button>
<button onclick="document.location.href='pdf/readtxt.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i>Read SCFT</button>

<button onclick="document.location.href='pdf/deletepdf.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i>Save PDF</button>
          <button onclick="document.location.href='pdf/read.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i>Read PDF</button>
           <button onclick="document.location.href='pdfconversion.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add PDF</button>
          </div>
          <!-- /.box-header -->
         <form action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" name="project_form" id="project_form" method="post" >
          <input type="hidden"  name="doaction" id="doaction" />
          <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>pdf name </th>
              <th>size [Bytes]</th>
              <th>type </th>
              <th>created </th>
            </tr>
          </thead>
          <tbody>
            <?php 
				if($num_rows>0){$sn=1;
					foreach($files as $row){ 			
			?>
            <tr>
              <td><?php echo $sn; ?></td>
              <td><?=$row['filename']?></td>
              <td><?=$row['size']?></td>
              <td><?=$row['type']?></td>
              <td><?=$row['created']?></td>
            </tr>
            <?php 
				$sn++;
				} 
				} 
				else{ ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php
				}  
			 ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>pdf name </th>
              <th>size [Bytes]</th>
              <th>type </th>
              <th>created </th>
            </tr>
          </tfoot>
        </table>
        </form>
      </div>
    </div>
  </div>
</div>
</section>
</div>
<?php include_once('footer.php');?>
<!-- jQuery 2.1.4 -->

<script src="<?=DOCUMENT_ROOT?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/fastclick/fastclick.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<!---my script----->
<script language="javascript">
	$(function(){
		$("#selectall").click(function () {
			  $('.checkboxes').attr('checked', this.checked);
		});
	 
		$(".checkboxes").click(function(){
	 
			if($(".checkboxes").length == $(".checkboxes:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}
	 
		});
	});
	</script>
<!---my script---->