<?php /*?><?php
	session_start();

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
		<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
    
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	
	if(isset($_REQUEST['doaction']) && ($_POST['doaction']=='Add_Po')){  
		
		$po->circle			=	$_POST['circle'];
		$po->project		=	$_POST['project'];
		$po->customer		=	$_POST['customer'];
		$po->endcustomer	=	$_POST['endcustomer'];
		$po->projectcode	=	$_POST['projectcode'];
		$po->activity		=	$_POST['activity'];
		$po->invoicedate	=	$_POST['invoicedate'];
		$po->pono			=	$_POST['pono'];
		$po->podate			=	$_POST['podate'];
		$po->siteid			=	$_POST['siteid'];
		$po->wpid			=	$_POST['wpid'];
		$po->iadate			=	$_POST['iadate'];
		$po->itemid			=	$_POST['itemid'];
		$po->workitem		=	$_POST['workitem'];
		$po->status 		=  	$_POST['status'];		
		
		$insert_id=$po->Addpo();
			if($insert_id!=""){	
				echo "<script>document.location.href='Editpo.php?id=".$insert_id."&message-success=po Added Successfully'</script>";
			}
			else{
				echo "<script>document.location.href='Addpo.php?message-error=Error! Something is not right here.'</script>";
			}
		
	}
	
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}	
			
	?>
    
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Add po</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Manage po</a></li>
            <li class="active">Add po</li>
          </ol>
        </section>

        <!-- Main content -->   
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<h3 class="box-title">Add pos</h3>-->
                  
                  <button type="submit" name="add_po" onclick="return add_po_validate()" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right"  onclick=""><i class="fa fa-fw fa-check-square-o"></i> Save </button> 
                                    <button onclick="document.location.href='po.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-arrows"></i> Manage po</button>    
  
<?php 

	if($message!=''){ 
	 	echo $message;
 	}
 ?>
                </div><!-- /.box-header -->
                <form name="po_form_add" id="po_form_add" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="doaction" id="doaction" />
                <div class="box-body">
                <hr />
                	<div class="row">
                    
                    <div class="col-md-12">
                    <div class="well well-sm"><strong>Customer Contact Details</strong></div>
                  <div class="col-md-12 col-lg-offset-0 table-responsive form-group">
                  <div ng-app="productsTableApp">
                    <div ng-controller="ProductsController">
                     <input class="form-control btn btn-block btn-success add" style="width:50px;" type="button" value="Add">
                     <table id="participantTable" class="table-responsive" border="0" cellpadding="0" cellspacing="0">

                    <tr class="participantRow">
                        <td> <select class="form-control" name="circle" id="circle" style="width:150px;">
                                  	  <option  value="">Select Circle</option>
                                  <?php 
								  $circles=$circle->circleListing();foreach($circles as $circle):?>
                                      <option  value="<?=$circle['id'];?>" ><?=$circle['circle'];?></option>
								  <?php endforeach; ?>
                                  </select>
                                  </td>
                        <td><select class="form-control" name="project" id="project" style="width: 150px;">
                                  	  <option  value="">Select Project</option>
                                  <?php 
								  $projects=$project->projectListing();
								  foreach($projects as $project):?>
                                      <option  value="<?=$project['id'];?>" ><?=$project['project'];?></option>
								  <?php endforeach; ?>
                                  </select></td>
                        <td><select class="form-control" name="customer" id="customer" style="width: 150px;">
                                      <option  value="">Select Customer</option>
                                  <?php 
								  $customers=$customer->customerListing();
								  foreach($customers as $customer):?>
                                      <option  value="<?=$customer['id'];?>" ><?=$customer['customer'];?></option>
								  <?php endforeach; ?>
                                  </select></td>
                        <td><select class="form-control" name="endcustomer" id="endcustomer" style="width: 150px;">
                                  	<option  value="">Select End Customer</option>
                                  <?php 
								  $endcustomers=$endcustomer->endcustomerListing();
								  foreach($endcustomers as $endcustomer):?>
                                      <option  value="<?=$endcustomer['id'];?>" ><?=$endcustomer['endcustomer'];?></option>
								  <?php endforeach; ?>
                                  </select></td>
                        <td><select class="form-control" name="projectcode" id="projectcode" style="width: 150px;">
                                      <option  value="">Select Project Code</option>
                                  <?php 
								  $projectcodes=$projectcode->projectcodeListing();
								  foreach($projectcodes as $projectcode):?>
                                      <option  value="<?=$projectcode['id'];?>" ><?=$projectcode['projectcode'];?></option>
								  <?php endforeach; ?>
                                  </select></td>
                        <td><select class="form-control" name="activity" id="activity" style="width: 150px;">
                                  	  <option  value="">Select Activity</option>
                                  <?php 
								  $activitys=$activity->activityListing();
								  foreach($activitys as $activity):?>
                                      <option  value="<?=$activity['id'];?>" ><?=$activity['activity'];?></option>
								  <?php endforeach; ?>
                                  </select></td>
                        <td><input type="text" class="form-control" id="invoicedate" name="invoicedate" placeholder="Enter Invoice Date" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="pono" name="pono" placeholder="Enter Purchase Order number" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="podate" name="podate" placeholder="Enter Purchase order Date" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="siteid" name="siteid" placeholder="Enter Site id" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="wpid" name="wpid" placeholder="Enter Invoice Date" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="iadate" name="iadate" placeholder="Enter Invoice Date" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="itemid" name="itemid" placeholder="Enter Invoice Date" style="width: 150px;"></td>
                        <td><input type="text" class="form-control" id="workitem" name="workitem" placeholder="Enter Invoice Date" style="width: 150px;"></td>
                         <td><input class="form-control btn btn-block btn-danger remove" style="width:50px;" type="button" value="Remove"></td>

                    </tr>

                </table>
                    </div>
                    </div></div></div>
                		
                 	</div>
                </div><!-- /.box-body -->
              <hr />
                 <button type="submit" name="add_po" onclick="return add_po_validate();" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right">
                 <i class="fa fa-fw fa-check-square-o"></i> Save</button> 
                </form>
              </div><!-- /.box -->

            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><?php include_once('footer.php');?>
<script type="text/javascript">

/* Variables */
var p = $("#participants").val();
var row = $(".participantRow");

/* Functions */
function getP(){
  p = $("#participants").val();
}

function addRow() {
  row.clone(true, true).appendTo("#participantTable");
}

function removeRow(button) {
  button.closest("tr").remove();
}
/* Doc ready */
$(".add").on('click', function () {
  getP();
  if($("#participantTable tr").length < 17) {
    addRow();
    var i = Number(p)+1;
    $("#participants").val(i);
  }
  $(this).closest("tr").appendTo("#participantTable");
  if ($("#participantTable tr").length === 3) {
    $(".remove").hide();
  } else {
    $(".remove").show();
  }
});
$(".remove").on('click', function () {
  getP();
  if($("#participantTable tr").length === 3) {
    //alert("Can't remove row.");
    $(".remove").hide();
  } else if($("#participantTable tr").length - 1 ==3) {
    $(".remove").hide();
    removeRow($(this));
    var i = Number(p)-1;
    $("#participants").val(i);
  } else {
    removeRow($(this));
    var i = Number(p)-1;
    $("#participants").val(i);
  }
});
$("#participants").change(function () {
  var i = 0;
  p = $("#participants").val();
  var rowCount = $("#participantTable tr").length - 2;
  if(p > rowCount) {
    for(i=rowCount; i<p; i+=1){
      addRow();
    }
    $("#participantTable #addButtonRow").appendTo("#participantTable");
  } else if(p < rowCount) {
  }
});

</script>
     