function AllowNumbersOnly(e)
{
var unicode=e.charCode?e.charCode:e.keyCode;
if(unicode!=8&&unicode!=9){
	if(unicode<48||unicode>57){
		return false;
		}
		}
return true;
}


/*  Brand Validation Start  */
function delete_Brand(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_brand');
		document.brand_form_manage.submit();
		}		
		}else{
			alert("Please select at least one Brand.");
		}

}

function add_brand_validate(){
	
	if($('#brand_title').val()==""){
		alert("Please Enter Brand Title");
		$("#brand_title").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Meta Title");
		$("#meta_title").focus();
		return false;	
	}
	
	if($('#pimage').val()==""){
		alert("Please upload a Image");
		$("#pimage").focus();
		return false;	
	}
	$("#doaction").val('Add_Brand');
	
	document.brand_form_add.submit();
	
}

function edit_brand_validate(){
	
	if($('#brand_title').val()==""){
		alert("Please Enter Brand Title");
		$("#brand_title").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Meta Title");
		$("#meta_title").focus();
		return false;	
	}
	
	$("#doaction").val('Edit_Brand');
	
	document.brand_form_edit.submit();
	
}
/*  Brand Validation End  */

/*  Product Validation start  */
function delete_Product(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_product');
		document.product_form_manage.submit();
		}		
		}else{
			alert("Please select at least one Product.");
		}

}

function add_product_validate(){
	
	if($('#brand').val()==""){
		alert("Please Select Brand");
		$("#brand").focus();
		return false;	
	}
	
	if($('#product_title').val()==""){
		alert("Please Enter Product Title");
		$("#product_title").focus();
		return false;	
	}
	
	if($('#price').val()==""){
		alert("Please Enter price");
		$("#price").focus();
		return false;	
	}
	
	if($('#quantity').val()==""){
		alert("Please Enter Quantity");
		$("#quantity").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Description");
		$("#meta_title").focus();
		return false;	
	}
	
	if($('#terms').val()==""){
		alert("Please Enter Terms & conditions");
		$("#terms").focus();
		return false;	
	}
	
	if($('#pimage').val()==""){
		alert("Please upload a Image");
		$("#pimage").focus();
		return false;	
	}
	$("#doaction").val('Add_Product');
	
	document.product_form_add.submit();
	
}

function edit_product_validate(){
	
	if($('#brand').val()==""){
		alert("Please Select Brand");
		$("#brand").focus();
		return false;	
	}
	
	if($('#product_title').val()==""){
		alert("Please Enter Product Title");
		$("#product_title").focus();
		return false;	
	}
	
	if($('#price').val()==""){
		alert("Please Enter price");
		$("#price").focus();
		return false;	
	}
	
	if($('#quantity').val()==""){
		alert("Please Enter Quantity");
		$("#quantity").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Description");
		$("#meta_title").focus();
		return false;	
	}
	
	if($('#terms').val()==""){
		alert("Please Enter Terms & conditions");
		$("#terms").focus();
		return false;	
	}
	
	$("#doaction").val('Edit_Product');
	
	document.product_form_edit.submit();
	
}

/*  Product Validation End  */



/*  CMS Validation start  */
function delete_Cms(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_cms');
		document.cms_form_manage.submit();
		}		
		}else{
			alert("Please select at least one page.");
		}

}

function add_cms_validate(){
	
	if($('#page_title').val()==""){
		alert("Please Enter Page Title");
		$("#page_title").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Meta Title");
		$("#meta_title").focus();
		return false;	
	}
	if($('#meta_keyword').val()==""){
		alert("Please Enter Meta Keywords");
		$("#meta_keyword").focus();
		return false;	
	}
	if($('#description').val()==""){
		alert("Please Enter Description ");
		$("#description").focus();
		return false;	
	}
	$("#doaction").val('Add_Cms');
	
	document.cms_form_add.submit();
	
}

function edit_cms_validate(){
	
	if($('#page_title').val()==""){
		alert("Please Enter Page Title");
		$("#page_title").focus();
		return false;	
	}
	
	if($('#meta_title').val()==""){
		alert("Please Enter Meta Title");
		$("#meta_title").focus();
		return false;	
	}
	if($('#meta_keyword').val()==""){
		alert("Please Enter Meta Keywords");
		$("#meta_keyword").focus();
		return false;	
	}
	if($('#description').val()==""){
		alert("Please Enter Description ");
		$("#description").focus();
		return false;	
	}
	
	$("#doaction").val('Edit_Cms');
	
	document.cms_form_edit.submit();
	
}

/*  CMS Validation End  */


/*  Place Validation start  */
function delete_Place(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_place');
		document.place_form_manage.submit();
		}		
		}else{
			alert("Please select at least one place.");
		}

}

function add_place_validate(){
	
	if($('#place').val()==""){
		alert("Please Enter Place ");
		$("#place").focus();
		return false;	
	}
	$("#doaction").val('Add_Place');
	
	document.place_form_add.submit();
	
}

function edit_place_validate(){
	
	if($('#place').val()==""){
		alert("Please Enter Place ");
		$("#place").focus();
		return false;	
	}
	
	$("#doaction").val('Edit_Place');
	
	document.place_form_edit.submit();
	
}

/*  Place Validation End  */


/* partner Add  */


function delete_partner(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_partner');
		document.partner_form_manage.submit();
		}		
		}else{
			alert("Please select at least one Partner.");
		}

}

function add_partner_validate(){
	
	if($('#partner_name').val()==""){
		alert("Please Enter Partner name");
		$("#partner_name").focus();
		return false;	
	}
	

	if($('#pimage').val()==""){
		alert("Please select partner Logo ");
		$("#pimage").focus();
		return false;	
	}
	$("#doaction").val('Add_partner');
	
	document.partner_form_add.submit();
	
}

function edit_partner_validate(){
	
	if($('#partner_name').val()==""){
		alert("Please Enter Partner name");
		$("#partner_name").focus();
		return false;	
	}


	
	$("#doaction").val('Edit_partner');
	
	document.partner_form_edit.submit();
	
}

/* partner Add */



/* Testimonial Add  */


function delete_testimonial(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_testimonial');
		document.testimonial_form_manage.submit();
		}		
		}else{
			alert("Please select at least one Testimonial.");
		}

}

function add_testimonial_validate(){
	
	if($('#name').val()==""){
		alert("Please Enter  Name");
		$("#name").focus();
		return false;	
	}
	if($('#designation').val()==""){
		alert("Please Enter  Designation");
		$("#designation").focus();
		return false;	
	}

	

	if($('#pimage').val()==""){
		alert("Please select image ");
		$("#pimage").focus();
		return false;	
	}
	$("#doaction").val('Add_testimonial');
	
	document.testimonial_form_add.submit();
	
}

function edit_testimonial_validate(){
	
	if($('#name').val()==""){
		alert("Please Enter  Name");
		$("#name").focus();
		return false;	
	}
	if($('#designation').val()==""){
		alert("Please Enter  Designation");
		$("#designation").focus();
		return false;	
	}



	
	$("#doaction").val('Edit_testimonial');
	
	document.testimonial_form_edit.submit();
	
}

/* Testimonial Add */











function getSubcategory(selected){
        var dataString = 'cat_id='+selected;
      
        	$.ajax({
        		type: "POST",
				dataType: "text",
        		url: "get_subcategory.php",
        		data: dataString,
        		beforeSend: function() {
        			//$('alert, .alert-success, alert-dismissable, #break').remove();
        			
        		},
        		success: function(data){ 
				$("#subcategory").html(data);

            }
        	});
}
	
function exclusive_products(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? Want to make product Exclusive.')){
		$("#doaction1").val('Exclusive');
		$("#doaction").val('');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one product.");
		}

}

function unexclusive_products(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? Want to make product Unexclusive.')){
		$("#doaction2").val('Unexclusive');
		$("#doaction").val('');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one product.");
		}

}

function delete_products(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana delete record.')){
		$("#doaction").val('Delete');
		$("#doaction1").val('');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one product.");
		}

}


function validate_product(){
		
		if( $("#product_title").val() ==''){
		alert("Please enter product name.");
		$("#product_title").focus();
		return false;
		}
		
		if( $("#city").val() ==''){
		alert("Please select a city.");
		$("#city").focus();
		return false;
		}
		
		if( $("#machine_type").val() ==''){
		alert("Please select a category.");
		$("#machine_type").focus();
		return false;
		}
		
		if( $("#category").val() ==''){
		alert("Please select a subcategory.");
		$("#category").focus();
		return false;
		}
	
		
		if( $("#color").val() ==''){
		alert("Please enter a color.");
		$("#color").focus();
		return false;
		}
		
		if( $("#size").val() ==''){
		alert("Please enter Size.");
		$("#size").focus();
		return false;
		}
		if( $("#material").val() ==''){
		alert("Please enter material used.");
		$("#material").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
		
		if( $("#product_desc").val() ==''){
		alert("Please enter Product description");
		$("#product_desc").focus();
		return false;
		}
		if( $("#product_spec").val() ==''){
		alert("Please enter Product Specification");
		$("#product_spec").focus();
		return false;
		}
		if( $("#product_care").val() ==''){
		alert("Please enter about Product care");
		$("#product_care").focus();
		return false;
		}
		if( $("#package_del").val() ==''){
		alert("Please enter about Package Delivery");
		$("#package_del").focus();
		return false;
		}

		
		/*if( $("#subcategory").val() ==''){
		alert("Please select a subcategory.");
		$("#subcategory").focus();
		return false;
		}*/
		
	
		/*if( $("#short_description").val() ==''){
		alert("Please enter short description");
		$("#short_description").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}*/
				
		$("#doaction").val('AddProduct');
		$("#category_name").val($("#category :selected").text());
	
	document.product_form.submit();
	
	}
	
	
function validate_product_edit(){
	
		
		if( $("#product_title").val() ==''){
		alert("Please enter product name.");
		$("#product_title").focus();
		return false;
		}
		
		if( $("#city").val() ==''){
		alert("Please select a city.");
		$("#city").focus();
		return false;
		}
		
		if( $("#machine_type").val() ==''){
		alert("Please select a category.");
		$("#machine_type").focus();
		return false;
		}
		
		if( $("#category").val() ==''){
		alert("Please select a subcategory.");
		$("#category").focus();
		return false;
		}
	
		
		if( $("#color").val() ==''){
		alert("Please enter a color.");
		$("#color").focus();
		return false;
		}
		
		if( $("#size").val() ==''){
		alert("Please enter Size.");
		$("#size").focus();
		return false;
		}
		if( $("#material").val() ==''){
		alert("Please enter material used.");
		$("#material").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
		
		if( $("#product_desc").val() ==''){
		alert("Please enter Product description");
		$("#product_desc").focus();
		return false;
		}
		if( $("#product_spec").val() ==''){
		alert("Please enter Product Specification");
		$("#product_spec").focus();
		return false;
		}
		if( $("#product_care").val() ==''){
		alert("Please enter about Product care");
		$("#product_care").focus();
		return false;
		}
		if( $("#package_del").val() ==''){
		alert("Please enter about Package Delivery");
		$("#package_del").focus();
		return false;
		}

		/*if( $("#subcategory").val() ==''){
		alert("Please select a subcategory.");
		$("#subcategory").focus();
		return false;
		}
		
	
		if( $("#short_description").val() ==''){
		alert("Please enter short description");
		$("#short_description").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}*/
		
		/*if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}*/
		
	
		/*if( $("#price").val() ==''){
		alert("Please enter product price.");
		$("#price").focus();
		return false;
		}*/
				
		$("#doaction").val('EditProduct');
		$("#category_name").val($("#category :selected").text());
	
	document.product_form.submit();
	
	
	}


function validate_project(){
	
		if( $("#product_title").val() ==''){
		alert("Please enter product name.");
		$("#product_title").focus();
		return false;
		}
		
		if( $("#category").val() ==''){
		alert("Please select a category.");
		$("#category").focus();
		return false;
		}
		if( $("#client_name").val() ==''){
		alert("Please enter client name.");
		$("#client_name").focus();
		return false;
		}
		
		if( $("#job_type").val() ==''){
		alert("Please enter jop type.");
		$("#job_type").focus();
		return false;
		}
	
		if( $("#short_description").val() ==''){
		alert("Please enter short description");
		$("#short_description").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
		

				
		$("#doaction").val('AddProject');
		$("#category_name").val($("#category :selected").text());
	
	document.product_form.submit();
	
	
	}
function validate_project_edit(){
	
		if( $("#product_title").val() ==''){
		alert("Please enter product name.");
		$("#product_title").focus();
		return false;
		}
		
		if( $("#category").val() ==''){
		alert("Please select a category.");
		$("#category").focus();
		return false;
		}
		if( $("#client_name").val() ==''){
		alert("Please enter client name.");
		$("#client_name").focus();
		return false;
		}
		
		if( $("#job_type").val() ==''){
		alert("Please enter jop type.");
		$("#job_type").focus();
		return false;
		}
	
		if( $("#short_description").val() ==''){
		alert("Please enter short description");
		$("#short_description").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
		

				
		$("#doaction").val('EditProject');
		$("#category_name").val($("#category :selected").text());
	
	document.product_form.submit();
	
	
	}
	
function validate_cmsform(){
	
		if( $("#title").val() ==''){
		alert("Please page name.");
		$("#title").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
     $("#doaction").val('AddCms');
	document.user_form.submit();
	}		
	
	
function validate_edicmsform(){
	
		if( $("#title").val() ==''){
		alert("Please page name.");
		$("#title").focus();
		return false;
		}
		
		var description = tinyMCE.get('description').getContent();
		if( description == ''){
		alert("Please enter description");
		$("#description").focus();
		return false;
		}
		
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
     $("#doaction").val('EditCms');
	document.user_form.submit();
	}
	
	
	
function validate_categoryform(){
	
	if( $("#category").val() ==''){
		alert("Please enter category.");
		$("#category").focus();
		return false;
		}
	
		if( $("#title").val() ==''){
		alert("Please enter title.");
		$("#title").focus();
		return false;
		}
		
		if( $("#cat_code").val() ==''){
		alert("Please enter cat code.");
		$("#cat_code").focus();
		return false;
		}
	
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
     $("#doaction").val('AddCategory');
	document.user_form.submit();
	}	
	
function validate_editcategoryform(){
	
	if( $("#category").val() ==''){
		alert("Please enter Category");
		$("#category").focus();
		return false;
		}
	
		if( $("#title").val() ==''){
		alert("Please enter title.");
		$("#title").focus();
		return false;
		}
		if( $("#cat_code").val() ==''){
		alert("Please enter Cat Code.");
		$("#cat_code").focus();
		return false;
		}
	
		if( $("#meta_title").val() ==''){
		alert("Please enter meta title");
		$("#meta_title").focus();
		return false;
		}
		
		if( $("#meta_keywords").val() ==''){
		alert("Please enter meta keywords.");
		$("#meta_keywords").focus();
		return false;
		}
		
		if( $("#meta_desc").val() ==''){
		alert("Please enter meta description");
		$("#meta_desc").focus();
		return false;
		}
     $("#doaction").val('EditCategory');
	document.user_form.submit();
}	

function validate_enquiryform(){
		if( $("#fname").val() ==''){
		alert("Please enter name.");
		$("#fname").focus();
		return false;
		}
		
		
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val())) {

    	}else{
		alert("Please enter a valid email.");
		$("#email").focus();
		return false;
		}
		
		if( $("#phone").val() ==''){
		alert("Please enter phone.");
		$("#phone").focus();
		return false;
		}
		
		if( $("#company").val() ==''){
		alert("Please enter company name");
		$("#company").focus();
		return false;
		}
		
		
		if( $("#address").val() ==''){
		alert("Please enter address");
		$("#address").focus();
		return false;
		}
		
		if( $("#subject").val() ==''){
		alert("Please enter subject");
		$("#subject").focus();
		return false;
		}
		
		if( $("#message").val() ==''){
		alert("Please enter message");
		$("#message").focus();
		return false;
		}
		
		
		$("#doaction").val('AddEnquiry');
		$("#country_name").val($("#country :selected").text());
	
	document.user_form.submit();
	
	}
	
function validate_userform(){
	
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val())) {

    	}else{
		alert("Please enter a valid email.");
		$("#email").focus();
		return false;
		}
		
		
		if( $("#password").val() ==''){
		alert("Please enter password.");
		$("#password").focus();
		return false;
		}
		
		if( $("#cpassword").val() ==''){
		alert("Please enter confirm password.");
		$("#cpassword").focus();
		return false;
		}
		
		if( $("#password").val() != $("#cpassword").val()){
		alert("Confirm password is not matching");
		$("#cpassword").focus();
		return false;
		}
		
		
		if( $("#username").val() ==''){
		alert("Please enter Username.");
		$("#username").focus();
		return false;
		}
		
		
		
		if( $("#fname").val() ==''){
		alert("Please enter first name.");
		$("#fname").focus();
		return false;
		}
		
		if( $("#lname").val() ==''){
		alert("Please enter last name.");
		$("#lname").focus();
		return false;
		}
		
			if( $("#designation").val() ==''){
		alert("Please enter designation");
		$("#designation").focus();
		return false;
		}
		
		if( $("#phone").val() ==''){
		alert("Please enter phone.");
		$("#phone").focus();
		return false;
		}
		
		if( $("#fax").val() ==''){
		alert("Please enter fax number");
		$("#fax").focus();
		return false;
		}
		
		
	
		
		
		if( $("#address").val() ==''){
		alert("Please enter address");
		$("#address").focus();
		return false;
		}
		
		if( $("#zipcode").val() ==''){
		alert("Please enter zip code");
		$("#zipcode").focus();
		return false;
		}
		
		if( $("#country").val() ==''){
		alert("Please select country");
		$("#country").focus();
		return false;
		}
		if( $("#state").val() ==''){
		alert("Please enter state");
		$("#state").focus();
		return false;
		}
		
		if( $("#city").val() ==''){
		alert("Please enter city");
		$("#city").focus();
		return false;
		}
		
	
		
		
		
		$("#doaction").val('AddUser');
		$("#country_name").val($("#country :selected").text());
	
	document.user_form.submit();
	
	}
	
			
function validate_edituserform(){
		
		if( $("#username").val() ==''){
		alert("Please enter Username.");
		$("#username").focus();
		return false;
		}
		
		if( $("#password").val() ==''){
		alert("Please enter password.");
		$("#password").focus();
		return false;
		}
		
		if( $("#cpassword").val() ==''){
		alert("Please enter confirm password.");
		$("#cpassword").focus();
		return false;
		}
		
		if( $("#password").val() != $("#cpassword").val()){
		alert("Confirm password is not matching");
		$("#cpassword").focus();
		return false;
		}
		
		
		
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val())) {

    	}else{
		alert("Please enter a valid email.");
		$("#email").focus();
		return false;
		}
		
		if( $("#phone").val() ==''){
		alert("Please enter phone.");
		$("#phone").focus();
		return false;
		}
		
		
		
		if( $("#address").val() ==''){
		alert("Please enter address");
		$("#address").focus();
		return false;
		}
		
		
		if( $("#country").val() ==''){
		alert("Please select country");
		$("#country").focus();
		return false;
		}
		

		$("#doaction").val('EditUser');
		$("#country_name").val($("#country :selected").text());
	
	document.user_form.submit();
	
	}
	
		function AllowNumbersOnly(e)
			{
			var unicode=e.charCode?e.charCode:e.keyCode;
			if(unicode!=8&&unicode!=9){
				if(unicode<48||unicode>57){
					return false;
					}
					}
			return true;
			}
	
function delete_cms(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana delete record.')){
		$("#doaction").val('Delete');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one page.");
		}

}


function delete_products(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana delete record.')){
		$("#doaction").val('Delete');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one record.");
		}

}


function delete_category(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana delete record.')){
		$("#doaction").val('Delete');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one record.");
		}

}

	function delete_orderss(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana delete record.')){
		$("#doaction").val('Delete');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one order.");
		}

}


function delete_users(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? Want to delete User.')){
		$("#doaction").val('Delete');
		document.product_form.submit();
		}		
		}else{
			alert("Please select at least one user.");
		}

}


function update_orderss(){
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure? wana to update order(s).')){
		$("#doaction").val('Update');
		var orderaction = $("#order_status").val();
		$("#email_message_touser").val($("#email_message").val());
		$("#order_action").val(orderaction);
		document.order_form_manage.submit();
		}		
		}else{
			alert("Please select at least one record.");
		}

}


//autofil search for vendors
function removethis(removethis){
	$( "#vendror_"+removethis ).remove();
	}
	
function selectVendor(vendor){
var dataString = 'searchCallingPlan=true&iso='+vendor.id;
$("#suggestme").html("").show("fast");
var selectedtext = '<span id="vendror_'+vendor.id+'" class="vendor-span">'+vendor.text+'<a onclick="removethis('+vendor.id+')" href="javascript:void(0);">&nbsp;&nbsp;X</a><input type="hidden" name="vendor_lists[]" value="'+vendor.id+'"></span>';
$("#selectedVendors").html($("#selectedVendors").html() + selectedtext);
$( "#vendor").val('');
}

function selectVendor1(vendor){
var dataString = 'searchCallingPlan=true&iso='+vendor.id;
$("#suggestme").html("").show("fast");
$( "#vendor_name").val(vendor.text);
$( "#vendor_ID").val(vendor.id);
}


function searchVendor(txtval){
txtval = txtval.toLowerCase();
var results='';


if(txtval!=''){
	var dataString = 'searchVendor=true&searchtxt='+txtval;
	$.ajax({
		type: "POST",
		dataType: "text",
		url: "search_vendor.php",
		async : false,
		data: dataString,
		beforeSend: function() {
			$("#searchresult").html("<div style='width:480px; margin-bottom:20px; margin-top:20px; text-align:center;'><img src='Images/loader.gif' ></div>").show("fast");
			},
		success: function(result){
		$("#suggestme").html(result);
		$("#searchresult").html('');

		}
	});
	}

}


function searchVendor1(txtval){
txtval = txtval.toLowerCase();
var results='';


if(txtval!=''){
	var dataString = 'searchVendor=true&searchtxt='+txtval;
	$.ajax({
		type: "POST",
		dataType: "text",
		url: "search_vendor1.php",
		async : false,
		data: dataString,
		beforeSend: function() {
			$("#searchresult").html("<div style='width:480px; margin-bottom:20px; margin-top:20px; text-align:center;'><img src='Images/loader.gif' ></div>").show("fast");
			},
		success: function(result){
		$("#suggestme").html(result);
		$("#searchresult").html('');

		}
	});
	}

}

function check_available_products(quantity,productId){
var results='';


if(productId!=''){
	var dataString = 'GetAvailableProduct=true&productId='+productId;
	$.ajax({
		type: "POST",
		dataType: "text",
		url: "add_order.php",
		async : false,
		data: dataString,
		beforeSend: function() {
			$( "<div style='margin-bottom:20px; margin-top:20px; text-align:center;' id='loader_image'><img src='Images/loader.gif' height='80' width='80' ></div>"  ).insertAfter("#available_products");
			//$("#available_products").html("<div style='width:480px; margin-bottom:20px; margin-top:20px; text-align:center;' id='loader_image'><img src='Images/loader.gif' ></div>").show("fast");
			},
		success: function(result){
			$("#loader_image").remove();
			$("#available_products_hidden").val(result);
		if(parseInt(quantity) > parseInt(result)){
			alert('Quantity should be less than or equal to available products');
			$("#quantity").focus();
			return false;
			}

		}
	});
	}

}



function validate_order(){
	
	if( $("#product_id").val() ==''){
		alert("Please select a product.");
		$("#product_id").focus();
		return false;
	}

	if( $("#quantity").val() ==''){
		alert("Please enter product quantity.");
		$("#quantity").focus();
		return false;
	}
	
	if( parseInt($("#quantity").val()) >   parseInt($("#available_products_hidden").val())){
		alert('Quantity should be less than or equal to available products');
		$("#quantity").focus();
		return false;
	}
	
	if( $("#price").val() ==''){
		alert("Please enter product price.");
		$("#price").focus();
		return false;
	}
	
	if( $("#vendor_id").val() ==''){
		alert("Please select a vendor.");
		$("#vendor_id").focus();
		return false;
	}

	if( $("#user_id").val() ==''){
		alert("Please select a user.");
		$("#user_id").focus();
		return false;
	}
	
	$("#doaction").val('AddOrder');
	
	document.order_form.submit();
}


function validate_admin_form(){
	
	
	
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val())) {

    	}else{
		alert("Please enter a valid email.");
		$("#email").focus();
		return false;
		}
		
		
	if( ($("#password").val() =='') || ($("#password").val().length <6 ) ){
		alert("Password length must be at least of 6 characters.");
		$("#password").focus();
		return false;
	}
	
	}
	
function DeleteProductGallery(product_id,imageName,id){
	
	confirmdelete =  confirm('Are you sure? wana delete this image.');
	
if(product_id!='' && confirmdelete==true){
	var dataString = 'op=delete&product_id='+product_id+'&name='+imageName;
	$.ajax({
		type: "POST",
		dataType: "text",
		url: "Upload/delete.php",
		async : false,
		data: dataString,
		beforeSend: function() {
			
			$("#gallery_"+id).append("<div style='margin-bottom:20px; margin-top:20px; text-align:center;' id='loader_image'><img src='Images/loader.gif' height='80' width='80' ></div>");
			},
		success: function(result){
			$("#gallery_"+id).remove();
			
			}
	});
	}
	
	}	
	
function DeleteProjectGallery(product_id,imageName,id){
	
	confirmdelete =  confirm('Are you sure? wana delete this image.');
	
if(product_id!='' && confirmdelete==true){
	var dataString = 'op=delete&product_id='+product_id+'&name='+imageName;
	$.ajax({
		type: "POST",
		dataType: "text",
		url: "Upload/delete2.php",
		async : false,
		data: dataString,
		beforeSend: function() {
			
			$("#gallery_"+id).append("<div style='margin-bottom:20px; margin-top:20px; text-align:center;' id='loader_image'><img src='Images/loader.gif' height='80' width='80' ></div>");
			},
		success: function(result){
			$("#gallery_"+id).remove();
			
			}
	});
	}
	
	}	