function AllowNumbersOnly(e)
{
var unicode=e.charCode?e.charCode:e.keyCode;
if(unicode!=8&&unicode!=9){
	if(unicode<48||unicode>57){
		return false;
		}
		}
return true;
}


/*  Brand Validation Start  */
function delete_Circle(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Circle');
		document.circle_form.submit();
		}		
		}else{
			alert("Please select at least one Customer.");
		}
}

function add_circle_validate(){
	
	if($('#circle').val()==""){
		alert("Please Enter Circle Name");
		$("#circle").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Circle');
	document.circle_form_add.submit();
	
}

function edit_circle_validate(){
	
	if($('#circle').val()==""){
		alert("Please Enter Circle Title");
		$("#circle").focus();
		return false;	
	}
	$("#doaction").val('Edit_Circle');
	document.circle_form_edit.submit();
	
}

/*  Brand Validation Start  */
function delete_Project(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Project');
		document.project_form.submit();
		}		
		}else{
			alert("Please select at least one Project.");
		}

}

function add_project_validate(){
	
	if($('#project').val()==""){
		alert("Please Enter Project");
		$("#project").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Project');
	document.project_form_add.submit();
	
}

function edit_project_validate(){
	
	if($('#project').val()==""){
		alert("Please Enter Project");
		$("#project").focus();
		return false;	
	}
	$("#doaction").val('Edit_Project');
	document.project_form_edit.submit();
	
}

/*  Brand Validation Start  */
function delete_Customer(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Customer');
		document.customer_form.submit();
		}		
		}else{
			alert("Please select at least one Customer.");
		}

}

function add_customer_validate(){
	
	if($('#customer').val()==""){
		alert("Please Enter Customer");
		$("#customer").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Customer');
	document.customer_form_add.submit();
	
}

function edit_customer_validate(){
	
	if($('#customer').val()==""){
		alert("Please Enter Customer");
		$("#customer").focus();
		return false;	
	}
	$("#doaction").val('Edit_Customer');
	document.customer_form_edit.submit();
	
}

/*  Brand Validation Start  */
function delete_Endcustomer(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Endcustomer');
		document.endcustomer_form.submit();
		}		
		}else{
			alert("Please select at least one EndCustomer.");
		}

}

function add_endcustomer_validate(){
	
	if($('#endcustomer').val()==""){
		alert("Please Enter Endcustomer");
		$("#endcustomer").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Endcustomer');
	document.endcustomer_form_add.submit();
	
}

function edit_endcustomer_validate(){
	
	if($('#endcustomer').val()==""){
		alert("Please Enter Endcustomer");
		$("#endcustomer").focus();
		return false;	
	}
	$("#doaction").val('Edit_Endcustomer');
	document.endcustomer_form_edit.submit();
	
}


/*  Brand Validation Start  */
function delete_Projectcode(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Projectcode');
		document.projectcode_form.submit();
		}		
		}else{
			alert("Please select at least one Project Code.");
		}

}

function add_projectcode_validate(){
	
	if($('#projectcode').val()==""){
		alert("Please Enter ProjectCode");
		$("#projectcode").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Projectcode');
	document.projectcode_form_add.submit();
	
}

function edit_projectcode_validate(){
	
	if($('#projectcode').val()==""){
		alert("Please Enter Projectcode");
		$("#projectcode").focus();
		return false;	
	}
	$("#doaction").val('Edit_Projectcode');
	document.projectcode_form_edit.submit();
	
}

/*  Brand Validation Start  */
function delete_Activity(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Activity');
		document.activity_form.submit();
		}		
		}else{
			alert("Please select at least one Activity.");
		}

}

function add_activity_validate(){
	
	if($('#activity').val()==""){
		alert("Please Enter Activity");
		$("#activity").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Activity');
	document.activity_form_add.submit();
	
}

function edit_activity_validate(){
	
	if($('#activity').val()==""){
		alert("Please Enter Activity");
		$("#activity").focus();
		return false;	
	}
	$("#doaction").val('Edit_Activity');
	document.activity_form_edit.submit();
	
}

/*  Purchase Order Validation Start  */
function delete_Po(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Po');
		document.po_form.submit();
		}		
		}else{
			alert("Please select at least one Purchase Order.");
		}

}

function add_po_validate(){
	
	if($('#po').val()==""){
		alert("Please Enter Purchase Order");
		$("#po").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Po');
	document.po_form_add.submit();
	
}

function edit_po_validate(){
	
	if($('#po').val()==""){
		alert("Please Enter Purchase Order");
		$("#po").focus();
		return false;	
	}
	$("#doaction").val('Edit_Po');
	document.po_form_edit.submit();
	
}



/*  Brand Validation Start  */
function delete_Activity(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Activity');
		document.activity_form.submit();
		}		
		}else{
			alert("Please select at least one Activity.");
		}

}

function add_activity_validate(){
	
	if($('#activity').val()==""){
		alert("Please Enter Activity");
		$("#activity").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Activity');
	document.activity_form_add.submit();
	
}

function edit_activity_validate(){
	
	if($('#activity').val()==""){
		alert("Please Enter Activity");
		$("#activity").focus();
		return false;	
	}
	$("#doaction").val('Edit_Activity');
	document.activity_form_edit.submit();
	
}

/*  Purchase Order Validation Start  */
function delete_Po(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to delete record?')){
		$("#doaction").val('Delete_Po');
		document.po_form.submit();
		}		
		}else{
			alert("Please select at least one Purchase Order.");
		}

}

function print_Po(){
	
	var total_checked =  $(".checkboxes:checked").length;
	if(total_checked>0){
		if(confirm('Are you sure, You want to print record?')){
		$("#doaction").val('Print_Po');
		document.po_form.submit();
		}		
		}else{
			alert("Please select at least one Purchase Order.");
		}

}

function add_po_validate(){
	
	if($('#circle').val()==""){
		alert("Please Select Circle");
		$("#circle").focus();
		return false;	
	}
	if($('#project').val()==""){
		alert("Please Select Project");
		$("#project").focus();
		return false;	
	}
	if($('#customer').val()==""){
		alert("Please Select customer");
		$("#customer").focus();
		return false;	
	}
	if($('#endcustomer').val()==""){
		alert("Please Select End Customer");
		$("#endcustomer").focus();
		return false;	
	}
	if($('#projectcode').val()==""){
		alert("Please Select Project Code");
		$("#projectcode").focus();
		return false;	
	}
	if($('#activity').val()==""){
		alert("Please Select Activity");
		$("#activity").focus();
		return false;	
	}
	if($('#invoicedate').val()==""){
		alert("Please Select Invoice Date");
		$("#invoicedate").focus();
		return false;	
	}
	if($('#pono').val()==""){
		alert("Please Enter Purchase Order");
		$("#pono").focus();
		return false;	
	}
	if($('#podate').val()==""){
		alert("Please Select Purchase order date");
		$("#podate").focus();
		return false;	
	}
	if($('#siteid').val()==""){
		alert("Please Enter Site Id");
		$("#siteid").focus();
		return false;	
	}
	if($('#wpid').val()==""){
		alert("Please Enter Wp ID");
		$("#wpid").focus();
		return false;	
	}
	if($('#iadate').val()==""){
		alert("Please Enter Inter Acceptance date");
		$("#iadate").focus();
		return false;	
	}
	
	if($('#itemid').val()==""){
		alert("Please Enter Item ID");
		$("#itemid").focus();
		return false;	
	}
	if($('#workitem').val()==""){
		alert("Please Enter Work Item");
		$("#workitem").focus();
		return false;	
	}
	
	$("#doaction").val('Add_Po');
	document.po_form_add.submit();
	
}

function edit_po_validate(){
	
		if($('#circle').val()==""){
		alert("Please Select Circle");
		$("#circle").focus();
		return false;	
	}
	if($('#project').val()==""){
		alert("Please Select Project");
		$("#project").focus();
		return false;	
	}
	if($('#customer').val()==""){
		alert("Please Select customer");
		$("#customer").focus();
		return false;	
	}
	if($('#endcustomer').val()==""){
		alert("Please Select End Customer");
		$("#endcustomer").focus();
		return false;	
	}
	if($('#projectcode').val()==""){
		alert("Please Select Project Code");
		$("#projectcode").focus();
		return false;	
	}
	if($('#activity').val()==""){
		alert("Please Select Activity");
		$("#activity").focus();
		return false;	
	}
	if($('#invoicedate').val()==""){
		alert("Please Select Invoice Date");
		$("#invoicedate").focus();
		return false;	
	}
	if($('#pono').val()==""){
		alert("Please Enter Purchase Order");
		$("#pono").focus();
		return false;	
	}
	if($('#podate').val()==""){
		alert("Please Select Purchase order date");
		$("#podate").focus();
		return false;	
	}
	if($('#siteid').val()==""){
		alert("Please Enter Site Id");
		$("#siteid").focus();
		return false;	
	}
	if($('#wpid').val()==""){
		alert("Please Enter Wp ID");
		$("#wpid").focus();
		return false;	
	}
	if($('#iadate').val()==""){
		alert("Please Enter Inter Acceptance date");
		$("#iadate").focus();
		return false;	
	}
	
	if($('#itemid').val()==""){
		alert("Please Enter Item ID");
		$("#itemid").focus();
		return false;	
	}
	if($('#workitem').val()==""){
		alert("Please Enter Work Item");
		$("#workitem").focus();
		return false;	
	}
	$("#doaction").val('Edit_Po');
	document.po_form_edit.submit();
	
}


// read pdf
