<?php /*?><?php
	session_start();
	//print_r($_SESSION);die;
	//error_reporting(-1); 

	if(($_SESSION['Admin']=="")&& ($_SESSION['Admin_login']!="yes")){
	?>
<script type="text/javascript">document.location.href="login.php";</script>
<?php
	}
	?><?php */?>
<?php 
	include_once('top_menu.php');
	include_once('sidebar.php');
	
		$query=$project->project_listing();
		$query->execute();
		$num_rows=$query->rowcount();
		
		
		
		if(isset($_POST['doaction'])=='Delete_Project'){ 
		//$brand=implode(",",$_POST['brand_to_delete']);
		$size = sizeof($_POST['project_to_delete']);
		
		if($size < 1){
		$message='<div class="message-error alert alert-error col-xs-6 alert1">Error! Plese select at least one record to delete.</div>';
		}else{
		$count = 0;
		//print_r($_POST['brand_to_delete']);die;
		foreach($_POST['project_to_delete'] as $del){
		$project->id = $del;
		if($project->delete_project()){
			##### Remove Brand Images ###
			//$path= 'images/brand_images/'.$brand->id;
			//array_map('unlink', glob("$path/*"));
			//rmdir($path);
			##### Remove Brand Images ###
		
		$count++;
		}		
		}
		$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$count.' Record(s) Deleted Successfully.</div>';
		}
		}	
			
			
	?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Dashboard <small>Manage Brand</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="#">Manage</a></li>
      <li class="active">Brands</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        <!--<h3 class="box-title pull-left">Manage Brands</h3>-->
        <?php 
  
  	if($_GET['message-success']){
	$message='<div class="message-success alert alert-success col-xs-6 alert1">'.$_GET['message-success'].'</div>';
	}
	if($_GET['message-error']){
	$message='<div class="message-error alert alert-error col-xs-6 alert1" >'.$_GET['message-error'].'</div>';
	}
	if($message!=''){ 
	 	echo $message;
 	}
 ?>
        
          <button type="submit" style=" margin-left:5px;background-color: #ffffff;" class="btn btn-default pull-right" value="Delete_brand" onclick="delete_Project();"><i class="fa fa-fw fa-close"></i> Delete</button>
          <button onclick="document.location.href='Addproject.php'" style="background-color: #ffffff;" class="btn btn-default pull-right"><i class="fa fa-fw fa-plus-square"></i> Add project</button>
          </div>
          <!-- /.box-header -->
          <form action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" name="project_form" id="project_form" method="post" >
          <input type="hidden"  name="doaction" id="doaction" />
          <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Sr. No</th>
              <th>project </th>
              <th>Action</th>
              <th><input name="selectall" id="selectall" type="checkbox"  /></th>
            </tr>
          </thead>
          <tbody>
            <?php 
				if($num_rows>0){$sn=1;
					while($row=$query->fetch()){ 			
			?>
            <tr>
              <td><?php echo $sn; ?></td>
              <td><?=$row['project']?></td>
              <td>
              	<a href="Editproject.php?id=<?=$row['id']?>" title="Edit Project"> <i class="fa fa-edit"></i> </a>
                <a href="Viewproject.php?id=<?=$row['id']?>" title="Edit Project"> <i class="fa fa-eye"></i> </a>
              </td>
              <td><input name="project_to_delete[]"  type="checkbox" value="<?=$row['id']?>"  class="checkboxes"/></td>
            </tr>
            <?php 
				$sn++;
				} 
				} 
				else{ ?>
            <tr>
              <td colspan="6"><strong>No record found</strong></td>
            </tr>
            <?php
				}  
			 ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Brand</th>
              <th>Action</th>
              <th><input name="selectall" id="selectall" type="checkbox" disabled="disabled"/></th>
            </tr>
          </tfoot>
        </form>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<?php include_once('footer.php');?>
<!-- jQuery 2.1.4 -->

<!-- DataTables -->
<script src="<?=DOCUMENT_ROOT?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=DOCUMENT_ROOT?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=DOCUMENT_ROOT?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=DOCUMENT_ROOT?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->

<!-- page script -->
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
<!---my script----->
<script language="javascript">
	$(function(){
		$("#selectall").click(function () {
			  $('.checkboxes').attr('checked', this.checked);
		});
	 
		$(".checkboxes").click(function(){
	 
			if($(".checkboxes").length == $(".checkboxes:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}
	 
		});
	});
	</script>
<!---my script---->
