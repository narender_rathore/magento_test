Using Composer

Add PDFParser to your composer.json file :

{
    "require": {
        "smalot/pdfparser": "*"
    }
}

Now ask for composer to download the bundle by running the command:

php composer.phar update smalot/pdfparser

As standalone library

First of all, download the library from Github by choosing a specific release or directly the master.

Once done, unzip it and run the following command line using composer.

php composer.phar update