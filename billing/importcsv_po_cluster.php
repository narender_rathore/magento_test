<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Upload page</title>
<style type="text/css">
body {
	background: #E3F4FC;
	font: normal 14px/30px Helvetica, Arial, sans-serif;
	color: #2b2b2b;
}
a {
	color:#898989;
	font-size:14px;
	font-weight:bold;
	text-decoration:none;
}
a:hover {
	color:#CC0033;
}

h1 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #CC0033;
}
h2 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #898989;
}
#container {
	background: #CCC;
	margin: 100px auto;
	width: 945px;
}
#form 			{padding: 20px 150px;}
#form input     {margin-bottom: 20px;}
</style>
</head>
<body>
<div id="container">
<div id="form">

<?php

include_once("classes/settings.php");//Connect to Database

//$deleterecords = "TRUNCATE TABLE invoice"; //empty the table of its current records
//mysql_query($deleterecords);
$convertpo= new Convertpo();
//Upload File
if (isset($_POST['submit'])) {
	if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
		echo "<h1>" . "File ". $_FILES['filename']['name'] ." uploaded successfully." . "</h1>";
		echo "<h2>Displaying contents:</h2>";
		readfile($_FILES['filename']['tmp_name']);
	}

	//Import uploaded file to Database
	$handle = fopen($_FILES['filename']['tmp_name'], "r");

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		//import="INSERT into purchase_order_invoice(po_no,po_date,site_id,amount,invoice_no,invoice_date,type) 
		//values('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]')";

		//mysql_query($import) or die(mysql_error());
		//print_r($data);die;
		$convertpo->pono					=  $data[0];
		$convertpo->podate					=  $data[1];
		$convertpo->siteid					=  $data[2];
		$convertpo->company					=  $data[3];
		$convertpo->lineitem10				=  $data[4];
		$convertpo->itemid10				=  '0000'.$data[5];
		$convertpo->unitprice10				=  $data[6];
		$convertpo->fielddrive10			=  $data[7];
		$convertpo->lineitem20				=  $data[8];
		$convertpo->itemid20				=  '000'.$data[9];
		$convertpo->unitprice20				=  $data[10];
		$convertpo->radionetwork20			=  $data[11];
		$convertpo->lineitem30				=  $data[12];
		$convertpo->itemid30				=  '0000'.$data[13];
		$convertpo->unitprice30				=  $data[14];
		$convertpo->fielddrive30			=  $data[15];
		$convertpo->deliverydate			=  $data[16];
		$convertpo->amount					=  $data[17];
		$convertpo->import_convertpo_cluster();
		
	}

	fclose($handle);

	print "Import done";

	//view upload form
}else {

	print "Upload new csv by browsing to file and clicking on Upload<br />\n";

	print "<form enctype='multipart/form-data' action='importcsv_po_cluster.php' method='post'>";

	print "File name to import:<br />\n";

	print "<input size='50' type='file' name='filename'><br />\n";

	print "<input type='submit' name='submit' value='Upload'></form>";

}

?>

</div>
</div>
</body>
</html>

