<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Upload page</title>
<style type="text/css">
body {
	background: #E3F4FC;
	font: normal 14px/30px Helvetica, Arial, sans-serif;
	color: #2b2b2b;
}
a {
	color:#898989;
	font-size:14px;
	font-weight:bold;
	text-decoration:none;
}
a:hover {
	color:#CC0033;
}

h1 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #CC0033;
}
h2 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #898989;
}
#container {
	background: #CCC;
	margin: 100px auto;
	width: 945px;
}
#form 			{padding: 20px 150px;}
#form input     {margin-bottom: 20px;}
</style>
</head>
<body>
<div id="container">
<div id="form">

<?php

include_once("classes/settings.php");//Connect to Database

//$deleterecords = "TRUNCATE TABLE invoice"; //empty the table of its current records
//mysql_query($deleterecords);
$iadone= new Iadone();
//Upload File
if (isset($_POST['submit'])) {
	if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
		echo "<h1>" . "File ". $_FILES['filename']['name'] ." uploaded successfully." . "</h1>";
		echo "<h2>Displaying contents:</h2>";
		readfile($_FILES['filename']['tmp_name']);
	}

	//Import uploaded file to Database
	$handle = fopen($_FILES['filename']['tmp_name'], "r");

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		//import="INSERT into purchase_order_invoice(po_no,po_date,site_id,amount,invoice_no,invoice_date,type) 
		//values('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]')";

		//mysql_query($import) or die(mysql_error());
		//print_r($data);die;
		$iadone->project				=  $data[0];
		$iadone->circle					=  $data[1];
		$iadone->site_customer			=  $data[2];
		$iadone->site_id				=  $data[3];
		$iadone->wpid					=  $data[4];
		$iadone->packagename			=  $data[5];
		$iadone->itemid					=  $data[6];
		$iadone->workitemsubtype		=  $data[7];
		$iadone->vendor					=  $data[8];
		$iadone->ponumber				=  $data[9];
		$iadone->company				=  $data[10];
		$iadone->completedate			=  $data[11];
		$iadone->internal_acceptance	=  $data[12];
		$iadone->goods_recipt			=  $data[13];
		$iadone->import_iadone();
		
	}

	fclose($handle);

	print "Import done";

	//view upload form
}else {

	print "Upload new csv by browsing to file and clicking on Upload<br />\n";

	print "<form enctype='multipart/form-data' action='importcsv_iadone.php' method='post'>";

	print "File name to import:<br />\n";

	print "<input size='50' type='file' name='filename'><br />\n";

	print "<input type='submit' name='submit' value='Upload'></form>";

}

?>

</div>
</div>
</body>
</html>

